<?php
class Proje extends AppModel{
    var $useTable = 'em_proje';
    public $actsAs = array('Containable');

    public $hasMany = array(
        'ProjeResim' => array(
            'className' => 'ProjeResim',
            'foreignKey' => 'proje_id'
        )
    );
}