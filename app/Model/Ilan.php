<?php
class Ilan extends AppModel{
    var $useTable = "em_ilan";
    public $actsAs = array('Containable');

    public $hasMany = array(
        'IlanResim' => array(
            'className' => 'IlanResim',
            'foreignKey' => 'ilan_id'
        ),
        'IlanDanisman' => array(
            'className' => 'IlanDanisman',
            'foreignKey' => 'ilan_id'
        ),
        'IlanOzellik' => array(
            'className' => 'IlanOzellik',
            'foreignKey' => 'ilan_id'
        )
    );

    public $belongsTo = array(
        'KonutBilgi' => array(
            'className' => 'KonutBilgi',
            'foreignKey' => false,
            'conditions' => array('Ilan.id = KonutBilgi.ilan_id')
        ),
        'IsyeriBilgi' => array(
            'className' => 'IsyeriBilgi',
            'foreignKey' => false,
            'conditions' => array('Ilan.id = IsyeriBilgi.ilan_id')
        ),
        'ArsaBilgi' => array(
            'className' => 'ArsaBilgi',
            'foreignKey' => false,
            'conditions' => array('Ilan.id = ArsaBilgi.ilan_id')
        ),
        'Sehir' => array(
            'className' => 'Sehir',
            'foreignKey' => 'sehir_id',
            'bindingKey' => 'id',
            'limit'=>1
        ),
        'Ilce' => array(
            'className' => 'Ilce',
            'foreignKey' => 'ilce_id',
            'bindingKey' => 'id',
            'limit'=>1
        ),
        'Semt' => array(
            'className' => 'Semt',
            'foreignKey' => 'semt_id',
            'bindingKey' => 'id',
            'limit'=>1
        ),
        'Mahalle' => array(
            'className' => 'Mahalle',
            'foreignKey' => 'mahalle_id',
            'bindingKey' => 'id',
            'limit'=>1
        )
    );

    public function beforeSave($options = array()){
        parent::beforeSave();
        if(!$this->id){
            $this->data['Ilan']['id'] = CakeText::uuid();
        }
        return true;
    }
}