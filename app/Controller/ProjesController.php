<?php
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('ImageManipulator','Vendor');
class ProjesController extends AppController{
    var $uses = array('Proje','ProjeResim','ProjeLocation');
    public $components = array('Paginator','HtmlMeta');
    
    public function home(){
        $this->paginate = array(
            'limit'=>10,
            'order'=>array('id'=>'DESC')
        );
        
        $projes = $this->paginate('Proje');
        $this->set('projes',$projes);
        
        if(!$projes){
            $this->Session->setFlash('Aradığınız kriterlere uygun sonuç yoktur.','default', array('class'=>'alert alert-danger'));
            return $this->redirect(
            array('controller' => 'users', 'action' => 'home')
            );
        }
        
        $projeRes = array();
        foreach ($projes as $row) {
            $projeRes[$row['Proje']['id']] = $this->ProjeResim->findAllByProjeId($row['Proje']['id']);
        }
        $this->set('projeRes',$projeRes);

        $this->HtmlMeta->addElement(array('name' => 'robots', 'content' => 'index, follow'));
        $this->HtmlMeta->addElement(array('name' => 'title', 'content' => 'Proje - Maden Gayrimenkul A.Ş.'));
        $this->HtmlMeta->addElement(array('name' => 'description', 'content' => 'Satılık, kiralık, konut, işyeri, arsa yatırım ve yönetimi - Mutlu Kazanç Kapısı...'));
        $this->HtmlMeta->addElement(array('property' => 'og:title', 'content' => 'Proje - Maden Gayrimenkul A.Ş.' ));
        $this->HtmlMeta->addElement(array('property' => 'og:description', 'content' => 'Satılık, kiralık, konut, işyeri, arsa yatırım ve yönetimi - Mutlu Kazanç Kapısı...'));
        $this->HtmlMeta->addElement(array('property' => 'og:keywords', 'content' => 'Eskişehir Yolu satılık yatırım arsaları, Temelli, Malıköy, Ihlamurkent satılık villa, Alcı, Türkobası, Anayurt, Bacı, Başkent OSB satılık arsa, kiralık fabrika, Anadolu OSB satılık fabrika arsa, ASO 2 OSB, Konutkent, Çayyolu, Westgate Residence, Kuleevo, Elmar Towers, Elya Tower, Azel Kule' ));
        $this->HtmlMeta->addElement(array('property' => 'og:url', 'content' => Router::url('/', true).'projes/home'));
        $this->HtmlMeta->addElement(array('property'=>'og:image', 'content'=>  Router::url('/',true).'img/shareicon/mpro.png'));
    }
    
    public function proje(){
        if($this->request->params['named']['ilanNo']){
           $ilanNo = $this->request->params['named']['ilanNo']; 
           
           $projes = $this->Proje->find('first',
                   array(
                       'fields'=>array('*'),
                       'contain'=>array('ProjeResim'),
                       'conditions'=>array("Proje.id"=>$ilanNo)
                       ));
        
        if(!$projes){
            $this->Session->setFlash('Aramış olduğunuz ilan kaldırılmıştır.','default', array('class'=>'alert alert-danger'));
            return $this->redirect(Router::url('/',true));
        }
        $this->set('projes',$projes);

        $this->HtmlMeta->addElement(array('name' => 'robots', 'content' => 'index, follow'));
        $this->HtmlMeta->addElement(array('name' => 'description', 'content' => $projes['Proje']['baslik']));
        $this->HtmlMeta->addElement(array('name' => 'description', 'content' => strip_tags($projes['Proje']['aciklama'])));
        $this->HtmlMeta->addElement(array('property' => 'og:title', 'content' => $projes['Proje']['baslik']));
        $this->HtmlMeta->addElement(array('property' => 'og:description', 'content' => strip_tags($projes['Proje']['aciklama'])));
            $this->HtmlMeta->addElement(array('property' => 'og:url', 'content' => Router::url('/', true).'projes/proje/ilanNo:'.$ilanNo));
            if(!empty($projes['ProjeResim'])){
                $this->HtmlMeta->addElement(array('property'=>'og:image', 'content'=>  Router::url('/',true).$projes['ProjeResim'][0]['path']));
            }else{
                $this->HtmlMeta->addElement(array('property'=>'og:image', 'content'=>  Router::url('/',true).'img/logo-xxs.png'));
            }

        }else{
            return $this->redirect(
            array('controller' => 'projes', 'action' => 'home')
            );    
        }
    }
    
    public function duzenle(){
        if($this->request->params['named']['ilanNo']){
           $ilanNo = $this->request->params['named']['ilanNo']; 
           
           $projes = $this->Proje->find('first',
                   array(
                       'fields'=>array('*'),
                       'conditions'=>array("Proje.id"=>$ilanNo),
                       'joins'=>array(
                            array(
                                'table'=>'em_proje_location',
                                'alias'=>'ProjeLocation',
                                'type'=>'LEFT',
                                'conditions'=>array('Proje.id=ProjeLocation.proje_id')
                            )
                        )
                       ));
        
        if(!$projes){
            $this->Session->setFlash('Aramış olduğunuz ilan kaldırılmıştır.','default', array('class'=>'alert alert-danger'));
            return $this->redirect(
            array('controller' => 'projes', 'action' => 'home')
            );    
        }
        $this->set('projes',$projes);
        
        $projeRes = $this->ProjeResim->findAllByProjeId($projes['Proje']['id']);
        $this->set('projeRes',$projeRes);
            $this->HtmlMeta->addElement(array('name' => 'robots', 'content' => 'noindex, nofollow'));
        }else{
            return $this->redirect(
            array('controller' => 'projes', 'action' => 'home')
            );    
        }
    }

    public function yeniproje(){
        $this->HtmlMeta->addElement(array('name' => 'robots', 'content' => 'noindex, nofollow'));
    }
    
    public function addProje(){
        $this->autoRender = false;
        if($this->request->is('post')){
            $files = array_key_exists('projeRes', $_FILES)?$_FILES['projeRes']:false;
            $filesVideo = array_key_exists('projeVideo', $_FILES)?$_FILES['projeVideo']:false;
            $data = $this->request->data;
            $this->Proje->create();
            $saveData = array('baslik'=>$data['baslik'],'aciklama'=>$data['aciklama'],'tarih'=> date('Y-m-d H:i:s'));
            if($this->Proje->save($saveData)){
                $projeId = $this->Proje->getInsertID();
                
                //Konut Maps
                $this->ProjeLocation->create();
                $this->ProjeLocation->save(array('proje_id'=>$projeId,'name'=>$data['location'],'latitude'=>$data['latitude'],'longitude'=>$data['longitude']));
                
                // Add Video
                $hataVid = 0;
                if($filesVideo){
                    if($filesVideo['error'] == 0 && $filesVideo['type'] != "video/mp4"){
                            $hataVid++;
                    }
                    else{
                        $fileType = explode('.', $filesVideo['name']);
                        $fileType = $fileType[count($fileType)-1];
                        $pathVideo = WWW_ROOT.'video/proje/'.$projeId.'.'.$fileType;
                        $path = 'video/proje/'.$projeId.'.'.$fileType;
                        if(move_uploaded_file($filesVideo['tmp_name'], $pathVideo)){
                            $this->Proje->updateAll(array('video'=>"'$path'"), array('id'=>$projeId));
                        }else{
                            $this->Proje->updateAll(array('video'=>null), array('id'=>$projeId));
                        }
                    }
                }
                
                //Add Image
                $hataRes = 0;
                if($files){
                    foreach($files['error'] as $key=>$value){
                        if($value == 0 && $files['type'][$key] != "image/jpeg" && $files['type'][$key] != "image/gif" && $files['type'][$key] != "image/png" && $files['type'][$key] != "image/tiff"){
                            $hataRes++;
                        }
                        else{
                            $fileType = explode('.', $files['name'][$key]);
                            $fileType = $fileType[count($fileType)-1];
                            $this->ProjeResim->create();
                            $this->ProjeResim->save(array('proje_id'=>$projeId));
                            $lastId = $this->ProjeResim->getLastInsertID();
                            $pathRes = WWW_ROOT.'img/proje/'.$lastId.'.'.$fileType;
                            $path = 'img/proje/'.$lastId.'.'.$fileType;
                            //image Resize
                            $manipulator = new ImageManipulator($files['tmp_name'][$key]);
                            $newImage = $manipulator->resample(1024, 768);
//                            move_uploaded_file($files['tmp_name'][$key], $pathRes)
                            if($manipulator->save($pathRes)){
                                $this->ProjeResim->updateAll(array('path'=>"'$path'"), array('id'=>$lastId));
                            }else{
                                $hataRes++;
                                $this->ProjeResim->deleteAll(array('id'=>$lastId));
                            }
                        }
                    }
                }
                if($hataRes>0){
                    $this->Session->setFlash('Yüklemek istediğiniz resimlerden bazıları sisteme yüklenememiştir. Sonra tekrar deneyin.','default', array('class'=>'alert alert-danger'));
                }else{
                    $this->Session->setFlash('Proje kayıt işlemi başarıyla sonuçlandı.','default', array('class'=>'alert alert-success'));
                }
            }else{
                $this->Session->setFlash('Proje kayıt işlemi başarısız. Lütfen tekrar deneyin.','default', array('class'=>'alert alert-danger'));
            }
            return $this->redirect(
            array('controller' => 'projes', 'action' => 'home')
            );
        }
    }
    
    public function upgradeproje(){
        $this->autoRender = false;
        if($this->request->is('post')){
            $files = array_key_exists('projeRes', $_FILES)?$_FILES['projeRes']:false;
            $filesVideo = array_key_exists('projeVideo', $_FILES)?$_FILES['projeVideo']:false;
            $data = $this->request->data;
            $projeId = $data['projeId'];
            $this->Proje->id = $projeId;
            $baslik = $data['baslik'];
            $aciklama = $data['aciklama'];
            $saveData = array(
                'baslik'=>$baslik,
                'aciklama'=>$aciklama
                 );

            if($this->Proje->save($saveData)){
                
                $this->ProjeLocation->deleteAll(array('proje_id'=>$projeId));
                //Konut Maps
                $this->ProjeLocation->create();
                $this->ProjeLocation->save(array('proje_id'=>$projeId,'name'=>$data['location'],'latitude'=>$data['latitude'],'longitude'=>$data['longitude']));
                
                //Add Video
                $hataVid = 0;
                if($filesVideo){
                    if($filesVideo['error'] == 0 && $filesVideo['type'] != "video/mp4"){
                            $hataVid++;
                    }
                    else{
                        $fileType = explode('.', $filesVideo['name']);
                        $fileType = $fileType[count($fileType)-1];
                        $pathVideo = WWW_ROOT.'video/proje/'.$projeId.'.'.$fileType;
                        $path = 'video/proje/'.$projeId.'.'.$fileType;
                        if(move_uploaded_file($filesVideo['tmp_name'], $pathVideo)){
                            $this->Proje->updateAll(array('video'=>"'$path'"), array('id'=>$projeId));
                        }else{
                            $this->Proje->updateAll(array('video'=>null), array('id'=>$projeId));
                        }
                    }
                }
                
                //Add Image
                $hataRes = 0;
                if($files){
                    foreach($files['error'] as $key=>$value){
                        if($value == 0 && $files['type'][$key] != "image/jpeg" && $files['type'][$key] != "image/gif" && $files['type'][$key] != "image/png" && $files['type'][$key] != "image/tiff"){
                            $hataRes++;
                        }
                        else{
                            $fileType = explode('.', $files['name'][$key]);
                            $fileType = $fileType[count($fileType)-1];
                            $this->ProjeResim->create();
                            $this->ProjeResim->save(array('proje_id'=>$projeId));
                            $lastId = $this->ProjeResim->getLastInsertID();
                            $pathRes = WWW_ROOT.'img/proje/'.$lastId.'.'.$fileType;
                            $path = 'img/proje/'.$lastId.'.'.$fileType;
                            //image Resize
                            $manipulator = new ImageManipulator($files['tmp_name'][$key]);
                            $newImage = $manipulator->resample(1024, 768);
//                            move_uploaded_file($files['tmp_name'][$key], $pathRes)
                            if($manipulator->save($pathRes)){
                                $this->ProjeResim->updateAll(array('path'=>"'$path'"), array('id'=>$lastId));
                            }else{
                                $hataRes++;
                                $this->ProjeResim->deleteAll(array('id'=>$lastId));
                            }
                        }
                    }
                }
                if($hataRes>0){
                    $this->Session->setFlash('Yüklemek istediğiniz resimlerden bazıları sisteme yüklenememiştir. Sonra tekrar deneyin.','default', array('class'=>'alert alert-danger'));
                }else{
                    $this->Session->setFlash('Proje düzenleme işlemi başarıyla sonuçlandı.','default', array('class'=>'alert alert-success'));
                    return $this->redirect(
                    array('controller' => 'projes', 'action' => 'duzenle/ilanNo:'.$projeId)
                    );
                }
            }else{
                $this->Session->setFlash('Proje düzenleme işlemi başarısız. Lütfen tekrar deneyin.','default', array('class'=>'alert alert-danger'));
            }
            return $this->redirect(
            array('controller' => 'projes', 'action' => 'yeniproje')
            );
        }else{
            $this->Session->setFlash('Proje düzenleme işlemi başarısız. Lütfen tekrar deneyin.','default', array('class'=>'alert alert-danger'));
            return $this->redirect(
            array('controller' => 'projes', 'action' => 'duzenle/ilanNo:'.$projeId)
            );
        }
    }
    
    public function deleteResim(){
        $this->autoRender = false;
        if($this->request->is('post')){
            $ResId = $this->request->data('resId');
            $resim = $this->ProjeResim->find('first',array('conditions'=>array('id'=>$ResId)));
            $fileRes = new File($resim['ProjeResim']['path']);
            $fileRes->delete();
            if($this->ProjeResim->deleteAll(array('id'=>$ResId))){
                echo json_encode(true);
            }else{
                echo json_encode(false);
            }
        }else{
            echo json_encode(false);
        }
    }
    
    public function ajaxDeleteProje(){
        $this->autoRender = false;
        if($this->request->is('post')){
            $projeId = $this->request->data('projeId');
            $projeRes = $this->ProjeResim->findAllByProjeId($projeId);
            $video = $this->Proje->find('first',array('conditions'=>array('id'=>$projeId)));
            foreach($projeRes as $row){
                $fileRes = new File($row['ProjeResim']['path']);
                $fileRes->delete();
            }
            $fileVideo = new File($video['Proje']['video']);
            $fileVideo->delete();
            
            $this->ProjeResim->deleteAll(array('proje_id'=>$projeId));
            $this->Proje->deleteAll(array('id'=>$projeId));
            echo json_encode(true);
        }
    }
    
    public function ajaxDeleteVideo(){
        $this->autoRender = false;
        if($this->request->is('post')){
            $projeId = $this->request->data('projeId');
            $video = $this->Proje->find('first',array('conditions'=>array('id'=>$projeId)));
            $fileVideo = new File($video['Proje']['video']);
            $fileVideo->delete();
            if($this->Proje->updateAll(array('video'=>null),array('id'=>$projeId))){
                echo json_encode(true);
            }else{
                echo json_encode(false);
            }
        }else{
            echo json_encode(false);
        }
    }
}