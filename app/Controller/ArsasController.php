<?php
/**
 * @property Arsa $Arsa
 * @property ArsaYer $ArsaYer
 */
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('ImageManipulator','Vendor');
class ArsasController extends AppController{

    var $uses = array('Arsa','Sehir','Ilce','Mahalle','ArsaYer','ArsaResim','Tip','ArsaLocation','Semt','IlanNo',
        'ArsaIletisim','IlanIletisim','IlanIletisimType');
    public $components = array('Paginator','HtmlMeta');

    public function arsaindex(){}

    public function home(){
        $named = $this->request->params['named'];
        if(array_key_exists('SatKir', $named) && ($named['SatKir']==1 || $named['SatKir']==2)){
           $SatKir = $named['SatKir'];
           
           if(array_key_exists('tarih', $named)){
               if($named['tarih']=='desc'){
                   $order = array('Arsa.tarih'=>'DESC');
               }else if($named['tarih']=='asc'){
                   $order = array('Arsa.tarih'=>'ASC');
               }
           }else if(array_key_exists('fiyat', $named)){
               if($named['fiyat']=='desc'){
                   $order = array('Arsa.fiyat'=>'DESC');
               }else if($named['fiyat'] == 'asc'){
                   $order = array('Arsa.fiyat'=>'ASC');
               }
           }else{
               $order = array('Arsa.tarih'=>'DESC');
           }
           $this->set('sirala',$order);
           
           $this->paginate = array(
                'fields'=>array('*'),
                'conditions'=>array('Arsa.durum'=>1,'Arsa.sat_kir'=>$SatKir),
                'limit'=>10,
                'order'=>$order,
                'joins'=>array(
                    array(
                        'table'=>'em_arsa_yer',
                        'alias'=>'ArsaYer',
                        'type'=>'INNER',
                        'conditions'=>array('Arsa.id=ArsaYer.arsa_id')
                    ),
                    array(
                        'table'=>'em_sehir',
                        'alias'=>'Sehir',
                        'type'=>'INNER',
                        'conditions'=>array('ArsaYer.sehir_id=Sehir.id')
                    ),
                    array(
                        'table'=>'em_ilce',
                        'alias'=>'Ilce',
                        'type'=>'LEFT',
                        'conditions'=>array('ArsaYer.ilce_id=Ilce.id')
                    ),
                    array(
                        'table'=>'em_semt',
                        'alias'=>'Semt',
                        'type'=>'LEFT',
                        'conditions'=>array('ArsaYer.semt_id=Semt.id')
                    ),
                    array(
                        'table'=>'em_mahalle',
                        'alias'=>'Mahalle',
                        'type'=>'LEFT',
                        'conditions'=>array('ArsaYer.mahalle_id=Mahalle.id')
                    )
                )
            );
        }
        else{
            if(array_key_exists('tarih', $named)){
               if($named['tarih']=='desc'){
                   $order = array('Arsa.tarih'=>'DESC');
               }else if($named['tarih']=='asc'){
                   $order = array('Arsa.tarih'=>'ASC');
               }
           }else if(array_key_exists('fiyat', $named)){
               if($named['fiyat']=='desc'){
                   $order = array('Arsa.fiyat'=>'DESC');
               }else if($named['fiyat'] == 'asc'){
                   $order = array('Arsa.fiyat'=>'ASC');
               }
           }else{
               $order = array('Arsa.tarih'=>'DESC');
           }
           $this->set('sirala',$order);
            
            $this->paginate = array(
                'fields'=>array('*'),
                'conditions'=>array('Arsa.durum'=>1),
                'limit'=>10,
                'order'=>$order,
                'joins'=>array(
                    array(
                        'table'=>'em_arsa_yer',
                        'alias'=>'ArsaYer',
                        'type'=>'INNER',
                        'conditions'=>array('Arsa.id=ArsaYer.arsa_id')
                    ),
                    array(
                        'table'=>'em_sehir',
                        'alias'=>'Sehir',
                        'type'=>'INNER',
                        'conditions'=>array('ArsaYer.sehir_id=Sehir.id')
                    ),
                    array(
                        'table'=>'em_ilce',
                        'alias'=>'Ilce',
                        'type'=>'LEFT',
                        'conditions'=>array('ArsaYer.ilce_id=Ilce.id')
                    ),
                    array(
                        'table'=>'em_semt',
                        'alias'=>'Semt',
                        'type'=>'LEFT',
                        'conditions'=>array('ArsaYer.semt_id=Semt.id')
                    ),
                    array(
                        'table'=>'em_mahalle',
                        'alias'=>'Mahalle',
                        'type'=>'LEFT',
                        'conditions'=>array('ArsaYer.mahalle_id=Mahalle.id')
                    )
                )
            );
        }
        $arsas = $this->paginate('Arsa');
        $this->set('arsas',$arsas);
        
        if(!$arsas){
            $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button>Aradığınız kriterlere uygun sonuç yoktur.','default', array('class'=>'alert alert-danger alert-dismissable'));
            return $this->redirect(Router::url('/',true));
        }
        
        $arsaRes = array();
        foreach ($arsas as $row) {
            $arsaRes[$row['Arsa']['id']] = $this->ArsaResim->findAllByArsaId($row['Arsa']['id']);
        }
        //$arsaRes = $this->ArsaResim->find('all');
        $this->set('arsaRes',$arsaRes);

        $this->HtmlMeta->addElement(array('name' => 'robots', 'content' => 'index, follow'));
        $this->HtmlMeta->addElement(array('name' => 'title', 'content' => 'Arsa - Maden Gayrimenkul A.Ş.'));
        $this->HtmlMeta->addElement(array('name' => 'description', 'content' => 'Satılık, kiralık, konut, işyeri, arsa yatırım ve yönetimi - Mutlu Kazanç Kapısı...'));
        $this->HtmlMeta->addElement(array('property' => 'og:title', 'content' => 'Arsa - Maden Gayrimenkul A.Ş.' ));
        $this->HtmlMeta->addElement(array('property' => 'og:description', 'content' => 'Satılık, kiralık, konut, işyeri, arsa yatırım ve yönetimi - Mutlu Kazanç Kapısı...'));
        $this->HtmlMeta->addElement(array('property' => 'og:keywords', 'content' => 'Eskişehir Yolu satılık yatırım arsaları, Temelli, Malıköy, Ihlamurkent satılık villa, Alcı, Türkobası, Anayurt, Bacı, Başkent OSB satılık arsa, kiralık fabrika, Anadolu OSB satılık fabrika arsa, ASO 2 OSB, Konutkent, Çayyolu, Westgate Residence, Kuleevo, Elmar Towers, Elya Tower, Azel Kule' ));
        $this->HtmlMeta->addElement(array('property' => 'og:url', 'content' => Router::url('/', true)));
        $this->HtmlMeta->addElement(array('property'=>'og:image', 'content'=>  Router::url('/',true).'img/shareicon/mar.png'));
    }
    
    public function yeniarsa(){
        $il = $this->Sehir->find('all',array('order'=>'sehir_adi ASC'));
        $this->set('il',$il);
        $iletisim = $this->IlanIletisim->find('all');
        $this->set('iletisim',$iletisim);
        $this->HtmlMeta->addElement(array('name' => 'robots', 'content' => 'noindex, nofollow'));
    }
    
    public function addarsa(){
        $this->autoRender = false;
        $files = array_key_exists('arsaRes', $_FILES)?$_FILES['arsaRes']:false;
        $data = $this->request->data;
        $this->Arsa->create();
        $saveData = array('tip'=>3,
            'baslik'=>$data['baslik'],
            'aciklama'=>$data['aciklama'],
            'imar_durum'=>$data['imar'],
            'm_kare'=>$data['m2'],
            'ada'=>$data['ada'],
            'parsel'=>$data['parsel'],
            'kredi'=>$data['kredi'],
            'fiyat'=>$data['fiyat'],
            'sat_kir'=>$data['sat_kir'],
            'durum'=>1,
            'tapu'=>$data['tapu'],
            'tarih'=> date('Y-m-d'),
            'parabirimi'=>$data['paraBirimi'],
            'mal_name'=>$data['malName'],
            'mal_tel'=>$data['malTelNo'],
            'mal_fiyat'=>$data['malFiyat'],
            'mal_parabirimi'=>$data['malParaBirimi'],
            'video'=>$data['video']);
        
        if($this->Arsa->save($saveData)){
            $arsaId = $this->Arsa->getInsertID();
            $ilanNo = $this->IlanNo->find('first');
//            $arsailanno = 'A'.$arsaId;
            $arsailanno = $ilanNo['IlanNo']['ilan_no']+1;
            $test = $this->IlanNo->updateAll(array('ilan_no'=>$arsailanno),array('id'=>1));
            $this->Arsa->updateAll(array('ilan_no'=>"'$arsailanno'"), array('id'=>$arsaId));

            //Arsa Yer
            $this->ArsaYer->create();
            $yerArray = array();
            $yerArray['sehir_id'] = $data['il'];
            $yerArray['ilce_id'] = array_key_exists('ilce', $data)?$data['ilce']:null;
            $yerArray['semt_id'] = array_key_exists('semt', $data)?$data['semt']:null;
            $yerArray['mahalle_id'] = array_key_exists('mahalle', $data)?$data['mahalle']:null;
            $yerArray['arsa_id'] = $arsaId;
            $this->ArsaYer->save($yerArray);
            
            //Arsa Maps
            $this->ArsaLocation->create();
            $this->ArsaLocation->save(array('arsa_id'=>$arsaId,'name'=>$data['location'],'latitude'=>$data['latitude'],'longitude'=>$data['longitude']));
            
            // Arsa İletisim Bilgileri
            $arsaIlet = array_key_exists('ilanilet',$data)?$data['ilanilet']:false;
            foreach($arsaIlet as $row){
                $this->ArsaIletisim->create();
                $this->ArsaIletisim->save(array('arsa_id'=>$arsaId,'ilet_id'=>$row));
            }

            $hataRes = 0;
            if($files){
                foreach($files['error'] as $key=>$value){
                    if($value == 0 && $files['type'][$key] != "image/jpeg" && $files['type'][$key] != "image/gif" && $files['type'][$key] != "image/png" && $files['type'][$key] != "image/tiff"){
                        $hataRes++;
                    }
                    else{
                        $fileType = pathinfo($files['name'][$key], PATHINFO_EXTENSION);
                        //$fileType = explode('.', $files['name'][$key]);
                        //$fileType = $fileType[count($fileType)-1];
                        $this->ArsaResim->create();
                        $this->ArsaResim->save(array('arsa_id'=>$arsaId));
                        $lastId = $this->ArsaResim->getLastInsertID();
                        $path = 'img/arsa/'.$arsaId.'/'.$lastId.'.'.$fileType;
                        $pathRes = WWW_ROOT.$path;
                        $pathThumb = 'img/arsa/'.$arsaId.'/'.$lastId.'thumb.'.$fileType;
                        $pathResThumb = WWW_ROOT.$pathThumb;
                        //image Resize
                        $manipulator = new ImageManipulator($files['tmp_name'][$key]);
                        $newImage = $manipulator->resample(800, 600, false);
//                        move_uploaded_file($files['tmp_name'][$key], $pathRes)
                        if($manipulator->save($pathRes)){
                            $this->ArsaResim->id = $lastId;
                            $this->ArsaResim->save(array('path'=>$path));
                            $manipulator = new ImageManipulator($pathRes);
                            $newImage = $manipulator->resample(200, 200);
                            if($manipulator->save($pathResThumb)){
                                $this->ArsaResim->updateAll(array('paththumb'=>"'$pathThumb'"),array('id'=>$lastId));
                                /*$this->ArsaResim->id = $lastId;
                                $this->ArsaResim->save(array('paththumb'=>$pathThumb));*/
                            }
                        }else{
                            $hataRes++;
                            $this->ArsaResim->deleteAll(array('id'=>$lastId));
                        }
                    }
                }
            }
            if($hataRes>0){
                $this->Session->setFlash('Yüklemek istediğiniz resimlerden bazıları sisteme yüklenememiştir. Sonra tekrar deneyin.','default', array('class'=>'alert alert-danger'));
            }else{
                $this->Session->setFlash('Arsa kayıt işlemi başarıyla sonuçlandı.','default', array('class'=>'alert alert-success'));
            }
        }else{
            $this->Session->setFlash('Arsa kayıt işlemi başarısız. Lütfen tekrar deneyin.','default', array('class'=>'alert alert-danger'));
        }
        return $this->redirect(
        array('controller' => 'arsas', 'action' => 'yeniarsa')
        );
    }
    
    function arsa(){
        if($this->request->params['named']['ilanNo']){
           $ilanNo = $this->request->params['named']['ilanNo'];

            /*if(!$this->Session->check('ilanNo.'.$ilanNo) && rand(1,5) == 3){
                $this->Session->write('ilanNo.'.$ilanNo,1);
                $this->redirect(SHORTE_URL.'arsa/ilanNo:'.$ilanNo);
            }*/

           $arsas = $this->Arsa->find('first',array(
            'fields'=>array('*'),
            'contain'=>array('KonutDanisman'=>array('Danisman')),
            'conditions'=>array("ilan_no" => $ilanNo,"durum"=>1),
            'joins'=>array(
                array('table'=>'em_arsa_yer', 'alias'=>'ArsaYer', 'type'=>'INNER', 'conditions'=>array('Arsa.id=ArsaYer.arsa_id')),
                array('table'=>'em_sehir', 'alias'=>'Sehir', 'type'=>'INNER', 'conditions'=>array('ArsaYer.sehir_id=Sehir.id')),
                array('table'=>'em_ilce', 'alias'=>'Ilce', 'type'=>'LEFT', 'conditions'=>array('ArsaYer.ilce_id=Ilce.id')),
                array('table'=>'em_semt', 'alias'=>'Semt', 'type'=>'LEFT', 'conditions'=>array('ArsaYer.semt_id=Semt.id')),
                array('table'=>'em_mahalle', 'alias'=>'Mahalle', 'type'=>'LEFT', 'conditions'=>array('ArsaYer.mahalle_id=Mahalle.id')),
                array('table'=>'em_arsa_location', 'alias'=>'ArsaLocation', 'type'=>'LEFT', 'conditions'=>array('Arsa.id=ArsaLocation.arsa_id'))
            )
        ));
        
        if(!$arsas){
            $this->Session->setFlash('Aramış olduğunuz ilan kaldırılmıştır.','default', array('class'=>'alert alert-danger'));
            return $this->redirect(
            array('controller' => 'arsas', 'action' => 'home')
            );    
        }
        $this->set('arsas',$arsas);
        
        $arsaRes = $this->ArsaResim->find('all',array('order'=>array('id'=>'ASC'),'fields'=>array('*'),'conditions'=>array('arsa_id'=>$arsas['Arsa']['id'])));
        $this->set('arsaRes',$arsaRes);

            $IlanTel = $this->ArsaIletisim->find('all',
                array(
                    'fields'=>array('IlanIlet.*','IlanIletType.*'),
                    'conditions'=>array('ArsaIletisim.arsa_id'=>$arsas['Arsa']['id'],'IlanIlet.type'=>1),
                    'orders'=>array('IlanIlet.type'=>'ASC'),
                    'joins'=>array(
                        array('table'=>'em_ilaniletisim', 'alias'=>'IlanIlet', 'type'=>'INNER', 'conditions'=>array('ArsaIletisim.ilet_id = IlanIlet.id')),
                        array('table'=>'em_ilaniletisimtype', 'alias'=>'IlanIletType', 'type'=>'INNER', 'conditions'=>array('IlanIlet.type = IlanIletType.id'))
                    )
                )
            );
            $this->set('IlanTel',$IlanTel);

            $IlanMail = $this->ArsaIletisim->find('all',
                array(
                    'fields'=>array('IlanIlet.*','IlanIletType.*'),
                    'conditions'=>array('ArsaIletisim.arsa_id'=>$arsas['Arsa']['id'],'IlanIlet.type'=>3),
                    'orders'=>array('IlanIlet.type'=>'ASC'),
                    'joins'=>array(
                        array('table'=>'em_ilaniletisim', 'alias'=>'IlanIlet', 'type'=>'INNER', 'conditions'=>array('ArsaIletisim.ilet_id = IlanIlet.id')),
                        array('table'=>'em_ilaniletisimtype', 'alias'=>'IlanIletType', 'type'=>'INNER', 'conditions'=>array('IlanIlet.type = IlanIletType.id'))
                    )
                )
            );
            $this->set('IlanMail',$IlanMail);

            $IlanAdres = $this->ArsaIletisim->find('all',
                array(
                    'fields'=>array('IlanIlet.*','IlanIletType.*'),
                    'conditions'=>array('ArsaIletisim.arsa_id'=>$arsas['Arsa']['id'],'IlanIlet.type'=>5),
                    'orders'=>array('IlanIlet.type'=>'ASC'),
                    'joins'=>array(
                        array('table'=>'em_ilaniletisim', 'alias'=>'IlanIlet', 'type'=>'INNER', 'conditions'=>array('ArsaIletisim.ilet_id = IlanIlet.id')),
                        array('table'=>'em_ilaniletisimtype', 'alias'=>'IlanIletType', 'type'=>'INNER', 'conditions'=>array('IlanIlet.type = IlanIletType.id'))
                    )
                )
            );
            $this->set('IlanAdres',$IlanAdres);
            $this->set('IlanNo',$ilanNo);

        $this->HtmlMeta->addElement(array('name' => 'robots', 'content' => 'index, follow'));
        $this->HtmlMeta->addElement(array('name' => 'title', 'content' => $arsas['Arsa']['baslik']));
        $this->HtmlMeta->addElement(array('name' => 'description', 'content' => strip_tags($arsas['Arsa']['aciklama'])));
        $this->HtmlMeta->addElement(array('property' => 'og:title', 'content' => $arsas['Arsa']['baslik'] ));
        $this->HtmlMeta->addElement(array('property' => 'og:description', 'content' => strip_tags($arsas['Arsa']['aciklama'])));
        $this->HtmlMeta->addElement(array('property' => 'og:keywords', 'content' => $arsas['Arsa']['baslik'] ));
        $this->HtmlMeta->addElement(array('property' => 'og:url', 'content' => Router::url('/', true).'arsa/ilanNo:'.$ilanNo));

        if($arsaRes){
            $this->HtmlMeta->addElement(array('property'=>'og:image', 'content'=>  Router::url('/',true).$arsaRes[0]['ArsaResim']['path']));
        }else{
            $this->HtmlMeta->addElement(array('property'=>'og:image', 'content'=>  Router::url('/',true).'img/logo-xxs.png'));
        }

            $video_code = false;
            if (preg_match("/youtube.com/", $arsas['Arsa']['aciklama']) || preg_match("/youtu.be/", $arsas['Arsa']['aciklama'])){
                if (preg_match('%(?:youtube\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $arsas['Arsa']['aciklama'], $match))
                {
                    $video_code = $match[1];
                }
                $source = 'http://www.youtube.com/e/'.$video_code;
                $picture = 'http://img.youtube.com/vi/'.$video_code.'/0.jpg';
            }
            else if (preg_match("/vimeo.com/", $arsas['Arsa']['aciklama']))
            {
                if (preg_match('/vimeo\.com\/(clip\:)?(\d+).*$/', $arsas['Arsa']['aciklama'], $match))
                {
                    $video_code = $match[2];
                }
                /* Get Vimeo thumbnail */
                $hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/$video_code.php"));
                $picture = $hash[0]['thumbnail_medium'];
                $source = 'https://secure.vimeo.com/moogaloop.swf?clip_id='.$video_code.'&autoplay=1';
            }

            if($video_code){
                $this->HtmlMeta->addElement(array('property' => 'og:type', 'content' => 'video' ));
                $this->HtmlMeta->addElement(array('property' => 'og:video', 'content' => $source ));
                /*$this->HtmlMeta->addElement(array('property' => 'og:video:width', 'content' => '374' ));
                $this->HtmlMeta->addElement(array('property' => 'og:video:height', 'content' => '202' ));*/
                $this->HtmlMeta->addElement(array('property' => 'og:video:type', 'content' => 'application/x-shockwave-flash' ));
            }
        }else{
            return $this->redirect(
            array('controller' => 'arsas', 'action' => 'home')
            );    
        }
    }
    
    public function duzenle(){
        if($this->request->params['named']['ilanNo']){
           $ilanNo = $this->request->params['named']['ilanNo']; 
           
           $arsas = $this->Arsa->find('first',array(
            'fields'=>array('*'),
            'conditions'=>array("ilan_no LIKE '%$ilanNo%'"),
            'joins'=>array(
                array(
                    'table'=>'em_arsa_yer',
                    'alias'=>'ArsaYer',
                    'type'=>'INNER',
                    'conditions'=>array('Arsa.id=ArsaYer.arsa_id')
                ),
                array(
                    'table'=>'em_sehir',
                    'alias'=>'Sehir',
                    'type'=>'INNER',
                    'conditions'=>array('ArsaYer.sehir_id=Sehir.id')
                ),
                array(
                    'table'=>'em_ilce',
                    'alias'=>'Ilce',
                    'type'=>'LEFT',
                    'conditions'=>array('ArsaYer.ilce_id=Ilce.id')
                ),
                array(
                    'table'=>'em_semt',
                    'alias'=>'Semt',
                    'type'=>'LEFT',
                    'conditions'=>array('ArsaYer.semt_id=Semt.id')
                ),
                array(
                    'table'=>'em_mahalle',
                    'alias'=>'Mahalle',
                    'type'=>'LEFT',
                    'conditions'=>array('ArsaYer.mahalle_id=Mahalle.id')
                ),
                array(
                    'table'=>'em_arsa_location',
                    'alias'=>'ArsaLocation',
                    'type'=>'LEFT',
                    'conditions'=>array('Arsa.id=ArsaLocation.arsa_id')
                )
            )
        ));
        
        $this->set('arsas',$arsas);
        if($arsas['Arsa']['user_id'] != $this->Session->read('UserId') && $this->Session->read('UserGroup') != 1){
            $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button>Bu ilanı düzeltme yetkiniz yoktur.','default', array('class'=>'alert alert-danger alert-dismissable'));
                return $this->redirect(
                array('controller' => 'arsas', 'action' => 'home')
                );
        }
        $arsaRes = $this->ArsaResim->findAllByArsaId($arsas['Arsa']['id']);
        $this->set('arsaRes',$arsaRes);
           
        $il = $this->Sehir->find('all',array('order'=>'id ASC'));
        $this->set('il',$il);
        $ilce = $this->Ilce->find('all',array('conditions'=>array('sehir_id'=>$arsas['ArsaYer']['sehir_id']),'order'=>'id ASC'));
        $this->set('ilce',$ilce);
        $semt = $this->Semt->find('all',array('conditions'=>array('ilce_id'=>$arsas['ArsaYer']['ilce_id']),'order'=>'id ASC'));
        $this->set('semt',$semt);
        $mahalle = $this->Mahalle->find('all',array('conditions'=>array('semt_id'=>$arsas['ArsaYer']['semt_id']),'order'=>'id ASC'));
        $this->set('mahalle',$mahalle);
        
        $iletisim = $this->IlanIletisim->find('all');
        $this->set('iletisim',$iletisim);
        $arsaIletisim = $this->ArsaIletisim->find('all',array(
            'fields'=>array('*'),
            'conditions'=>array("ArsaIletisim.arsa_id"=>$arsas['Arsa']['id']),
            'joins'=>array(
                array(
                    'table'=>'em_ilaniletisim',
                    'alias'=>'IlanIlet',
                    'type'=>'INNER',
                    'conditions'=>array('ArsaIletisim.ilet_id = IlanIlet.id')
                )
                )
            ));
        $this->set('ArsaIlet',$arsaIletisim);
            $this->HtmlMeta->addElement(array('name' => 'robots', 'content' => 'noindex, nofollow'));
        }else{
            $this->Session->setFlash('Bir hata meydana geldi. Lütfen tekrar deneyin.','default', array('class'=>'alert alert-danger'));
            return $this->redirect(
            array('controller' => 'arsas', 'action' => 'home')
            );    
        }
    }
    
    public function deleteResim(){
        $this->autoRender = false;
        if($this->request->is('post')){
            $ResId = $this->request->data('resId');
            $resim = $this->ArsaResim->find('first',array('conditions'=>array('id'=>$ResId)));
            $fileRes = new File($resim['ArsaResim']['path']);
            $fileRes->delete();
            $fileResThumb = new File($resim['ArsaResim']['paththumb']);
            if($fileResThumb){
                $fileResThumb->delete();
            }
            if($this->ArsaResim->deleteAll(array('id'=>$ResId))){
                echo json_encode(true);
            }else{
                echo json_encode(false);
            }
        }else{
            echo json_encode(false);
        }
    }
    
    public function upgradearsa(){
        $this->autoRender = false;
        if($this->request->params['named']['ilanNo']){
           $ilanNo = $this->request->params['named']['ilanNo']; 
           
           $arsa = $this->Arsa->find('first',array('conditions'=>array('ilan_no' => $ilanNo)));
           $arsaId = $arsa['Arsa']['id'];
           
            $files = array_key_exists('arsaRes', $_FILES)?$_FILES['arsaRes']:false;
            $data = $this->request->data;
            $this->Arsa->id = $arsaId;
            
            $baslik = $data['baslik'];
            $aciklama = $data['aciklama'];
            $imardurumu = $data['imar'];
            $mkare = $data['m2'];
            $ada = $data['ada'];
            $parsel = $data['parsel'];
            $kredi = $data['kredi'];
            $fiyat = $data['fiyat'];
            $tapu = $data['tapu'];
            $paraBirim = $data['paraBirimi'];

            $saveData = array(
                'baslik'=>$baslik,
                'aciklama'=>$aciklama,
                'imar_durum'=>$imardurumu,
                'm_kare'=>$mkare,
                'ada'=>$ada,
                'parsel'=>$parsel,
                'kredi'=>$kredi,
                'fiyat'=>$fiyat,
                'sat_kir'=>$data['sat_kir'],
                'tapu'=>$tapu,
                'parabirimi'=>$paraBirim,
                'mal_name'=>$data['malName'],
                'mal_tel'=>$data['malTelNo'],
                'mal_fiyat'=>$data['malFiyat'],
                'mal_parabirimi'=>$data['malParaBirimi'],
                'video'=>$data['video']);

            if($this->Arsa->save($saveData)){
                $this->ArsaYer->deleteAll(array('arsa_id'=>$arsaId));
                //Arsa Yer
                $this->ArsaYer->create();
                $yerArray = array();
                $yerArray['sehir_id'] = $data['il'];
                $yerArray['ilce_id'] = array_key_exists('ilce', $data)?$data['ilce']:null;
                $yerArray['semt_id'] = array_key_exists('semt', $data)?$data['semt']:null;
                $yerArray['mahalle_id'] = array_key_exists('mahalle', $data)?$data['mahalle']:null;
                $yerArray['arsa_id'] = $arsaId;
                $this->ArsaYer->save($yerArray);

                $this->ArsaLocation->deleteAll(array('arsa_id'=>$arsaId));
                //Arsa Maps
                $this->ArsaLocation->create();
                $this->ArsaLocation->save(array('arsa_id'=>$arsaId,'name'=>$data['location'],'latitude'=>$data['latitude'],'longitude'=>$data['longitude']));

                // Arsa İletisim Bilgileri
                $arsaIlet = array_key_exists('ilanilet',$data)?$data['ilanilet']:false;
                $this->ArsaIletisim->deleteAll(array('arsa_id'=>$arsaId));
                foreach($arsaIlet as $row){
                    $this->ArsaIletisim->create();
                    $this->ArsaIletisim->save(array('arsa_id'=>$arsaId,'ilet_id'=>$row));
                }

                $hataRes = 0;
                if($files){
                    foreach($files['error'] as $key=>$value){
                        if($value == 0 && $files['type'][$key] != "image/jpeg" && $files['type'][$key] != "image/gif" && $files['type'][$key] != "image/png" && $files['type'][$key] != "image/tiff"){
                            $hataRes++;
                        }
                        else{
                            $fileType = pathinfo($files['name'][$key], PATHINFO_EXTENSION);
                            //$fileType = explode('.', $files['name'][$key]);
                            //$fileType = $fileType[count($fileType)-1];
                            $this->ArsaResim->create();
                            $this->ArsaResim->save(array('arsa_id'=>$arsaId));
                            $lastId = $this->ArsaResim->getLastInsertID();
                            $path = 'img/arsa/'.$arsaId.'/'.$lastId.'.'.$fileType;
                            $pathRes = WWW_ROOT.$path;
                            $pathThumb = 'img/arsa/'.$arsaId.'/'.$lastId.'thumb.'.$fileType;
                            $pathResThumb = WWW_ROOT.$pathThumb;
                            //image Resize
                            $manipulator = new ImageManipulator($files['tmp_name'][$key]);
                            $newImage = $manipulator->resample(800, 600, false);
//                            move_uploaded_file($files['tmp_name'][$key], $pathRes)
                            if($manipulator->save($pathRes)){
                                $this->ArsaResim->id = $lastId;
                                $this->ArsaResim->save(array('path'=>$path));
                                $manipulator = new ImageManipulator($pathRes);
                                $newImage = $manipulator->resample(200, 200);
                                if($manipulator->save($pathResThumb)){
                                    $this->ArsaResim->updateAll(array('paththumb'=>"'$pathThumb'"),array('id'=>$lastId));
                                    /*$this->ArsaResim->id = $lastId;
                                    $this->ArsaResim->save(array('paththumb'=>$pathThumb));*/
                                }
                            }else{
                                $hataRes++;
                                $this->ArsaResim->deleteAll(array('id'=>$lastId));
                            }
                        }
                    }
                }
                if($hataRes>0){
                    $this->Session->setFlash('Yüklemek istediğiniz resimlerden bazıları sisteme yüklenememiştir. Sonra tekrar deneyin.','default', array('class'=>'alert alert-danger'));
                }else{
                    $this->Session->setFlash('Arsa düzenleme işlemi başarıyla sonuçlandı.','default', array('class'=>'alert alert-success'));
                    return $this->redirect(
                    array('controller' => 'arsas', 'action' => 'duzenle/ilanNo:'.$ilanNo)
                    );
                }
            }else{
                $this->Session->setFlash('Arsa düzenleme işlemi başarısız. Lütfen tekrar deneyin.','default', array('class'=>'alert alert-danger'));
            }
            return $this->redirect(
            array('controller' => 'arsas', 'action' => 'yeniarsa')
            );
        }else{
            $this->Session->setFlash('Arsa düzenleme işlemi başarısız. Lütfen tekrar deneyin.','default', array('class'=>'alert alert-danger'));
            return $this->redirect(
            array('controller' => 'arsas', 'action' => 'home')
            );
        }
    }
    
    public function getAjaxVideo(){
        $this->autoRender = false;
        if($this->request->is('post')){
            $arsaId = $this->request->data('arsaId');
            $video = $this->Arsa->find('first',array('conditions'=>array('id'=>$arsaId),'fields'=>array('video')));
            echo json_encode($video);
        }else{
            echo json_encode(false);
        }
    }
    
    public function ajaxDeleteArsa(){
        $this->autoRender = false;
        if($this->request->is('post')){
            $arsaId = $this->request->data('arsaId');
            $arsas = $this->Arsa->findById($arsaId);
            if($arsas['Arsa']['user_id'] != $this->Session->read('UserId') && $this->Session->read('UserGroup') != 1){
                echo json_encode(array('hata'=>true, 'message'=>'Bu ilanı silme yetkiniz yoktur.'));
            }else{
                $arsaRes = $this->ArsaResim->findAllByArsaId($arsaId);
                foreach($arsaRes as $row){
                    $fileRes = new File($row['ArsaResim']['path']);
                    $fileRes->delete();
                    $fileResThumb = new File($row['ArsaResim']['paththumb']);
                    if($fileResThumb){
                        $fileResThumb->delete();
                    }
                }
                if($arsas['Arsa']['video'] != '' || empty($arsas['Arsa']['video'])){
                    $fileVid = new File(WWW_ROOT.$arsas['Arsa']['video']);
                    $fileVid->delete();
                }
                $this->ArsaResim->deleteAll(array('arsa_id'=>$arsaId));
                $this->ArsaLocation->deleteAll(array('arsa_id'=>$arsaId));
                $this->ArsaYer->deleteAll(array('arsa_id'=>$arsaId));
                $this->Arsa->deleteAll(array('id'=>$arsaId));
                echo json_encode(array('hata'=>false, 'message'=>'İlan başarıyla silindi.'));
            }
        }
    }
    
    public function ajaxDeleteVideo(){
        $this->autoRender = false;
        if($this->request->is('post')){
            $arsaId = $this->request->data('arsaId');
            $video = $this->Arsa->find('first',array('conditions'=>array('id'=>$arsaId)));
            $fileVideo = new File($video['Arsa']['video']);
            $fileVideo->delete();
            if($this->Arsa->updateAll(array('video'=>null),array('id'=>$arsaId))){
                echo json_encode(true);
            }else{
                echo json_encode(false);
            }
        }else{
            echo json_encode(false);
        }
    }

    public function aciklamagetir(){
        $this->autoRender = false;
        if($this->request->is('post')){
            $aId = $this->request->data('aId');
            $arsa = $this->Arsa->findById($aId);
            if($arsa){
                echo json_encode($arsa['Arsa']['aciklama']);
            }else{
                echo json_encode(false);
            }
        }else{
            echo json_encode(false);
        }
    }
}