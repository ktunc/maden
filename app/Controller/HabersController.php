<?php
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('ImageManipulator','Vendor');
class HabersController extends AppController{
    var $uses = array('Haber','HaberResim','HaberLocation');
    public $components = array('Paginator','HtmlMeta');
    
    public function home(){
        $this->paginate = array(
            'limit'=>10,
            'order'=>array('Haber.id'=>'desc')
        );
        
        $habers = $this->paginate('Haber');
        $this->set('habers',$habers);
        
        if(!$habers){
            $this->Session->setFlash('Aradığınız kriterlere uygun sonuç yoktur.','default', array('class'=>'alert alert-danger'));
            return $this->redirect(Router::url('/',true));
        }
        
        $haberRes = array();
        foreach ($habers as $row) {
            $haberRes[$row['Haber']['id']] = $this->HaberResim->findAllByHaberId($row['Haber']['id']);
        }
        $this->set('haberRes',$haberRes);

        $this->HtmlMeta->addElement(array('name' => 'robots', 'content' => 'index, follow'));
        $this->HtmlMeta->addElement(array('name' => 'title', 'content' => 'Haber - Maden Gayrimenkul A.Ş.'));
        $this->HtmlMeta->addElement(array('name' => 'description', 'content' => 'Satılık, kiralık, konut, işyeri, arsa yatırım ve yönetimi - Mutlu Kazanç Kapısı...'));
        $this->HtmlMeta->addElement(array('property' => 'og:title', 'content' => 'Haber - Maden Gayrimenkul A.Ş.' ));
        $this->HtmlMeta->addElement(array('property' => 'og:description', 'content' => 'Satılık, kiralık, konut, işyeri, arsa yatırım ve yönetimi - Mutlu Kazanç Kapısı...'));
        $this->HtmlMeta->addElement(array('property' => 'og:keywords', 'content' => 'Eskişehir Yolu satılık yatırım arsaları, Temelli, Malıköy, Ihlamurkent satılık villa, Alcı, Türkobası, Anayurt, Bacı, Başkent OSB satılık arsa, kiralık fabrika, Anadolu OSB satılık fabrika arsa, ASO 2 OSB, Konutkent, Çayyolu, Westgate Residence, Kuleevo, Elmar Towers, Elya Tower, Azel Kule' ));
        $this->HtmlMeta->addElement(array('property' => 'og:url', 'content' => Router::url('/', true).'habers/home'));
        $this->HtmlMeta->addElement(array('property'=>'og:image', 'content'=>  Router::url('/',true).'img/shareicon/mhab.png'));
    }
    
    public function haber(){
        if($this->request->params['named']['ilanNo']){
           $ilanNo = $this->request->params['named']['ilanNo'];

           $habers = $this->Haber->find('first',
                   array(
                       'fields'=>array('*'),
                       'contain'=>array('HaberResim'),
                       'conditions'=>array("Haber.id"=>$ilanNo)
                       ));
        
            if(!$habers){
                $this->Session->setFlash('Aramış olduğunuz ilan kaldırılmıştır.','default', array('class'=>'alert alert-danger'));
                return $this->redirect(
                array('controller' => 'habers', 'action' => 'home')
                );
            }
            $this->set('habers',$habers);

            $this->HtmlMeta->addElement(array('name' => 'robots', 'content' => 'index, follow'));
            $this->HtmlMeta->addElement(array('name' => 'title', 'content' => $habers['Haber']['baslik']));
            $this->HtmlMeta->addElement(array('name' => 'description', 'content' => strip_tags($habers['Haber']['aciklama'])));
            $this->HtmlMeta->addElement(array('property' => 'og:title', 'content' => $habers['Haber']['baslik']));
            $this->HtmlMeta->addElement(array('property' => 'og:description', 'content' => strip_tags($habers['Haber']['aciklama'])));
            $this->HtmlMeta->addElement(array('property' => 'og:keywords', 'content' => $habers['Haber']['baslik'] ));
            $this->HtmlMeta->addElement(array('property' => 'og:url', 'content' => Router::url('/', true).'haber/ilanNo:'.$ilanNo));
            if(!empty($habers['HaberResim'])){
                $this->HtmlMeta->addElement(array('property'=>'og:image', 'content'=>  Router::url('/',true).$habers['HaberResim'][0]['path']));
            }else{
                $this->HtmlMeta->addElement(array('property'=>'og:image', 'content'=>  Router::url('/',true).'img/logo-xxs.png'));
            }
        }else{
            return $this->redirect(
            array('controller' => 'habers', 'action' => 'home')
            );    
        }
    }
    
    public function duzenle(){
        if($this->request->params['named']['ilanNo']){
           $ilanNo = $this->request->params['named']['ilanNo']; 
           
           $habers = $this->Haber->find('first',
                   array(
                       'fields'=>array('*'),
                       'conditions'=>array("Haber.id"=>$ilanNo),
                       'joins'=>array(
                            array(
                                'table'=>'em_haber_location',
                                'alias'=>'HaberLocation',
                                'type'=>'LEFT',
                                'conditions'=>array('Haber.id=HaberLocation.haber_id')
                            )
                        )
                       ));
        
        if(!$habers){
            $this->Session->setFlash('Aramış olduğunuz ilan kaldırılmıştır.','default', array('class'=>'alert alert-danger'));
            return $this->redirect(
            array('controller' => 'habers', 'action' => 'home')
            );    
        }
        $this->set('habers',$habers);
        
        $haberRes = $this->HaberResim->findAllByHaberId($habers['Haber']['id']);
        $this->set('haberRes',$haberRes);
            $this->HtmlMeta->addElement(array('name' => 'robots', 'content' => 'noindex, nofollow'));
        }else{
            return $this->redirect(
            array('controller' => 'habers', 'action' => 'home')
            );    
        }
    }

    public function yenihaber(){
        $this->HtmlMeta->addElement(array('name' => 'robots', 'content' => 'noindex, nofollow'));
    }
    
    public function addHaber(){
        $this->autoRender = false;
        if($this->request->is('post')){
            $files = array_key_exists('haberRes', $_FILES)?$_FILES['haberRes']:false;
            $filesVideo = array_key_exists('haberVideo', $_FILES)?$_FILES['haberVideo']:false;
            $data = $this->request->data;
            $this->Haber->create();
            $saveData = array('baslik'=>$data['baslik'],'aciklama'=>$data['aciklama'],'tarih'=> date('Y-m-d H:i:s'));
            if($this->Haber->save($saveData)){
                $haberId = $this->Haber->getInsertID();
                
//                //Konut Maps
//                $this->HaberLocation->create();
//                $this->HaberLocation->save(array('haber_id'=>$haberId,'name'=>$data['location'],'latitude'=>$data['latitude'],'longitude'=>$data['longitude']));
                
                // Add Video
                $hataVid = 0;
                if($filesVideo){
                    if($filesVideo['error'] == 0 && $filesVideo['type'] != "video/mp4"){
                            $hataVid++;
                    }
                    else{
                        $fileType = explode('.', $filesVideo['name']);
                        $fileType = $fileType[count($fileType)-1];
                        $pathVideo = WWW_ROOT.'video/haber/'.$haberId.'.'.$fileType;
                        $path = 'video/haber/'.$haberId.'.'.$fileType;
                        if(move_uploaded_file($filesVideo['tmp_name'], $pathVideo)){
                            $this->Haber->updateAll(array('video'=>"'$path'"), array('id'=>$haberId));
                        }else{
                            $this->Haber->updateAll(array('video'=>null), array('id'=>$haberId));
                        }
                    }
                }
                
                //Add Image
                $hataRes = 0;
                if($files){
                    foreach($files['error'] as $key=>$value){
                        if($value == 0 && $files['type'][$key] != "image/jpeg" && $files['type'][$key] != "image/gif" && $files['type'][$key] != "image/png" && $files['type'][$key] != "image/tiff"){
                            $hataRes++;
                        }
                        else{
                            $fileType = explode('.', $files['name'][$key]);
                            $fileType = $fileType[count($fileType)-1];
                            $this->HaberResim->create();
                            $this->HaberResim->save(array('haber_id'=>$haberId));
                            $lastId = $this->HaberResim->getLastInsertID();
                            $pathRes = WWW_ROOT.'img/haber/'.$lastId.'.'.$fileType;
                            $path = 'img/haber/'.$lastId.'.'.$fileType;
                            //image Resize
                            $manipulator = new ImageManipulator($files['tmp_name'][$key]);
                            $newImage = $manipulator->resample(1024, 768);
//                            move_uploaded_file($files['tmp_name'][$key], $pathRes)
                            if($manipulator->save($pathRes)){
                                $this->HaberResim->updateAll(array('path'=>"'$path'"), array('id'=>$lastId));
                            }else{
                                $hataRes++;
                                $this->HaberResim->deleteAll(array('id'=>$lastId));
                            }
                        }
                    }
                }
                if($hataRes>0){
                    $this->Session->setFlash('Yüklemek istediğiniz resimlerden bazıları sisteme yüklenememiştir. Sonra tekrar deneyin.','default', array('class'=>'alert alert-danger'));
                }else{
                    $this->Session->setFlash('Haber kayıt işlemi başarıyla sonuçlandı.','default', array('class'=>'alert alert-success'));
                }
            }else{
                $this->Session->setFlash('Haber kayıt işlemi başarısız. Lütfen tekrar deneyin.','default', array('class'=>'alert alert-danger'));
            }
            return $this->redirect(
            array('controller' => 'habers', 'action' => 'home')
            );
        }
    }
    
    public function upgradehaber(){
        $this->autoRender = false;
        if($this->request->is('post')){
            $files = array_key_exists('haberRes', $_FILES)?$_FILES['haberRes']:false;
            $filesVideo = array_key_exists('haberVideo', $_FILES)?$_FILES['haberVideo']:false;
            $data = $this->request->data;
            $haberId = $data['haberId'];
            $this->Haber->id = $haberId;
            $baslik = $data['baslik'];
            $aciklama = $data['aciklama'];
            $saveData = array(
                'baslik'=>$baslik,
                'aciklama'=>$aciklama
                 );

            if($this->Haber->save($saveData)){
                
                //Konut Maps
//                $this->HaberLocation->deleteAll(array('haber_id'=>$haberId));
//                $this->HaberLocation->create();
//                $this->HaberLocation->save(array('haber_id'=>$haberId,'name'=>$data['location'],'latitude'=>$data['latitude'],'longitude'=>$data['longitude']));
                
                // Add Video
                $hataVid = 0;
                if($filesVideo){
                    if($filesVideo['error'] == 0 && $filesVideo['type'] != "video/mp4"){
                            $hataVid++;
                    }
                    else{
                        $fileType = explode('.', $filesVideo['name']);
                        $fileType = $fileType[count($fileType)-1];
                        $pathVideo = WWW_ROOT.'video/haber/'.$haberId.'.'.$fileType;
                        $path = 'video/haber/'.$haberId.'.'.$fileType;
                        if(move_uploaded_file($filesVideo['tmp_name'], $pathVideo)){
                            $this->Haber->updateAll(array('video'=>"'$path'"), array('id'=>$haberId));
                        }else{
                            $this->Haber->updateAll(array('video'=>null), array('id'=>$haberId));
                        }
                    }
                }
                
                //Add Image
                $hataRes = 0;
                if($files){
                    foreach($files['error'] as $key=>$value){
                        if($value == 0 && $files['type'][$key] != "image/jpeg" && $files['type'][$key] != "image/gif" && $files['type'][$key] != "image/png" && $files['type'][$key] != "image/tiff"){
                            $hataRes++;
                        }
                        else{
                            $fileType = explode('.', $files['name'][$key]);
                            $fileType = $fileType[count($fileType)-1];
                            $this->HaberResim->create();
                            $this->HaberResim->save(array('haber_id'=>$haberId));
                            $lastId = $this->HaberResim->getLastInsertID();
                            $pathRes = WWW_ROOT.'img/haber/'.$lastId.'.'.$fileType;
                            $path = 'img/haber/'.$lastId.'.'.$fileType;
                            //image Resize
                            $manipulator = new ImageManipulator($files['tmp_name'][$key]);
                            $newImage = $manipulator->resample(1024, 768);
//                            move_uploaded_file($files['tmp_name'][$key], $pathRes)
                            if($manipulator->save($pathRes)){
                                $this->HaberResim->updateAll(array('path'=>"'$path'"), array('id'=>$lastId));
                            }else{
                                $hataRes++;
                                $this->HaberResim->deleteAll(array('id'=>$lastId));
                            }
                        }
                    }
                }
                if($hataRes>0){
                    $this->Session->setFlash('Yüklemek istediğiniz resimlerden bazıları sisteme yüklenememiştir. Sonra tekrar deneyin.','default', array('class'=>'alert alert-danger'));
                }else{
                    $this->Session->setFlash('Haber düzenleme işlemi başarıyla sonuçlandı.','default', array('class'=>'alert alert-success'));
                    return $this->redirect(
                    array('controller' => 'habers', 'action' => 'duzenle/ilanNo:'.$haberId)
                    );
                }
            }else{
                $this->Session->setFlash('Haber düzenleme işlemi başarısız. Lütfen tekrar deneyin.','default', array('class'=>'alert alert-danger'));
            }
            return $this->redirect(
            array('controller' => 'habers', 'action' => 'yenihaber')
            );
        }else{
            $this->Session->setFlash('Haber düzenleme işlemi başarısız. Lütfen tekrar deneyin.','default', array('class'=>'alert alert-danger'));
            return $this->redirect(
            array('controller' => 'habers', 'action' => 'duzenle/ilanNo:'.$haberId)
            );
        }
    }
    
    public function deleteResim(){
        $this->autoRender = false;
        if($this->request->is('post')){
            $ResId = $this->request->data('resId');
            $resim = $this->HaberResim->find('first',array('conditions'=>array('id'=>$ResId)));
            $fileRes = new File($resim['HaberResim']['path']);
            $fileRes->delete();
            if($this->HaberResim->deleteAll(array('id'=>$ResId))){
                echo json_encode(true);
            }else{
                echo json_encode(false);
            }
        }else{
            echo json_encode(false);
        }
    }
    
    public function ajaxDeleteHaber(){
        $this->autoRender = false;
        if($this->request->is('post')){
            $haberId = $this->request->data('haberId');
            $haberRes = $this->HaberResim->findAllByHaberId($haberId);
            foreach($haberRes as $row){
                $fileRes = new File($row['HaberResim']['path']);
                $fileRes->delete();
            }
            //Delete Video
            $video = $this->Haber->find('first',array('conditions'=>array('id'=>$haberId)));
            $fileVideo = new File($video['Haber']['video']);
            $fileVideo->delete();
            
            $this->HaberResim->deleteAll(array('haber_id'=>$haberId));
            $this->Haber->deleteAll(array('id'=>$haberId));
            echo json_encode(true);
        }
    }
    
    public function ajaxDeleteVideo(){
        $this->autoRender = false;
        if($this->request->is('post')){
            $haberId = $this->request->data('haberId');
            $video = $this->Haber->find('first',array('conditions'=>array('id'=>$haberId)));
            $fileVideo = new File($video['Haber']['video']);
            $fileVideo->delete();
            if($this->Haber->updateAll(array('video'=>null),array('id'=>$haberId))){
                echo json_encode(true);
            }else{
                echo json_encode(false);
            }
        }else{
            echo json_encode(false);
        }
    }
}