<?php
/**
 * @property Danisman $Danisman
 * @property DanismanIletisim $DanismanIletisim
 * @property IlanIletisimType $IlanIletisimType
 */
class DanismansController extends AppController{
    var $uses = array('Danisman', 'DanismanIletisim', 'IlanIletisimType', 'Konut', 'KonutResim', 'Isyeri', 'IsyeriResim', 'Arsa', 'ArsaResim');
    public $components = array('Paginator','HtmlMeta');

    public function index(){
        $this->paginate = array(
            'limit'=>10,
            'order'=>array('Danisman.ad'=>'asc', 'Danisman.soyad'=>'asc')
        );

        $danismans = $this->paginate('Danisman');
        $this->set('danismans',$danismans);

        if(!$danismans){
            $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button> Aradığınız kriterlere uygun sonuç yoktur.','default', array('class'=>'alert alert-danger'));
            return $this->redirect(Router::url('/',true));
        }

        $this->HtmlMeta->addElement(array('name' => 'robots', 'content' => 'index, follow'));
        $this->HtmlMeta->addElement(array('name' => 'title', 'content' => 'Danışmanlar - Maden Gayrimenkul A.Ş.'));
        $this->HtmlMeta->addElement(array('name' => 'description', 'content' => 'Satılık, kiralık, konut, işyeri, arsa yatırım ve yönetimi - Mutlu Kazanç Kapısı...'));
        $this->HtmlMeta->addElement(array('property' => 'og:title', 'content' => 'Haber - Maden Gayrimenkul A.Ş.' ));
        $this->HtmlMeta->addElement(array('property' => 'og:description', 'content' => 'Satılık, kiralık, konut, işyeri, arsa yatırım ve yönetimi - Mutlu Kazanç Kapısı...'));
        $this->HtmlMeta->addElement(array('property' => 'og:keywords', 'content' => 'Eskişehir Yolu satılık yatırım arsaları, Temelli, Malıköy, Ihlamurkent satılık villa, Alcı, Türkobası, Anayurt, Bacı, Başkent OSB satılık arsa, kiralık fabrika, Anadolu OSB satılık fabrika arsa, ASO 2 OSB, Konutkent, Çayyolu, Westgate Residence, Kuleevo, Elmar Towers, Elya Tower, Azel Kule' ));
        $this->HtmlMeta->addElement(array('property' => 'og:url', 'content' => Router::url('/', true).'danismans/index'));
        $this->HtmlMeta->addElement(array('property'=>'og:image', 'content'=>  Router::url('/',true).'img/shareicon/mhab.png'));
    }

    public function ilanlar(){
        $named = $this->request->named;
        $this->passedArgs = $named;
        if(array_key_exists('id',$named) && $this->Danisman->findById($named['id'])){
            $danisman = $this->Danisman->find('first',array('conditions'=>array('Danisman.id'=>$named['id']), 'contain'=>array('DanismanIletisim'=>array('IlanIletisimType'))));
            $this->set('danisman',$danisman);

            //ilanlar
            $limit = 5;
            $pageCount = array_key_exists('page', $named)?$named['page']:1;

            // Konut Sorgusu
            $KonSql = array(
                'fields'=>array('Konut.*'),
                'contain'=>array('KonutDanisman')
            );
            $konutCount = $this->Konut->find('count',$KonSql);

            if($konutCount>(($pageCount-1)*$limit)){
                $this->paginate = array(
                    'fields'=>array('*'),
                    'contain'=>array('KonutDanisman'=>array('conditions'=>array('danisman_id'=>$named['id']))),
                    'limit'=>$limit,
                    'order'=>array('tarih'),
                    'joins'=>array(
                        array('table'=>'em_konut_yer', 'alias'=>'KonutYer', 'type'=>'INNER', 'conditions'=>array('Konut.id=KonutYer.konut_id')),
                        array('table'=>'em_sehir', 'alias'=>'Sehir', 'type'=>'INNER', 'conditions'=>array('KonutYer.sehir_id=Sehir.id')),
                        array('table'=>'em_ilce', 'alias'=>'Ilce', 'type'=>'LEFT', 'conditions'=>array('KonutYer.ilce_id=Ilce.id')),
                        array('table'=>'em_semt', 'alias'=>'Semt', 'type'=>'LEFT', 'conditions'=>array('KonutYer.semt_id=Semt.id')),
                        array('table'=>'em_mahalle', 'alias'=>'Mahalle', 'type'=>'LEFT', 'conditions'=>array('KonutYer.mahalle_id=Mahalle.id')),
                        array('table'=>'em_konut_location', 'alias'=>'KonutLocation', 'type'=>'LEFT', 'conditions'=>array('Konut.id=KonutLocation.konut_id'))
                    )
                );
                $konuts = $this->paginate('Konut');
                $this->set('konuts',$konuts);
                $konutRes = array();
                foreach ($konuts as $row) {
                    $konutRes[$row['Konut']['id']] = $this->KonutResim->find('first',array('order'=>array('id'=>'ASC'),'fields'=>array('paththumb','id'),'conditions'=>array('konut_id'=>$row['Konut']['id'])));
                    //$konutRes[$row['Konut']['id']] = $this->KonutResim->findAllByKonutId($row['Konut']['id']);
                }
                $this->set('konutRes',$konutRes);
            }else{
                $this->set('konuts',false);
            }
            // Konut Sorgusu Son

            // Isyeri Sorgusu
            $IsSql = array(
                'fields'=>array('*'),
                'contain'=>array('IsyeriDanisman'=>array('conditions'=>array('danisman_id'=>$named['id']))),
                'joins'=>array(
                    array('table'=>'em_isyeri_yer', 'alias'=>'IsyeriYer', 'type'=>'INNER', 'conditions'=>array('Isyeri.id=IsyeriYer.isyeri_id')),
                    array('table'=>'em_sehir', 'alias'=>'Sehir', 'type'=>'INNER', 'conditions'=>array('IsyeriYer.sehir_id=Sehir.id')),
                    array('table'=>'em_ilce', 'alias'=>'Ilce', 'type'=>'LEFT', 'conditions'=>array('IsyeriYer.ilce_id=Ilce.id')),
                    array('table'=>'em_semt', 'alias'=>'Semt', 'type'=>'LEFT', 'conditions'=>array('IsyeriYer.semt_id=Semt.id')),
                    array('table'=>'em_mahalle', 'alias'=>'Mahalle', 'type'=>'LEFT', 'conditions'=>array('IsyeriYer.mahalle_id=Mahalle.id')),
                    array('table'=>'em_isyeri_location', 'alias'=>'IsyeriLocation', 'type'=>'LEFT', 'conditions'=>array('Isyeri.id=IsyeriLocation.isyeri_id'))
                )
            );
            $isyeriCount = $this->Isyeri->find('count',$IsSql);

            if($isyeriCount>(($pageCount-1)*$limit)){
                $this->paginate = array(
                    'fields'=>array('*'),
                    'contain'=>array('IsyeriDanisman'=>array('conditions'=>array('danisman_id'=>$named['id']))),
                    'conditions'=>array('Isyeri.id IN (SELECT isyeri_id FROM em_isyeri_danisman where danisman_id = '.$named['id'].')'),
                    'limit'=>$limit,
                    'order'=>array('tarih'),
                    'joins'=>array(
                        array('table'=>'em_isyeri_yer', 'alias'=>'IsyeriYer', 'type'=>'INNER', 'conditions'=>array('Isyeri.id=IsyeriYer.isyeri_id')),
                        array('table'=>'em_sehir', 'alias'=>'Sehir', 'type'=>'INNER', 'conditions'=>array('IsyeriYer.sehir_id=Sehir.id')),
                        array('table'=>'em_ilce', 'alias'=>'Ilce', 'type'=>'LEFT', 'conditions'=>array('IsyeriYer.ilce_id=Ilce.id')),
                        array('table'=>'em_semt', 'alias'=>'Semt', 'type'=>'LEFT', 'conditions'=>array('IsyeriYer.semt_id=Semt.id')),
                        array('table'=>'em_mahalle', 'alias'=>'Mahalle', 'type'=>'LEFT', 'conditions'=>array('IsyeriYer.mahalle_id=Mahalle.id')),
                        array('table'=>'em_isyeri_location', 'alias'=>'IsyeriLocation', 'type'=>'LEFT', 'conditions'=>array('Isyeri.id=IsyeriLocation.isyeri_id'))
                    )
                );
                $isyeris = $this->paginate('Isyeri');
                $this->set('isyeris',$isyeris);
                $isyeriRes = array();
                foreach ($isyeris as $row) {
                    $isyeriRes[$row['Isyeri']['id']] = $this->IsyeriResim->find('first',array('order'=>array('id'=>'ASC'),'fields'=>array('paththumb','id'),'conditions'=>array('isyeri_id'=>$row['Isyeri']['id'])));
                    //$isyeriRes[$row['Isyeri']['id']] = $this->IsyeriResim->findAllByIsyeriId($row['Isyeri']['id']);
                }
                $this->set('isyeriRes',$isyeriRes);
            }else{
                $this->set('isyeris',false);
            }
            // İsyeri Sorgusu Son

            //Arsa Sorgusu
            $ArSql = array(
                'fields'=>array('*'),
                'contain'=>array('ArsaDanisman'=>array('conditions'=>array('danisman_id'=>$named['id']))),
                'joins'=>array(
                    array('table'=>'em_arsa_yer', 'alias'=>'ArsaYer', 'type'=>'INNER', 'conditions'=>array('Arsa.id=ArsaYer.arsa_id')),
                    array('table'=>'em_sehir', 'alias'=>'Sehir', 'type'=>'INNER', 'conditions'=>array('ArsaYer.sehir_id=Sehir.id')),
                    array('table'=>'em_ilce', 'alias'=>'Ilce', 'type'=>'LEFT', 'conditions'=>array('ArsaYer.ilce_id=Ilce.id')),
                    array('table'=>'em_semt', 'alias'=>'Semt', 'type'=>'LEFT', 'conditions'=>array('ArsaYer.semt_id=Semt.id')),
                    array('table'=>'em_mahalle', 'alias'=>'Mahalle', 'type'=>'LEFT', 'conditions'=>array('ArsaYer.mahalle_id=Mahalle.id')),
                    array('table'=>'em_arsa_location', 'alias'=>'ArsaLocation', 'type'=>'LEFT', 'conditions'=>array('Arsa.id=ArsaLocation.arsa_id'))
                )
            );
            $arsaCount = $this->Arsa->find('count',$ArSql);

            if($arsaCount>(($pageCount-1)*$limit)){
                $this->paginate = array(
                    'fields'=>array('*'),
                    'contain'=>array('ArsaDanisman'=>array('conditions'=>array('danisman_id'=>$named['id']))),
                    'limit'=>$limit,
                    'order'=>array('tarih'),
                    'joins'=>array(
                        array('table'=>'em_arsa_yer', 'alias'=>'ArsaYer', 'type'=>'INNER', 'conditions'=>array('Arsa.id=ArsaYer.arsa_id')),
                        array('table'=>'em_sehir', 'alias'=>'Sehir', 'type'=>'INNER', 'conditions'=>array('ArsaYer.sehir_id=Sehir.id')),
                        array('table'=>'em_ilce', 'alias'=>'Ilce', 'type'=>'LEFT', 'conditions'=>array('ArsaYer.ilce_id=Ilce.id')),
                        array('table'=>'em_semt', 'alias'=>'Semt', 'type'=>'LEFT', 'conditions'=>array('ArsaYer.semt_id=Semt.id')),
                        array('table'=>'em_mahalle', 'alias'=>'Mahalle', 'type'=>'LEFT', 'conditions'=>array('ArsaYer.mahalle_id=Mahalle.id')),
                        array('table'=>'em_arsa_location', 'alias'=>'ArsaLocation', 'type'=>'LEFT', 'conditions'=>array('Arsa.id=ArsaLocation.arsa_id'))
                    )
                );
                $arsas = $this->paginate('Arsa');
                $this->set('arsas',$arsas);
                $arsaRes = array();
                foreach ($arsas as $row) {
                    $arsaRes[$row['Arsa']['id']] = $this->ArsaResim->find('first',array('order'=>array('id'=>'ASC'),'fields'=>array('paththumb','id'),'conditions'=>array('arsa_id'=>$row['Arsa']['id'])));
                    //$arsaRes[$row['Arsa']['id']] = $this->ArsaResim->findAllByArsaId($row['Arsa']['id']);
                }
                $this->set('arsaRes',$arsaRes);
            }else {
                $this->set('arsas',false);
            }
            // Arsa Sorgusu Son

            if(!$konuts && !$isyeris && !$arsas){
                $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button>Aradığınız kriterlere uygun sonuç yoktur.','default', array('class'=>'alert alert-warning alert-dismissable'));
                return $this->redirect(
                    array('controller' => 'users', 'action' => 'index')
                );
            }

        }else{
            $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button> Aradığınız kriterlere uygun sonuç yoktur.','default', array('class'=>'alert alert-danger'));
            return $this->redirect(array('controller'=>'danismans','action'=>'index'));
        }
    }
}