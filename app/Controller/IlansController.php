<?php
/**
 * @property Sehir $Sehir
 * @property Ilce $Ilce
 * @property Mahalle $Mahalle
 * @property Semt $Semt
 * @property User $User
 * @property Danisman $Danisman
 * @property DanismanIletisim $DanismanIletisim
 * @property IlanIletisimType $IlanIletisimType
 * @property IlanDanisman $IlanDanisman
 * @property IsyeriDanisman $IsyeriDanisman
 * @property ArsaDanisman $ArsaDanisman
 * @property Ilan $Ilan
 * @property IlanBilgi $IlanBilgi
 * @property ArsaBilgi $ArsaBilgi
 * @property IsyeriBilgi $IsyeriBilgi
 */
class IlansController extends AppController{
    var $uses = array('Ilan','IlanResim', 'IlanBilgi', 'ArsaBilgi', 'IsyeriBilgi', 'IlanDanisman', 'Tip','Sehir','Ilce','Mahalle','Semt', 'ArsaIletisim','IlanIletisim','IlanIletisimType', 'User', 'Danisman', 'DanismanIletisim','IlanDanisman','IsyeriDanisman','ArsaDanisman', 'Haber', 'Proje', 'Ozellik', 'IlanOzellik');

    public function index(){
        if($this->request->is('post')){
            $data = $this->request->data;
            $this->passedArgs = $data;
        }else{
            $data = $this->passedArgs;
        }
        $limit = 10;

        $conditions = $order = $sehirCon = $ilceCon = $semtCon = $mahalleCon = array();
        $sval = '';
        if(array_key_exists('sval', $data)) {
            $sval = $data['sval'];
            $this->set('sval',$sval);
            $conditions['OR'] = array("Ilan.baslik LIKE '%$sval%'",
                "Ilan.aciklama LIKE '%$sval%'",
                "Ilan.ilan_no LIKE '%$sval%'",
                "Ilan.location_name LIKE '%$sval%'",
                "Sehir.sehir_adi LIKE '%$sval%'",
                "Ilce.ilce_adi LIKE '%$sval%'",
                "Semt.semt_adi LIKE '%$sval%'",
                "Mahalle.mahalle_adi LIKE '%$sval%'"
                );
        }

        if(array_key_exists('tarih', $data)){
            if($data['tarih']=='desc'){
                $order['Ilan.tarih'] = 'DESC';
            }else if($data['tarih']=='asc'){
                $order['Ilan.tarih'] = 'ASC';
            }
        }else if(array_key_exists('fiyat', $data)){
            if($data['fiyat']=='desc'){
                $order['Ilan.fiyat'] = 'DESC';
            }else if($data['fiyat']=='asc'){
                $order['Ilan.fiyat'] = 'ASC';
            }
        }else{
            $order['Ilan.update_tarihi'] = 'DESC';
        }

        // İlan Tipi
        if(array_key_exists('tip',$data) && $data['tip'] != 0){
            $conditions['Ilan.ilan_tipi'] = $data['tip'];
        }
        // İlan Satılık-Kiralik
        if(array_key_exists('satkir',$data) && $data['satkir'] != 0){
            $conditions['Ilan.sat_kir'] = $data['satkir'];
        }
        // İlan Şehir
        if(array_key_exists('il',$data) && $data['il'] != 0){
            $conditions['Ilan.sehir_id'] = $data['il'];
        }

        //İlan Fiyatı
        if(array_key_exists('fiy1',$data) && $data['fiy1'] != 0){
            $conditions[] = "Ilan.fiyat >= ".$data['fiy1'];
        }
        if(array_key_exists('fiy2',$data) && $data['fiy2'] != 0){
            $conditions[] = "Ilan.fiyat <= ".$data['fiy2'];
        }

        //İlan Metre Kare
        if(array_key_exists('m1',$data) && $data['m1'] != 0){
            $conditions[] = "Ilan.m_kare >= ".$data['m1'];
        }
        if(array_key_exists('m2',$data) && $data['m2'] != 0){
            $conditions[] = "Ilan.m_kare <= ".$data['m2'];
        }

        $ilce = array_key_exists('ilce', $data)?$data['ilce']:0;
        $semt = array_key_exists('semt', $data)?$data['semt']:0;
        $mahalle = array_key_exists('mahalle', $data)?$data['mahalle']:0;
        // İlan İlçe
        if($ilce != 0){
            $conditions['Ilan.ilce_id'] = $ilce;
        }
        // İlan Semt
        if($semt != 0){
            $conditions['Ilan.semt_id'] = $semt;
        }
        // İlan Mahalle
        if($mahalle != 0){
            $conditions['Ilan.mahalle_id'] = $mahalle;
        }

        $this->paginate = array(
            'fields'=>array('*'),
            'contain'=>array('KonutBilgi','IsyeriBilgi','ArsaBilgi','IlanResim'=>array('order'=>array('sira'=>'ASC'),'limit'=>1),'Sehir','Ilce','Semt','Mahalle'),
            'conditions'=>$conditions,
            'limit'=>$limit,
            'order'=>$order
        );
        $ilanlar = $this->paginate('Ilan');
        $this->set('ilanlar',$ilanlar);
        $this->set('sTip',(array_key_exists('tip',$data)?$data['tip']:0));
        $this->set('sSatKir',(array_key_exists('satkir',$data)?$data['satkir']:0));
        $this->set('sval',$sval);
    }

    public function ilan(){
        $named = $this->request->params['named'];
        if(array_key_exists('ilan_id',$named)){
            $ilan = $this->Ilan->find('first',array(
                'conditions'=>array('Ilan.id'=>$named['ilan_id']),
                'contain'=>array('KonutBilgi','IsyeriBilgi','ArsaBilgi','IlanResim','IlanDanisman'=>array('Danisman'), 'Sehir', 'Ilce', 'Semt', 'Mahalle', 'IlanOzellik'=>array('Ozellik'))
            ));

            if(empty($ilan)){
                $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button>Aradığınız ilan bulunmadı ya da silinmiş olabilir.','default', array('class'=>'alert alert-danger alert-dismissable'));
                $this->redirect(array('controller'=>'admins','action'=>'index'));
            }
            $this->set('ilan',$ilan);

        }else{
            $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button>Aradığınız ilan bulunmadı ya da silinmiş olabilir.','default', array('class'=>'alert alert-danger alert-dismissable'));
            $this->redirect(array('controller'=>'admins','action'=>'index'));
        }
    }

    public function ilanSay(){
        $this->autoRender = false;
        $kCount = $this->Ilan->find('count',array('conditions'=>array('ilan_tipi'=>1)));
        $kSatCount = $this->Ilan->find('count',array('conditions'=>array('ilan_tipi'=>1,'sat_kir'=>1)));
        $kKirCount = $this->Ilan->find('count',array('conditions'=>array('ilan_tipi'=>1,'sat_kir'=>2)));
        $iCount = $this->Ilan->find('count',array('conditions'=>array('ilan_tipi'=>2)));
        $iSatCount = $this->Ilan->find('count',array('conditions'=>array('ilan_tipi'=>2,'sat_kir'=>1)));
        $iKirCount = $this->Ilan->find('count',array('conditions'=>array('ilan_tipi'=>2,'sat_kir'=>2)));
        $aCount = $this->Ilan->find('count',array('conditions'=>array('ilan_tipi'=>3)));
        $aSatCount = $this->Ilan->find('count',array('conditions'=>array('ilan_tipi'=>3,'sat_kir'=>1)));
        $aKirCount = $this->Ilan->find('count',array('conditions'=>array('ilan_tipi'=>3,'sat_kir'=>2)));
        $projeCount = $this->Proje->find('count');
        $haberCount = $this->Haber->find('count');
        return array(
            'kCount'=>$kCount,
            'kSatCount'=>$kSatCount,
            'kKirCount'=>$kKirCount,
            'iCount'=>$iCount,
            'iSatCount'=>$iSatCount,
            'iKirCount'=>$iKirCount,
            'aCount'=>$aCount,
            'aSatCount'=>$aSatCount,
            'aKirCount'=>$aKirCount,
            'projeCount'=>$projeCount,
            'haberCount'=>$haberCount
        );
    }
}