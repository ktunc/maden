<?php
/*
 * @uses          AppController
 * */
App::uses('ImageManipulator','Vendor');
class TekniksController extends AppController{
    var $uses = array('TeknikAnaliz','TeknikAnalizResim','TeknikAnalizYorum');
    public $components = array('Paginator','HtmlMeta');

    public function home(){
        $this->paginate = array(
            'limit'=>10,
            'order'=>array('TeknikAnaliz.id'=>'desc')
        );

        $tekniks = $this->paginate('TeknikAnaliz');
        $this->set('tekniks',$tekniks);

        if(!$tekniks){
            $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button>Aradığınız kriterlere uygun sonuç yoktur.','default', array('class'=>'alert alert-danger alert-dismissable'));
            return $this->redirect(Router::url('/',true));
        }

        $teknikRes = array();
        foreach ($tekniks as $row) {
            $teknikRes[$row['TeknikAnaliz']['id']] = $this->TeknikAnalizResim->findAllByTeknikAnalizId($row['TeknikAnaliz']['id']);
        }
        $this->set('teknikRes',$teknikRes);

        $this->HtmlMeta->addElement(array('name' => 'robots', 'content' => 'index, follow'));
        $this->HtmlMeta->addElement(array('name' => 'title', 'content' => 'Teknik Analiz & Yorumlar - Maden Gayrimenkul A.Ş.'));
        $this->HtmlMeta->addElement(array('name' => 'description', 'content' => 'Satılık, kiralık, konut, işyeri, arsa yatırım ve yönetimi - Mutlu Kazanç Kapısı...'));
        $this->HtmlMeta->addElement(array('property' => 'og:title', 'content' => 'Teknik Analiz & Yorumlar - Maden Gayrimenkul A.Ş.' ));
        $this->HtmlMeta->addElement(array('property' => 'og:description', 'content' => 'Satılık, kiralık, konut, işyeri, arsa yatırım ve yönetimi - Mutlu Kazanç Kapısı...'));
        $this->HtmlMeta->addElement(array('property' => 'og:keywords', 'content' => 'Eskişehir Yolu satılık yatırım arsaları, Temelli, Malıköy, Ihlamurkent satılık villa, Alcı, Türkobası, Anayurt, Bacı, Başkent OSB satılık arsa, kiralık fabrika, Anadolu OSB satılık fabrika arsa, ASO 2 OSB, Konutkent, Çayyolu, Westgate Residence, Kuleevo, Elmar Towers, Elya Tower, Azel Kule' ));
        $this->HtmlMeta->addElement(array('property' => 'og:url', 'content' => Router::url('/', true).'tekniks/home'));
        $this->HtmlMeta->addElement(array('property'=>'og:image', 'content'=>  Router::url('/',true).'img/shareicon/mteknik.png'));
    }

    public function teknikanaliz(){
        if($this->request->params['named']['ilanNo']){
            $ilanNo = $this->request->params['named']['ilanNo'];

            /*if(!$this->Session->check('TeknikIlan.'.$ilanNo) && rand(1,5) == 3){
                $this->Session->write('TeknikIlan.'.$ilanNo,1);
                $this->redirect(SHORTE_URL.'teknikanaliz/ilanNo:'.$ilanNo);
            }*/

            $tekniks = $this->TeknikAnaliz->find('first',
                array(
                    'fields'=>array('*'),
                    'conditions'=>array("TeknikAnaliz.id"=>$ilanNo)
                ));

            if(!$tekniks){
                $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button>Aramış olduğunuz ilan kaldırılmıştır.','default', array('class'=>'alert alert-danger alert-dismissable'));
                return $this->redirect(
                    array('controller' => 'tekniks', 'action' => 'home')
                );
            }
            $this->set('tekniks',$tekniks);

            $teknikRes = $this->TeknikAnalizResim->findAllByTeknikAnalizId($tekniks['TeknikAnaliz']['id']);
            $this->set('teknikRes',$teknikRes);

            $teknikYorum = $this->TeknikAnalizYorum->find('all',array(
                'conditions'=>array('teknik_analiz_id'=>$tekniks['TeknikAnaliz']['id']),
                'order'=>array('tarih'=>'DESC','id'=>'DESC')
            ));
            $this->set('teknikYorum',$teknikYorum);

            $this->HtmlMeta->addElement(array('name' => 'robots', 'content' => 'index, follow'));
            $this->HtmlMeta->addElement(array('name' => 'title', 'content' => $tekniks['TeknikAnaliz']['baslik']));
            $this->HtmlMeta->addElement(array('name' => 'description', 'content' => strip_tags($tekniks['TeknikAnaliz']['aciklama'])));
            $this->HtmlMeta->addElement(array('property' => 'og:title', 'content' => $tekniks['TeknikAnaliz']['baslik']));
            $this->HtmlMeta->addElement(array('property' => 'og:description', 'content' => strip_tags($tekniks['TeknikAnaliz']['aciklama'])));
            $this->HtmlMeta->addElement(array('property' => 'og:keywords', 'content' => $tekniks['TeknikAnaliz']['baslik'] ));
            $this->HtmlMeta->addElement(array('property' => 'og:url', 'content' => Router::url('/', true).'teknikanaliz/ilanNo:'.$ilanNo));
            if($teknikRes){
                $this->HtmlMeta->addElement(array('property'=>'og:image', 'content'=>  Router::url('/',true).$teknikRes[0]['TeknikAnalizResim']['path']));
            }else{
                $this->HtmlMeta->addElement(array('property'=>'og:image', 'content'=>  Router::url('/',true).'img/logo-xxs.png'));
            }

            $video_code = false;
            if (preg_match("/youtube.com/", $tekniks['TeknikAnaliz']['aciklama']) || preg_match("/youtu.be/", $tekniks['TeknikAnaliz']['aciklama'])){
                if (preg_match('%(?:youtube\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $tekniks['TeknikAnaliz']['aciklama'], $match))
                {
                    $video_code = $match[1];
                }
                $source = 'http://www.youtube.com/e/'.$video_code;
                $picture = 'http://img.youtube.com/vi/'.$video_code.'/0.jpg';
            }
            else if (preg_match("/vimeo.com/", $tekniks['TeknikAnaliz']['aciklama']))
            {
                if (preg_match('/vimeo\.com\/(clip\:)?(\d+).*$/', $tekniks['TeknikAnaliz']['aciklama'], $match))
                {
                    $video_code = $match[2];
                }
                /* Get Vimeo thumbnail */
                $hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/$video_code.php"));
                $picture = $hash[0]['thumbnail_medium'];
                $source = 'https://secure.vimeo.com/moogaloop.swf?clip_id='.$video_code.'&autoplay=1';
            }

            if($video_code){
                $this->HtmlMeta->addElement(array('property' => 'og:type', 'content' => 'video' ));
                $this->HtmlMeta->addElement(array('property' => 'og:video', 'content' => $source ));
                /*$this->HtmlMeta->addElement(array('property' => 'og:video:width', 'content' => '374' ));
                $this->HtmlMeta->addElement(array('property' => 'og:video:height', 'content' => '202' ));*/
                $this->HtmlMeta->addElement(array('property' => 'og:video:type', 'content' => 'application/x-shockwave-flash' ));
            }
        }else{
            return $this->redirect(
                array('controller' => 'tekniks', 'action' => 'home')
            );
        }
    }

    public function duzenle(){
        if($this->request->params['named']['ilanNo']){
            $ilanNo = $this->request->params['named']['ilanNo'];

            $tekniks = $this->TeknikAnaliz->find('first',
                array(
                    'fields'=>array('*'),
                    'conditions'=>array("TeknikAnaliz.id"=>$ilanNo)
                ));

            if(!$tekniks){
                $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button>Aramış olduğunuz ilan kaldırılmıştır.','default', array('class'=>'alert alert-danger alert-dismissable'));
                return $this->redirect(
                    array('controller' => 'tekniks', 'action' => 'home')
                );
            }
            $this->set('tekniks',$tekniks);

            $teknikRes = $this->TeknikAnalizResim->findAllByTeknikAnalizId($tekniks['TeknikAnaliz']['id']);
            $this->set('teknikRes',$teknikRes);
            $this->HtmlMeta->addElement(array('name' => 'robots', 'content' => 'noindex, nofollow'));
        }else{
            return $this->redirect(
                array('controller' => 'tekniks', 'action' => 'home')
            );
        }
    }

    public function yeniteknik(){
        $this->HtmlMeta->addElement(array('name' => 'robots', 'content' => 'noindex, nofollow'));
    }

    public function addteknikanaliz(){
        $this->autoRender = false;
        if($this->request->is('post')){
            $files = array_key_exists('teknikRes', $_FILES)?$_FILES['teknikRes']:false;
            $filesVideo = array_key_exists('teknikVideo', $_FILES)?$_FILES['teknikVideo']:false;
            $data = $this->request->data;
            $this->TeknikAnaliz->create();
            $saveData = array('baslik'=>$data['baslik'],'aciklama'=>$data['aciklama'],'tarih'=> date('Y-m-d H:i:s'));
            if($this->TeknikAnaliz->save($saveData)){
                $teknikId = $this->TeknikAnaliz->getInsertID();


                // Add Video
                $hataVid = 0;
                if($filesVideo){
                    if($filesVideo['error'] == 0 && $filesVideo['type'] != "video/mp4"){
                        $hataVid++;
                    }
                    else{
                        $fileType = explode('.', $filesVideo['name']);
                        $fileType = $fileType[count($fileType)-1];
                        $pathVideo = WWW_ROOT.'video/teknik/'.$teknikId.'.'.$fileType;
                        $path = 'video/teknik/'.$teknikId.'.'.$fileType;
                        if(move_uploaded_file($filesVideo['tmp_name'], $pathVideo)){
                            $this->TeknikAnaliz->updateAll(array('video'=>"'$path'"), array('id'=>$teknikId));
                        }else{
                            $this->TeknikAnaliz->updateAll(array('video'=>null), array('id'=>$teknikId));
                        }
                    }
                }

                //Add Image
                $hataRes = 0;
                if($files){
                    foreach($files['error'] as $key=>$value){
                        if($value == 0 && $files['type'][$key] != "image/jpeg" && $files['type'][$key] != "image/gif" && $files['type'][$key] != "image/png" && $files['type'][$key] != "image/tiff"){
                            $hataRes++;
                        }
                        else{
                            $fileType = explode('.', $files['name'][$key]);
                            $fileType = $fileType[count($fileType)-1];
                            $path = 'img/teknik/'.$teknikId.'/'.time().'_'.rand(1,1000).'.'.$fileType;
                            $pathRes = WWW_ROOT.$path;

                            //image Resize
                            $manipulator = new ImageManipulator($files['tmp_name'][$key]);
                            $newImage = $manipulator->resample(800, 800);
//                            move_uploaded_file($files['tmp_name'][$key], $pathRes)
                            if($manipulator->save($pathRes)){
                                $this->TeknikAnalizResim->create();
                                $this->TeknikAnalizResim->save(array('teknik_analiz_id'=>$teknikId,'path'=>$path));
                            }else{
                                $hataRes++;
                            }
                        }
                    }
                }
                if($hataRes>0){
                    $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button>Yüklemek istediğiniz resimlerden bazıları sisteme yüklenememiştir. Sonra tekrar deneyin.','default', array('class'=>'alert alert-danger alert-dismissable'));
                }else{
                    $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button>TeknikAnaliz kayıt işlemi başarıyla sonuçlandı.','default', array('class'=>'alert alert-success alert-dismissable'));
                }
            }else{
                $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button>TeknikAnaliz kayıt işlemi başarısız. Lütfen tekrar deneyin.','default', array('class'=>'alert alert-danger alert-dismissable'));
            }
            return $this->redirect(
                array('controller' => 'tekniks', 'action' => 'home')
            );
        }
    }

    public function upgradeteknik(){
        $this->autoRender = false;
        if($this->request->is('post')){
            $files = array_key_exists('teknikRes', $_FILES)?$_FILES['teknikRes']:false;
            $filesVideo = array_key_exists('teknikVideo', $_FILES)?$_FILES['teknikVideo']:false;
            $data = $this->request->data;
            $teknikId = $data['teknikId'];
            $this->TeknikAnaliz->id = $teknikId;
            $baslik = $data['baslik'];
            $aciklama = $data['aciklama'];
            $saveData = array(
                'baslik'=>$baslik,
                'aciklama'=>$aciklama
            );

            if($this->TeknikAnaliz->save($saveData)){
                
                // Add Video
                $hataVid = 0;
                if($filesVideo){
                    if($filesVideo['error'] == 0 && $filesVideo['type'] != "video/mp4"){
                        $hataVid++;
                    }
                    else{
                        $fileType = explode('.', $filesVideo['name']);
                        $fileType = $fileType[count($fileType)-1];
                        $pathVideo = WWW_ROOT.'video/teknik/'.$teknikId.'.'.$fileType;
                        $path = 'video/teknik/'.$teknikId.'.'.$fileType;
                        if(move_uploaded_file($filesVideo['tmp_name'], $pathVideo)){
                            $this->TeknikAnaliz->updateAll(array('video'=>"'$path'"), array('id'=>$teknikId));
                        }else{
                            $this->TeknikAnaliz->updateAll(array('video'=>null), array('id'=>$teknikId));
                        }
                    }
                }

                //Add Image
                $hataRes = 0;
                if($files){
                    foreach($files['error'] as $key=>$value){
                        if($value == 0 && $files['type'][$key] != "image/jpeg" && $files['type'][$key] != "image/gif" && $files['type'][$key] != "image/png" && $files['type'][$key] != "image/tiff"){
                            $hataRes++;
                        }
                        else{
                            $fileType = explode('.', $files['name'][$key]);
                            $fileType = $fileType[count($fileType)-1];
                            $path = 'img/teknik/'.$teknikId.'/'.time().'_'.rand(1,1000).'.'.$fileType;
                            $pathRes = WWW_ROOT.$path;

                            //image Resize
                            $manipulator = new ImageManipulator($files['tmp_name'][$key]);
                            $newImage = $manipulator->resample(800, 800);
//                            move_uploaded_file($files['tmp_name'][$key], $pathRes)
                            if($manipulator->save($pathRes)){
                                $this->TeknikAnalizResim->create();
                                $this->TeknikAnalizResim->save(array('teknik_analiz_id'=>$teknikId,'path'=>$path));
                            }else{
                                $hataRes++;
                            }
                        }
                    }
                }
                if($hataRes>0){
                    $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button>Yüklemek istediğiniz resimlerden bazıları sisteme yüklenememiştir. Sonra tekrar deneyin.','default', array('class'=>'alert alert-danger alert-dismissable'));
                }else{
                    $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button>TeknikAnaliz düzenleme işlemi başarıyla sonuçlandı.','default', array('class'=>'alert alert-succes alert-dismissables'));
                    return $this->redirect(
                        array('controller' => 'tekniks', 'action' => 'duzenle/ilanNo:'.$teknikId)
                    );
                }
            }else{
                $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button>TeknikAnaliz düzenleme işlemi başarısız. Lütfen tekrar deneyin.','default', array('class'=>'alert alert-danger alert-dismissable'));
            }
            return $this->redirect(
                array('controller' => 'tekniks', 'action' => 'yeniteknik')
            );
        }else{
            $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button>TeknikAnaliz düzenleme işlemi başarısız. Lütfen tekrar deneyin.','default', array('class'=>'alert alert-danger alert-dismissable'));
            return $this->redirect(
                array('controller' => 'tekniks', 'action' => 'home')
            );
        }
    }

    public function deleteResim(){
        $this->autoRender = false;
        if($this->request->is('post')){
            $ResId = $this->request->data('resId');
            $resim = $this->TeknikAnalizResim->find('first',array('conditions'=>array('id'=>$ResId)));
            $fileRes = new File($resim['TeknikAnalizResim']['path']);
            $fileRes->delete();
            if($this->TeknikAnalizResim->deleteAll(array('id'=>$ResId))){
                echo json_encode(true);
            }else{
                echo json_encode(false);
            }
        }else{
            echo json_encode(false);
        }
    }

    public function ajaxDeleteTeknikAnaliz(){
        $this->autoRender = false;
        if($this->request->is('post')){
            $teknikId = $this->request->data('teknikId');
            $teknikRes = $this->TeknikAnalizResim->findAllByTeknikAnalizId($teknikId);
            foreach($teknikRes as $row){
                $fileRes = new File($row['TeknikAnalizResim']['path']);
                $fileRes->delete();
            }
            //Delete Video
            $video = $this->TeknikAnaliz->find('first',array('conditions'=>array('id'=>$teknikId)));
            $fileVideo = new File($video['TeknikAnaliz']['video']);
            $fileVideo->delete();

            $this->TeknikAnalizResim->deleteAll(array('teknik_analiz_id'=>$teknikId));
            $this->TeknikAnaliz->deleteAll(array('id'=>$teknikId));
            echo json_encode(true);
        }
    }

    public function ajaxDeleteVideo(){
        $this->autoRender = false;
        if($this->request->is('post')){
            $teknikId = $this->request->data('teknikId');
            $video = $this->TeknikAnaliz->find('first',array('conditions'=>array('id'=>$teknikId)));
            $fileVideo = new File($video['TeknikAnaliz']['video']);
            $fileVideo->delete();
            if($this->TeknikAnaliz->updateAll(array('video'=>null),array('id'=>$teknikId))){
                echo json_encode(true);
            }else{
                echo json_encode(false);
            }
        }else{
            echo json_encode(false);
        }
    }

    public function yorumkaydet(){
        $this->autoRender = false;
        $return = array('hata'=>true);
        if($this->request->is('post')){
            $data = $this->request->data;
            $ip = $this->request->clientIp();
            $teknikId = $data['teknikId'];
            $yorumId = $data['yorumId'];
            $isim = trim($data['isim']);
            $email = trim($data['email']);
            $yorum = trim($data['yorum']);
            $tarih = date('Y-m-d H:i:s');
            $savedArr = array('isim'=>$isim,'mail'=>$email,'yorum'=>$yorum);
            if($this->Session->check('UserLogin')){
                $savedArr['user'] = 1;
            }

            if($yorumId == 0 || empty($yorumId)){
                $this->TeknikAnalizYorum->create();
                $savedArr['tarih'] = $tarih;
                $savedArr['ip'] = $ip;
                $savedArr['teknik_analiz_id'] = $teknikId;
            }else{
                $this->TeknikAnalizYorum->id = $yorumId;
            }

            if($this->TeknikAnalizYorum->save($savedArr)){
                $return['hata'] = false;
            }
        }
        echo json_encode($return);
    }

    public function ajaxGetTeknikYorum(){
        $this->autoRender = false;
        $return = array('hata'=>true);
        if($this->request->is('post')){
            $data = $this->request->data;
            $teknikId = $data['tId'];
            $yorumId = $data['yId'];
            $return['data'] = $this->TeknikAnalizYorum->findById($yorumId);
            $return['hata'] = false;
        }
        echo json_encode($return);
    }

    public function ajaxGetTeknikYorumSil(){
        $this->autoRender = false;
        $return = array('hata'=>true);
        if($this->request->is('post')){
            $data = $this->request->data;
            $yorumId = $data['yId'];
            if($this->TeknikAnalizYorum->deleteAll(array('id'=>$yorumId))){
                $return['hata'] = false;
            }
        }
        echo json_encode($return);
    }
}