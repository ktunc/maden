<?php
App::uses('Security', 'Utility');
App::uses ( 'String', 'Utility' );
App::uses('ImageManipulator','Vendor');
App::uses('FilerUploader','Vendor');
class UsersController extends AppController{
    var $uses = array('Konut','Sehir','Ilce','Mahalle','KonutYer',
        'KonutResim','Tip','KonutLocation','Isyeri',
        'IsyeriYer','IsyeriResim','IsyeriLocation',
        'Arsa','ArsaYer','ArsaResim','ArsaLocation','User','Semt','Proje','Haber',
        'Iletisim','Location','IlanIletisim','ArsaIletisim','KonutIletisim','IsyeriIletisim',
        'TeknikAnaliz','Slider');
    public $components = array('Paginator','HtmlMeta');

    public function index(){
        //$slayts = $this->Slider->find('all',array('order'=>array('sira'=>'ASC')));
        //$this->set('slayts',$slayts);
    }

    public function gmaphome(){
        $sorguVarmi = false;
        if($this->request->is('post')){
            $data = $this->request->data;
            if(array_key_exists('tip',$data) || array_key_exists('mapIlan',$data)){
                $this->passedArgs = $data;
            }else{
                $data = false;
            }
        }else{
            $data = $this->passedArgs;
            if(array_key_exists('tip',$data) || array_key_exists('mapIlan',$data)){
                $this->passedArgs = $data;
            }else{
                $data = false;
                $this->passedArgs = array();
            }
        }
        $dd = false;
        $mapIlan = '';
        $conditions = array('durum'=>1);
        $KonCon['Konut.durum'] = 1;
        $IsCon['Isyeri.durum'] = 1;
        $ArsaCon['Arsa.durum'] = 1;

        if($data){
            if(array_key_exists('mapIlan',$data)){
                if(strlen($data['mapIlan']) > 0){
                    $dd = true;
                }
                $mapIlan = $data['mapIlan'];

                $KonCon['OR'] = array("Konut.baslik LIKE '%$mapIlan%'",
                    "Konut.aciklama LIKE '%$mapIlan%'",
                    "Konut.ilan_no LIKE '%$mapIlan%'",
                    "KonutLoc.name LIKE '%$mapIlan%'",
                    "Sehir.sehir_adi LIKE '%$mapIlan%'",
                    "Ilce.ilce_adi LIKE '%$mapIlan%'",
                    "Semt.semt_adi LIKE '%$mapIlan%'",
                    "Mahalle.mahalle_adi LIKE '%$mapIlan%'");
                $IsCon['OR'] = array("Isyeri.baslik LIKE '%$mapIlan%'",
                    "Isyeri.aciklama LIKE '%$mapIlan%'",
                    "Isyeri.ilan_no LIKE '%$mapIlan%'",
                    "IsyeriLoc.name LIKE '%$mapIlan%'",
                    "Sehir.sehir_adi LIKE '%$mapIlan%'",
                    "Ilce.ilce_adi LIKE '%$mapIlan%'",
                    "Semt.semt_adi LIKE '%$mapIlan%'",
                    "Mahalle.mahalle_adi LIKE '%$mapIlan%'");
                $ArsaCon['OR'] = array("Arsa.baslik LIKE '%$mapIlan%'",
                    "Arsa.aciklama LIKE '%$mapIlan%'",
                    "Arsa.ilan_no LIKE '%$mapIlan%'",
                    "ArsaLoc.name LIKE '%$mapIlan%'",
                    "Sehir.sehir_adi LIKE '%$mapIlan%'",
                    "Ilce.ilce_adi LIKE '%$mapIlan%'",
                    "Semt.semt_adi LIKE '%$mapIlan%'",
                    "Mahalle.mahalle_adi LIKE '%$mapIlan%'");
            }else{
                $sorguVarmi = true;
                // İlan Tipi
                if(array_key_exists('tip',$data) && $data['tip'] != 0){
                    $KonCon['Konut.tip'] = $data['tip'];
                    $IsCon['Isyeri.tip'] = $data['tip'];
                    $ArsaCon['Arsa.tip'] = $data['tip'];
                }

                // İlan Satılık-Kiralik
                if(array_key_exists('satkir',$data) && $data['satkir'] != 0){
                    $KonCon['Konut.sat_kir'] = $data['satkir'];
                    $IsCon['Isyeri.sat_kir'] = $data['satkir'];
                    $ArsaCon['Arsa.sat_kir'] = $data['satkir'];
                }

                // İlan Şehir
                if(array_key_exists('il',$data) && $data['il'] != 0){
                    $KonCon['KonutYer.sehir_id'] = $data['il'];
                    $IsCon['IsyeriYer.sehir_id'] = $data['il'];
                    $ArsaCon['ArsaYer.sehir_id'] = $data['il'];
                }

                //İlan Fiyatı
                if(array_key_exists('fiy1',$data) && $data['fiy1'] != 0){
                    $KonCon[] = "Konut.fiyat >= ".$data['fiy1'];
                    $IsCon[] = "Isyeri.fiyat >= ".$data['fiy1'];
                    $ArsaCon[] = "Arsa.fiyat >= ".$data['fiy1'];
                }
                if(array_key_exists('fiy2',$data) && $data['fiy2'] != 0){
                    $KonCon[] = "Konut.fiyat <= ".$data['fiy2'];
                    $IsCon[] = "Isyeri.fiyat <= ".$data['fiy2'];
                    $ArsaCon[] = "Arsa.fiyat <= ".$data['fiy2'];
                }

                //İlan Metre Kare
                if(array_key_exists('m1',$data) && $data['m1'] != 0){
                    $KonCon[] = "Konut.m_kare >= ".$data['m1'];
                    $IsCon[] = "Isyeri.m_kare >= ".$data['m1'];
                    $ArsaCon[] = "Arsa.m_kare >= ".$data['m1'];
                }
                if(array_key_exists('m2',$data) && $data['m2'] != 0){
                    $KonCon[] = "Konut.m_kare <= ".$data['m2'];
                    $IsCon[] = "Isyeri.m_kare <= ".$data['m2'];
                    $ArsaCon[] = "Arsa.m_kare <= ".$data['m2'];
                }

                $ilce = array_key_exists('ilce', $data)?$data['ilce']:0;
                $semt = array_key_exists('semt', $data)?$data['semt']:0;
                $mahalle = array_key_exists('mahalle', $data)?$data['mahalle']:0;
                // İlan İlçe
                if($ilce != 0){
                    $KonCon['KonutYer.ilce_id'] = $ilce;
                    $IsCon['IsyeriYer.ilce_id'] = $ilce;
                    $ArsaCon['ArsaYer.ilce_id'] = $ilce;
                }
                // İlan Semt
                if($semt != 0){
                    $KonCon['KonutYer.semt_id'] = $semt;
                    $IsCon['IsyeriYer.semt_id'] = $semt;
                    $ArsaCon['ArsaYer.semt_id'] = $semt;
                }
                // İlan Mahalle
                if($mahalle != 0){
                    $KonCon['KonutYer.mahalle_id'] = $mahalle;
                    $IsCon['IsyeriYer.mahalle_id'] = $mahalle;
                    $ArsaCon['ArsaYer.mahalle_id'] = $mahalle;
                }
            }
        }

        $arsas = $this->Arsa->find('all',
                array(
                    'fields'=>array('Arsa.*','ArsaLoc.*'),
                    'conditions'=>$ArsaCon,
                    'order'=>array('Arsa.ilan_no'=>'DESC'),
                    'joins'=>array(
                        array('table'=>'em_arsa_location', 'alias'=>'ArsaLoc', 'type'=>'INNER', 'conditions'=>array('Arsa.id=ArsaLoc.arsa_id')),
                        array('table'=>'em_arsa_yer', 'alias'=>'ArsaYer', 'type'=>'INNER', 'conditions'=>array('Arsa.id=ArsaYer.arsa_id')),
                        array('table'=>'em_sehir', 'alias'=>'Sehir', 'type'=>'INNER', 'conditions'=>array('ArsaYer.sehir_id=Sehir.id')),
                        array('table'=>'em_ilce', 'alias'=>'Ilce', 'type'=>'LEFT', 'conditions'=>array('ArsaYer.ilce_id=Ilce.id')),
                        array('table'=>'em_semt', 'alias'=>'Semt', 'type'=>'LEFT', 'conditions'=>array('ArsaYer.semt_id=Semt.id')),
                        array('table'=>'em_mahalle', 'alias'=>'Mahalle', 'type'=>'LEFT', 'conditions'=>array('ArsaYer.mahalle_id=Mahalle.id'))
                    )
                )
                );
        $konuts = $this->Konut->find('all',
                array(
                    'fields'=>array('Konut.*','KonutLoc.*'),
                    'conditions'=>$KonCon,
                    'order'=>array('Konut.ilan_no'=>'DESC'),
                    'joins'=>array(
                        array('table'=>'em_konut_location', 'alias'=>'KonutLoc', 'type'=>'INNER', 'conditions'=>array('Konut.id=KonutLoc.konut_id')),
                        array('table'=>'em_konut_yer', 'alias'=>'KonutYer', 'type'=>'INNER', 'conditions'=>array('Konut.id=KonutYer.konut_id')),
                        array('table'=>'em_sehir', 'alias'=>'Sehir', 'type'=>'INNER', 'conditions'=>array('KonutYer.sehir_id=Sehir.id')),
                        array('table'=>'em_ilce', 'alias'=>'Ilce', 'type'=>'LEFT', 'conditions'=>array('KonutYer.ilce_id=Ilce.id')),
                        array('table'=>'em_semt', 'alias'=>'Semt', 'type'=>'LEFT', 'conditions'=>array('KonutYer.semt_id=Semt.id')),
                        array('table'=>'em_mahalle', 'alias'=>'Mahalle', 'type'=>'LEFT', 'conditions'=>array('KonutYer.mahalle_id=Mahalle.id'))
                    )
                )
            );
        $isyeris = $this->Isyeri->find('all',
                array(
                    'fields'=>array('Isyeri.*','IsyeriLoc.*'),
                    'conditions'=>$IsCon,
                    'order'=>array('Isyeri.ilan_no'=>'DESC'),
                    'joins'=>array(
                        array('table'=>'em_isyeri_location', 'alias'=>'IsyeriLoc', 'type'=>'INNER', 'conditions'=>array('Isyeri.id=IsyeriLoc.isyeri_id')),
                        array('table'=>'em_isyeri_yer', 'alias'=>'IsyeriYer', 'type'=>'INNER', 'conditions'=>array('Isyeri.id=IsyeriYer.isyeri_id')),
                        array('table'=>'em_sehir', 'alias'=>'Sehir', 'type'=>'INNER', 'conditions'=>array('IsyeriYer.sehir_id=Sehir.id')),
                        array('table'=>'em_ilce', 'alias'=>'Ilce', 'type'=>'LEFT', 'conditions'=>array('IsyeriYer.ilce_id=Ilce.id')),
                        array('table'=>'em_semt', 'alias'=>'Semt', 'type'=>'LEFT', 'conditions'=>array('IsyeriYer.semt_id=Semt.id')),
                        array('table'=>'em_mahalle', 'alias'=>'Mahalle', 'type'=>'LEFT', 'conditions'=>array('IsyeriYer.mahalle_id=Mahalle.id'))
                    )
                )
            );
        
        $ilans = array();
        foreach($konuts as $row){
            $ilanArray = array();
            $ilanArray['id'] = $row['Konut']['id'];
            $ilanArray['ilanNo'] = $row['Konut']['ilan_no'];
            $ilanArray['tip'] = $row['Konut']['tip'];
            $ilanArray['baslik'] = $row['Konut']['baslik'];
            $ilanArray['satkir'] = $row['Konut']['sat_kir'];
            $ilanArray['lat'] = floatval(trim($row['KonutLoc']['latitude']));
            $ilanArray['lon'] = floatval(trim($row['KonutLoc']['longitude']));
            $ilans[] = $ilanArray;
        }
        foreach($isyeris as $row){
            $ilanArray = array();
            $ilanArray['id'] = $row['Isyeri']['id'];
            $ilanArray['ilanNo'] = $row['Isyeri']['ilan_no'];
            $ilanArray['tip'] = $row['Isyeri']['tip'];
            $ilanArray['baslik'] = $row['Isyeri']['baslik'];
            $ilanArray['satkir'] = $row['Isyeri']['sat_kir'];
            $ilanArray['lat'] = floatval(trim($row['IsyeriLoc']['latitude']));
            $ilanArray['lon'] = floatval(trim($row['IsyeriLoc']['longitude']));
            $ilans[] = $ilanArray;
        }
        foreach($arsas as $row){
            $ilanArray = array();
            $ilanArray['id'] = $row['Arsa']['id'];
            $ilanArray['ilanNo'] = $row['Arsa']['ilan_no'];
            $ilanArray['tip'] = $row['Arsa']['tip'];
            $ilanArray['baslik'] = $row['Arsa']['baslik'];
            $ilanArray['satkir'] = $row['Arsa']['sat_kir'];
            $ilanArray['lat'] = floatval(trim($row['ArsaLoc']['latitude']));
            $ilanArray['lon'] = floatval(trim($row['ArsaLoc']['longitude']));
            $ilans[] = $ilanArray;
        }

        if($sorguVarmi && count($ilans) > 0){
            $dd = true;
        }

        if(count($ilans) == 0){
             $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button>Arama kriterinize göre sonuç bulunmamaktadır.','default', array('class'=>'alert alert-warning alert-dismissable'));
        }

        $this->HtmlMeta->addElement(array('name' => 'robots', 'content' => 'index, follow'));
        $this->HtmlMeta->addElement(array('name' => 'title', 'content' => 'MADEN Gayrimenkul A.Ş.'));
        $this->HtmlMeta->addElement(array('name' => 'description', 'content' => 'Satılık, kiralık, konut, işyeri, arsa yatırım ve yönetimi - Mutlu Kazanç Kapısı...'));
        $this->HtmlMeta->addElement(array('property' => 'og:title', 'content' => 'MADEN Gayrimenkul A.Ş.' ));
        $this->HtmlMeta->addElement(array('property' => 'og:description', 'content' => 'Satılık, kiralık, konut, işyeri, arsa yatırım ve yönetimi - Mutlu Kazanç Kapısı...'));
        $this->HtmlMeta->addElement(array('property' => 'og:keywords', 'content' => 'Eskişehir Yolu satılık yatırım arsaları, Temelli, Malıköy, Ihlamurkent satılık villa, Alcı, Türkobası, Anayurt, Bacı, Başkent OSB satılık arsa, kiralık fabrika, Anadolu OSB satılık fabrika arsa, ASO 2 OSB, Konutkent, Çayyolu, Westgate Residence, Kuleevo, Elmar Towers, Elya Tower, Azel Kule' ));
        $this->HtmlMeta->addElement(array('property' => 'og:url', 'content' => Router::url('/', true)));
        $this->HtmlMeta->addElement(array('property'=>'og:image', 'content'=>  Router::url('/',true).'img/logo.png'));

        $this->set('ilans',  json_encode($ilans));
        $this->set('dd',  json_encode($dd));
        $this->set('mapIlan',$mapIlan);
    }
    
    public function adres(){
        
    }
    
    public function home(){
        $kCount = $this->Konut->find('count');
        $kSatCount = $this->Konut->find('count',array('conditions'=>array('sat_kir'=>1)));
        $kKirCount = $this->Konut->find('count',array('conditions'=>array('sat_kir'=>2)));
        $iCount = $this->Isyeri->find('count');
        $iSatCount = $this->Isyeri->find('count',array('conditions'=>array('sat_kir'=>1)));
        $iKirCount = $this->Isyeri->find('count',array('conditions'=>array('sat_kir'=>2)));
        $aCount = $this->Arsa->find('count');
        $aSatCount = $this->Arsa->find('count',array('conditions'=>array('sat_kir'=>1)));
        $aKirCount = $this->Arsa->find('count',array('conditions'=>array('sat_kir'=>2)));
        $projeCount = $this->Proje->find('count');
        $haberCount = $this->Haber->find('count');
        $this->set('kCount',$kCount);
        $this->set('kSatCount',$kSatCount);
        $this->set('kKirCount',$kKirCount);
        $this->set('iCount',$iCount);
        $this->set('iSatCount',$iSatCount);
        $this->set('iKirCount',$iKirCount);
        $this->set('aCount',$aCount);
        $this->set('aSatCount',$aSatCount);
        $this->set('aKirCount',$aKirCount);
        $this->set('projeCount',$projeCount);
        $this->set('haberCount',$haberCount);
    }
    
    public function kurumsal(){
        $this->HtmlMeta->addElement(array('name' => 'robots', 'content' => 'index, follow'));
        $this->HtmlMeta->addElement(array('name' => 'title', 'content' => 'Kurumsal - MADEN Gayrimenkul A.Ş.'));
        $this->HtmlMeta->addElement(array('name' => 'description', 'content' => 'Satılık, kiralık, konut, işyeri, arsa yatırım ve yönetimi - Mutlu Kazanç Kapısı...'));
        $this->HtmlMeta->addElement(array('property' => 'og:title', 'content' => 'Kurumsal - MADEN Gayrimenkul A.Ş.' ));
        $this->HtmlMeta->addElement(array('property' => 'og:description', 'content' => 'Satılık, kiralık, konut, işyeri, arsa yatırım ve yönetimi - Mutlu Kazanç Kapısı...'));
        $this->HtmlMeta->addElement(array('property' => 'og:keywords', 'content' => 'Eskişehir Yolu satılık yatırım arsaları, Temelli, Malıköy, Ihlamurkent satılık villa, Alcı, Türkobası, Anayurt, Bacı, Başkent OSB satılık arsa, kiralık fabrika, Anadolu OSB satılık fabrika arsa, ASO 2 OSB, Konutkent, Çayyolu, Westgate Residence, Kuleevo, Elmar Towers, Elya Tower, Azel Kule' ));
        $this->HtmlMeta->addElement(array('property' => 'og:url', 'content' => Router::url('/', true).'users/kurumsal'));
        $this->HtmlMeta->addElement(array('property'=>'og:image', 'content'=>  Router::url('/',true).'img/shareicon/mkur.png'));
    }

    public function arama(){
        //if($this->request->is('post')){
        if($this->request->is('post')){
            $data = $this->request->data;
            $this->passedArgs = $data;
        }else{
            $data = $this->passedArgs;
        }
        $limit = 5;

        $arsas = false; $konuts = false; $isyeris = false;

        $KonOrder = array();
        $IsOrder = array();
        $ArsaOrder = array();
        $KonCon['Konut.durum'] = 1;
        $IsCon['Isyeri.durum'] = 1;
        $ArsaCon['Arsa.durum'] = 1;

        $sval = '';
        if(array_key_exists('sval', $data)) {
            $sval = $data['sval'];
            $this->set('sval',$sval);
            $KonCon['OR'] = array("Konut.baslik LIKE '%$sval%'",
                "Konut.aciklama LIKE '%$sval%'",
                "Konut.ilan_no LIKE '%$sval%'",
                "KonutLocation.name LIKE '%$sval%'",
                "Sehir.sehir_adi LIKE '%$sval%'",
                "Ilce.ilce_adi LIKE '%$sval%'",
                "Semt.semt_adi LIKE '%$sval%'",
                "Mahalle.mahalle_adi LIKE '%$sval%'");
            $IsCon['OR'] = array("Isyeri.baslik LIKE '%$sval%'",
                "Isyeri.aciklama LIKE '%$sval%'",
                "Isyeri.ilan_no LIKE '%$sval%'",
                "IsyeriLocation.name LIKE '%$sval%'",
                "Sehir.sehir_adi LIKE '%$sval%'",
                "Ilce.ilce_adi LIKE '%$sval%'",
                "Semt.semt_adi LIKE '%$sval%'",
                "Mahalle.mahalle_adi LIKE '%$sval%'");
            $ArsaCon['OR'] = array("Arsa.baslik LIKE '%$sval%'",
                "Arsa.aciklama LIKE '%$sval%'",
                "Arsa.ilan_no LIKE '%$sval%'",
                "ArsaLocation.name LIKE '%$sval%'",
                "Sehir.sehir_adi LIKE '%$sval%'",
                "Ilce.ilce_adi LIKE '%$sval%'",
                "Semt.semt_adi LIKE '%$sval%'",
                "Mahalle.mahalle_adi LIKE '%$sval%'");
        }

        if(array_key_exists('tarih', $data)){
            if($data['tarih']=='desc'){
                $KonOrder['Konut.tarih'] = 'DESC';
                $IsOrder['Isyeri.tarih'] = 'DESC';
                $ArsaOrder['Arsa.tarih'] = 'DESC';
            }else if($data['tarih']=='asc'){
                $KonOrder['Konut.tarih'] = 'ASC';
                $IsOrder['Isyeri.tarih'] = 'ASC';
                $ArsaOrder['Arsa.tarih'] = 'ASC';
            }
        }else if(array_key_exists('fiyat', $data)){
            if($data['fiyat']=='desc'){
                $KonOrder['Konut.fiyat'] = 'DESC';
                $IsOrder['Isyeri.fiyat'] = 'DESC';
                $ArsaOrder['Arsa.fiyat'] = 'DESC';
            }else if($data['fiyat']=='asc'){
                $KonOrder['Konut.fiyat'] = 'ASC';
                $IsOrder['Isyeri.fiyat'] = 'ASC';
                $ArsaOrder['Arsa.fiyat'] = 'ASC';
            }
        }else{
            $KonOrder['Konut.tarih'] = 'DESC';
            $IsOrder['Isyeri.tarih'] = 'DESC';
            $ArsaOrder['Arsa.tarih'] = 'DESC';
        }

        // İlan Tipi
        if(array_key_exists('tip',$data) && $data['tip'] != 0){
            $limit = 10;
            $KonCon['Konut.tip'] = $data['tip'];
            $IsCon['Isyeri.tip'] = $data['tip'];
            $ArsaCon['Arsa.tip'] = $data['tip'];
        }
        // İlan Satılık-Kiralik
        if(array_key_exists('satkir',$data) && $data['satkir'] != 0){
            $KonCon['Konut.sat_kir'] = $data['satkir'];
            $IsCon['Isyeri.sat_kir'] = $data['satkir'];
            $ArsaCon['Arsa.sat_kir'] = $data['satkir'];
        }
        // İlan Şehir
        if(array_key_exists('il',$data) && $data['il'] != 0){
            $KonCon['KonutYer.sehir_id'] = $data['il'];
            $IsCon['IsyeriYer.sehir_id'] = $data['il'];
            $ArsaCon['ArsaYer.sehir_id'] = $data['il'];
        }

        //İlan Fiyatı
        if(array_key_exists('fiy1',$data) && $data['fiy1'] != 0){
            $KonCon[] = "Konut.fiyat >= ".$data['fiy1'];
            $IsCon[] = "Isyeri.fiyat >= ".$data['fiy1'];
            $ArsaCon[] = "Arsa.fiyat >= ".$data['fiy1'];
        }
        if(array_key_exists('fiy2',$data) && $data['fiy2'] != 0){
            $KonCon[] = "Konut.fiyat <= ".$data['fiy2'];
            $IsCon[] = "Isyeri.fiyat <= ".$data['fiy2'];
            $ArsaCon[] = "Arsa.fiyat <= ".$data['fiy2'];
        }

        //İlan Metre Kare
        if(array_key_exists('m1',$data) && $data['m1'] != 0){
            $KonCon[] = "Konut.m_kare >= ".$data['m1'];
            $IsCon[] = "Isyeri.m_kare >= ".$data['m1'];
            $ArsaCon[] = "Arsa.m_kare >= ".$data['m1'];
        }
        if(array_key_exists('m2',$data) && $data['m2'] != 0){
            $KonCon[] = "Konut.m_kare <= ".$data['m2'];
            $IsCon[] = "Isyeri.m_kare <= ".$data['m2'];
            $ArsaCon[] = "Arsa.m_kare <= ".$data['m2'];
        }

        $ilce = array_key_exists('ilce', $data)?$data['ilce']:0;
        $semt = array_key_exists('semt', $data)?$data['semt']:0;
        $mahalle = array_key_exists('mahalle', $data)?$data['mahalle']:0;
        // İlan İlçe
        if($ilce != 0){
            $KonCon['KonutYer.ilce_id'] = $ilce;
            $IsCon['IsyeriYer.ilce_id'] = $ilce;
            $ArsaCon['ArsaYer.ilce_id'] = $ilce;
        }
        // İlan Semt
        if($semt != 0){
            $KonCon['KonutYer.semt_id'] = $semt;
            $IsCon['IsyeriYer.semt_id'] = $semt;
            $ArsaCon['ArsaYer.semt_id'] = $semt;
        }
        // İlan Mahalle
        if($mahalle != 0){
            $KonCon['KonutYer.mahalle_id'] = $mahalle;
            $IsCon['IsyeriYer.mahalle_id'] = $mahalle;
            $ArsaCon['ArsaYer.mahalle_id'] = $mahalle;
        }

        // Konut Sorgusu
        $KonSql = array(
            'fields'=>array('*'),
            'conditions'=>$KonCon,
            'joins'=>array(
                array('table'=>'em_konut_yer', 'alias'=>'KonutYer', 'type'=>'INNER', 'conditions'=>array('Konut.id=KonutYer.konut_id')),
                array('table'=>'em_sehir', 'alias'=>'Sehir', 'type'=>'INNER', 'conditions'=>array('KonutYer.sehir_id=Sehir.id')),
                array('table'=>'em_ilce', 'alias'=>'Ilce', 'type'=>'LEFT', 'conditions'=>array('KonutYer.ilce_id=Ilce.id')),
                array('table'=>'em_semt', 'alias'=>'Semt', 'type'=>'LEFT', 'conditions'=>array('KonutYer.semt_id=Semt.id')),
                array('table'=>'em_mahalle', 'alias'=>'Mahalle', 'type'=>'LEFT', 'conditions'=>array('KonutYer.mahalle_id=Mahalle.id')),
                array('table'=>'em_konut_location', 'alias'=>'KonutLocation', 'type'=>'LEFT', 'conditions'=>array('Konut.id=KonutLocation.konut_id'))
            )
        );
        $konutCount = $this->Konut->find('count',$KonSql);
        $pageCount = array_key_exists('page', $data)?$data['page']:1;
        if($konutCount>(($pageCount-1)*$limit)){
            $this->paginate = array(
                'fields'=>array('*'),
                'conditions'=>$KonCon,
                'limit'=>$limit,
                'order'=>$KonOrder,
                'joins'=>array(
                    array('table'=>'em_konut_yer', 'alias'=>'KonutYer', 'type'=>'INNER', 'conditions'=>array('Konut.id=KonutYer.konut_id')),
                    array('table'=>'em_sehir', 'alias'=>'Sehir', 'type'=>'INNER', 'conditions'=>array('KonutYer.sehir_id=Sehir.id')),
                    array('table'=>'em_ilce', 'alias'=>'Ilce', 'type'=>'LEFT', 'conditions'=>array('KonutYer.ilce_id=Ilce.id')),
                    array('table'=>'em_semt', 'alias'=>'Semt', 'type'=>'LEFT', 'conditions'=>array('KonutYer.semt_id=Semt.id')),
                    array('table'=>'em_mahalle', 'alias'=>'Mahalle', 'type'=>'LEFT', 'conditions'=>array('KonutYer.mahalle_id=Mahalle.id')),
                    array('table'=>'em_konut_location', 'alias'=>'KonutLocation', 'type'=>'LEFT', 'conditions'=>array('Konut.id=KonutLocation.konut_id'))
                )
            );
            $konuts = $this->paginate('Konut');
            $this->set('konuts',$konuts);
            $konutRes = array();
            foreach ($konuts as $row) {
                $konutRes[$row['Konut']['id']] = $this->KonutResim->find('first',array('order'=>array('id'=>'ASC'),'fields'=>array('paththumb','id'),'conditions'=>array('konut_id'=>$row['Konut']['id'])));
                //$konutRes[$row['Konut']['id']] = $this->KonutResim->findAllByKonutId($row['Konut']['id']);
            }
            $this->set('konutRes',$konutRes);
        }else{
            $this->set('konuts',false);
        }
        // Konut Sorgusu Son

        // Isyeri Sorgusu
        $IsSql = array(
            'fields'=>array('*'),
            'conditions'=>$IsCon,
            'joins'=>array(
                array('table'=>'em_isyeri_yer', 'alias'=>'IsyeriYer', 'type'=>'INNER', 'conditions'=>array('Isyeri.id=IsyeriYer.isyeri_id')),
                array('table'=>'em_sehir', 'alias'=>'Sehir', 'type'=>'INNER', 'conditions'=>array('IsyeriYer.sehir_id=Sehir.id')),
                array('table'=>'em_ilce', 'alias'=>'Ilce', 'type'=>'LEFT', 'conditions'=>array('IsyeriYer.ilce_id=Ilce.id')),
                array('table'=>'em_semt', 'alias'=>'Semt', 'type'=>'LEFT', 'conditions'=>array('IsyeriYer.semt_id=Semt.id')),
                array('table'=>'em_mahalle', 'alias'=>'Mahalle', 'type'=>'LEFT', 'conditions'=>array('IsyeriYer.mahalle_id=Mahalle.id')),
                array('table'=>'em_isyeri_location', 'alias'=>'IsyeriLocation', 'type'=>'LEFT', 'conditions'=>array('Isyeri.id=IsyeriLocation.isyeri_id'))
            )
        );
        $isyeriCount = $this->Isyeri->find('count',$IsSql);
        $pageCount = array_key_exists('page', $data)?$data['page']:1;
        if($isyeriCount>(($pageCount-1)*$limit)){
            $this->paginate = array(
                'fields'=>array('*'),
                'conditions'=>$IsCon,
                'limit'=>$limit,
                'order'=>$IsOrder,
                'joins'=>array(
                    array('table'=>'em_isyeri_yer', 'alias'=>'IsyeriYer', 'type'=>'INNER', 'conditions'=>array('Isyeri.id=IsyeriYer.isyeri_id')),
                    array('table'=>'em_sehir', 'alias'=>'Sehir', 'type'=>'INNER', 'conditions'=>array('IsyeriYer.sehir_id=Sehir.id')),
                    array('table'=>'em_ilce', 'alias'=>'Ilce', 'type'=>'LEFT', 'conditions'=>array('IsyeriYer.ilce_id=Ilce.id')),
                    array('table'=>'em_semt', 'alias'=>'Semt', 'type'=>'LEFT', 'conditions'=>array('IsyeriYer.semt_id=Semt.id')),
                    array('table'=>'em_mahalle', 'alias'=>'Mahalle', 'type'=>'LEFT', 'conditions'=>array('IsyeriYer.mahalle_id=Mahalle.id')),
                    array('table'=>'em_isyeri_location', 'alias'=>'IsyeriLocation', 'type'=>'LEFT', 'conditions'=>array('Isyeri.id=IsyeriLocation.isyeri_id'))
                )
            );
            $isyeris = $this->paginate('Isyeri');
            $this->set('isyeris',$isyeris);
            $isyeriRes = array();
            foreach ($isyeris as $row) {
                $isyeriRes[$row['Isyeri']['id']] = $this->IsyeriResim->find('first',array('order'=>array('id'=>'ASC'),'fields'=>array('paththumb','id'),'conditions'=>array('isyeri_id'=>$row['Isyeri']['id'])));
                //$isyeriRes[$row['Isyeri']['id']] = $this->IsyeriResim->findAllByIsyeriId($row['Isyeri']['id']);
            }
            $this->set('isyeriRes',$isyeriRes);
        }else{
            $this->set('isyeris',false);
        }
        // İsyeri Sorgusu Son

        //Arsa Sorgusu
        $ArSql = array(
            'fields'=>array('*'),
            'conditions'=>$ArsaCon,
            'joins'=>array(
                array('table'=>'em_arsa_yer', 'alias'=>'ArsaYer', 'type'=>'INNER', 'conditions'=>array('Arsa.id=ArsaYer.arsa_id')),
                array('table'=>'em_sehir', 'alias'=>'Sehir', 'type'=>'INNER', 'conditions'=>array('ArsaYer.sehir_id=Sehir.id')),
                array('table'=>'em_ilce', 'alias'=>'Ilce', 'type'=>'LEFT', 'conditions'=>array('ArsaYer.ilce_id=Ilce.id')),
                array('table'=>'em_semt', 'alias'=>'Semt', 'type'=>'LEFT', 'conditions'=>array('ArsaYer.semt_id=Semt.id')),
                array('table'=>'em_mahalle', 'alias'=>'Mahalle', 'type'=>'LEFT', 'conditions'=>array('ArsaYer.mahalle_id=Mahalle.id')),
                array('table'=>'em_arsa_location', 'alias'=>'ArsaLocation', 'type'=>'LEFT', 'conditions'=>array('Arsa.id=ArsaLocation.arsa_id'))
            )
        );
        $arsaCount = $this->Arsa->find('count',$ArSql);
        $pageCount = array_key_exists('page', $data)?$data['page']:1;
        if($arsaCount>(($pageCount-1)*$limit)){
            $this->paginate = array(
                'fields'=>array('*'),
                'conditions'=>$ArsaCon,
                'limit'=>$limit,
                'order'=>$ArsaOrder,
                'joins'=>array(
                    array('table'=>'em_arsa_yer', 'alias'=>'ArsaYer', 'type'=>'INNER', 'conditions'=>array('Arsa.id=ArsaYer.arsa_id')),
                    array('table'=>'em_sehir', 'alias'=>'Sehir', 'type'=>'INNER', 'conditions'=>array('ArsaYer.sehir_id=Sehir.id')),
                    array('table'=>'em_ilce', 'alias'=>'Ilce', 'type'=>'LEFT', 'conditions'=>array('ArsaYer.ilce_id=Ilce.id')),
                    array('table'=>'em_semt', 'alias'=>'Semt', 'type'=>'LEFT', 'conditions'=>array('ArsaYer.semt_id=Semt.id')),
                    array('table'=>'em_mahalle', 'alias'=>'Mahalle', 'type'=>'LEFT', 'conditions'=>array('ArsaYer.mahalle_id=Mahalle.id')),
                    array('table'=>'em_arsa_location', 'alias'=>'ArsaLocation', 'type'=>'LEFT', 'conditions'=>array('Arsa.id=ArsaLocation.arsa_id'))
                )
            );
            $arsas = $this->paginate('Arsa');
            $this->set('arsas',$arsas);
            $arsaRes = array();
            foreach ($arsas as $row) {
                $arsaRes[$row['Arsa']['id']] = $this->ArsaResim->find('first',array('order'=>array('id'=>'ASC'),'fields'=>array('paththumb','id'),'conditions'=>array('arsa_id'=>$row['Arsa']['id'])));
                //$arsaRes[$row['Arsa']['id']] = $this->ArsaResim->findAllByArsaId($row['Arsa']['id']);
            }
            $this->set('arsaRes',$arsaRes);
        }else {
            $this->set('arsas',false);
        }
        // Arsa Sorgusu Son

        if(!$konuts && !$isyeris && !$arsas){
            $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button>Aradığınız kriterlere uygun sonuç yoktur.','default', array('class'=>'alert alert-warning alert-dismissable'));
            return $this->redirect(
                array('controller' => 'users', 'action' => 'index')
            );
        }

        $this->HtmlMeta->addElement(array('name' => 'robots', 'content' => 'index, follow'));
        $this->HtmlMeta->addElement(array('name' => 'description', 'content' => 'Satılık, kiralık, konut, işyeri, arsa yatırım ve yönetimi - Mutlu Kazanç Kapısı...'));
        $this->HtmlMeta->addElement(array('property' => 'og:description', 'content' => 'Satılık, kiralık, konut, işyeri, arsa yatırım ve yönetimi - Mutlu Kazanç Kapısı...'));
        $this->HtmlMeta->addElement(array('property' => 'og:keywords', 'content' => 'Eskişehir Yolu satılık yatırım arsaları, Temelli, Malıköy, Ihlamurkent satılık villa, Alcı, Türkobası, Anayurt, Bacı, Başkent OSB satılık arsa, kiralık fabrika, Anadolu OSB satılık fabrika arsa, ASO 2 OSB, Konutkent, Çayyolu, Westgate Residence, Kuleevo, Elmar Towers, Elya Tower, Azel Kule' ));
        if(array_key_exists('tip',$data) && $data['tip'] == 1){
            if(array_key_exists('satkir',$data) && $data['satkir'] == 1){
                $this->HtmlMeta->addElement(array('name' => 'title', 'content' => 'Satılık Konut - MADEN Gayrimenkul A.Ş.'));
                $this->HtmlMeta->addElement(array('property' => 'og:title', 'content' => 'Satılık Konut - MADEN Gayrimenkul A.Ş.' ));
            }else if(array_key_exists('satkir',$data) && $data['satkir'] == 2){
                $this->HtmlMeta->addElement(array('name' => 'title', 'content' => 'Kiralık Konut - MADEN Gayrimenkul A.Ş.'));
                $this->HtmlMeta->addElement(array('property' => 'og:title', 'content' => 'Kiralık Konut - MADEN Gayrimenkul A.Ş.' ));
            }else{
                $this->HtmlMeta->addElement(array('name' => 'title', 'content' => 'Konut - MADEN Gayrimenkul A.Ş.'));
                $this->HtmlMeta->addElement(array('property' => 'og:title', 'content' => 'Konut - MADEN Gayrimenkul A.Ş.' ));
            }
            $this->HtmlMeta->addElement(array('property' => 'og:url', 'content' => Router::url('/', true).'users/arama/tip:1'));
            $this->HtmlMeta->addElement(array('property'=>'og:image', 'content'=>  Router::url('/',true).'img/shareicon/mkon.png'));
        }else if(array_key_exists('tip',$data) && $data['tip'] == 2){
            if(array_key_exists('satkir',$data) && $data['satkir'] == 1){
                $this->HtmlMeta->addElement(array('name' => 'title', 'content' => 'Satılık İşyeri - MADEN Gayrimenkul A.Ş.'));
                $this->HtmlMeta->addElement(array('property' => 'og:title', 'content' => 'Satılık İşyeri - MADEN Gayrimenkul A.Ş.' ));
            }else if(array_key_exists('satkir',$data) && $data['satkir'] == 2){
                $this->HtmlMeta->addElement(array('name' => 'title', 'content' => 'Kiralık İşyeri - MADEN Gayrimenkul A.Ş.'));
                $this->HtmlMeta->addElement(array('property' => 'og:title', 'content' => 'Kiralık İşyeri - MADEN Gayrimenkul A.Ş.' ));
            }else{
                $this->HtmlMeta->addElement(array('name' => 'title', 'content' => 'İşyeri - MADEN Gayrimenkul A.Ş.'));
                $this->HtmlMeta->addElement(array('property' => 'og:title', 'content' => 'İşyeri - MADEN Gayrimenkul A.Ş.' ));
            }
            $this->HtmlMeta->addElement(array('property' => 'og:url', 'content' => Router::url('/', true).'users/arama/tip:2'));
            $this->HtmlMeta->addElement(array('property'=>'og:image', 'content'=>  Router::url('/',true).'img/shareicon/mis.png'));
        }else if(array_key_exists('tip',$data) && $data['tip'] == 3){
            if(array_key_exists('satkir',$data) && $data['satkir'] == 1){
                $this->HtmlMeta->addElement(array('name' => 'title', 'content' => 'Satılık Arsa - MADEN Gayrimenkul A.Ş.'));
                $this->HtmlMeta->addElement(array('property' => 'og:title', 'content' => 'Satılık Arsa - MADEN Gayrimenkul A.Ş.' ));
            }else if(array_key_exists('satkir',$data) && $data['satkir'] == 2){
                $this->HtmlMeta->addElement(array('name' => 'title', 'content' => 'Kiralık Arsa - MADEN Gayrimenkul A.Ş.'));
                $this->HtmlMeta->addElement(array('property' => 'og:title', 'content' => 'Kiralık Arsa - MADEN Gayrimenkul A.Ş.' ));
            }else{
                $this->HtmlMeta->addElement(array('name' => 'title', 'content' => 'Arsa - MADEN Gayrimenkul A.Ş.'));
                $this->HtmlMeta->addElement(array('property' => 'og:title', 'content' => 'Arsa - MADEN Gayrimenkul A.Ş.' ));
            }

            $this->HtmlMeta->addElement(array('property' => 'og:url', 'content' => Router::url('/', true).'users/arama/tip:3'));
            $this->HtmlMeta->addElement(array('property'=>'og:image', 'content'=>  Router::url('/',true).'img/shareicon/mar.png'));
        }else{
            $this->HtmlMeta->addElement(array('name' => 'title', 'content' => 'MADEN Gayrimenkul A.Ş.'));
            $this->HtmlMeta->addElement(array('property' => 'og:title', 'content' => 'MADEN Gayrimenkul A.Ş.' ));
            $this->HtmlMeta->addElement(array('property' => 'og:url', 'content' => Router::url('/', true).'users/arama'));
            $this->HtmlMeta->addElement(array('property'=>'og:image', 'content'=>  Router::url('/',true).'img/logo.png'));
        }
        $this->set('sTip',(array_key_exists('tip',$data)?$data['tip']:0));
        $this->set('sSatKir',(array_key_exists('satkir',$data)?$data['satkir']:0));
        $this->set('sval',$sval);
    }
    
    public function detaylisearch(){
        $il = $this->Sehir->find('all',array('order'=>'sehir_adi ASC'));
        $this->set('il',$il);

        $this->HtmlMeta->addElement(array('name' => 'robots', 'content' => 'index, follow'));
        $this->HtmlMeta->addElement(array('name' => 'title', 'content' => 'Detaylı Arama - MADEN Gayrimenkul A.Ş.'));
        $this->HtmlMeta->addElement(array('name' => 'description', 'content' => 'Satılık, kiralık, konut, işyeri, arsa yatırım ve yönetimi - Mutlu Kazanç Kapısı...'));
        $this->HtmlMeta->addElement(array('property' => 'og:title', 'content' => 'Detaylı Arama - MADEN Gayrimenkul A.Ş.' ));
        $this->HtmlMeta->addElement(array('property' => 'og:description', 'content' => 'Satılık, kiralık, konut, işyeri, arsa yatırım ve yönetimi - Mutlu Kazanç Kapısı...'));
        $this->HtmlMeta->addElement(array('property' => 'og:keywords', 'content' => 'Eskişehir Yolu satılık yatırım arsaları, Temelli, Malıköy, Ihlamurkent satılık villa, Alcı, Türkobası, Anayurt, Bacı, Başkent OSB satılık arsa, kiralık fabrika, Anadolu OSB satılık fabrika arsa, ASO 2 OSB, Konutkent, Çayyolu, Westgate Residence, Kuleevo, Elmar Towers, Elya Tower, Azel Kule' ));
        $this->HtmlMeta->addElement(array('property' => 'og:url', 'content' => Router::url('/', true).'users/detaylisearch'));
        $this->HtmlMeta->addElement(array('property'=>'og:image', 'content'=>  Router::url('/',true).'img/shareicon/mfilter.png'));
    }
    
    public function login(){
        $this->HtmlMeta->addElement(array('name' => 'robots', 'content' => 'noindex, nofollow'));
    }
    
    public function loginCheck(){
        $this->autoRender = false;
        if($this->request->is('post')){
            $username = $this->request->data('username');
            $pass = $this->request->data('pass');
            $pass = Security::hash($pass, 'sha1', true);
            
            $data = $this->User->findByUsernameAndPass($username,$pass);
            if($data){
                $this->Session->write('UserLogin', true);
                $this->Session->write('UserId', $data['User']['id']);
                $this->Session->write('UserGroup', $data['User']['group']);
                $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button>Giriş başarılı.','default', array('class'=>'alert alert-success alert-dismissable'));
                return $this->redirect(
                array('controller' => 'users', 'action' => 'index')
                );
            }
            else{
                $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button>Giriş başarısız.','default', array('class'=>'alert alert-danger alert-dismissable'));
                return $this->redirect(
                array('controller' => 'users', 'action' => 'login')
                );
            }
        }
    }
    
    public function logout(){
        $this->autoRender = false;
        $this->Session->destroy('all');
        $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button>Çıkış başarılı.','default', array('class'=>'alert alert-success alert-dismissable'));
                return $this->redirect(
                array('controller' => 'users', 'action' => 'index')
                );
    }

    public function westgate(){
        
    }
    
    public function search(){
        if($this->request->is('post')){
            $data = $this->request->data;
            $this->passedArgs = $data;
        }else{
            $data = $this->passedArgs;
        }
        if(!empty($data)){
            $searchName = $data['searchName'];
            
            $order = '';
            if(array_key_exists('tarih', $data)){
                if($data['tarih']=='desc'){
                    $order = 'tarih DESC';
                    $orderK = array('Konut.tarih'=>'DESC');
                    $orderI = array('Isyeri.tarih'=>'DESC');
                    $orderA = array('Arsa.tarih'=>'DESC');
                }else if($data['tarih']=='asc'){
                    $order = 'tarih ASC';
                    $orderK = array('Konut.tarih'=>'ASC');
                    $orderI = array('Isyeri.tarih'=>'ASC');
                    $orderA = array('Arsa.tarih'=>'ASC');
                }
            }else if(array_key_exists('fiyat', $data)){
                if($data['fiyat']=='desc'){
                    $order = 'fiyat DESC';
                    $orderK = array('Konut.fiyat'=>'DESC');
                    $orderI = array('Isyeri.fiyat'=>'DESC');
                    $orderA = array('Arsa.fiyat'=>'DESC');
                }else if($data['fiyat']=='asc'){
                    $order = 'fiyat ASC';
                    $orderK = array('Konut.fiyat'=>'ASC');
                    $orderI = array('Isyeri.fiyat'=>'ASC');
                    $orderA = array('Arsa.fiyat'=>'ASC');
                }
            }else{
                $orderK = array('Konut.tarih'=>'DESC');
                $orderI = array('Isyeri.tarih'=>'DESC');
                $orderA = array('Arsa.tarih'=>'DESC');
            }
            
            $konutCount = $this->Konut->find('count',array(
            'fields'=>array('*'),
            'conditions'=>array('Konut.durum'=>1,
                'OR'=>array("Konut.baslik LIKE '%$searchName%'",
                    "Konut.aciklama LIKE '%$searchName%'",
                    "Konut.ilan_no LIKE '%$searchName%'",
                    "KonutLocation.name LIKE '%$searchName%'",
                    "Sehir.sehir_adi LIKE '%$searchName%'",
                    "Ilce.ilce_adi LIKE '%$searchName%'",
                    "Semt.semt_adi LIKE '%$searchName%'",
                    "Mahalle.mahalle_adi LIKE '%$searchName%'")),
            'joins'=>array(
                array(
                    'table'=>'em_konut_yer',
                    'alias'=>'KonutYer',
                    'type'=>'INNER',
                    'conditions'=>array('Konut.id=KonutYer.konut_id')
                ),
                array(
                    'table'=>'em_sehir',
                    'alias'=>'Sehir',
                    'type'=>'INNER',
                    'conditions'=>array('KonutYer.sehir_id=Sehir.id')
                ),
                array(
                    'table'=>'em_ilce',
                    'alias'=>'Ilce',
                    'type'=>'LEFT',
                    'conditions'=>array('KonutYer.ilce_id=Ilce.id')
                ),
                array(
                    'table'=>'em_semt',
                    'alias'=>'Semt',
                    'type'=>'LEFT',
                    'conditions'=>array('KonutYer.semt_id=Semt.id')
                ),
                array(
                    'table'=>'em_mahalle',
                    'alias'=>'Mahalle',
                    'type'=>'LEFT',
                    'conditions'=>array('KonutYer.mahalle_id=Mahalle.id')
                ),
                array(
                    'table'=>'em_konut_location',
                    'alias'=>'KonutLocation',
                    'type'=>'INNER',
                    'conditions'=>array('Konut.id=KonutLocation.konut_id')
                )
            )
            ));
            
            $pageCount = array_key_exists('page', $data)?$data['page']:1;
            if($konutCount>(($pageCount-1)*5)){
                $this->paginate = array(
                'fields'=>array('*'),
                'conditions'=>array('Konut.durum'=>1,
                    'OR'=>array("Konut.baslik LIKE '%$searchName%'",
                        "Konut.aciklama LIKE '%$searchName%'",
                        "Konut.ilan_no LIKE '%$searchName%'",
                        "KonutLocation.name LIKE '%$searchName%'",
                        "Sehir.sehir_adi LIKE '%$searchName%'",
                        "Ilce.ilce_adi LIKE '%$searchName%'",
                        "Semt.semt_adi LIKE '%$searchName%'",
                        "Mahalle.mahalle_adi LIKE '%$searchName%'")),
                'limit'=>5,
                'order'=>$orderK,
                'joins'=>array(
                    array(
                        'table'=>'em_konut_yer',
                        'alias'=>'KonutYer',
                        'type'=>'INNER',
                        'conditions'=>array('Konut.id=KonutYer.konut_id')
                    ),
                    array(
                        'table'=>'em_sehir',
                        'alias'=>'Sehir',
                        'type'=>'INNER',
                        'conditions'=>array('KonutYer.sehir_id=Sehir.id')
                    ),
                    array(
                        'table'=>'em_ilce',
                        'alias'=>'Ilce',
                        'type'=>'LEFT',
                        'conditions'=>array('KonutYer.ilce_id=Ilce.id')
                    ),
                    array(
                        'table'=>'em_semt',
                        'alias'=>'Semt',
                        'type'=>'LEFT',
                        'conditions'=>array('KonutYer.semt_id=Semt.id')
                    ),
                    array(
                        'table'=>'em_mahalle',
                        'alias'=>'Mahalle',
                        'type'=>'LEFT',
                        'conditions'=>array('KonutYer.mahalle_id=Mahalle.id')
                    ),
                    array(
                        'table'=>'em_konut_location',
                        'alias'=>'KonutLocation',
                        'type'=>'INNER',
                        'conditions'=>array('Konut.id=KonutLocation.konut_id')
                    )
                )
                );

                $konuts = $this->paginate('Konut');
                $this->set('konuts',$konuts);

                $konutRes = array();
                foreach ($konuts as $row) {
                    $konutRes[$row['Konut']['id']] = $this->KonutResim->findAllByKonutId($row['Konut']['id']);
                }

                $this->set('konutRes',$konutRes);
            }else{
                $this->set('konuts',false);
                $konuts = false;
            }
            
            $isyeriCount = $this->Isyeri->find('count',array(
                'fields'=>array('*'),
                'conditions'=>array('Isyeri.durum'=>1,
                    'OR'=>array("Isyeri.baslik LIKE '%$searchName%'",
                        "Isyeri.aciklama LIKE '%$searchName%'",
                        "Isyeri.ilan_no LIKE '%$searchName%'",
                        "IsyeriLocation.name LIKE '%$searchName%'",
                        "Sehir.sehir_adi LIKE '%$searchName%'",
                        "Ilce.ilce_adi LIKE '%$searchName%'",
                        "Semt.semt_adi LIKE '%$searchName%'",
                        "Mahalle.mahalle_adi LIKE '%$searchName%'")),
                'joins'=>array(
                    array(
                        'table'=>'em_isyeri_yer',
                        'alias'=>'IsyeriYer',
                        'type'=>'INNER',
                        'conditions'=>array('Isyeri.id=IsyeriYer.isyeri_id')
                    ),
                    array(
                        'table'=>'em_sehir',
                        'alias'=>'Sehir',
                        'type'=>'INNER',
                        'conditions'=>array('IsyeriYer.sehir_id=Sehir.id')
                    ),
                    array(
                        'table'=>'em_ilce',
                        'alias'=>'Ilce',
                        'type'=>'LEFT',
                        'conditions'=>array('IsyeriYer.ilce_id=Ilce.id')
                    ),
                    array(
                        'table'=>'em_semt',
                        'alias'=>'Semt',
                        'type'=>'LEFT',
                        'conditions'=>array('IsyeriYer.semt_id=Semt.id')
                    ),
                    array(
                        'table'=>'em_mahalle',
                        'alias'=>'Mahalle',
                        'type'=>'LEFT',
                        'conditions'=>array('IsyeriYer.mahalle_id=Mahalle.id')
                    ),
                    array(
                        'table'=>'em_isyeri_location',
                        'alias'=>'IsyeriLocation',
                        'type'=>'INNER',
                        'conditions'=>array('Isyeri.id=IsyeriLocation.isyeri_id')
                    )
                )
            ));
            $pageCount = array_key_exists('page', $data)?$data['page']:1;
            if($isyeriCount>(($pageCount-1)*5)){
                $this->paginate = array(
                    'fields'=>array('*'),
                    'conditions'=>array('Isyeri.durum'=>1,
                        'OR'=>array("Isyeri.baslik LIKE '%$searchName%'",
                            "Isyeri.aciklama LIKE '%$searchName%'",
                            "Isyeri.ilan_no LIKE '%$searchName%'",
                            "IsyeriLocation.name LIKE '%$searchName%'",
                            "Sehir.sehir_adi LIKE '%$searchName%'",
                            "Ilce.ilce_adi LIKE '%$searchName%'",
                            "Semt.semt_adi LIKE '%$searchName%'",
                            "Mahalle.mahalle_adi LIKE '%$searchName%'")),
                    'limit'=>5,
                    'order'=>$orderI,
                    'joins'=>array(
                        array(
                            'table'=>'em_isyeri_yer',
                            'alias'=>'IsyeriYer',
                            'type'=>'INNER',
                            'conditions'=>array('Isyeri.id=IsyeriYer.isyeri_id')
                        ),
                        array(
                            'table'=>'em_sehir',
                            'alias'=>'Sehir',
                            'type'=>'INNER',
                            'conditions'=>array('IsyeriYer.sehir_id=Sehir.id')
                        ),
                        array(
                            'table'=>'em_ilce',
                            'alias'=>'Ilce',
                            'type'=>'LEFT',
                            'conditions'=>array('IsyeriYer.ilce_id=Ilce.id')
                        ),
                        array(
                            'table'=>'em_semt',
                            'alias'=>'Semt',
                            'type'=>'LEFT',
                            'conditions'=>array('IsyeriYer.semt_id=Semt.id')
                        ),
                        array(
                            'table'=>'em_mahalle',
                            'alias'=>'Mahalle',
                            'type'=>'LEFT',
                            'conditions'=>array('IsyeriYer.mahalle_id=Mahalle.id')
                        ),
                        array(
                            'table'=>'em_isyeri_location',
                            'alias'=>'IsyeriLocation',
                            'type'=>'INNER',
                            'conditions'=>array('Isyeri.id=IsyeriLocation.isyeri_id')
                        )
                    )
                );
                $isyeris = $this->paginate('Isyeri');
                $this->set('isyeris',$isyeris);

                $isyeriRes = array();
                foreach ($isyeris as $row) {
                    $isyeriRes[$row['Isyeri']['id']] = $this->IsyeriResim->findAllByIsyeriId($row['Isyeri']['id']);
                }
                $this->set('isyeriRes',$isyeriRes);
            }else{
                $this->set('isyeris',false);
                $isyeris = false;
            }

            $arsaCount = $this->Arsa->find('count',array(
                'fields'=>array('*'),
                'conditions'=>array('Arsa.durum'=>1,
                    'OR'=>array("Arsa.baslik LIKE '%$searchName%'",
                        "Arsa.aciklama LIKE '%$searchName%'",
                        "Arsa.ilan_no LIKE '%$searchName%'",
                        "ArsaLocation.name LIKE '%$searchName%'",
                        "Sehir.sehir_adi LIKE '%$searchName%'",
                        "Ilce.ilce_adi LIKE '%$searchName%'",
                        "Semt.semt_adi LIKE '%$searchName%'",
                        "Mahalle.mahalle_adi LIKE '%$searchName%'")),
                'joins'=>array(
                    array(
                        'table'=>'em_arsa_yer',
                        'alias'=>'ArsaYer',
                        'type'=>'INNER',
                        'conditions'=>array('Arsa.id=ArsaYer.arsa_id')
                    ),
                    array(
                        'table'=>'em_sehir',
                        'alias'=>'Sehir',
                        'type'=>'INNER',
                        'conditions'=>array('ArsaYer.sehir_id=Sehir.id')
                    ),
                    array(
                        'table'=>'em_ilce',
                        'alias'=>'Ilce',
                        'type'=>'LEFT',
                        'conditions'=>array('ArsaYer.ilce_id=Ilce.id')
                    ),
                    array(
                        'table'=>'em_semt',
                        'alias'=>'Semt',
                        'type'=>'LEFT',
                        'conditions'=>array('ArsaYer.semt_id=Semt.id')
                    ),
                    array(
                        'table'=>'em_mahalle',
                        'alias'=>'Mahalle',
                        'type'=>'LEFT',
                        'conditions'=>array('ArsaYer.mahalle_id=Mahalle.id')
                    ),
                    array(
                        'table'=>'em_arsa_location',
                        'alias'=>'ArsaLocation',
                        'type'=>'INNER',
                        'conditions'=>array('Arsa.id=ArsaLocation.arsa_id')
                    )
                )
            ));
            $pageCount = array_key_exists('page', $data)?$data['page']:1;
            if($arsaCount>(($pageCount-1)*5)){
                $this->paginate = array(
                    'fields'=>array('*'),
                    'conditions'=>array('Arsa.durum'=>1,
                        'OR'=>array("Arsa.baslik LIKE '%$searchName%'",
                            "Arsa.aciklama LIKE '%$searchName%'",
                            "Arsa.ilan_no LIKE '%$searchName%'",
                            "ArsaLocation.name LIKE '%$searchName%'",
                            "Sehir.sehir_adi LIKE '%$searchName%'",
                            "Ilce.ilce_adi LIKE '%$searchName%'",
                            "Semt.semt_adi LIKE '%$searchName%'",
                            "Mahalle.mahalle_adi LIKE '%$searchName%'")),
                    'limit'=>5,
                    'order'=>$orderA,
                    'joins'=>array(
                        array(
                            'table'=>'em_arsa_yer',
                            'alias'=>'ArsaYer',
                            'type'=>'INNER',
                            'conditions'=>array('Arsa.id=ArsaYer.arsa_id')
                        ),
                        array(
                            'table'=>'em_sehir',
                            'alias'=>'Sehir',
                            'type'=>'INNER',
                            'conditions'=>array('ArsaYer.sehir_id=Sehir.id')
                        ),
                        array(
                            'table'=>'em_ilce',
                            'alias'=>'Ilce',
                            'type'=>'LEFT',
                            'conditions'=>array('ArsaYer.ilce_id=Ilce.id')
                        ),
                        array(
                            'table'=>'em_semt',
                            'alias'=>'Semt',
                            'type'=>'LEFT',
                            'conditions'=>array('ArsaYer.semt_id=Semt.id')
                        ),
                        array(
                            'table'=>'em_mahalle',
                            'alias'=>'Mahalle',
                            'type'=>'LEFT',
                            'conditions'=>array('ArsaYer.mahalle_id=Mahalle.id')
                        ),
                        array(
                            'table'=>'em_arsa_location',
                            'alias'=>'ArsaLocation',
                            'type'=>'INNER',
                            'conditions'=>array('Arsa.id=ArsaLocation.arsa_id')
                        )
                    )
                );
                $arsas = $this->paginate('Arsa');
                $this->set('arsas',$arsas);

                $arsaRes = array();
                foreach ($arsas as $row) {
                    $arsaRes[$row['Arsa']['id']] = $this->ArsaResim->findAllByArsaId($row['Arsa']['id']);
                }
                $this->set('arsaRes',$arsaRes);
            }else{
                $this->set('arsas',false);
                $arsas = false;
            }

            
            
            if(!$konuts && !$isyeris && !$arsas){
                $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button>Aradığınız kriterlere uygun sonuç yoktur.','default', array('class'=>'alert alert-warning alert-dismissable'));
                return $this->redirect(
                array('controller' => 'users', 'action' => 'index')
                );
            }
        }
    }
    
    function iletisimedit(){
        $iletisim = $this->Iletisim->find('all',array('order'=>array('type'=>'ASC')));
        $location = $this->Location->find('first');
        $this->set('ilet',$iletisim);
        $this->set('loc',$location);
    }
    
    function iletisimkaydet(){
        $this->autoRender = false;
        if($this->request->is('post')){
            $post = $this->request->data;
            $type = $post['iletType'];
            $ilet = $post['iletName'];
            $location = $post['location'];
            $latitude = $post['latitude'];
            $longitude = $post['longitude'];
            
            $this->ButunIletSil();
            $hata = 0;
            foreach($type as $key=>$val){
                $this->Iletisim->create();
                $saveData = array('iletisim'=>$ilet[$key],'type'=>$val);
                if(!$this->Iletisim->save($saveData)){
                    $hata++;
                }
            }
            
            if(!empty($location)){
                $this->Location->create();
                $locSave = array('name'=>$location,'latitude'=>$latitude,'longitude'=>$longitude);
                if(!$this->Location->save($locSave)){
                    $hata++;
                }
            }
            if($hata > 0){
                $this->Session->setFlash('<i class="fa fa-close"></i> <button class="close" data-dismiss="alert" type="button">×</button> İletişim Bilgilerinden Bazıları Kaydedirken Bir Hata Meydana Geldi.','default', array('class'=>'alert alert-danger alert-dismissable'));
            }else{
                $this->Session->setFlash('<i class="fa fa-check"></i> <button class="close" data-dismiss="alert" type="button">×</button> İletişim Başarıyla Kaydedildi..','default', array('class'=>'alert alert-success alert-dismissable'));
            }
            
            return $this->redirect(
                array('controller' => 'users', 'action'=>'iletisimedit')
            );
            
        }else{
            return $this->redirect(
                array('controller' => 'users', 'action'=>'home')
            );
        }
    }
    
    private function ButunIletSil(){
        $this->autoRender = FALSE;
        $this->Iletisim->deleteAll(array('1 = 1'));
        $this->Location->deleteAll(array('1 = 1'));
        
    }
    
    function iletisim(){
        $iletisim = $this->Iletisim->find('all',array('order'=>array('type'=>'ASC')));
        $location = $this->Location->find('first');

        $this->HtmlMeta->addElement(array('name' => 'robots', 'content' => 'index, follow'));
        $this->HtmlMeta->addElement(array('name' => 'title', 'content' => 'İletişim - MADEN Gayrimenkul A.Ş.'));
        $this->HtmlMeta->addElement(array('name' => 'description', 'content' => 'Satılık, kiralık, konut, işyeri, arsa yatırım ve yönetimi - Mutlu Kazanç Kapısı...'));
        $this->HtmlMeta->addElement(array('property' => 'og:title', 'content' => 'İletişim - MADEN Gayrimenkul A.Ş.' ));
        $this->HtmlMeta->addElement(array('property' => 'og:description', 'content' => 'Satılık, kiralık, konut, işyeri, arsa yatırım ve yönetimi - Mutlu Kazanç Kapısı...'));
        $this->HtmlMeta->addElement(array('property' => 'og:keywords', 'content' => 'Eskişehir Yolu satılık yatırım arsaları, Temelli, Malıköy, Ihlamurkent satılık villa, Alcı, Türkobası, Anayurt, Bacı, Başkent OSB satılık arsa, kiralık fabrika, Anadolu OSB satılık fabrika arsa, ASO 2 OSB, Konutkent, Çayyolu, Westgate Residence, Kuleevo, Elmar Towers, Elya Tower, Azel Kule' ));
        $this->HtmlMeta->addElement(array('property' => 'og:url', 'content' => Router::url('/', true).'users/iletisim'));
        $this->HtmlMeta->addElement(array('property'=>'og:image', 'content'=>  Router::url('/',true).'img/shareicon/milet.png'));

        $this->set('ilet',$iletisim);
        $this->set('loc',$location);
    }
    
//    function IlanIletSil(){
//        $this->autoRender = false;
//        if($this->request->is('post')){
//            $post = $this->request->data;
//            $this->IlanIletisim->deleteAll(array('id'=>$post['IletId']));
//            $this->KonutIletisim->deleteAll(array('iletid'=>$post['IletId']));
//            $this->IsyeriIletisim->deleteAll(array('iletid'=>$post['IletId']));
//            $this->ArsaIletisim->deleteAll(array('iletid'=>$post['IletId']));
//        }
//        echo json_encode(true);
//    }
    
    function ilaniletisim(){
        $ilet = $this->IlanIletisim->find('all',array('order'=>array('type'=>'ASC')));
        $this->set('ilet',$ilet);
    }
    
    function ilaniletkaydet(){
        $this->autoRender = false;
        if($this->request->is('post')){
            $data = $this->request->data;
            $iletType = $data['iletType'];
            $iletName = $data['iletName'];
            foreach($iletType as $key=>$val){
                if($iletName[$key] != '' && !empty($iletName[$key])){
                    $this->IlanIletisim->create();
                    $this->IlanIletisim->save(array('iletisim'=>$iletName[$key],'type'=>$val));
                }
            }
        }
        return $this->redirect(
                array('controller' => 'users', 'action'=>'ilaniletisim')
            );
    }
    
    function ilaniletsil(){
        $this->autoRender = false;
        if($this->request->is('post')){
            $iletId = $this->request->data('IletId');
            $this->IlanIletisim->deleteAll(array('id'=>$iletId));
            $this->ArsaIletisim->deleteAll(array('iletid'=>$iletId));
            $this->KonutIletisim->deleteAll(array('iletid'=>$iletId));
            $this->IsyeriIletisim->deleteAll(array('iletid'=>$iletId));
            echo json_encode(true);
        }
    }
    
    function AjaxGetMapIlanInfo(){
        $this->autoRender = false;
        if($this->request->is('post')){
            $ilanId = $this->request->data('ilanId');
            $ilanTip = $this->request->data('ilanTip');
            $ilan = array();
            if($ilanTip == 1){ // Konut
                $konuts = $this->Konut->find('first',
                    array(
                    'fields'=>array('*'),
                    'conditions'=>array('Konut.id'=>$ilanId),
                    'joins'=>array(
                        array('table'=>'em_konut_yer', 'alias'=>'KonutYer', 'type'=>'INNER', 'conditions'=>array('Konut.id=KonutYer.konut_id')),
                        array('table'=>'em_sehir', 'alias'=>'Sehir', 'type'=>'INNER', 'conditions'=>array('KonutYer.sehir_id=Sehir.id')),
                        array('table'=>'em_ilce', 'alias'=>'Ilce', 'type'=>'LEFT', 'conditions'=>array('KonutYer.ilce_id=Ilce.id')),
                        array('table'=>'em_semt', 'alias'=>'Semt', 'type'=>'LEFT', 'conditions'=>array('KonutYer.semt_id=Semt.id')),
                        array('table'=>'em_mahalle', 'alias'=>'Mahalle', 'type'=>'LEFT', 'conditions'=>array('KonutYer.mahalle_id=Mahalle.id'))
                    )
                ));
                
                if($konuts){
                    $ilan['id'] = $ilanId;
                    $ilan['ilanNo'] = $konuts['Konut']['ilan_no'];
                    $ilan['baslik'] = $konuts['Konut']['baslik'];
                    $ilan['fiyat'] = $konuts['Konut']['fiyat'];
                    $ilan['fiyat_birim'] = $konuts['Konut']['parabirimi'];
                    $ilan['tarih'] = $konuts['Konut']['tarih'];
                    $ilan['il'] = $konuts['Sehir']['sehir_adi'];
                    $ilan['ilce'] = $konuts['Ilce']['ilce_adi'];
                    $ilan['semt'] = $konuts['Semt']['semt_adi'];
                    $ilan['mahalle'] = $konuts['Mahalle']['mahalle_adi'];
                    if($konuts['Konut']['sat_kir'] == 1){
                        $ilan['satkir'] = 'Satılık';
                    }else{
                        $ilan['satkir'] = 'Kiralık';
                    }
                    
                    
                    $konutResim = $this->KonutResim->find('all',array('order'=>array('id'=>'ASC'),'fields'=>array('paththumb','id'),'conditions'=>array('KonutResim.konut_id'=>$ilanId)));
                    if($konutResim){
                        foreach($konutResim as $row){
                            $ilan['img'][] = $row['KonutResim']['paththumb'];
                        }
                    }else{
                        $ilan['img'][] = 'img/nofoto.png';
                    }
                }else{
                    $ilan['hata'] = false;
                }
            }else if($ilanTip == 2){ // İşyeri
                $isyeris = $this->Isyeri->find('first',
                    array(
                    'fields'=>array('*'),
                    'conditions'=>array('Isyeri.id'=>$ilanId),
                    'joins'=>array(
                        array('table'=>'em_isyeri_yer', 'alias'=>'IsyeriYer', 'type'=>'INNER', 'conditions'=>array('Isyeri.id=IsyeriYer.isyeri_id')),
                        array('table'=>'em_sehir', 'alias'=>'Sehir', 'type'=>'INNER', 'conditions'=>array('IsyeriYer.sehir_id=Sehir.id')),
                        array('table'=>'em_ilce', 'alias'=>'Ilce', 'type'=>'LEFT', 'conditions'=>array('IsyeriYer.ilce_id=Ilce.id')),
                        array('table'=>'em_semt', 'alias'=>'Semt', 'type'=>'LEFT', 'conditions'=>array('IsyeriYer.semt_id=Semt.id')),
                        array('table'=>'em_mahalle', 'alias'=>'Mahalle', 'type'=>'LEFT', 'conditions'=>array('IsyeriYer.mahalle_id=Mahalle.id'))
                    )
                ));
                
                if($isyeris){
                    $ilan['id'] = $ilanId;
                    $ilan['ilanNo'] = $isyeris['Isyeri']['ilan_no'];
                    $ilan['baslik'] = $isyeris['Isyeri']['baslik'];
                    $ilan['fiyat'] = $isyeris['Isyeri']['fiyat'];
                    $ilan['fiyat_birim'] = $isyeris['Isyeri']['parabirimi'];
                    $ilan['tarih'] = $isyeris['Isyeri']['tarih'];
                    $ilan['il'] = $isyeris['Sehir']['sehir_adi'];
                    $ilan['ilce'] = $isyeris['Ilce']['ilce_adi'];
                    $ilan['semt'] = $isyeris['Semt']['semt_adi'];
                    $ilan['mahalle'] = $isyeris['Mahalle']['mahalle_adi'];
                    if($isyeris['Isyeri']['sat_kir'] == 1){
                        $ilan['satkir'] = 'Satılık';
                    }else{
                        $ilan['satkir'] = 'Kiralık';
                    }
                    
                    
                    $Resim = $this->IsyeriResim->find('all',array('order'=>array('id'=>'ASC'),'fields'=>array('paththumb','id'),'conditions'=>array('IsyeriResim.isyeri_id'=>$ilanId)));
                    if($Resim){
                        foreach($Resim as $row){
                            $ilan['img'][] = $row['IsyeriResim']['paththumb'];
                        }
                    }else{
                        $ilan['img'][] = 'img/nofoto.png';
                    }
                }else{
                    $ilan['hata'] = false;
                }
            }else if($ilanTip == 3){ // Arsa
                $arsas = $this->Arsa->find('first',
                    array(
                    'fields'=>array('*'),
                    'conditions'=>array('Arsa.id'=>$ilanId),
                    'joins'=>array(
                        array('table'=>'em_arsa_yer', 'alias'=>'ArsaYer', 'type'=>'INNER', 'conditions'=>array('Arsa.id=ArsaYer.arsa_id')),
                        array('table'=>'em_sehir', 'alias'=>'Sehir', 'type'=>'INNER', 'conditions'=>array('ArsaYer.sehir_id=Sehir.id')),
                        array('table'=>'em_ilce', 'alias'=>'Ilce', 'type'=>'LEFT', 'conditions'=>array('ArsaYer.ilce_id=Ilce.id')),
                        array('table'=>'em_semt', 'alias'=>'Semt', 'type'=>'LEFT', 'conditions'=>array('ArsaYer.semt_id=Semt.id')),
                        array('table'=>'em_mahalle', 'alias'=>'Mahalle', 'type'=>'LEFT', 'conditions'=>array('ArsaYer.mahalle_id=Mahalle.id'))
                    )
                ));
                
                if($arsas){
                    $ilan['id'] = $ilanId;
                    $ilan['ilanNo'] = $arsas['Arsa']['ilan_no'];
                    $ilan['baslik'] = $arsas['Arsa']['baslik'];
                    $ilan['fiyat'] = $arsas['Arsa']['fiyat'];
                    $ilan['fiyat_birim'] = $arsas['Arsa']['parabirimi'];
                    $ilan['tarih'] = $arsas['Arsa']['tarih'];
                    $ilan['il'] = $arsas['Sehir']['sehir_adi'];
                    $ilan['ilce'] = $arsas['Ilce']['ilce_adi'];
                    $ilan['semt'] = $arsas['Semt']['semt_adi'];
                    $ilan['mahalle'] = $arsas['Mahalle']['mahalle_adi'];
                    if($arsas['Arsa']['sat_kir'] == 1){
                        $ilan['satkir'] = 'Satılık';
                    }else{
                        $ilan['satkir'] = 'Kiralık';
                    }
                    
                    
                    $Resim = $this->ArsaResim->find('all',array('order'=>array('id'=>'ASC'),'fields'=>array('paththumb','id'),'conditions'=>array('ArsaResim.arsa_id'=>$ilanId)));
                    if($Resim){
                        foreach($Resim as $row){
                            $ilan['img'][] = $row['ArsaResim']['paththumb'];
                        }
                    }else{
                        $ilan['img'][] = 'img/nofoto.png';
                    }
                }else{
                    $ilan['hata'] = false;
                }
            }else{
                $ilan['hata'] = true;
            }
            
            echo json_encode($ilan);
        }else{
            $ilan['hata'] = true;
            echo json_encode($ilan);
        }
    }

    public function GetIl(){
        $this->autoRender = false;
        $il = $this->Sehir->find('all',array('order'=>'sehir_adi ASC'));
        return $il;
    }

    public function GetPagUrl(){
        $this->autoRender = false;
        $data = $this->passedArgs;
        $ekle = '';
        foreach($data as $key=>$val){
            $ekle .= '/'.$key.':'.$val;
        }
        return $ekle;
    }
    public function GetPagUrlWithoutSval(){
        $this->autoRender = false;
        $data = $this->passedArgs;
        $ekle = '';
        foreach($data as $key=>$val){
            if($key != 'sval'){
                $ekle .= '/'.$key.':'.$val;
            }
        }
        return $ekle;
    }

    public function AjaxGetBatiInfo(){
        $this->autoRender = false;
        if($this->request->is('post')){
            $IletTel = $this->Iletisim->find('first',array('conditions'=>array('type'=>1)));
            $IletMail = $this->Iletisim->find('first',array('conditions'=>array('type'=>3)));
            $IletAdres = $this->Iletisim->find('first',array('conditions'=>array('type'=>5)));
            $ilan['hata'] = false;
            $ilan['tel'] = $IletTel?$IletTel['Iletisim']['iletisim']:'';
            $ilan['adres'] = $IletAdres?$IletAdres['Iletisim']['iletisim']:'';
            $ilan['mail'] = $IletMail?$IletMail['Iletisim']['iletisim']:'';
            echo json_encode($ilan);
        }else{
            $ilan['hata'] = true;
            echo json_encode($ilan);
        }
    }

    public function globresimler(){
        /*foreach(glob(WWW_ROOT.'img/arsa/*.jpg') as $row){
            $fileinfo = pathinfo($row);
            $fileExtention = $fileinfo['extension'];
            $filebase = basename($row,'.'.$fileExtention);
            $resimler = $this->ArsaResim->find('first',array('conditions'=>array('id'=>$filebase)));
            $path = 'img/arsa/'.$resimler['ArsaResim']['arsa_id'].'/'.$filebase.'.'.$fileExtention;
            $pathRes = WWW_ROOT.$path;
            $pathThumb = 'img/arsa/'.$resimler['ArsaResim']['arsa_id'].'/'.$filebase.'thumb.'.$fileExtention;
            $pathResThumb = WWW_ROOT.$pathThumb;

            $manipulator = new ImageManipulator($row);
            $newImage = $manipulator->resample(800, 800);
            if($manipulator->save($pathRes)){
                $this->ArsaResim->id = $filebase;
                $this->ArsaResim->save(array('path'=>$path));
            }

            $manipulator = new ImageManipulator($row);
            $newImage = $manipulator->resample(200, 200);
            if($manipulator->save($pathResThumb)){
                $this->ArsaResim->updateAll(array('paththumb'=>"'$pathThumb'"),array('id'=>$filebase));
            }
        }*/
    }

    public function ilanSay(){
        $this->autoRender = false;
        $kCount = $this->Konut->find('count');
        $kSatCount = $this->Konut->find('count',array('conditions'=>array('sat_kir'=>1)));
        $kKirCount = $this->Konut->find('count',array('conditions'=>array('sat_kir'=>2)));
        $iCount = $this->Isyeri->find('count');
        $iSatCount = $this->Isyeri->find('count',array('conditions'=>array('sat_kir'=>1)));
        $iKirCount = $this->Isyeri->find('count',array('conditions'=>array('sat_kir'=>2)));
        $aCount = $this->Arsa->find('count');
        $aSatCount = $this->Arsa->find('count',array('conditions'=>array('sat_kir'=>1)));
        $aKirCount = $this->Arsa->find('count',array('conditions'=>array('sat_kir'=>2)));
        $projeCount = $this->Proje->find('count');
        $haberCount = $this->Haber->find('count');
        $teknikCount = $this->TeknikAnaliz->find('count');
        return array(
            'kCount'=>$kCount,
            'kSatCount'=>$kSatCount,
            'kKirCount'=>$kKirCount,
            'iCount'=>$iCount,
            'iSatCount'=>$iSatCount,
            'iKirCount'=>$iKirCount,
            'aCount'=>$aCount,
            'aSatCount'=>$aSatCount,
            'aKirCount'=>$aKirCount,
            'projeCount'=>$projeCount,
            'haberCount'=>$haberCount,
            'teknikCount'=>$teknikCount
        );
    }

    function homeslider(){
        $slayts = $this->Slider->find('all',array('order'=>array('sira'=>'ASC')));
        $this->set('slayts',$slayts);
    }
    
    function addslayt(){
        $this->autoRender = false;
        if($this->request->is('post')){
            $files = array_key_exists('Res', $_FILES)?$_FILES['Res']:false;
            $hata = 0;
            foreach ($files['error'] as $key=>$value){
                if($value == 0 && $files['type'][$key] != "image/jpeg" && $files['type'][$key] != "image/gif" && $files['type'][$key] != "image/png" && $files['type'][$key] != "image/tiff"){
                    $hata++;
                }else{
                    $fileType = pathinfo($files['name'][$key], PATHINFO_EXTENSION);
                    $name = time().'_'.rand(0,100).'.'.$fileType;
                    $pathRes = WWW_ROOT.'img/slayt/'.$name;
                    $path = 'img/slayt/'.$name;
                    //image Resize
                    $manipulator = new ImageManipulator($files['tmp_name'][$key]);
                    $manipulator->resample(720, 390);
                    if($manipulator->save($pathRes)){
                        $sira = 1;
                        $lastSlider = $this->Slider->find('first',array('order'=>array('sira'=>'DESC')));
                        if($lastSlider){
                            $sira += $lastSlider['Slider']['sira'];
                        }
                        $saved = array('path'=>$path,'sira'=>$sira);
                        $this->Slider->create();
                        if(!$this->Slider->save($saved)){
                            $hata++;
                        }
                    }else{
                        $hata++;
                    }
                }
            }

            if($hata>0){
                $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button>Yüklemek istediğiniz resimler sisteme yüklenememiştir. Sonra tekrar deneyin.','default', array('class'=>'alert alert-danger alert-dismissable'));
            }else{
                $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button>Resimler başarıyla sisteme yüklendi.','default', array('class'=>'alert alert-success alert-dismissable'));
            }
        }else{
            $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button>Yüklemek istediğiniz resimler sisteme yüklenememiştir. Sonra tekrar deneyin.','default', array('class'=>'alert alert-danger alert-dismissable'));
        }

        return $this->redirect(
            array('controller' => 'users', 'action' => 'homeslider')
        );
    }

    public function slidersiradegis(){
        $this->autoRender = false;
        $return['sonuc'] = false;
        if($this->request->is('post')){
            $data = $this->request->data;
            $sira = $data['sira'];
            $sId = $data['sId'];
            $this->Slider->id = $sId;
            if($this->Slider->save(array('sira'=>$sira))){
                $return['sonuc'] = true;
            }
        }
        echo json_encode($return);
    }

    public function slidesil(){
        $this->autoRender = false;
        $return['sonuc'] = false;
        if($this->request->is('post')){
            $data = $this->request->data;
            $sId = $data['sId'];
            $slide = $this->Slider->findById($sId);
            $fileRes = new File($slide['Slider']['path']);
            $fileRes->delete();
            if($this->Slider->deleteAll(array('id'=>$sId))){
                $return['sonuc'] = true;
            }
        }
        echo json_encode($return);
    }
	
	public function uploadimage(){
		$this->autoRender = false;
		$uploader = new FilerUploader();
		$data = $uploader->upload($_FILES['files'], array(
			'limit' => 10, //Maximum Limit of files. {null, Number}
			'maxSize' => 10, //Maximum Size of files {null, Number(in MB's)}
			'extensions' => null, //Whitelist for file extension. {null, Array(ex: array('jpg', 'png'))}
			'required' => false, //Minimum one file is required for upload {Boolean}
			'uploadDir' => 'img/gecici/', //Upload directory {String}
			'title' => array('randomVEtime'), //New file name {null, String, Array} *please read documentation in README.md
			'removeFiles' => true, //Enable file exclusion {Boolean(extra for jQuery.filer), String($_POST field name containing json data with file names)}
			'replace' => false, //Replace the file if it already exists  {Boolean}
			'perms' => null, //Uploaded file permisions {null, Number}
			'onCheck' => null, //A callback function name to be called by checking a file for errors (must return an array) | ($file) | Callback
			'onError' => null, //A callback function name to be called if an error occured (must return an array) | ($errors, $file) | Callback
			'onSuccess' => null, //A callback function name to be called if all files were successfully uploaded | ($files, $metas) | Callback
			'onUpload' => null, //A callback function name to be called if all files were successfully uploaded (must return an array) | ($file) | Callback
			'onComplete' => null, //A callback function name to be called when upload is complete | ($file) | Callback
			'onRemove' => null //A callback function name to be called by removing files (must return an array) | ($removed_files) | Callback
		));

		if($data['isComplete']){
			$files = $data['data'];

			echo json_encode($files['metas'][0]['name']);
		}

		if($data['hasErrors']){
			$errors = $data['errors'];
			echo json_encode($errors);
		}
	}
	
	public function removeimage(){
		$this->autoRender = false;
		if(isset($_POST['file'])){
			$file = 'img/gecici/' . $_POST['file'];
			if(file_exists($file)){
				unlink($file);
			}
		}
		echo json_encode(true);
	}
}