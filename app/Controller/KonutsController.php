<?php
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('ImageManipulator','Vendor');
class KonutsController extends AppController{
    var $uses = array('Konut','Sehir','Ilce','Semt','KonutYer','KonutResim','Tip','KonutLocation','Mahalle','IlanNo',
        'IlanIletisim','KonutIletisim','IlanIletisimType');
    public $components = array('Paginator','HtmlMeta');

    public function konutindex(){}

    public function home(){
        
        $named = $this->request->params['named'];
        if(array_key_exists('SatKir', $named) && ($named['SatKir']==1 || $named['SatKir']==2)){
           $SatKir = $named['SatKir'];
           
           if(array_key_exists('tarih', $named)){
               if($named['tarih']=='desc'){
                   $order = array('Konut.tarih'=>'DESC');
               }else if($named['tarih']=='asc'){
                   $order = array('Konut.tarih'=>'ASC');
               }
           }else if(array_key_exists('fiyat', $named)){
               if($named['fiyat']=='desc'){
                   $order = array('Konut.fiyat'=>'DESC');
               }else if($named['fiyat'] == 'asc'){
                   $order = array('Konut.fiyat'=>'ASC');
               }
           }else{
               $order = array('Konut.tarih'=>'DESC');
           }
           $this->set('sirala',$order);
           
           $this->paginate = array(
                'fields'=>array('*'),
                'limit'=>10,
                'conditions'=>array('Konut.durum'=>1,'Konut.sat_kir'=>$SatKir),
                'order'=>$order,
                'joins'=>array(
                    array(
                        'table'=>'em_konut_yer',
                        'alias'=>'KonutYer',
                        'type'=>'INNER',
                        'conditions'=>array('Konut.id=KonutYer.konut_id')
                    ),
                    array(
                        'table'=>'em_sehir',
                        'alias'=>'Sehir',
                        'type'=>'INNER',
                        'conditions'=>array('KonutYer.sehir_id=Sehir.id')
                    ),
                    array(
                        'table'=>'em_ilce',
                        'alias'=>'Ilce',
                        'type'=>'LEFT',
                        'conditions'=>array('KonutYer.ilce_id=Ilce.id')
                    ),
                    array(
                        'table'=>'em_semt',
                        'alias'=>'Semt',
                        'type'=>'LEFT',
                        'conditions'=>array('KonutYer.semt_id=Semt.id')
                    ),
                    array(
                        'table'=>'em_mahalle',
                        'alias'=>'Mahalle',
                        'type'=>'LEFT',
                        'conditions'=>array('KonutYer.mahalle_id=Mahalle.id')
                    )
                )
            );
        }
        else{
            if(array_key_exists('tarih', $named)){
               if($named['tarih']=='desc'){
                   $order = array('Konut.tarih'=>'DESC');
               }else if($named['tarih']=='asc'){
                   $order = array('Konut.tarih'=>'ASC');
               }
           }else if(array_key_exists('fiyat', $named)){
               if($named['fiyat']=='desc'){
                   $order = array('Konut.fiyat'=>'DESC');
               }else if($named['fiyat'] == 'asc'){
                   $order = array('Konut.fiyat'=>'ASC');
               }
           }else{
               $order = array('Konut.tarih'=>'DESC');
           }
           $this->set('sirala',$order);
           
            $this->paginate = array(
                'fields'=>array('*'),
                'limit'=>10,
                'conditions'=>array('Konut.durum'=>1),
                'order'=>$order,
                'joins'=>array(
                    array(
                        'table'=>'em_konut_yer',
                        'alias'=>'KonutYer',
                        'type'=>'INNER',
                        'conditions'=>array('Konut.id=KonutYer.konut_id')
                    ),
                    array(
                        'table'=>'em_sehir',
                        'alias'=>'Sehir',
                        'type'=>'INNER',
                        'conditions'=>array('KonutYer.sehir_id=Sehir.id')
                    ),
                    array(
                        'table'=>'em_ilce',
                        'alias'=>'Ilce',
                        'type'=>'LEFT',
                        'conditions'=>array('KonutYer.ilce_id=Ilce.id')
                    ),
                    array(
                        'table'=>'em_semt',
                        'alias'=>'Semt',
                        'type'=>'LEFT',
                        'conditions'=>array('KonutYer.semt_id=Semt.id')
                    ),
                    array(
                        'table'=>'em_mahalle',
                        'alias'=>'Mahalle',
                        'type'=>'LEFT',
                        'conditions'=>array('KonutYer.mahalle_id=Mahalle.id')
                    )
                )
            );
        }
        
        $konuts = $this->paginate('Konut');
        $this->set('konuts',$konuts);
        
        if(!$konuts){
            $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button>Aradığınız kriterlere uygun sonuç yoktur.','default', array('class'=>'alert alert-danger alert-dismissable'));
            return $this->redirect(Router::url('/',true));
        }
        $konutRes = array();
        foreach ($konuts as $row) {
            $konutRes[$row['Konut']['id']] = $this->KonutResim->findAllByKonutId($row['Konut']['id']);
        }
        $this->set('konutRes',$konutRes);

        $this->HtmlMeta->addElement(array('name' => 'robots', 'content' => 'index, follow'));
        $this->HtmlMeta->addElement(array('name' => 'title', 'content' => 'Konut - Maden Gayrimenkul A.Ş.'));
        $this->HtmlMeta->addElement(array('name' => 'description', 'content' => 'Satılık, kiralık, konut, işyeri, arsa yatırım ve yönetimi - Mutlu Kazanç Kapısı...'));
        $this->HtmlMeta->addElement(array('property' => 'og:title', 'content' => 'Konut - Maden Gayrimenkul A.Ş.' ));
        $this->HtmlMeta->addElement(array('property' => 'og:description', 'content' => 'Satılık, kiralık, konut, işyeri, arsa yatırım ve yönetimi - Mutlu Kazanç Kapısı...'));
        $this->HtmlMeta->addElement(array('property' => 'og:keywords', 'content' => 'Eskişehir Yolu satılık yatırım arsaları, Temelli, Malıköy, Ihlamurkent satılık villa, Alcı, Türkobası, Anayurt, Bacı, Başkent OSB satılık arsa, kiralık fabrika, Anadolu OSB satılık fabrika arsa, ASO 2 OSB, Konutkent, Çayyolu, Westgate Residence, Kuleevo, Elmar Towers, Elya Tower, Azel Kule' ));
        $this->HtmlMeta->addElement(array('property' => 'og:url', 'content' => Router::url('/', true)));
        $this->HtmlMeta->addElement(array('property'=>'og:image', 'content'=>  Router::url('/',true).'img/shareicon/mkon.png'));
    }
    
    public function yenikonut(){
        $il = $this->Sehir->find('all',array('order'=>'sehir_adi ASC'));
        $this->set('il',$il);
        $iletisim = $this->IlanIletisim->find('all');
        $this->set('iletisim',$iletisim);
        $this->HtmlMeta->addElement(array('name' => 'robots', 'content' => 'noindex, nofollow'));
    }
    
    public function addkonut(){
        $this->autoRender = false;
        $files = array_key_exists('konutRes', $_FILES)?$_FILES['konutRes']:false;
        //$filesVideo = array_key_exists('konutVideo', $_FILES)?$_FILES['konutVideo']:false;
        $data = $this->request->data;
		$yuklenenfile = array_key_exists('yuklenenfile',$data)?$data['yuklenenfile']:false;
        $this->Konut->create();
        $saveData = array('tip'=>1,
            'baslik'=>$data['baslik'],
            'aciklama'=>$data['aciklama'],
            'oda_sayisi'=>$data['oda'],
            'm_kare'=>$data['m2'],
            'kat'=>$data['kat'],
            'bina_kat'=>$data['binakat'],
            'kredi'=>$data['kredi'],
            'fiyat'=>$data['fiyat'],
            'sat_kir'=>$data['sat_kir'],
            'durum'=>1,
            'tarih'=> date('Y-m-d'),
            'parabirimi'=>$data['paraBirimi'],
            'mal_name'=>$data['malName'],
            'mal_tel'=>$data['malTelNo'],
            'mal_fiyat'=>$data['malFiyat'],
            'mal_parabirimi'=>$data['malParaBirimi'],
            'video'=>$data['video']);
        
        if($this->Konut->save($saveData)){
            $konutId = $this->Konut->getInsertID();
            $ilanNo = $this->IlanNo->find('first');
//            $konutilanno = 'K'.$konutId;
            $konutilanno = $ilanNo['IlanNo']['ilan_no']+1;
            $test = $this->IlanNo->updateAll(array('ilan_no'=>$konutilanno),array('id'=>1));
            $this->Konut->updateAll(array('ilan_no'=>"'$konutilanno'"), array('id'=>$konutId));

            //KONUTYER
            $this->KonutYer->create();
            $yerArray = array();
            $yerArray['sehir_id'] = $data['il'];
            $yerArray['ilce_id'] = array_key_exists('ilce', $data)?$data['ilce']:null;
            $yerArray['semt_id'] = array_key_exists('semt', $data)?$data['semt']:null;
            $yerArray['mahalle_id'] = array_key_exists('mahalle', $data)?$data['mahalle']:null;
            
            $yerArray['konut_id'] = $konutId;
            $this->KonutYer->save($yerArray);
            
            //Konut Maps
            $this->KonutLocation->create();
            $this->KonutLocation->save(array('konut_id'=>$konutId,'name'=>$data['location'],'latitude'=>$data['latitude'],'longitude'=>$data['longitude']));
            
            // Konut İletisim Bilgileri
            /*$Ilet = array_key_exists('ilanilet',$data)?$data['ilanilet']:false;
            foreach($Ilet as $row){
                $this->KonutIletisim->create();
                $this->KonutIletisim->save(array('konut_id'=>$konutId,'ilet_id'=>$row));
            }*/
            
            $hataVid = 0;
            $hataRes = 0;
            if($yuklenenfile){
                foreach($yuklenenfile as $value){
						$asilpath = WWW_ROOT.'img/gecici/'.$value;
                        $fileType = pathinfo($value, PATHINFO_EXTENSION);
                        $this->KonutResim->create();
                        $this->KonutResim->save(array('konut_id'=>$konutId));
                        $lastId = $this->KonutResim->getLastInsertID();
                        $pathRes = WWW_ROOT.'img/konut/'.$konutId.'/'.$lastId.'.'.$fileType;
                        $path = 'img/konut/'.$konutId.'/'.$lastId.'.'.$fileType;
                        $pathResThumb = WWW_ROOT.'img/konut/'.$konutId.'/'.$lastId.'thumb.'.$fileType;
                        $pathThumb = 'img/konut/'.$konutId.'/'.$lastId.'thumb.'.$fileType;
                        //image Resize
                        $manipulator = new ImageManipulator($asilpath);
                        $newImage = $manipulator->resample(800, 600, false);
                        if($manipulator->save($pathRes)){
                            $this->KonutResim->id = $lastId;
                            $this->KonutResim->save(array('path'=>$path));
                            $manipulator = new ImageManipulator($pathRes);
                            $newImage = $manipulator->resample(200, 200);
                            if($manipulator->save($pathResThumb)){
                                $this->KonutResim->updateAll(array('paththumb'=>"'$pathThumb'"),array('id'=>$lastId));
                                if(file_exists($asilpath)){
									unlink($asilpath);
								}
                            }
                        }else{
                            $hataRes++;
                            $this->KonutResim->deleteAll(array('id'=>$lastId));
                        }
                }
            }
            
            if($hataVid>0){
                    $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button>Yüklemek istediğiniz video sisteme yüklenememiştir. Sonra tekrar deneyin.','default', array('class'=>'alert alert-danger alert-dismissable'));
                }
            
            if($hataRes>0){
                $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button>Yüklemek istediğiniz resimlerden bazıları sisteme yüklenememiştir. Sonra tekrar deneyin.','default', array('class'=>'alert alert-danger alert-dismissable'));
            }else{
                $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button>Konut kayıt işlemi başarıyla sonuçlandı.','default', array('class'=>'alert alert-success alert-dismissable'));
            }
        }else{
            $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button>Konut kayıt işlemi başarısız. Lütfen tekrar deneyin.','default', array('class'=>'alert alert-danger alert-dismissable'));
        }
        return $this->redirect(
        array('controller' => 'konuts', 'action' => 'yenikonut')
        );
    }
    
    function konut(){
        if($this->request->params['named']['ilanNo']){
           $ilanNo = $this->request->params['named']['ilanNo'];

            /*if(!$this->Session->check('ilanNo.'.$ilanNo) && rand(1,5) == 3){
                $this->Session->write('ilanNo.'.$ilanNo,1);
                $this->redirect(SHORTE_URL.'konut/ilanNo:'.$ilanNo);
            }*/

            $konuts = $this->Konut->find('first',array(
            'fields'=>array('*'),
            'contain'=>array('KonutDanisman'=>array('Danisman'=>array('DanismanIletisim'=>array('IlanIletisimType')))),
            'conditions'=>array("Konut.ilan_no LIKE '%$ilanNo%'","Konut.durum"=>1),
            'joins'=>array(
                array('table'=>'em_konut_yer', 'alias'=>'KonutYer', 'type'=>'INNER', 'conditions'=>array('Konut.id=KonutYer.konut_id')),
                array('table'=>'em_sehir', 'alias'=>'Sehir', 'type'=>'INNER', 'conditions'=>array('KonutYer.sehir_id=Sehir.id')),
                array('table'=>'em_ilce', 'alias'=>'Ilce', 'type'=>'LEFT', 'conditions'=>array('KonutYer.ilce_id=Ilce.id')),
                array('table'=>'em_semt', 'alias'=>'Semt', 'type'=>'LEFT', 'conditions'=>array('KonutYer.semt_id=Semt.id')),
                array('table'=>'em_mahalle', 'alias'=>'Mahalle', 'type'=>'LEFT', 'conditions'=>array('KonutYer.mahalle_id=Mahalle.id')),
                array('table'=>'em_konut_location', 'alias'=>'KonutLocation', 'type'=>'LEFT', 'conditions'=>array('Konut.id=KonutLocation.konut_id'))
            )
        ));
        
        if(!$konuts){
            $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button>Aramış olduğunuz ilan kaldırılmıştır.','default', array('class'=>'alert alert-info alert-dismissable'));
            return $this->redirect(
            array('controller' => 'konuts', 'action' => 'home')
            );    
        }
        $this->set('konuts',$konuts);

        $konutRes = $this->KonutResim->find('all',array('order'=>array('sira'=>'ASC'),'fields'=>array('*'),'conditions'=>array('konut_id'=>$konuts['Konut']['id'])));
        $this->set('konutRes',$konutRes);

            $IlanTel = $this->KonutIletisim->find('all',
                array(
                    'fields'=>array('IlanIlet.*','IlanIletType.*'),
                    'conditions'=>array('KonutIletisim.konut_id'=>$konuts['Konut']['id'],'IlanIlet.type'=>1),
                    'orders'=>array('IlanIlet.type'=>'ASC'),
                    'joins'=>array(
                        array('table'=>'em_ilaniletisim', 'alias'=>'IlanIlet', 'type'=>'INNER', 'conditions'=>array('KonutIletisim.ilet_id = IlanIlet.id')),
                        array('table'=>'em_ilaniletisimtype', 'alias'=>'IlanIletType', 'type'=>'INNER', 'conditions'=>array('IlanIlet.type = IlanIletType.id'))
                    )
                )
            );
            $this->set('IlanTel',$IlanTel);

            $IlanMail = $this->KonutIletisim->find('all',
                array(
                    'fields'=>array('IlanIlet.*','IlanIletType.*'),
                    'conditions'=>array('KonutIletisim.konut_id'=>$konuts['Konut']['id'],'IlanIlet.type'=>3),
                    'orders'=>array('IlanIlet.type'=>'ASC'),
                    'joins'=>array(
                        array('table'=>'em_ilaniletisim', 'alias'=>'IlanIlet', 'type'=>'INNER', 'conditions'=>array('KonutIletisim.ilet_id = IlanIlet.id')),
                        array('table'=>'em_ilaniletisimtype', 'alias'=>'IlanIletType', 'type'=>'INNER', 'conditions'=>array('IlanIlet.type = IlanIletType.id'))
                    )
                )
            );
            $this->set('IlanMail',$IlanMail);

            $IlanAdres = $this->KonutIletisim->find('all',
                array(
                    'fields'=>array('IlanIlet.*','IlanIletType.*'),
                    'conditions'=>array('KonutIletisim.konut_id'=>$konuts['Konut']['id'],'IlanIlet.type'=>5),
                    'orders'=>array('IlanIlet.type'=>'ASC'),
                    'joins'=>array(
                        array('table'=>'em_ilaniletisim', 'alias'=>'IlanIlet', 'type'=>'INNER', 'conditions'=>array('KonutIletisim.ilet_id = IlanIlet.id')),
                        array('table'=>'em_ilaniletisimtype', 'alias'=>'IlanIletType', 'type'=>'INNER', 'conditions'=>array('IlanIlet.type = IlanIletType.id'))
                    )
                )
            );
            $this->set('IlanAdres',$IlanAdres);
            $this->set('IlanNo',$ilanNo);

        $this->HtmlMeta->addElement(array('name' => 'robots', 'content' => 'index, follow'));
        $this->HtmlMeta->addElement(array('name' => 'title', 'content' => $konuts['Konut']['baslik']));
        $this->HtmlMeta->addElement(array('name' => 'description', 'content' => strip_tags($konuts['Konut']['aciklama'])));
        $this->HtmlMeta->addElement(array('property' => 'og:title', 'content' => $konuts['Konut']['baslik'] ));
        $this->HtmlMeta->addElement(array('property' => 'og:description', 'content' => strip_tags($konuts['Konut']['aciklama'])));
        $this->HtmlMeta->addElement(array('property' => 'og:keywords', 'content' => $konuts['Konut']['baslik'] ));
        $this->HtmlMeta->addElement(array('property' => 'og:url', 'content' => Router::url('/', true).'konut/ilanNo:'.$ilanNo));
		
		if($konutRes){
            $this->HtmlMeta->addElement(array('property'=>'og:image', 'content'=>  Router::url('/',true).$konutRes[0]['KonutResim']['path']));
        }else{
            $this->HtmlMeta->addElement(array('property'=>'og:image', 'content'=>  Router::url('/',true).'img/logo-xxs.png'));
        }
		
		$video_code = false;
		if (preg_match("/youtube.com/", $konuts['Konut']['aciklama']) || preg_match("/youtu.be/", $konuts['Konut']['aciklama'])){
                    if (preg_match('%(?:youtube\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $konuts['Konut']['aciklama'], $match))
                    {
                        $video_code = $match[1];
                    }
                   $source = 'http://www.youtube.com/e/'.$video_code; 
               $picture = 'http://img.youtube.com/vi/'.$video_code.'/0.jpg';
                }
                else if (preg_match("/vimeo.com/", $konuts['Konut']['aciklama']))
                {
                    if (preg_match('/vimeo\.com\/(clip\:)?(\d+).*$/', $konuts['Konut']['aciklama'], $match))
                    {
                        $video_code = $match[2];
                     }
                    /* Get Vimeo thumbnail */
                    $hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/$video_code.php"));
                    $picture = $hash[0]['thumbnail_medium'];  
                    $source = 'https://secure.vimeo.com/moogaloop.swf?clip_id='.$video_code.'&autoplay=1';
                }

        if($video_code){
			$this->HtmlMeta->addElement(array('property' => 'og:type', 'content' => 'video' ));
			$this->HtmlMeta->addElement(array('property' => 'og:video', 'content' => $source ));
			/*$this->HtmlMeta->addElement(array('property' => 'og:video:width', 'content' => '374' ));
			$this->HtmlMeta->addElement(array('property' => 'og:video:height', 'content' => '202' ));*/
			$this->HtmlMeta->addElement(array('property' => 'og:video:type', 'content' => 'application/x-shockwave-flash' ));
		}
        
        }else{
            return $this->redirect(
            array('controller' => 'konuts', 'action' => 'home')
            );    
        }
    }
    
    public function duzenle(){
        if($this->request->params['named']['ilanNo']){
           $ilanNo = $this->request->params['named']['ilanNo']; 
           
           $konuts = $this->Konut->find('first',array(
            'fields'=>array('*'),
            'conditions'=>array("ilan_no LIKE '%$ilanNo%'"),
            'joins'=>array(
                array(
                    'table'=>'em_konut_yer',
                    'alias'=>'KonutYer',
                    'type'=>'INNER',
                    'conditions'=>array('Konut.id=KonutYer.konut_id')
                ),
                array(
                    'table'=>'em_sehir',
                    'alias'=>'Sehir',
                    'type'=>'INNER',
                    'conditions'=>array('KonutYer.sehir_id=Sehir.id')
                ),
                array(
                    'table'=>'em_ilce',
                    'alias'=>'Ilce',
                    'type'=>'LEFT',
                    'conditions'=>array('KonutYer.ilce_id=Ilce.id')
                ),
                array(
                    'table'=>'em_semt',
                    'alias'=>'Semt',
                    'type'=>'LEFT',
                    'conditions'=>array('KonutYer.semt_id=Semt.id')
                ),
                array(
                    'table'=>'em_mahalle',
                    'alias'=>'Mahalle',
                    'type'=>'LEFT',
                    'conditions'=>array('KonutYer.mahalle_id=Mahalle.id')
                ),
                array(
                    'table'=>'em_konut_location',
                    'alias'=>'KonutLocation',
                    'type'=>'LEFT',
                    'conditions'=>array('Konut.id=KonutLocation.konut_id')
                )
            )
        ));
        
        $this->set('konuts',$konuts);
        if($konuts['Konut']['user_id'] != $this->Session->read('UserId') && $this->Session->read('UserGroup') != 1){
            $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button>Bu ilanı düzeltme yetkiniz yoktur.','default', array('class'=>'alert alert-danger alert-dismissable'));
                return $this->redirect(
                array('controller' => 'konuts', 'action' => 'home')
                );
        }
        
        $konutRes = $this->KonutResim->findAllByKonutId($konuts['Konut']['id']);
        $this->set('konutRes',$konutRes);
           
        $il = $this->Sehir->find('all',array('order'=>'id ASC'));
        $this->set('il',$il);
        $ilce = $this->Ilce->find('all',array('conditions'=>array('sehir_id'=>$konuts['KonutYer']['sehir_id']),'order'=>'id ASC'));
        $this->set('ilce',$ilce);
        $semt = $this->Semt->find('all',array('conditions'=>array('ilce_id'=>$konuts['KonutYer']['ilce_id']),'order'=>'id ASC'));
        $this->set('semt',$semt);
        $mahalle = $this->Mahalle->find('all',array('conditions'=>array('semt_id'=>$konuts['KonutYer']['semt_id']),'order'=>'id ASC'));
        $this->set('mahalle',$mahalle);
        
        $iletisim = $this->IlanIletisim->find('all');
        $this->set('iletisim',$iletisim);
        $KonutIletisim = $this->KonutIletisim->find('all',array(
            'fields'=>array('*'),
            'conditions'=>array("KonutIletisim.konut_id"=>$konuts['Konut']['id']),
            'joins'=>array(
                array(
                    'table'=>'em_ilaniletisim',
                    'alias'=>'IlanIlet',
                    'type'=>'INNER',
                    'conditions'=>array('KonutIletisim.ilet_id = IlanIlet.id')
                )
                )
            ));
        $this->set('KonutIlet',$KonutIletisim);
            $this->HtmlMeta->addElement(array('name' => 'robots', 'content' => 'noindex, nofollow'));
        }else{
            $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button>Bir hata meydana geldi. Lütfen tekrar deneyin.','default', array('class'=>'alert alert-danger alert-dismissable'));
            return $this->redirect(
            array('controller' => 'konuts', 'action' => 'home')
            );    
        }
    }
    
    public function deleteResim(){
        $this->autoRender = false;
        if($this->request->is('post')){
            $ResId = $this->request->data('resId');
            $resim = $this->KonutResim->find('first',array('conditions'=>array('id'=>$ResId)));
            $fileRes = new File($resim['KonutResim']['path']);
            $fileRes->delete();
            $fileResThumb = new File($resim['KonutResim']['paththumb']);
            if($fileResThumb){
                $fileResThumb->delete();
            }

            if($this->KonutResim->deleteAll(array('id'=>$ResId))){
                echo json_encode(true);
            }else{
                echo json_encode(false);
            }
        }else{
            echo json_encode(false);
        }
    }
    
    public function upgradekonut(){
        $this->autoRender = false;
        if($this->request->params['named']['ilanNo']){
           $ilanNo = $this->request->params['named']['ilanNo']; 
           
           $konut = $this->Konut->find('first',array('conditions'=>array("ilan_no"=>$ilanNo)));
           $konutId = $konut['Konut']['id'];
           $this->Konut->id = $konutId;
           
            $files = array_key_exists('konutRes', $_FILES)?$_FILES['konutRes']:false;
            //$filesVideo = array_key_exists('konutVideo', $_FILES)?$_FILES['konutVideo']:false;
            $data = $this->request->data;
            $baslik = $data['baslik'];
            $aciklama = $data['aciklama'];
            $odasayisi = $data['oda'];
            $mkare = $data['m2'];
            $kat = $data['kat'];
            $binakat = $data['binakat'];
            $kredi = $data['kredi'];
            $fiyat = $data['fiyat'];
            $paraBirim = $data['paraBirimi'];
            $saveData = array(
                'baslik'=>$baslik,
                'aciklama'=>$aciklama,
                'oda_sayisi'=>$odasayisi,
                'm_kare'=>$mkare,
                'kat'=>$kat,
                'bina_kat'=>$binakat,
                'kredi'=>$kredi,
                'fiyat'=>$fiyat,
                'sat_kir'=>$data['sat_kir'],
                'parabirimi'=>$paraBirim,
                'mal_name'=>$data['malName'],
                'mal_tel'=>$data['malTelNo'],
                'mal_fiyat'=>$data['malFiyat'],
                'mal_parabirimi'=>$data['malParaBirimi'],
                'video'=>$data['video']);

            if($this->Konut->save($saveData)){
                $this->KonutYer->deleteAll(array('konut_id'=>$konutId));
                //KONUTYER
                $this->KonutYer->create();
                $yerArray = array();
                $yerArray['sehir_id'] = $data['il'];
                $yerArray['ilce_id'] = array_key_exists('ilce', $data)?$data['ilce']:null;
                $yerArray['semt_id'] = array_key_exists('semt', $data)?$data['semt']:null;
                $yerArray['mahalle_id'] = array_key_exists('mahalle', $data)?$data['mahalle']:null;
                $yerArray['konut_id'] = $konutId;
                $this->KonutYer->save($yerArray);

                $this->KonutLocation->deleteAll(array('konut_id'=>$konutId));
                //Konut Maps
                $this->KonutLocation->create();
                $this->KonutLocation->save(array('konut_id'=>$konutId,'name'=>$data['location'],'latitude'=>$data['latitude'],'longitude'=>$data['longitude']));
                // Konut İletisim Bilgileri
                $konutIlet = array_key_exists('ilanilet',$data)?$data['ilanilet']:false;
                $this->KonutIletisim->deleteAll(array('konut_id'=>$konutId));
                foreach($konutIlet as $row){
                    $this->KonutIletisim->create();
                    $this->KonutIletisim->save(array('konut_id'=>$konutId,'ilet_id'=>$row));
                }
                
                $hataVid = 0;
                $hataRes = 0;
                if($files){
                    foreach($files['error'] as $key=>$value){
                        if($value == 0 && $files['type'][$key] != "image/jpeg" && $files['type'][$key] != "image/gif" && $files['type'][$key] != "image/png" && $files['type'][$key] != "image/tiff"){
                            $hataRes++;
                        }
                        else{
                            $fileType = pathinfo($files['name'][$key], PATHINFO_EXTENSION);
                            //$fileType = explode('.', $files['name'][$key]);
                            //$fileType = $fileType[count($fileType)-1];
                            $this->KonutResim->create();
                            $this->KonutResim->save(array('konut_id'=>$konutId));
                            $lastId = $this->KonutResim->getLastInsertID();
                            $pathRes = WWW_ROOT.'img/konut/'.$konutId.'/'.$lastId.'.'.$fileType;
                            $pathResThumb = WWW_ROOT.'img/konut/'.$konutId.'/'.$lastId.'thumb.'.$fileType;
                            $path = 'img/konut/'.$konutId.'/'.$lastId.'.'.$fileType;
                            $pathThumb = 'img/konut/'.$konutId.'/'.$lastId.'thumb.'.$fileType;
                            //image Resize
                            $manipulator = new ImageManipulator($files['tmp_name'][$key]);
                            $newImage = $manipulator->resample(800, 600, false);
//                            move_uploaded_file($files['tmp_name'][$key], $pathRes)
                            if($manipulator->save($pathRes)){
                                $this->KonutResim->id = $lastId;
                                $this->KonutResim->save(array('path'=>$path));
                                $manipulator = new ImageManipulator($pathRes);
                                $newImage = $manipulator->resample(200, 200);
                                if($manipulator->save($pathResThumb)){
                                    $this->KonutResim->updateAll(array('paththumb'=>"'$pathThumb'"),array('id'=>$lastId));
                                    /*$this->KonutResim->id = $lastId;
                                    $this->KonutResim->save(array('paththumb'=>$pathThumb));*/
                                }
                            }else{
                                $hataRes++;
                                $this->KonutResim->deleteAll(array('id'=>$lastId));
                            }
                        }
                    }
                }
                if($hataVid>0){
                    $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button>Yüklemek istediğiniz video sisteme yüklenememiştir. Sonra tekrar deneyin.','default', array('class'=>'alert alert-danger alert-dismissable'));
                }
                if($hataRes>0){
                    $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button>Yüklemek istediğiniz resimlerden bazıları sisteme yüklenememiştir. Sonra tekrar deneyin.','default', array('class'=>'alert alert-danger alert-dismissable'));
                }else{
                    $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button>Konut düzenleme işlemi başarıyla sonuçlandı.','default', array('class'=>'alert alert-success alert-dismissable'));
                    return $this->redirect(
                    array('controller' => 'konuts', 'action' => 'duzenle/ilanNo:'.$ilanNo)
                    );
                }
            }else{
                $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button>Konut düzenleme işlemi başarısız. Lütfen tekrar deneyin.','default', array('class'=>'alert alert-danger alert-dismissable'));
            }
            return $this->redirect(
            array('controller' => 'konuts', 'action' => 'yenikonut')
            );
        }else{
            $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button>Konut düzenleme işlemi başarısız. Lütfen tekrar deneyin.','default', array('class'=>'alert alert-danger alert-dismissable'));
            return $this->redirect(
            array('controller' => 'konuts', 'action' => 'home')
            );
        }
    }
    
    public function getIlce(){
        $this->autoRender = false;
        if($this->request->is('post')){
            $il = $this->request->data('il');
//            $ilce = $this->Ilce->find('all',array('conditions'=>array('sehir_id'=>$il),'order'=>array('ilce_adi'=>'ASC')));
            $ilce = $this->Ilce->findAllBySehirId($il);
            echo json_encode($ilce);
        }else{
            echo json_encode(false);
        }
    }
    
    public function getSemt(){
        $this->autoRender = false;
        if($this->request->is('post')){
            $ilce = $this->request->data('ilce');
//            $semt = $this->Semt->find('all',array('conditions'=>array('ilce_id'=>$ilce),'order'=>array('semt_adi','ASC')));
            $semt = $this->Semt->findAllByIlceId($ilce);
            echo json_encode($semt);
        }else{
            echo json_encode(false);
        }
    }
    
    public function getMahalle(){
        $this->autoRender = false;
        if($this->request->is('post')){
            $semt = $this->request->data('semt');
//            $mahalle = $this->Mahalle->find('all',array('conditions'=>array('semt_id'=>$semt),'order'=>array('mahalle_adi','ASC')));
            $mahalle = $this->Mahalle->findAllBySemtId($semt);
            echo json_encode($mahalle);
        }else{
            echo json_encode(false);
        }
    }
    
    public function getAjaxVideo(){
        $this->autoRender = false;
        if($this->request->is('post')){
            $konutId = $this->request->data('konutId');
            $video = $this->Konut->find('first',array('conditions'=>array('id'=>$konutId),'fields'=>array('video')));
            echo json_encode($video);
        }else{
            echo json_encode(false);
        }
    }
    
    public function ajaxDeleteKonut(){
        $this->autoRender = false;
        if($this->request->is('post')){
            $konutId = $this->request->data('konutId');
            $konuts = $this->Konut->findById($konutId);
            if($konuts['Konut']['user_id'] != $this->Session->read('UserId') && $this->Session->read('UserGroup') != 1){
                echo json_encode(array('hata'=>true, 'message'=>'Bu ilanı silme yetkiniz yoktur.'));
            }else{
                $konutRes = $this->KonutResim->findAllByKonutId($konutId);
                foreach($konutRes as $row){
                    $fileRes = new File($row['KonutResim']['path']);
                    $fileRes->delete();
                    $fileResThumb = new File($row['KonutResim']['paththumb']);
                    if($fileResThumb){
                        $fileResThumb->delete();
                    }
                }
                if($konuts['Konut']['video'] != '' || empty($konuts['Konut']['video'])){
                    $fileVid = new File($konuts['Konut']['video']);
                    $fileVid->delete();
                }
                $this->KonutResim->deleteAll(array('konut_id'=>$konutId));
                $this->KonutLocation->deleteAll(array('konut_id'=>$konutId));
                $this->KonutYer->deleteAll(array('konut_id'=>$konutId));
                $this->Konut->deleteAll(array('id'=>$konutId));
                echo json_encode(array('hata'=>false, 'message'=>'İlan başarıyla silindi.'));
            }
        }
    }
    
    public function ajaxDeleteVideo(){
        $this->autoRender = false;
        if($this->request->is('post')){
            $konutId = $this->request->data('konutId');
            $video = $this->Konut->find('first',array('conditions'=>array('id'=>$konutId)));
            $fileVideo = new File($video['Konut']['video']);
            $fileVideo->delete();
            if($this->Konut->updateAll(array('video'=>null),array('id'=>$konutId))){
                echo json_encode(true);
            }else{
                echo json_encode(false);
            }
        }else{
            echo json_encode(false);
        }
    }

    public function aciklamagetir(){
        $this->autoRender = false;
        if($this->request->is('post')){
            $kId = $this->request->data('kId');
            $konut = $this->Konut->findById($kId);
            if($konut){
                echo json_encode($konut['Konut']['aciklama']);
            }else{
                echo json_encode(false);
            }
        }else{
            echo json_encode(false);
        }
    }
}