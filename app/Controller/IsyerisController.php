<?php
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('ImageManipulator','Vendor');
class IsyerisController extends AppController{
    var $uses = array('Isyeri','Sehir','Ilce','Mahalle','IsyeriYer','IsyeriResim','Tip','IsyeriLocation','Semt','IlanNo',
        'IlanIletisim','IsyeriIletisim','IlanIletisimType');
    public $components = array('Paginator','HtmlMeta');

    public function isyeriindex(){}

    public function home(){
        $named = $this->request->params['named'];
        if(array_key_exists('SatKir', $named) && ($named['SatKir']==1 || $named['SatKir']==2)){
           $SatKir = $named['SatKir'];
           
           if(array_key_exists('tarih', $named)){
               if($named['tarih']=='desc'){
                   $order = array('Isyeri.tarih'=>'DESC');
               }else if($named['tarih']=='asc'){
                   $order = array('Isyeri.tarih'=>'ASC');
               }
           }else if(array_key_exists('fiyat', $named)){
               if($named['fiyat']=='desc'){
                   $order = array('Isyeri.fiyat'=>'DESC');
               }else if($named['fiyat'] == 'asc'){
                   $order = array('Isyeri.fiyat'=>'ASC');
               }
           }else{
               $order = array('Isyeri.tarih'=>'DESC');
           }
           $this->set('sirala',$order);
           
            $this->paginate = array(
                'fields'=>array('*'),
                'conditions'=>array('Isyeri.durum'=>1,'Isyeri.sat_kir'=>$SatKir),
                'limit'=>10,
                'order'=>$order,
                'joins'=>array(
                    array(
                        'table'=>'em_isyeri_yer',
                        'alias'=>'IsyeriYer',
                        'type'=>'INNER',
                        'conditions'=>array('Isyeri.id=IsyeriYer.isyeri_id')
                    ),
                    array(
                        'table'=>'em_sehir',
                        'alias'=>'Sehir',
                        'type'=>'INNER',
                        'conditions'=>array('IsyeriYer.sehir_id=Sehir.id')
                    ),
                    array(
                        'table'=>'em_ilce',
                        'alias'=>'Ilce',
                        'type'=>'LEFT',
                        'conditions'=>array('IsyeriYer.ilce_id=Ilce.id')
                    ),
                    array(
                        'table'=>'em_semt',
                        'alias'=>'Semt',
                        'type'=>'LEFT',
                        'conditions'=>array('IsyeriYer.semt_id=Semt.id')
                    ),
                    array(
                        'table'=>'em_mahalle',
                        'alias'=>'Mahalle',
                        'type'=>'LEFT',
                        'conditions'=>array('IsyeriYer.mahalle_id=Mahalle.id')
                    )
                )
            );
        }
        else{
            if(array_key_exists('tarih', $named)){
               if($named['tarih']=='desc'){
                   $order = array('Isyeri.tarih'=>'DESC');
               }else if($named['tarih']=='asc'){
                   $order = array('Isyeri.tarih'=>'ASC');
               }
           }else if(array_key_exists('fiyat', $named)){
               if($named['fiyat']=='desc'){
                   $order = array('Isyeri.fiyat'=>'DESC');
               }else if($named['fiyat'] == 'asc'){
                   $order = array('Isyeri.fiyat'=>'ASC');
               }
           }else{
               $order = array('Isyeri.tarih'=>'DESC');
           }
           $this->set('sirala',$order);
           
            $this->paginate = array(
                'fields'=>array('*'),
                'conditions'=>array('Isyeri.durum'=>1),
                'limit'=>10,
                'order'=>$order,
                'joins'=>array(
                    array(
                        'table'=>'em_isyeri_yer',
                        'alias'=>'IsyeriYer',
                        'type'=>'INNER',
                        'conditions'=>array('Isyeri.id=IsyeriYer.isyeri_id')
                    ),
                    array(
                        'table'=>'em_sehir',
                        'alias'=>'Sehir',
                        'type'=>'INNER',
                        'conditions'=>array('IsyeriYer.sehir_id=Sehir.id')
                    ),
                    array(
                        'table'=>'em_ilce',
                        'alias'=>'Ilce',
                        'type'=>'LEFT',
                        'conditions'=>array('IsyeriYer.ilce_id=Ilce.id')
                    ),
                    array(
                        'table'=>'em_semt',
                        'alias'=>'Semt',
                        'type'=>'LEFT',
                        'conditions'=>array('IsyeriYer.semt_id=Semt.id')
                    ),
                    array(
                        'table'=>'em_mahalle',
                        'alias'=>'Mahalle',
                        'type'=>'LEFT',
                        'conditions'=>array('IsyeriYer.mahalle_id=Mahalle.id')
                    )
                )
            );
        }
        $isyeris = $this->paginate('Isyeri');
        $this->set('isyeris',$isyeris);
        
        if(!$isyeris){
            $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button>Aradığınız kriterlere uygun sonuç yoktur.','default', array('class'=>'alert alert-danger alert-dismissable'));
            return $this->redirect(Router::url('/',true));
        }
        
        $isyeriRes = array();
        foreach ($isyeris as $row) {
            $isyeriRes[$row['Isyeri']['id']] = $this->IsyeriResim->findAllByIsyeriId($row['Isyeri']['id']);
        }
        
        //$isyeriRes = $this->IsyeriResim->find('all');
        $this->set('isyeriRes',$isyeriRes);

        $this->HtmlMeta->addElement(array('name' => 'robots', 'content' => 'index, follow'));
        $this->HtmlMeta->addElement(array('name' => 'title', 'content' => 'İşyeri - Maden Gayrimenkul A.Ş.'));
        $this->HtmlMeta->addElement(array('name' => 'description', 'content' => 'Satılık, kiralık, konut, işyeri, arsa yatırım ve yönetimi - Mutlu Kazanç Kapısı...'));
        $this->HtmlMeta->addElement(array('property' => 'og:title', 'content' => 'İşyeri - Maden Gayrimenkul A.Ş.' ));
        $this->HtmlMeta->addElement(array('property' => 'og:description', 'content' => 'Satılık, kiralık, konut, işyeri, arsa yatırım ve yönetimi - Mutlu Kazanç Kapısı...'));
        $this->HtmlMeta->addElement(array('property' => 'og:keywords', 'content' => 'Eskişehir Yolu satılık yatırım arsaları, Temelli, Malıköy, Ihlamurkent satılık villa, Alcı, Türkobası, Anayurt, Bacı, Başkent OSB satılık arsa, kiralık fabrika, Anadolu OSB satılık fabrika arsa, ASO 2 OSB, Konutkent, Çayyolu, Westgate Residence, Kuleevo, Elmar Towers, Elya Tower, Azel Kule' ));
        $this->HtmlMeta->addElement(array('property' => 'og:url', 'content' => Router::url('/', true)));
        $this->HtmlMeta->addElement(array('property'=>'og:image', 'content'=>  Router::url('/',true).'img/shareicon/mis.png'));
    }
    
    public function yeniisyeri(){
        $il = $this->Sehir->find('all',array('order'=>'sehir_adi ASC'));
        $this->set('il',$il);
        $iletisim = $this->IlanIletisim->find('all');
        $this->set('iletisim',$iletisim);
        $this->HtmlMeta->addElement(array('name' => 'robots', 'content' => 'noindex, nofollow'));
    }
    
    public function addisyeri(){
        $this->autoRender = false;
        $files = array_key_exists('isyeriRes', $_FILES)?$_FILES['isyeriRes']:false;
        $filesVideo = array_key_exists('isyeriVideo', $_FILES)?$_FILES['isyeriVideo']:false;
        $data = $this->request->data;
        $this->Isyeri->create();
        $saveData = array('tip'=>2,
            'baslik'=>$data['baslik'],
            'aciklama'=>$data['aciklama'],
            'oda_sayisi'=>$data['oda'],
            'm_kare'=>$data['m2'],
            'kat'=>$data['kat'],
            'bina_kat'=>$data['binakat'],
            'kredi'=>$data['kredi'],
            'fiyat'=>$data['fiyat'],
            'sat_kir'=>$data['sat_kir'],
            'durum'=>1,
            'tarih'=> date('Y-m-d'),
            'parabirimi'=>$data['paraBirimi'],
            'mal_name'=>$data['malName'],
            'mal_tel'=>$data['malTelNo'],
            'mal_fiyat'=>$data['malFiyat'],
            'mal_parabirimi'=>$data['malParaBirimi'],
            'video'=>$data['video']);
        
        if($this->Isyeri->save($saveData)){
            $isyeriId = $this->Isyeri->getInsertID();
            $ilanNo = $this->IlanNo->find('first');
//            $isyeriilanno = 'I'.$isyeriId;
            $isyeriilanno = $ilanNo['IlanNo']['ilan_no']+1;
            $test = $this->IlanNo->updateAll(array('ilan_no'=>$isyeriilanno),array('id'=>1));
            $this->Isyeri->updateAll(array('ilan_no'=>"'$isyeriilanno'"), array('id'=>$isyeriId));

            $this->IsyeriYer->create();
            $yerArray = array();
            $yerArray['sehir_id'] = $data['il'];
            $yerArray['ilce_id'] = array_key_exists('ilce', $data)?$data['ilce']:null;
            $yerArray['semt_id'] = array_key_exists('semt', $data)?$data['semt']:null;
            $yerArray['mahalle_id'] = array_key_exists('mahalle', $data)?$data['mahalle']:null;
            $yerArray['isyeri_id'] = $isyeriId;
            $this->IsyeriYer->save($yerArray);
            
            //Isyeri Maps
            $this->IsyeriLocation->create();
            $this->IsyeriLocation->save(array('isyeri_id'=>$isyeriId,'name'=>$data['location'],'latitude'=>$data['latitude'],'longitude'=>$data['longitude']));
            
            // İsyeri İletisim Bilgileri
            $Ilet = array_key_exists('ilanilet',$data)?$data['ilanilet']:false;
            foreach($Ilet as $row){
                $this->IsyeriIletisim->create();
                $this->IsyeriIletisim->save(array('isyeri_id'=>$isyeriId,'ilet_id'=>$row));
            }

            $hataRes = 0;
            if($files){
                foreach($files['error'] as $key=>$value){
                    if($value == 0 && $files['type'][$key] != "image/jpeg" && $files['type'][$key] != "image/gif" && $files['type'][$key] != "image/png" && $files['type'][$key] != "image/tiff"){
                        $hataRes++;
                    }
                    else{
                        $fileType = pathinfo($files['name'][$key], PATHINFO_EXTENSION);
                        //$fileType = explode('.', $files['name'][$key]);
                        //$fileType = $fileType[count($fileType)-1];
                        $this->IsyeriResim->create();
                        $this->IsyeriResim->save(array('isyeri_id'=>$isyeriId));
                        $lastId = $this->IsyeriResim->getLastInsertID();
                        $path = 'img/isyeri/'.$isyeriId.'/'.$lastId.'.'.$fileType;
                        $pathRes = WWW_ROOT.$path;
                        $pathThumb = 'img/isyeri/'.$isyeriId.'/'.$lastId.'thumb.'.$fileType;
                        $pathResThumb = WWW_ROOT.$pathThumb;
                        //image Resize
                        $manipulator = new ImageManipulator($files['tmp_name'][$key]);
                        $newImage = $manipulator->resample(800, 600, false);
                        if($manipulator->save($pathRes)){
                            $this->IsyeriResim->id = $lastId;
                            $this->IsyeriResim->save(array('path'=>$path));
                            $manipulator = new ImageManipulator($pathRes);
                            $newImage = $manipulator->resample(200, 200);
                            if($manipulator->save($pathResThumb)){
                                $this->IsyeriResim->updateAll(array('paththumb'=>"'$pathThumb'"),array('id'=>$lastId));
                                /*$this->IsyeriResim->id = $lastId;
                                $this->IsyeriResim->save(array('paththumb'=>$pathThumb));*/
                            }
                        }else{
                            $hataRes++;
                            $this->IsyeriResim->deleteAll(array('id'=>$lastId));
                        }
                    }
                }
            }
            if($hataRes>0){
                $this->Session->setFlash('Yüklemek istediğiniz resimlerden bazıları sisteme yüklenememiştir. Sonra tekrar deneyin.','default', array('class'=>'alert alert-danger'));
            }else{
                $this->Session->setFlash('Isyeri kayıt işlemi başarıyla sonuçlandı.','default', array('class'=>'alert alert-success'));
            }
        }else{
            $this->Session->setFlash('Isyeri kayıt işlemi başarısız. Lütfen tekrar deneyin.','default', array('class'=>'alert alert-danger'));
        }
        return $this->redirect(
        array('controller' => 'isyeris', 'action' => 'yeniisyeri')
        );
    }
    
    function isyeri(){
        if($this->request->params['named']['ilanNo']){
           $ilanNo = $this->request->params['named']['ilanNo'];

            /*if(!$this->Session->check('ilanNo.'.$ilanNo) && rand(1,5) == 3){
                $this->Session->write('ilanNo.'.$ilanNo,1);
                $this->redirect(SHORTE_URL.'isyeri/ilanNo:'.$ilanNo);
            }*/

           $isyeris = $this->Isyeri->find('first',array(
            'fields'=>array('*'),
            'contain'=>array('KonutDanisman'=>array('Danisman')),
            'conditions'=>array("ilan_no LIKE '%$ilanNo%'","durum"=>1),
            'joins'=>array(
                array(
                    'table'=>'em_isyeri_yer',
                    'alias'=>'IsyeriYer',
                    'type'=>'INNER',
                    'conditions'=>array('Isyeri.id=IsyeriYer.isyeri_id')
                ),
                array(
                    'table'=>'em_sehir',
                    'alias'=>'Sehir',
                    'type'=>'INNER',
                    'conditions'=>array('IsyeriYer.sehir_id=Sehir.id')
                ),
                array(
                    'table'=>'em_ilce',
                    'alias'=>'Ilce',
                    'type'=>'LEFT',
                    'conditions'=>array('IsyeriYer.ilce_id=Ilce.id')
                ),
                array(
                    'table'=>'em_semt',
                    'alias'=>'Semt',
                    'type'=>'LEFT',
                    'conditions'=>array('IsyeriYer.semt_id=Semt.id')
                ),
                array(
                    'table'=>'em_mahalle',
                    'alias'=>'Mahalle',
                    'type'=>'LEFT',
                    'conditions'=>array('IsyeriYer.mahalle_id=Mahalle.id')
                ),
                array(
                    'table'=>'em_isyeri_location',
                    'alias'=>'IsyeriLocation',
                    'type'=>'LEFT',
                    'conditions'=>array('Isyeri.id=IsyeriLocation.isyeri_id')
                )
            )
        ));
        
        if(!$isyeris){
            $this->Session->setFlash('Aramış olduğunuz ilan kaldırılmıştır.','default', array('class'=>'alert alert-danger'));
            return $this->redirect(
            array('controller' => 'isyeris', 'action' => 'home')
            );    
        }
        $this->set('isyeris',$isyeris);

        $isyeriRes = $this->IsyeriResim->find('all',array('order'=>array('id'=>'ASC'),'fields'=>array('*'),'conditions'=>array('isyeri_id'=>$isyeris['Isyeri']['id'])));
        $this->set('isyeriRes',$isyeriRes);

            $IlanTel = $this->IsyeriIletisim->find('all',
                array(
                    'fields'=>array('IlanIlet.*','IlanIletType.*'),
                    'conditions'=>array('IsyeriIletisim.isyeri_id'=>$isyeris['Isyeri']['id'],'IlanIlet.type'=>1),
                    'orders'=>array('IlanIlet.type'=>'ASC'),
                    'joins'=>array(
                        array('table'=>'em_ilaniletisim', 'alias'=>'IlanIlet', 'type'=>'INNER', 'conditions'=>array('IsyeriIletisim.ilet_id = IlanIlet.id')),
                        array('table'=>'em_ilaniletisimtype', 'alias'=>'IlanIletType', 'type'=>'INNER', 'conditions'=>array('IlanIlet.type = IlanIletType.id'))
                    )
                )
            );
            $this->set('IlanTel',$IlanTel);

            $IlanMail = $this->IsyeriIletisim->find('all',
                array(
                    'fields'=>array('IlanIlet.*','IlanIletType.*'),
                    'conditions'=>array('IsyeriIletisim.isyeri_id'=>$isyeris['Isyeri']['id'],'IlanIlet.type'=>3),
                    'orders'=>array('IlanIlet.type'=>'ASC'),
                    'joins'=>array(
                        array('table'=>'em_ilaniletisim', 'alias'=>'IlanIlet', 'type'=>'INNER', 'conditions'=>array('IsyeriIletisim.ilet_id = IlanIlet.id')),
                        array('table'=>'em_ilaniletisimtype', 'alias'=>'IlanIletType', 'type'=>'INNER', 'conditions'=>array('IlanIlet.type = IlanIletType.id'))
                    )
                )
            );
            $this->set('IlanMail',$IlanMail);

            $IlanAdres = $this->IsyeriIletisim->find('all',
                array(
                    'fields'=>array('IlanIlet.*','IlanIletType.*'),
                    'conditions'=>array('IsyeriIletisim.isyeri_id'=>$isyeris['Isyeri']['id'],'IlanIlet.type'=>5),
                    'orders'=>array('IlanIlet.type'=>'ASC'),
                    'joins'=>array(
                        array('table'=>'em_ilaniletisim', 'alias'=>'IlanIlet', 'type'=>'INNER', 'conditions'=>array('IsyeriIletisim.ilet_id = IlanIlet.id')),
                        array('table'=>'em_ilaniletisimtype', 'alias'=>'IlanIletType', 'type'=>'INNER', 'conditions'=>array('IlanIlet.type = IlanIletType.id'))
                    )
                )
            );
            $this->set('IlanAdres',$IlanAdres);
            $this->set('IlanNo',$ilanNo);

        $this->HtmlMeta->addElement(array('name' => 'robots', 'content' => 'index, follow'));
        $this->HtmlMeta->addElement(array('name' => 'title', 'content' => $isyeris['Isyeri']['baslik']));
        $this->HtmlMeta->addElement(array('name' => 'description', 'content' => strip_tags($isyeris['Isyeri']['aciklama'])));
        $this->HtmlMeta->addElement(array('property' => 'og:title', 'content' => $isyeris['Isyeri']['baslik']));
        $this->HtmlMeta->addElement(array('property' => 'og:description', 'content' => strip_tags($isyeris['Isyeri']['aciklama'])));
        $this->HtmlMeta->addElement(array('property' => 'og:keywords', 'content' => $isyeris['Isyeri']['baslik'] ));
        $this->HtmlMeta->addElement(array('property' => 'og:url', 'content' => Router::url('/', true).'isyeri/ilanNo:'.$ilanNo));
        if($isyeriRes){
            $this->HtmlMeta->addElement(array('property'=>'og:image', 'content'=>  Router::url('/',true).$isyeriRes[0]['IsyeriResim']['path']));
        }else{
            $this->HtmlMeta->addElement(array('property'=>'og:image', 'content'=>  Router::url('/',true).'img/logo-xxs.png'));
        }

            $video_code = false;
            if (preg_match("/youtube.com/", $isyeris['Isyeri']['aciklama']) || preg_match("/youtu.be/", $isyeris['Isyeri']['aciklama'])){
                if (preg_match('%(?:youtube\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $isyeris['Isyeri']['aciklama'], $match))
                {
                    $video_code = $match[1];
                }
                $source = 'http://www.youtube.com/e/'.$video_code;
                $picture = 'http://img.youtube.com/vi/'.$video_code.'/0.jpg';
            }
            else if (preg_match("/vimeo.com/", $isyeris['Isyeri']['aciklama']))
            {
                if (preg_match('/vimeo\.com\/(clip\:)?(\d+).*$/', $isyeris['Isyeri']['aciklama'], $match))
                {
                    $video_code = $match[2];
                }
                /* Get Vimeo thumbnail */
                $hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/$video_code.php"));
                $picture = $hash[0]['thumbnail_medium'];
                $source = 'https://secure.vimeo.com/moogaloop.swf?clip_id='.$video_code.'&autoplay=1';
            }

            if($video_code){
                $this->HtmlMeta->addElement(array('property' => 'og:type', 'content' => 'video' ));
                $this->HtmlMeta->addElement(array('property' => 'og:video', 'content' => $source ));
                /*$this->HtmlMeta->addElement(array('property' => 'og:video:width', 'content' => '374' ));
                $this->HtmlMeta->addElement(array('property' => 'og:video:height', 'content' => '202' ));*/
                $this->HtmlMeta->addElement(array('property' => 'og:video:type', 'content' => 'application/x-shockwave-flash' ));
            }
        }else{
            return $this->redirect(
            array('controller' => 'isyeris', 'action' => 'home')
            );    
        }
    }
    
    public function duzenle(){
        if($this->request->params['named']['ilanNo']){
           $ilanNo = $this->request->params['named']['ilanNo']; 
           
           $isyeris = $this->Isyeri->find('first',array(
            'fields'=>array('*'),
            'conditions'=>array("ilan_no LIKE '%$ilanNo%'"),
            'joins'=>array(
                array(
                    'table'=>'em_isyeri_yer',
                    'alias'=>'IsyeriYer',
                    'type'=>'INNER',
                    'conditions'=>array('Isyeri.id=IsyeriYer.isyeri_id')
                ),
                array(
                    'table'=>'em_sehir',
                    'alias'=>'Sehir',
                    'type'=>'INNER',
                    'conditions'=>array('IsyeriYer.sehir_id=Sehir.id')
                ),
                array(
                    'table'=>'em_ilce',
                    'alias'=>'Ilce',
                    'type'=>'LEFT',
                    'conditions'=>array('IsyeriYer.ilce_id=Ilce.id')
                ),
                array(
                    'table'=>'em_semt',
                    'alias'=>'Semt',
                    'type'=>'LEFT',
                    'conditions'=>array('IsyeriYer.semt_id=Semt.id')
                ),
                array(
                    'table'=>'em_mahalle',
                    'alias'=>'Mahalle',
                    'type'=>'LEFT',
                    'conditions'=>array('IsyeriYer.mahalle_id=Mahalle.id')
                ),
                array(
                    'table'=>'em_isyeri_location',
                    'alias'=>'IsyeriLocation',
                    'type'=>'LEFT',
                    'conditions'=>array('Isyeri.id=IsyeriLocation.isyeri_id')
                )
            )
        ));
        
        $this->set('isyeris',$isyeris);
        if($isyeris['Isyeri']['user_id'] != $this->Session->read('UserId') && $this->Session->read('UserGroup') != 1){
            $this->Session->setFlash('<button class="close" data-dismiss="alert" type="button">×</button>Bu ilanı düzeltme yetkiniz yoktur.','default', array('class'=>'alert alert-danger alert-dismissable'));
                return $this->redirect(
                array('controller' => 'isyeris', 'action' => 'home')
                );
        }
        
        $isyeriRes = $this->IsyeriResim->findAllByIsyeriId($isyeris['Isyeri']['id']);
        $this->set('isyeriRes',$isyeriRes);
           
        $il = $this->Sehir->find('all',array('order'=>'id ASC'));
        $this->set('il',$il);
        $ilce = $this->Ilce->find('all',array('conditions'=>array('sehir_id'=>$isyeris['IsyeriYer']['sehir_id']),'order'=>'id ASC'));
        $this->set('ilce',$ilce);
        $semt = $this->Semt->find('all',array('conditions'=>array('ilce_id'=>$isyeris['IsyeriYer']['ilce_id']),'order'=>'id ASC'));
        $this->set('semt',$semt);
        $mahalle = $this->Mahalle->find('all',array('conditions'=>array('semt_id'=>$isyeris['IsyeriYer']['semt_id']),'order'=>'id ASC'));        
        $this->set('mahalle',$mahalle);
        
        $iletisim = $this->IlanIletisim->find('all');
        $this->set('iletisim',$iletisim);
        $IsIletisim = $this->IsyeriIletisim->find('all',array(
            'fields'=>array('*'),
            'conditions'=>array("IsyeriIletisim.isyeri_id"=>$isyeris['Isyeri']['id']),
            'joins'=>array(
                array(
                    'table'=>'em_ilaniletisim',
                    'alias'=>'IlanIlet',
                    'type'=>'INNER',
                    'conditions'=>array('IsyeriIletisim.ilet_id = IlanIlet.id')
                )
                )
            ));
        $this->set('IsIlet',$IsIletisim);
            $this->HtmlMeta->addElement(array('name' => 'robots', 'content' => 'noindex, nofollow'));
        }else{
            $this->Session->setFlash('Bir hata meydana geldi. Lütfen tekrar deneyin.','default', array('class'=>'alert alert-danger'));
            return $this->redirect(
            array('controller' => 'isyeris', 'action' => 'home')
            );    
        }
    }
    
    public function deleteResim(){
        $this->autoRender = false;
        if($this->request->is('post')){
            $ResId = $this->request->data('resId');
            $resim = $this->IsyeriResim->find('first',array('conditions'=>array('id'=>$ResId)));
            $fileRes = new File($resim['IsyeriResim']['path']);
            $fileRes->delete();
            $fileResThumb = new File($resim['IsyeriResim']['paththumb']);
            if($fileResThumb){
                $fileResThumb->delete();
            }
            if($this->IsyeriResim->deleteAll(array('id'=>$ResId))){
                echo json_encode(true);
            }else{
                echo json_encode(false);
            }
        }else{
            echo json_encode(false);
        }
    }
    
    public function upgradeisyeri(){
        $this->autoRender = false;
        if($this->request->params['named']['ilanNo']){
           $ilanNo = $this->request->params['named']['ilanNo']; 
           
           $isyeri = $this->Isyeri->find('first',array('conditions'=>array("ilan_no" => $ilanNo)));
           $isyeriId = $isyeri['Isyeri']['id'];
           
           $this->Isyeri->id = $isyeriId;
            $files = array_key_exists('isyeriRes', $_FILES)?$_FILES['isyeriRes']:false;
            $filesVideo = array_key_exists('isyeriVideo', $_FILES)?$_FILES['isyeriVideo']:false;
            $data = $this->request->data;
            $baslik = $data['baslik'];
            $aciklama = $data['aciklama'];
            $odasayisi = $data['oda'];
            $mkare = $data['m2'];
            $kat = $data['kat'];
            $binakat = $data['binakat'];
            $kredi = $data['kredi'];
            $fiyat = $data['fiyat'];
            $paraBirim = $data['paraBirimi'];
            $saveData = array(
                'baslik'=>$baslik,
                'aciklama'=>$aciklama,
                'oda_sayisi'=>$odasayisi,
                'm_kare'=>$mkare,
                'kat'=>$kat,
                'bina_kat'=>$binakat,
                'kredi'=>$kredi,
                'fiyat'=>$fiyat,
                'sat_kir'=>$data['sat_kir'],
                'parabirimi'=>$paraBirim,
                'mal_name'=>$data['malName'],
                'mal_tel'=>$data['malTelNo'],
                'mal_fiyat'=>$data['malFiyat'],
                'mal_parabirimi'=>$data['malParaBirimi'],
                'video'=>$data['video']);

            if($this->Isyeri->save($saveData)){
                $this->IsyeriYer->deleteAll(array('isyeri_id'=>$isyeriId));
                //KONUTYER
                $this->IsyeriYer->create();
                $yerArray = array();
                $yerArray['sehir_id'] = $data['il'];
                $yerArray['ilce_id'] = array_key_exists('ilce', $data)?$data['ilce']:null;
                $yerArray['semt_id'] = array_key_exists('semt', $data)?$data['semt']:null;
                $yerArray['mahalle_id'] = array_key_exists('mahalle', $data)?$data['mahalle']:null;
                $yerArray['isyeri_id'] = $isyeriId;
                $this->IsyeriYer->save($yerArray);

                $this->IsyeriLocation->deleteAll(array('isyeri_id'=>$isyeriId));
                //Isyeri Maps
                $this->IsyeriLocation->create();
                $this->IsyeriLocation->save(array('isyeri_id'=>$isyeriId,'name'=>$data['location'],'latitude'=>$data['latitude'],'longitude'=>$data['longitude']));
                
                // Isyeri İletisim Bilgileri
                $Ilet = array_key_exists('ilanilet',$data)?$data['ilanilet']:false;
                $this->IsyeriIletisim->deleteAll(array('isyeri_id'=>$isyeriId));
                foreach($Ilet as $row){
                    $this->IsyeriIletisim->create();
                    $this->IsyeriIletisim->save(array('isyeri_id'=>$isyeriId,'ilet_id'=>$row));
                }

                $hataRes = 0;
                if($files){
                    foreach($files['error'] as $key=>$value){
                        if($value == 0 && $files['type'][$key] != "image/jpeg" && $files['type'][$key] != "image/gif" && $files['type'][$key] != "image/png" && $files['type'][$key] != "image/tiff"){
                            $hataRes++;
                        }
                        else{
                            $fileType = pathinfo($files['name'][$key], PATHINFO_EXTENSION);
                            //$fileType = explode('.', $files['name'][$key]);
                            //$fileType = $fileType[count($fileType)-1];
                            $this->IsyeriResim->create();
                            $this->IsyeriResim->save(array('isyeri_id'=>$isyeriId));
                            $lastId = $this->IsyeriResim->getLastInsertID();
                            $path = 'img/isyeri/'.$isyeriId.'/'.$lastId.'.'.$fileType;
                            $pathRes = WWW_ROOT.$path;
                            $pathThumb = 'img/isyeri/'.$isyeriId.'/'.$lastId.'thumb.'.$fileType;
                            $pathResThumb = WWW_ROOT.$pathThumb;
                             //image Resize
                            $manipulator = new ImageManipulator($files['tmp_name'][$key]);
                            $newImage = $manipulator->resample(800, 600, false);
    //                        move_uploaded_file($files['tmp_name'][$key], $pathRes)
                            if($manipulator->save($pathRes)){
                                $this->IsyeriResim->id = $lastId;
                                $this->IsyeriResim->save(array('path'=>$path));
                                $manipulator = new ImageManipulator($pathRes);
                                $newImage = $manipulator->resample(200, 200);
                                if($manipulator->save($pathResThumb)){
                                    $this->IsyeriResim->updateAll(array('paththumb'=>"'$pathThumb'"),array('id'=>$lastId));
                                    /*$this->IsyeriResim->id = $lastId;
                                    $this->IsyeriResim->save(array('paththumb'=>$pathThumb));*/
                                }
                            }else{
                                $hataRes++;
                                $this->IsyeriResim->deleteAll(array('id'=>$lastId));
                            }
                        }
                    }
                }
                if($hataRes>0){
                    $this->Session->setFlash('Yüklemek istediğiniz resimlerden bazıları sisteme yüklenememiştir. Sonra tekrar deneyin.','default', array('class'=>'alert alert-danger'));
                }else{
                    $this->Session->setFlash('Isyeri düzenleme işlemi başarıyla sonuçlandı.','default', array('class'=>'alert alert-success'));
                    return $this->redirect(
                    array('controller' => 'isyeris', 'action' => 'duzenle/ilanNo:'.$ilanNo)
                    );
                }
            }else{
                $this->Session->setFlash('Isyeri düzenleme işlemi başarısız. Lütfen tekrar deneyin.','default', array('class'=>'alert alert-danger'));
            }
            return $this->redirect(
            array('controller' => 'isyeris', 'action' => 'yeniisyeri')
            );
        }else{
            $this->Session->setFlash('Isyeri düzenleme işlemi başarısız. Lütfen tekrar deneyin.','default', array('class'=>'alert alert-danger'));
            return $this->redirect(
            array('controller' => 'isyeris', 'action' => 'home')
            );
        }
    }
    
    public function getAjaxVideo(){
        $this->autoRender = false;
        if($this->request->is('post')){
            $isyeriId = $this->request->data('isyeriId');
            $video = $this->Isyeri->find('first',array('conditions'=>array('id'=>$isyeriId),'fields'=>array('video')));
            echo json_encode($video);
        }else{
            echo json_encode(false);
        }
    }
    
    public function ajaxDeleteIsyeri(){
        $this->autoRender = false;
        if($this->request->is('post')){
            $isyeriId = $this->request->data('isyeriId');
            $isyeris = $this->Isyeri->findById($isyeriId);
            if($isyeris['Isyeri']['user_id'] != $this->Session->read('UserId') && $this->Session->read('UserGroup') != 1){
                echo json_encode(array('hata'=>true, 'message'=>'Bu ilanı silme yetkiniz yoktur.'));
            }else{
                $isyeriRes = $this->IsyeriResim->findAllByIsyeriId($isyeriId);
                foreach($isyeriRes as $row){
                    $fileRes = new File($row['IsyeriResim']['path']);
                    $fileRes->delete();
                    $fileResThumb = new File($row['IsyeriResim']['paththumb']);
                    if($fileResThumb){
                        $fileResThumb->delete();
                    }
                }
                if($isyeris['Isyeri']['video'] != '' || empty($isyeris['Isyeri']['video'])){
                    $fileVid = new File($isyeris['Isyeri']['video']);
                    $fileVid->delete();
                }
                $this->IsyeriResim->deleteAll(array('isyeri_id'=>$isyeriId));
                $this->IsyeriLocation->deleteAll(array('isyeri_id'=>$isyeriId));
                $this->IsyeriYer->deleteAll(array('isyeri_id'=>$isyeriId));
                $this->Isyeri->deleteAll(array('id'=>$isyeriId));
                echo json_encode(array('hata'=>false,'message'=>'İlan başarıyla silindi.'));
            }
        }
    }
    
    public function ajaxDeleteVideo(){
        $this->autoRender = false;
        if($this->request->is('post')){
            $isyeriId = $this->request->data('isyeriId');
            $video = $this->Isyeri->find('first',array('conditions'=>array('id'=>$isyeriId)));
            $fileVideo = new File($video['Isyeri']['video']);
            $fileVideo->delete();
            if($this->Isyeri->updateAll(array('video'=>null),array('id'=>$isyeriId))){
                echo json_encode(true);
            }else{
                echo json_encode(false);
            }
        }else{
            echo json_encode(false);
        }
    }

    public function aciklamagetir(){
        $this->autoRender = false;
        if($this->request->is('post')){
            $iId = $this->request->data('iId');
            $isyeri = $this->Isyeri->findById($iId);
            if($isyeri){
                echo json_encode($isyeri['Isyeri']['aciklama']);
            }else{
                echo json_encode(false);
            }
        }else{
            echo json_encode(false);
        }
    }
}