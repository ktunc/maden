<?php
echo $this->Html->css('photoswipe');
echo $this->Html->script('simple-inheritance.min');
echo $this->Html->script('code-photoswipe-1.0.11');
echo $this->Html->css('../js/Flickerplate-master/css/flickerplate');
echo $this->Html->script('Flickerplate-master/js/min/modernizr-custom-v2.7.1.min');
echo $this->Html->script('Flickerplate-master/js/min/jquery-finger-v0.1.0.min');
echo $this->Html->script('Flickerplate-master/js/min/flickerplate.min');
?>
<div class="bg-white printwidth">
    <div class="ilanUst row">
        <div class="col-xs-12 text-danger h3 fontBold"><?php echo $ilan['Ilan']['baslik'];?></div>
        <div class="col-xs-12 h6"><?php echo $ilan['Sehir']['sehir_adi'];
            if($ilan['Ilce']['id']){
                echo ' / '.$ilan['Ilce']['ilce_adi'];
                if($ilan['Semt']['id']){
                    echo ' / '.$ilan['Semt']['semt_adi'];
                    if($ilan['Mahalle']['id']){
                        echo ' / '.$ilan['Mahalle']['mahalle_adi'];
                    }
                }
            }?></div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <div class="FlickPadLR">
                <div class="flicker-example flickCSS hidden-print" data-block-text="false">
                    <ul id="Gallery">
                        <?php
                        if(count($ilan['IlanResim'])>0){
                            foreach($ilan['IlanResim'] as $row){
                                echo '<li style="background-size:100% 100%">'
                                    .'<div class="flick-title">'
                                    . '<a href="'.$this->Html->url('/').$row['path'].'">'
                                    . '<img class="imgIcerik" src="'.$this->Html->url('/').$row['path'].'" alt="'.$ilan['Ilan']['baslik'].' '.$row['id'].'"/>'
                                    . '</a>'
                                    . '</div>'
                                    . '</li>';
                            }
                        }else{
                            echo '<li style="background-size:100% 100%">'
                                .'<div class="flick-title">'
                                . '<a href="'.$this->Html->url('/').'img/nofoto.png">'
                                . '<img class="imgIcerik" src="'.$this->Html->url('/').'img/nofoto.png" alt="'.$ilan['Ilan']['baslik'].'"/>'
                                . '</a>'
                                . '</div>'
                                . '</li>';
                        } ?>
                    </ul>

                    <div class="IlanShareIcon">
                        <button class="btn btn-warning btn-sm" id="ShareBut"><i class="fa fa-2x fa-share-alt"></i></button>
                    </div>
                    <div class="IlanSatKir">
                        <button type="button" class="btn btn-warning btn-sm"><?=$ilan['Ilan']['sat_kir']==1?'Satılık':'Kiralık'?></button>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 font16">
            <?php
            $str = strrev($ilan['Ilan']['fiyat']);
            echo '<div class="anaDiv text-danger h4 fontBold"><i class="fa fa-try"></i> '.substr(strrev(chunk_split($str,3,".")),1).'</div>';
            ?>
            <table class="table table-bordered table-striped table-hover">
                <tr>
                    <td>İlan Numarası</td>
                    <td><?php echo $ilan['Ilan']['ilan_no'];?></td>
                </tr>
                <?php
                if($ilan['Ilan']['ilan_tipi'] == 1){
                    echo $this->element('IlanTipView/konut',array('data'=>$ilan['KonutBilgi']));
                }else if($ilan['Ilan']['ilan_tipi'] == 2){
                    echo $this->element('IlanTipView/isyeri',array('data'=>$ilan['IsyeriBilgi']));
                }else if($ilan['Ilan']['ilan_tipi'] == 3){
                    echo $this->element('IlanTipView/arsa',array('data'=>$ilan['ArsaBilgi']));
                }
                ?>
                <?php if($this->Session->check('UserLogin')){?>
                    <tr>
                        <td>Mal Sahibi Adı Soyadı:</td>
                        <td><?php echo $ilan['Ilan']['mal_name'];?></td>
                    </tr>
                    <tr>
                        <td>Mal Sahibi Telefon No:</td>
                        <td><?php echo $ilan['Ilan']['mal_tel'];?></td>
                    </tr>
                    <tr>
                        <td>Mal Sahibi Fiyat:</td>
                        <?php
                        $str = strrev($ilan['Ilan']['mal_fiyat']);
                        echo '<td><i class="fa fa-try"></i>'.substr(strrev(chunk_split($str,3,".")),1).'</td>';
                        ?>
                    </tr>
                <?php } ?>
            </table>
            <div class="FlickPadLR hidden-print" style="width: 100%;">
                <?php if($ilan['Ilan']['harita'] == 1){ ?>
                    <div class="divYan">
                        <!--<a target="_blank" href="https://maps.google.com?saddr=Current+Location&daddr=<?php echo $ilan['KonutLocation']['latitude']; ?>,<?php echo $ilan['KonutLocation']['longitude']; ?>"><img class="ShareIcon" alt="map_direction_icon" src="<?php echo $this->webroot;?>img/map.png" /></a>-->
                        <img class="ShareIcon" onclick="FuncMapGoruntule(<?php echo $ilan['Ilan']['latitude']; ?>,<?php echo $ilan['Ilan']['longitude']; ?>)" alt="map_direction_icon" src="<?php echo $this->webroot;?>img/map.png" />
                    </div>
                <?php } ?>
                <?php if(count($ilan['IlanDanisman']) > 0){ ?>
                    <div class="divYan">
                        <img class="ShareIcon" onclick="FuncDanismanGoruntule()" alt="danisman_icon" src="<?php echo $this->webroot;?>img/shareicon/mkur.png" />
                    </div>
                <?php } ?>

<!--                --><?php //if($IlanAdres && 1==0){ ?>
<!--                    <div class="divYan fRight">-->
<!--                        <img class="ShareIcon" id="OpenIlanAdresModal" src="--><?php //echo $this->webroot;?><!--img/shareicon/adres.png"/>-->
<!--                    </div>-->
<!--                --><?php //} ?>
<!--                --><?php //if($IlanMail && 1==0){ ?>
<!--                    <div class="divYan fRight">-->
<!--                        <img class="ShareIcon" id="OpenIlanMailModal" src="--><?php //echo $this->webroot;?><!--img/shareicon/mail.png"/>-->
<!--                    </div>-->
<!--                --><?php //} ?>
<!--                --><?php //if($IlanTel && 1==0){ ?>
<!--                    <div class="divYan fRight">-->
<!--                        <img class="ShareIcon" id="OpenIlanTelModal" src="--><?php //echo $this->webroot;?><!--img/shareicon/tel.png"/>-->
<!--                    </div>-->
<!--                --><?php //} ?>
            </div>
        </div>
    </div>
    <?php
    if(!empty($ilan['IlanOzellik'])){
    ?>
    <div class="panel-body">
        <div class="panel-group" id="accordion">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h5 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">İlan Özellikleri</a>
                    </h5>
                </div>
                <div id="collapseOne" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <?php
                        foreach ($ilan['IlanOzellik'] as $row) {
                            echo '<div class="col-xs-12 col-sm-3" style="margin-bottom: 10px;"><div class="icheckbox_line-green checked"><input class="icheksOzellik" checked="" type="checkbox" name="ozellik" id="ozellik" style="position: absolute; opacity: 0;"><div class="icheck_line-icon"></div>'.$row['Ozellik']['ozellik'].'<ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div></div>';
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
</div>

<input type="hidden" id="ilan_id" value="<?php echo $ilan['Ilan']['id']; ?>" />
<?php if(!empty($ilan['Ilan']['aciklama'])){ ?>
    <div class="kback text-white printwidth">
        <div class="row">
            <div class="text-center col-xs-12 hidden-print">
                <h4 type="button" onclick="FuncIlanAciklamaGoruntule()" >Açıklama</h4>
            </div>
            <div class="text-center col-xs-12 visible-print ilanaciklama">
                <?php echo nl2br($ilan['Ilan']['aciklama']); ?>
            </div>
        </div>
    </div>
<?php }?>

<?= $this->element('DanismanModal', array('danismanlar' => $ilan['IlanDanisman'])) ?>
<?= $this->element('IlanAciklamaModal', array('data' => $ilan['Ilan']['aciklama'])) ?>

<script type="text/javascript">

    $(document).ready(function() {
        document.title = "<?php echo $ilan['Ilan']['baslik'];?>";

        Code.photoSwipe('a', '#Gallery');

        $('.flicker-example').flicker({
            auto_flick: true,
            auto_flick_delay: 5,
            flick_animation: "transform-slide"
        });

//        $('input.icheksOzellik').each(function () {
//            var self = $(this),
//                label = self.next(),
//                label_text = label.text();
//
//            label.remove();
//            self.iCheck({
//                checkboxClass:'icheckbox_line-green',
//                insert:'<div class="icheck_line-icon"></div>'+label_text
//            });
//        });

        $('#ShareBut').on('click',function(){
            $('#ShareIlanModal').modal({
                keyboard:false,
                backdrop:'static'
            });
        });
        $('#OpenIlanTelModal').on('click',function(){
            $('#IlanTelModal').modal({
                keyboard:false,
                backdrop:'static'
            });
        });
        $('#OpenIlanAdresModal').on('click',function(){
            $('#IlanAdresModal').modal({
                keyboard:false,
                backdrop:'static'
            });
        });
        $('#OpenIlanMailModal').on('click',function(){
            $('#IlanMailModal').modal({
                keyboard:false,
                backdrop:'static'
            });
        });
    });
    //function OpenMaps(lati,long){
    //    window.location.href = 'http://www.google.com/maps/@'+lati+','+long+',18z';
    //}
</script>
