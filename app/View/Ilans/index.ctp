<?php
/* @var $this View */
/* @var $html HtmlHelper */
//pr($ilanlar);
//exit();
$pagg = $this->passedArgs;
$this->Paginator->options(array('url' => $pagg));

$bgClass = 'kurback';
//if($sTip == 1){
//    $bgClass = 'kback';
//}else if($sTip == 2){
//    $bgClass = 'iback';
//}else if($sTip == 3){
//    $bgClass = 'aback';
//}
?>
<div class="aramaclass kurback">
    <?php
    echo '<div class="row text-center"><h3 class="text-white">İLANLAR</h3></div>';
//    if($sTip == 1){
//        echo '<div class="row text-center"><h3 class="text-white">';
//
//        if($sSatKir == 1){
//            echo ' SATILIK KONUT';
//        }else if($sSatKir == 2){
//            echo ' KİRALIK KONUT';
//        }else{
//            echo ' KONUT';
//        }
//        echo '</h3></div>';
//    }else if($sTip == 2){
//        echo '<div class="row text-center"><h3 class="text-white">';
//
//        if($sSatKir == 1){
//            echo ' SATILIK İŞYERİ';
//        }else if($sSatKir == 2){
//            echo ' KİRALIK İŞYERİ';
//        }else{
//            echo ' İŞYERİ';
//        }
//        echo '</h3></div>';
//    }else if($sTip == 3){
//        echo '<div class="row text-center"><h3 class="text-white">';
//
//        if($sSatKir == 1){
//            echo ' SATILIK ARSA';
//        }else if($sSatKir == 2){
//            echo ' KİRALIK ARSA';
//        }else{
//            echo ' ARSA';
//        }
//        echo '</h3></div>';
//    }else{
//        echo '<div class="row text-center">';
//        if($sSatKir == 1){
//            echo '<h3 class="text-white">SATILIK İLANLAR</h3>';
//        }else if($sSatKir == 2){
//            echo '<h3 class="text-white">KİRALIK İLANLAR</h3>';
//        }else{
//            echo '<h3 class="text-white">İLANLAR</h3>';
//        }
//        echo '</div>';
//    }
    ?>

    <?php

    foreach ($ilanlar as $row){
    ?>
        <blockquote class="bg-white borderRadius">
            <div class="row" style="margin-bottom: 1%">
                <?php
                $ilanLink = $this->Html->url('/').'ilans/ilan/ilan_id:'.$row['Ilan']['id'];
                ?>

                <div class="col-xs-6 col-sm-5 col-md-3 col-lg-2 text-center">
                    <a href="<?php echo $ilanLink;?>">
                        <?php
                        if(!empty($row['IlanResim'])){
                            echo '<img class="imgHome" alt="madengayrimenkul_'.$row['IlanResim'][0]['id'].'" src="'.$this->Html->url('/').$row['IlanResim'][0]['paththumb'].'"/>';
                        }else{
                            echo '<img class="imgHome"  alt="madengayrimenkul_logo_arama" src="'.$this->Html->url('/').'img/nofoto.png"/>';
                        }
                        ?>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-7 col-md-7 col-lg-8 aramaMarTop10">
                    <div class="fontBold hidden-md hidden-lg" style="text-align: right;"><strong><?php echo $row['Ilan']['ilan_no'];?></strong></div>
                    <div class="fontBold"><?php echo CakeText::truncate($row['Ilan']['baslik'], 45);?></div>
                    <div class="hidden-xs hidden-sm"><?php echo CakeText::truncate(strip_tags($row['Ilan']['aciklama']), 250);?></div>
                    <div class="hidden-md hidden-lg">
                        <div class="">
                            <?php
                            if($row['Ilan']['ilan_tipi'] == 1){
                                echo $row['KonutBilgi']['m_kare'].'m<sup>2</sup>';
                            }else if($row['Ilan']['ilan_tipi'] == 2){
                                echo $row['IsyeriBilgi']['m_kare'].'m<sup>2</sup>';
                            }else if($row['Ilan']['ilan_tipi'] == 3){
                                echo $row['ArsaBilgi']['m_kare'].'m<sup>2</sup>';
                            }
                            ?>
                        </div>
                        <div class=""><?php
                            $str = strrev($row['Ilan']['fiyat']);
                            echo '<i class="fa fa-try"></i> '.substr(strrev(chunk_split($str,3,".")),1);
                            ?></div>
                        <div class=""><?php echo $row['Ilan']['sat_kir']==1?'Satılık':'Kiralık';?></div>

                        <div class="" style="text-align: right;">
                            <a href="<?php echo $ilanLink;?>" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-chevron-right"></span></a>
                        </div>
                    </div>
                </div>
                <div class="hidden-xs hidden-sm col-md-2 col-lg-2 text-right aramaMarTop10">
                    <p class="fontBold pMarBot2"><?php echo $row['Ilan']['ilan_no'];?></p>
                    <p class="pMarBot2">
                    <?php
                    if($row['Ilan']['ilan_tipi'] == 1){
                        echo $row['KonutBilgi']['m_kare'].'m<sup>2</sup>';
                    }else if($row['Ilan']['ilan_tipi'] == 2){
                        echo $row['IsyeriBilgi']['m_kare'].'m<sup>2</sup>';
                    }else if($row['Ilan']['ilan_tipi'] == 3){
                        echo $row['ArsaBilgi']['m_kare'].'m<sup>2</sup>';
                    }
                    ?>
                    </p>

                    <p class="pMarBot2"><?php
                        $str = strrev($row['Ilan']['fiyat']);
                        echo '<i class="fa fa-try"></i> '.substr(strrev(chunk_split($str,3,".")),1);
                        ?></p>
                    <p class="pMarBot2"><?php echo $row['Ilan']['sat_kir']==1?'Satılık':'Kiralık';?></p>
                    <div class="" style="text-align: right;">
                        <a href="<?php echo $ilanLink;?>" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-chevron-right"></span></a>
                    </div>
                </div>
            </div>
        </blockquote>
    <?php
    }

    echo '<div class="row"><ul class="pagination" style="display:table; margin:0 auto;">';
    echo $this->Paginator->prev( '<<', array( 'class' => '', 'tag' => 'li','escape' => false), null, array( 'class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a','escape' => false) );
    echo $this->Paginator->numbers( array( 'tag' => 'li', 'separator' => '', 'currentClass' => 'active', 'currentTag'=>'a' ) );
    echo $this->Paginator->next( '>>', array( 'class' => '', 'tag' => 'li','escape' => false ), null, array( 'class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a','escape' => false) );
    echo '</ul></div>';
    ?>
    <div class="anaDiv">
        <div class="row">
            <div class="col-xs-6">
                <div class="input-group">
                    <input type="text" class="form-control" name="sval" value="<?php echo $sval; ?>"/>
                    <span class="input-group-btn">
                     <button class="btn btn-sm btn-primary" type="button" id="searchButton"><span class="fa fa-search fa-2x"></span></button>
                  </span>
                </div>
            </div>
            <div class="col-xs-6">
                <select id="siralama" class="form-control inputM">
                    <?php $selected = 'selected="selected"'; ?>
                    <?php
                    if(array_key_exists('tarih', $pagg)){
                        if($pagg['tarih']=='desc'){
                            echo '<option value="tarih:desc" selected="selected">Tarih Azalan</option>';
                            echo '<option value="tarih:asc">Tarih Artan</option>'
                                . '<option value="fiyat:desc">Fiyat Azalan</option>
                        <option value="fiyat:asc">Fiyat Artan</option>';
                        }else if($pagg['tarih']=='asc'){
                            echo '<option value="tarih:desc">Tarih Azalan</option>';
                            echo '<option value="tarih:asc" selected="selected">Tarih Artan</option>'
                                . '<option value="fiyat:desc">Fiyat Azalan</option>
                       <option value="fiyat:asc">Fiyat Artan</option>';
                        }
                    }else if(array_key_exists('fiyat', $pagg)){
                        if($pagg['fiyat']=='desc'){
                            echo '<option value="tarih:desc">Tarih Azalan</option>';
                            echo '<option value="tarih:asc">Tarih Artan</option>'
                                . '<option value="fiyat:desc" selected="selected">Fiyat Azalan</option>
                        <option value="fiyat:asc">Fiyat Artan</option>';
                        }else if($pagg['fiyat']=='asc'){
                            echo '<option value="tarih:desc">Tarih Azalan</option>';
                            echo '<option value="tarih:asc">Tarih Artan</option>'
                                . '<option value="fiyat:desc">Fiyat Azalan</option>
                        <option value="fiyat:asc" selected="selected">Fiyat Artan</option>';
                        }
                    }else{
                        echo '<option value="tarih:desc" selected="selected">Tarih Azalan</option>';
                        echo '<option value="tarih:asc">Tarih Artan</option>'
                            . '<option value="fiyat:desc">Fiyat Azalan</option>
                        <option value="fiyat:asc">Fiyat Artan</option>';
                    }
                    ?>
                </select>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        <?php if(array_key_exists('tip',$pagg) && $pagg['tip'] == 1){ ?>
        document.title = "Konut - MADEN Gayrimenkul A.Ş";
        <?php }else if(array_key_exists('tip',$pagg) && $pagg['tip'] == 2){ ?>
        document.title = "İşyeri - MADEN Gayrimenkul A.Ş";
        <?php }else if(array_key_exists('tip',$pagg) && $pagg['tip'] == 3){ ?>
        document.title = "Arsa - MADEN Gayrimenkul A.Ş";
        <?php }else{ ?>
        document.title = "MADEN Gayrimenkul A.Ş";
        <?php
        }
        ?>
        $('#siralama').on('change',function(){
            var vall = $(this).val();
            var urll = '<?php echo $this->Html->url('/');?>ilans/index<?php echo $this->requestAction(array('controller'=>'users','action'=>'GetPagUrl'),$pagg);?>';
            urll = urll.replace('/tarih:desc','');
            urll = urll.replace('/tarih:asc','');
            urll = urll.replace('/fiyat:desc','');
            urll = urll.replace('/fiyat:asc','');
            window.location.href = urll+'/'+vall;
        });

        $('#searchButton').on('click',function(){
            window.location.href = '<?php echo $this->Html->url('/').'ilans/index'.$this->requestAction(array('controller'=>'users','action'=>'GetPagUrlWithoutSval'),$pagg);?>/sval:'+$('input[name="sval"]').val();
        });

    });
</script>