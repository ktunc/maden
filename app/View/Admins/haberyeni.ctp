<?php
echo $this->Html->css('jquery.filer');
echo $this->Html->css('themes/jquery.filer-dragdropbox-theme');
echo $this->Html->script('jquery.filer.min');
echo $this->Html->script('jquery.filer.custom');
?>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Yeni Haber</h5>
            </div>
            <div class="ibox-content">
    <form method="POST" role="form" class="form-horizontal" id="HaberForm" enctype="multipart/form-data">
        <input type="hidden" name="haber_id" value="0"/>
        <div class="form-group">
            <label for="baslik" class="col-xs-3 col-md-2 control-label">Başlık</label>
            <div class="col-xs-9 col-md-10">
                <input type="text" name="baslik" id="baslik" placeholder="Başlik" class="form-control" />
            </div>
        </div>
        <div class="form-group">
            <label for="aciklama" class="col-xs-3 col-md-2 control-label">Açıklama</label>
            <div class="col-xs-9 col-md-10">
                <textarea name="aciklama" id="aciklama" placeholder="Açıklama" class="form-control" rows="10"></textarea>
            </div>
        </div>

        <!-- Resim -->
        <div class="form-group">
            <label for="resim" class="control-label col-xs-3 col-md-2 ">Resimler</label>
            <div class="col-xs-9 col-md-10">
                <div id="content">
                    <!-- Example 2 -->
                    <input type="file" name="files[]" id="filer_input2" multiple="multiple" accept="image/*">
                    <!-- end of Example 2 -->
                </div>
            </div>
        </div>
        <div class="form-group">

        </div>
        <div class="form-group">
            <hr>
        </div>
        <div class="form-group">
            <div class="col-xs-offset-3 col-sm-offset-2">
                <div class="col-xs-6"><button type="reset" class="btn btn-danger">Sıfırla</button></div>
                <div class="col-xs-6 text-right"><button type="button" class="btn btn-primary" id="HaberKaydet">Kaydet</button></div>
            </div>
        </div>
    </form>
</div>
</div>
</div>
</div>
<?php echo $this->Html->script('tinymce/tinymce.min'); ?>
<script type="text/javascript">
    $(document).ready(function(){
        tinymce.init({
            selector: '#aciklama',
            language: 'tr_TR',
            height: 300,
            theme: 'modern',
            plugins: [
                'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'emoticons template paste textcolor colorpicker textpattern imagetools'
            ],
            toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            toolbar2: 'print preview media | forecolor backcolor emoticons',
            image_advtab: true,
            setup: function (editor) {
                editor.on('change', function () {
                    tinymce.triggerSave();
                });
            }
        });


        $('#HaberKaydet').on('click',function(){
            if($('#baslik').val() == ''){
                $.alert({
                    theme:'modern',
                    type:'orange',
                    columnClass:'l',
                    icon:'fa fa-exclamation',
                    title:"Lütfen 'Başlık' alanını boş bırakmayınız.",
                    content:''
                });
            }
            else{
                var formdata = new FormData($('form#HaberForm').get(0));
                $.ajax({
                    type:'POST',
                    url:'<?php echo $this->Html->url('/');?>admins/haberkaydet',
                    data:formdata,
                    beforeSend:function () {
                        $.blockUI();
                    },
                    processData: false,
                    contentType: false
                }).done(function (data) {
                    var dat = $.parseJSON(data);
                    if(dat['hata']){
                        $.confirm({
                            theme:'modern',
                            type:'red',
                            icon:'fa fa-close',
                            title:dat['mesaj'],
                            content:'',
                            onContentReady:function () {
                                $.unblockUI();
                            },
                            buttons:{
                                tamam:{
                                    text:'Tamam',
                                    action:function () {
                                        if(dat['link']){
                                            window.location.href = dat['link'];
                                        }
                                    }
                                }
                            }
                        });
                    }else{
                        $.confirm({
                            theme:'modern',
                            type:'green',
                            icon:'fa fa-check',
                            title:dat['mesaj'],
                            content:'',
                            onContentReady:function () {
                                $.unblockUI();
                            },
                            buttons:{
                                tamam:{
                                    text:'Tamam',
                                    action:function () {
                                        window.location.href = dat['link'];
                                    }
                                }
                            }
                        });
                    }
                }).fail(function () {
                    $.alert({
                        theme:'modern',
                        type:'red',
                        icon:'fa fa-close',
                        title:'Bir hata meydana geldi. Lütfen tekrar deneyin.',
                        content:'',
                        onContentReady:function () {
                            $.unblockUI();
                        }
                    });
                });
            }
        });


    });
</script>