<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        <img src="<?=$this->webroot;?>img/logo.png" style="width: 100%;"/>
        <form class="m-t" role="form" action="<?=$this->Html->url('/');?>admins/login" method="post">
            <div class="form-group">
                <input type="text" name="username" class="form-control" placeholder="Kullanıcı Adı" required="">
            </div>
            <div class="form-group">
                <input type="password" name="pass" class="form-control" placeholder="Şifre" required="">
            </div>
            <button type="submit" class="btn btn-primary block full-width m-b">Giriş</button>

            <a href="#"><small>Forgot password?</small></a>
        </form>
    </div>
</div>