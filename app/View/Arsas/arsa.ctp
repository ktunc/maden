<?php 
echo $this->Html->css('photoswipe');
echo $this->Html->script('simple-inheritance.min');
echo $this->Html->script('code-photoswipe-1.0.11');
echo $this->Html->css('../js/Flickerplate-master/css/flickerplate');
echo $this->Html->script('Flickerplate-master/js/min/modernizr-custom-v2.7.1.min');
echo $this->Html->script('Flickerplate-master/js/min/jquery-finger-v0.1.0.min');
echo $this->Html->script('Flickerplate-master/js/min/flickerplate.min');
?>
<div class="bg-white printwidth">
<div class="ilanUst row">
    <div class="h3 col-xs-12 text-danger fontBold"><?php echo $arsas['Arsa']['baslik'];?></div>
    <div class="col-xs-12 h6"><?php echo $arsas['Sehir']['sehir_adi'];
    if($arsas['Ilce']['id']){
                echo ' / '.$arsas['Ilce']['ilce_adi'];
                if($arsas['Semt']['id']){
                    echo ' / '.$arsas['Semt']['semt_adi'];
                    if($arsas['Mahalle']['id']){
                        echo ' / '.$arsas['Mahalle']['mahalle_adi'];
                    }
                }
            }?>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-sm-6">
        <div class="FlickPadLR">
        <div class="flicker-example flickCSS hidden-print" data-block-text="false">
            <ul id="Gallery">
                <?php
                if(count($arsaRes)>0){
                    foreach($arsaRes as $row){
                         echo '<li style="background-size:100% 100%">'
                                 .'<div class="flick-title">'
                                 . '<a href="'.$this->Html->url('/').$row['ArsaResim']['path'].'">'
                                 . '<img class="imgIcerik" src="'.$this->Html->url('/').$row['ArsaResim']['path'].'" alt="'.$arsas['Arsa']['baslik'].' '.$row['ArsaResim']['id'].'"/>'
                                 . '</a>'
                                 . '</div>'
                                 . '</li>';
                    }
                }else{
                    echo '<li style="background-size:100% 100%">'
                        .'<div class="flick-title">'
                        . '<a href="'.$this->Html->url('/').'img/logo.png">'
                        . '<img class="imgIcerik" src="'.$this->Html->url('/').'img/logo.png" alt="'.$arsas['Arsa']['baslik'].'"/>'
                        . '</a>'
                        . '</div>'
                        . '</li>';
                } ?>
            </ul>
            <!--
            <div class="IlanShareIcon">
                <button class="btn btn-warning btn-sm" id="ShareBut"><i class="fa fa-2x fa-share-alt"></i></button>
            </div>
            -->
        </div>
            <div class="visible-print">
                <?php
                if(count($arsaRes)>0){
                    echo '<img class="imgIcerik" src="'.$this->Html->url('/').$arsaRes[0]['ArsaResim']['path'].'" alt="'.$arsas['Arsa']['baslik'].' '.$arsaRes[0]['ArsaResim']['id'].'"/>';
                }else{
                    echo '<img class="imgIcerik" src="'.$this->Html->url('/').'img/logo.png" alt="'.$arsas['Arsa']['baslik'].'"/>';
                } ?>
            </div>
        </div>
        <?php
        $str = strrev($arsas['Arsa']['fiyat']);
        echo '<div class="h4 anaDiv text-danger fontBold">'.substr(strrev(chunk_split($str,3,".")),1).' '.$arsas['Arsa']['parabirimi'].'</div>';
        ?>
    </div>
    <div class="col-xs-12 col-sm-6 font16">
        <table class="table table-bordered table-striped table-hover">
            <tr>
                <td>İlan Numarası</td>
                <td><?php echo $arsas['Arsa']['ilan_no'];?></td>
            </tr>
            <tr>
                <td>Emlak Tipi</td>
                <td>Arsa</td>
            </tr>
            <?php
            $arsaKey = array('tarih'=>'İlan Tarihi','imar_durum'=>'İmar Durumu','m_kare'=>'m<sup>2</sup>',
                'ada'=>'Ada','parsel'=>'Parsel','tapu'=>'Tapu','kredi'=>'Krediye Uygunluk');
            foreach($arsaKey as $key=>$val){
                if(!empty($arsas['Arsa'][$key])){
                    echo '<tr>';
                    echo '<td>'.$val.'</td>';
                    echo '<td>'.$arsas['Arsa'][$key].'</td>';
                    echo '</tr>';
                }
            }

            if($this->Session->check('UserLogin')){?>
                <tr>
                    <td>Mal Sahibi Adı Soyadı:</td>
                    <td><?php echo $arsas['Arsa']['mal_name'];?></td>
                </tr>
                <tr>
                    <td>Mal Sahibi Telefon No:</td>
                    <td><?php echo $arsas['Arsa']['mal_tel'];?></td>
                </tr>
                <tr>
                    <td>Mal Sahibi Fiyat:</td>
                    <?php
                    $str = strrev($arsas['Arsa']['mal_fiyat']);
                    echo '<td>'.substr(strrev(chunk_split($str,3,".")),1).' '.$arsas['Arsa']['mal_parabirimi'].'</td>';
                    ?>
                </tr>
            <?php } ?>
        </table>
        <div class="FlickPadLR hidden-print" style="width: 100%;">
            <?php if(!empty($arsas['ArsaLocation']) && !empty($arsas['ArsaLocation']['id'])){?>
                <div class="divYan">
                    <img class="ShareIcon" onclick="FuncMapGoruntule(<?php echo $arsas['ArsaLocation']['latitude']; ?>,<?php echo $arsas['ArsaLocation']['longitude']; ?>)" alt="map_direction_icon" src="<?php echo $this->webroot;?>img/map.png" />
                </div>
            <?php } ?>
            
            <?php if(!empty($arsas['Arsa']['video'])){ ?>
                <div class="divYan fRight">
                    <img class="ShareIcon ytvideo" onclick="YTVideoAc(1)" data-yturl="<?php echo $arsas['Arsa']['video']; ?>" src="<?php echo $this->webroot;?>img/shareicon/cam.png"/>
                </div>
            <?php } ?>

            <?php if($IlanAdres && 1==0){ ?>
                <div class="divYan fRight">
                    <img class="ShareIcon" id="OpenIlanAdresModal" src="<?php echo $this->webroot;?>img/shareicon/adres.png"/>
                </div>
            <?php } ?>
            <?php if($IlanMail && 1==0){ ?>
                <div class="divYan fRight">
                    <img class="ShareIcon" id="OpenIlanMailModal" src="<?php echo $this->webroot;?>img/shareicon/mail.png"/>
                </div>
            <?php } ?>
            <?php if($IlanTel && 1==0){ ?>
                <div class="divYan fRight">
                    <img class="ShareIcon" id="OpenIlanTelModal" src="<?php echo $this->webroot;?>img/shareicon/tel.png"/>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
</div>
<input type="hidden" id="arsaId" value="<?php echo $arsas['Arsa']['id']; ?>" />
<?php if(!empty($arsas['Arsa']['aciklama'])){ ?>
    <div class="aback text-white printwidth">
        <div class="row">
            <div class="text-center col-xs-12 hidden-print">
                <h4 type="button" onclick="FuncAciklamaGetir(<?php echo $arsas['Arsa']['id']; ?>)" >Açıklama</h4>
            </div>
            <div class="text-center col-xs-12 visible-print ilanaciklama">
                <?php echo nl2br($arsas['Arsa']['aciklama']); ?>
            </div>
        </div>
    </div>
<?php }?>

<?php 
function generateTarih($tarih){
    $date = explode(' ', $tarih);
    $tarih = explode('-', $date[0]);
    return $tarih[2].'/'.$tarih[1].'/'.$tarih[0];
}
?>
<script type="text/javascript">
$(document).ready(function() {
    document.title = "<?php echo $arsas['Arsa']['baslik'];?>";

Code.photoSwipe('a', '#Gallery');

$('.flicker-example').flicker({
    auto_flick: true,
    auto_flick_delay: 5,
    flick_animation: "transform-slide"
});

    $('#ShareBut').on('click',function(){
        $('#ShareIlanModal').modal({
            keyboard:false,
            backdrop:'static'
        });
    });
    $('#OpenIlanTelModal').on('click',function(){
        $('#IlanTelModal').modal({
            keyboard:false,
            backdrop:'static'
        });
    });
    $('#OpenIlanAdresModal').on('click',function(){
        $('#IlanAdresModal').modal({
            keyboard:false,
            backdrop:'static'
        });
    });
    $('#OpenIlanMailModal').on('click',function(){
        $('#IlanMailModal').modal({
            keyboard:false,
            backdrop:'static'
        });
    });
});


function FuncAciklamaGetir(aId){
    $.blockUI({ css: { backgroundColor: 'transparent', border: 'none'},message: $('#LoaderBlock') });
    setTimeout(function(){
        $.ajax({
            async:false,
            type:'POST',
            url:'<?php echo $this->Html->url('/');?>arsas/aciklamagetir',
            data:'aId='+aId
        }).done(function(data){
            var dat = $.parseJSON(data);
            if(dat){
                $('#AciklamaModal .bodyaciklama').html(dat);
                $('#AciklamaModal #header', '#AciklamaModal #footer').removeClass();
                $('#AciklamaModal #header').addClass('modal-header aback text-white');
                $('#AciklamaModal #footer').addClass('modal-footer aback text-white');
                $('#AciklamaModal').modal({
                    keyboard:false,
                    backdrop:'static'
                });
                $.unblockUI();
            }else{
                $('#UyariModal #UyariContent').html('Bir hata meydana geldi. Lütfen sayfayı yenileyerek tekrar deneyin');
                $('#UyariModal').modal({
                    keyboard:false,
                    backdrop:'static'
                });
                $.unblockUI();
            }
        }).fail(function(){
            $('#UyariModal #UyariContent').html('Lütfen internet bağlantınızı kontrol ediniz.');
            $('#UyariModal').modal({
                keyboard:false,
                backdrop:'static'
            });
            $.unblockUI();
        });
    },500);

}
//function OpenMaps(lati,long){
//    window.location.href = 'http://www.google.com/maps/@'+lati+','+long+',18z';
//}

</script>