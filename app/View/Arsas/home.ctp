<div class="row">
    <div class="col-xs-6">
<!-- SearcForm Bas -->
<?php echo $this->element('searchForm'); ?>
<!-- SearcForm Son -->
    </div>
    <div class="col-xs-6">
        <select id="siralama" class="form-control">
            <?php $selected = 'selected="selected"'; ?>
            <?php
            if(array_key_exists('Arsa.tarih', $sirala)){
                if($sirala['Arsa.tarih']=='DESC'){
                    echo '<option value="tarih:desc" selected="selected">Tarih Azalan</option>';
                    echo '<option value="tarih:asc">Tarih Artan</option>'
                    . '<option value="fiyat:desc">Fiyat Azalan</option>
                        <option value="fiyat:asc">Fiyat Artan</option>';
                }else if($sirala['Arsa.tarih']=='ASC'){
                    echo '<option value="tarih:desc">Tarih Azalan</option>';
                    echo '<option value="tarih:asc" selected="selected">Tarih Artan</option>'
                    . '<option value="fiyat:desc">Fiyat Azalan</option>
                       <option value="fiyat:asc">Fiyat Artan</option>';
                }
            }else if(array_key_exists('Arsa.fiyat', $sirala)){
                if($sirala['Arsa.fiyat']=='DESC'){
                    echo '<option value="tarih:desc">Tarih Azalan</option>';
                    echo '<option value="tarih:asc">Tarih Artan</option>'
                    . '<option value="fiyat:desc" selected="selected">Fiyat Azalan</option>
                        <option value="fiyat:asc">Fiyat Artan</option>';
                }else if($sirala['Arsa.fiyat']=='ASC'){
                    echo '<option value="tarih:desc">Tarih Azalan</option>';
                    echo '<option value="tarih:asc">Tarih Artan</option>'
                    . '<option value="fiyat:desc">Fiyat Azalan</option>
                        <option value="fiyat:asc" selected="selected">Fiyat Artan</option>';
                }
            }else{
                echo '<option value="tarih:desc" selected="selected">Tarih Azalan</option>';
                    echo '<option value="tarih:asc">Tarih Artan</option>'
                    . '<option value="fiyat:desc">Fiyat Azalan</option>
                        <option value="fiyat:asc">Fiyat Artan</option>';
            }
            ?>
        </select>
    </div>
</div>
<div class="row"><hr></div>
<?php 
echo $this->Html->css('font-awesome.min');

foreach($arsas as $row){
?>
<blockquote class="bg-success"> 
<div class="row" style="margin-bottom: 1%">
    <a href="<?php echo $this->Html->url('/');?>arsas/arsa/ilanNo:<?php echo $row['Arsa']['ilan_no'];?>">
    <div class="col-xs-6 col-sm-5 text-center" >
         <?php 
         if($arsaRes[$row['Arsa']['id']]){
             echo '<img class="imgHome" src="'.$this->Html->url('/').$arsaRes[$row['Arsa']['id']][0]['ArsaResim']['path'].'" alt="'.$row['Arsa']['baslik'].' '.$arsaRes[$row['Arsa']['id']][0]['ArsaResim']['id'].'"/>';
        }else{
            echo '<img class="imgHome" src="'.$this->Html->url('/').'img/nofoto.png" style="width:100%" alt="'.$row['Arsa']['baslik'].'"/>';
        }
        ?>
    </div>
    </a>
    <div class="col-xs-6 col-sm-7">
        <!--<div class="row" style="margin-top: 1%;">-->
            <div class="fontBold" style="text-align: right;"><strong><?php echo $row['Arsa']['ilan_no'];?></strong></div>
            <div class="fontBold"><?php echo CakeText::truncate($row['Arsa']['baslik'],45);?></div>
            <div class=""><?php echo $row['Arsa']['m_kare'];?>m<sup>2</sup></div>
            <div class=""><?php 
            $str = strrev($row['Arsa']['fiyat']);
            echo substr(strrev(chunk_split($str,3,".")),1).''.$row['Arsa']['parabirimi'];
            ?></div>
            <div class=""><?php echo $row['Arsa']['sat_kir']==1?'Satılık':'Kiralık';?></div>

            <?php 
            // Giriş yapan User
            if($this->Session->check('UserLogin')){
            ?>
            <div class="fRight" style="text-align: right">
                <a href="<?php echo $this->Html->url('/');?>arsas/arsa/ilanNo:<?php echo $row['Arsa']['ilan_no'];?>" class="btn btn-warning btn-sm"><span class="glyphicon glyphicon-chevron-right"></span></a>
                <a href="<?php echo $this->Html->url('/');?>arsas/duzenle/ilanNo:<?php echo $row['Arsa']['ilan_no'];?>" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-pencil"></span></a>
                <button type="button" class="btn btn-danger btn-sm" onclick="ArsaSil(<?php echo $row['Arsa']['id'];?>)"><span class="glyphicon glyphicon-remove"></span></button>
            </div>
            <?php
            }else{
            ?>
            <div class="fRight" style="text-align: right;">
                <a onclick="<?php echo $this->Html->url('/');?>arsas/arsa/ilanNo:<?php echo $row['Arsa']['ilan_no'];?>" class="btn btn-warning btn-sm"><span class="glyphicon glyphicon-chevron-right"></span></a>
            </div>
            <?php
            }
            ?>
        <!--</div>-->
    </div>
</div>
</blockquote>
<?php
}
echo '<ul class="pagination" style="display:table; margin:0 auto;">';
 
        echo $this->Paginator->prev( '<<', array( 'class' => '', 'tag' => 'li','escape' => false), null, array( 'class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a','escape' => false) );
        echo $this->Paginator->numbers( array( 'tag' => 'li', 'separator' => '', 'currentClass' => 'active', 'currentTag'=>'a' ) );
        echo $this->Paginator->next( '>>', array( 'class' => '', 'tag' => 'li','escape' => false ), null, array( 'class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a','escape' => false) );
        
    echo '</ul>'; 
?>
<script type="text/javascript">
$(function() {
document.title = "ARSA - Maden Gayrimenkul";
      
$('#siralama').on('change',function(){
   var vall = $(this).val();
   var urll = '<?php echo $this->Html->url();?>';
   urll = urll.replace('/tarih:desc','');
   urll = urll.replace('/tarih:asc','');
   urll = urll.replace('/fiyat:desc','');
   urll = urll.replace('/fiyat:asc','');
   window.location.href = urll+'/'+vall;
});
});

function ArsaSil(arsaId){
    if(confirm('Arsayı silmek istediğinizden emin misiniz?')){
        $('#Loader').modal({
            keyboard:false,
            backdrop:'static'
        });
        $.ajax({
            async: false,
            type: 'POST',
            url: "<?php echo $this->Html->url('/');?>arsas/ajaxDeleteArsa",
            data:"arsaId="+arsaId,
            success: function (data) {
                $('#Loader').modal('hide');
                var dat = $.parseJSON(data);
                if(dat['hata']){
                    alert(dat['message']);
                }else{
                    alert(dat['message']);
                    window.location.reload();
                }
            }
        });
    }else{
        return false;
    }
}
</script>