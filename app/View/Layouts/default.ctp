<?php
/**
 *
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

//$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
?>
<!DOCTYPE html>
<html lang="tr-TR">
<head>
	<?php echo $this->Html->charset(); 
                echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
    ?>
    
    <meta name="description" content="Satılık, kiralık, konut, işyeri, arsa yatırım ve yönetimi - Mutlu Kazanç Kapısı..." />
    <meta name="keywords" content="Eskişehir Yolu satılık yatırım arsaları, Temelli, Malıköy, Ihlamurkent satılık villa, Alcı, Türkobası, Anayurt, Bacı, Başkent OSB, kiralık fabrika, Anadolu OSB, ASO 2 OSB, Konutkent, Çayyolu, Kuleevo, Elmar Towers, Elya Tower, Azel Kule" />
    <!-- <meta name="robots" content="index, follow" /> -->
    <meta name="rating" content="All" />
    <!--<meta name="copyright" content="www.batikapigayrimenkul.com" />
    <meta name="author" content="Soner HAYIRLI" />
	<meta name = "alexaVerifyID" content = " SiGSWe5sSp2pqHrlPdwbGEUXRzk " />-->
    <?php echo HtmlMetaComponent::elements(); ?>
    
	<title>
        <?php
        echo 'MADEN Gayrimenkul A.Ş.';
        ?>
	</title>
        <link rel="icon" type="image/x-icon" href="<?php echo $this->Html->url('/');?>img/logo.png">
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo $this->Html->url('/');?>img/logo.png">
        <link hreflang="tr">
	<?php
        echo $this->Html->css('bootsrap-readable.min');
        echo $this->Html->css('font-awesome.min');
        echo $this->Html->css('select2.min');
        echo $this->Html->css('batikapi');
        echo $this->Html->css('jquery-confirm.min');
        echo $this->Html->css(array('admin/plugins/tooltipster/tooltipster.bundle.min',
            'admin/plugins/tooltipster/themes/tooltipster-sideTip-punk.min',
            'admin/plugins/iCheck/all'));
        echo $this->Html->script('jquery-1.11.min');
        echo $this->Html->script('bootstrap.min');
        echo $this->Html->script('select2/select2.full.min');
        echo $this->Html->script( 'admin/plugins/tooltipster/tooltipster.bundle.min');
        echo $this->Html->script( 'jquery-confirm.min');
        echo $this->Html->script( 'admin/plugins/iCheck/icheck.min');
	?>
<!--    <meta name="viewport" content="width=device-width, initial-scale=1">-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
</head>
<body cz-shortcut-listen="true">
<script type="text/javascript">
$(document).ready(function(){
	$.blockUI({ css: { backgroundColor: 'transparent', border: 'none'},message: $('#LoaderBlock') });
});
</script>
<!-- Facebook -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/tr_TR/sdk.js#xfbml=1&version=v2.4";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>

<!-- Google Plus -->
<script src="https://apis.google.com/js/platform.js" async defer>
//  {
//      lang: 'tr'
//  }
</script>

<!-- Twitter -->
<script>
!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');
</script>
    <?php //echo $this->element('VideoModal'); ?>

<div class="container100">
    <div id="st-container" class="st-container st-effect-11">
    <?php echo $this->element('menu');?>
        <div class="st-pusher">
            <div class="st-content"><!-- this is the wrapper for the content -->
                <div class="st-content-inner">
                    <div class="main clearfix">
                        <div id="content" class="container-fluid-main">
							<div class="container">
								<div class="row" id="LogoBaslik" style="margin-top:5px;margin-bottom:5px">
									<div class="col-xs-8 col-sm-6 text-left">
										<img alt="maden_logo" src="<?php echo $this->Html->url('/');?>img/logo.png" class="logo" onclick="window.location.href='<?php echo $this->Html->url('/');?>'"/>
									</div>
									<div class="col-xs-4 col-sm-6 text-right">
										<span id="st-trigger-effects">
											<button data-effect="st-effect-11" type="button" class="btn btn-maden btn-xs"><i class="fa fa-2x fa-bars"></i></button>
											<!--<button data-effect="st-effect-11" type="button" class="btn btn-warning btn-xs"><i class="fa fa-2x fa-bars"></i></button>-->
										</span>
									</div>
								</div>
							</div>
                            <!-- <div class="row"><hr></div> -->
                            <div class="container">
                            <?php echo $this->Session->flash(); ?>
                            <?php echo $this->fetch('content'); ?>
                                <?php  //echo $this->element('sql_dump'); ?>
                            </div>
                            <div class="container cfooter hidden-print">
                                <?php echo $this->element('iletisim'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <?php
    echo $this->element('TelModal');
    echo $this->element('UyariModal');
    echo $this->element('mapText');
    echo $this->element('detSearch');
    echo $this->element('LoadModal');
    echo $this->element('ShareIlanModal');
    //echo $this->element('IlanTelModal');
    //echo $this->element('IlanAdresModal');
    //echo $this->element('IlanMailModal');
    //echo $this->element('YorumModal');
    echo $this->element('YoutubeModal');
    echo $this->element('AciklamaModal');
    echo $this->element('MapModal');
    echo $this->element('loader');
    ?>
<?php
echo $this->Html->script('menu/classie.min');
echo $this->Html->script('menu/sidebarEffects');
echo $this->Html->script('blockUI');
?>
<script type="text/javascript">
$(document).ready(function(){
	$('body,body>div.container,div.st-container').css('min-height',$(window).height());
	$('div.container-fluid-main,div.container-body').css('min-height',$('div.container100').height());

    $('.tooltipss').tooltipster({
        theme: 'tooltipster-punk',
        contentAsHTML: true,
        animation:'grow'
    });
});
$(window).load(function(){
    var windowHeight = $(window).height();
    var HeaderHeight = $('#content.container-fluid-main').children('div.container:eq(0)').outerHeight();
    var BodyHeight = $('#content.container-fluid-main').children('div.container:eq(1)').outerHeight();
    var FooterHeight = $('#content.container-fluid-main').children('div.container:eq(2)').outerHeight();
    if(windowHeight > (HeaderHeight+BodyHeight+FooterHeight)){
        $('.cfooter').css('margin-top',(windowHeight-(HeaderHeight+BodyHeight+FooterHeight)));
    }
    $.unblockUI();
});
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : evt.keyCode;

        if (!(charCode == 8
                || (charCode >= 35 && charCode <= 40)
                || (charCode >= 48 && charCode <= 57)
            )){
            return false;
        }

        return true;
    }

function checkemail(str){
    var filter=/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    if (filter.test(str))
        return true;
    else{
        return false;
    }
}

function FuncAlert(title, content, columnClass, type) {
    $.alert({
        theme:'modern',
        type: type,
        columnClass: columnClass,
        title: title,
        content: content
    });
}
</script>
</body>
</html>