<?php
/**
 *
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

//$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
?>
<!DOCTYPE html>
<html lang="tr-TR">
<head>
    <?php echo $this->Html->charset();
    echo $this->fetch('meta');
    echo $this->fetch('css');
    echo $this->fetch('script');
    ?>

    <meta name="description" content="Satılık, kiralık, konut, işyeri, arsa yatırım ve yönetimi - Mutlu Kazanç Kapısı..." />
    <meta name="keywords" content="Eskişehir Yolu satılık yatırım arsaları, Temelli, Malıköy, Ihlamurkent satılık villa, Alcı, Türkobası, Anayurt, Bacı, Başkent OSB, kiralık fabrika, Anadolu OSB, ASO 2 OSB, Konutkent, Çayyolu, Kuleevo, Elmar Towers, Elya Tower, Azel Kule" />
    <!-- <meta name="robots" content="index, follow" /> -->
    <meta name="rating" content="All" />
    <!--<meta name="copyright" content="www.batikapigayrimenkul.com" />
    <meta name="author" content="Soner HAYIRLI" />
	<meta name = "alexaVerifyID" content = " SiGSWe5sSp2pqHrlPdwbGEUXRzk " />-->
    <?php echo HtmlMetaComponent::elements(); ?>

    <title>
        <?php
        echo 'MADEN Gayrimenkul A.Ş.';
        ?>
    </title>
    <link rel="icon" type="image/x-icon" href="<?php echo $this->Html->url('/');?>img/logo.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo $this->Html->url('/');?>img/logo.png">
    <link hreflang="tr">
    <?php
    echo $this->Html->css(array(
        'admin/bootstrap.min',
        'admin/style',
        'admin/animate',
        'font-awesome.min'
    ));
    echo $this->Html->script(array(
        'admin/jquery-3.1.1.min',
        'admin/bootstrap.min'
    ));
    ?>
    <!--    <meta name="viewport" content="width=device-width, initial-scale=1">-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
</head>
<body class="maden-bg">
<?php echo $this->Session->flash(); ?>
<?php echo $this->fetch('content'); ?>
</body>
</html>