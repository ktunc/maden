<?php 
echo $this->Html->css('font-awesome.min');
$pagg = $this->passedArgs;
$this->Paginator->options(array('url' => $pagg));
?>
<div class="row">
    <div class="col-xs-6">
<!-- SearcForm Bas -->
<?php echo $this->element('searchForm'); ?>
<!-- SearcForm Son -->
    </div>
    <div class="col-xs-6">
        <select id="siralama" class="form-control">
            <?php $selected = 'selected="selected"'; ?>
            <?php
            if(array_key_exists('tarih', $pagg)){
                if($pagg['tarih']=='desc'){
                    echo '<option value="tarih:desc" selected="selected">Tarih Azalan</option>';
                    echo '<option value="tarih:asc">Tarih Artan</option>'
                    . '<option value="fiyat:desc">Fiyat Azalan</option>
                        <option value="fiyat:asc">Fiyat Artan</option>';
                }else if($pagg['tarih']=='asc'){
                    echo '<option value="tarih:desc">Tarih Azalan</option>';
                    echo '<option value="tarih:asc" selected="selected">Tarih Artan</option>'
                    . '<option value="fiyat:desc">Fiyat Azalan</option>
                       <option value="fiyat:asc">Fiyat Artan</option>';
                }
            }else if(array_key_exists('fiyat', $pagg)){
                if($pagg['fiyat']=='desc'){
                    echo '<option value="tarih:desc">Tarih Azalan</option>';
                    echo '<option value="tarih:asc">Tarih Artan</option>'
                    . '<option value="fiyat:desc" selected="selected">Fiyat Azalan</option>
                        <option value="fiyat:asc">Fiyat Artan</option>';
                }else if($pagg['fiyat']=='asc'){
                    echo '<option value="tarih:desc">Tarih Azalan</option>';
                    echo '<option value="tarih:asc">Tarih Artan</option>'
                    . '<option value="fiyat:desc">Fiyat Azalan</option>
                        <option value="fiyat:asc" selected="selected">Fiyat Artan</option>';
                }
            }else{
                echo '<option value="tarih:desc" selected="selected">Tarih Azalan</option>';
                    echo '<option value="tarih:asc">Tarih Artan</option>'
                    . '<option value="fiyat:desc">Fiyat Azalan</option>
                        <option value="fiyat:asc">Fiyat Artan</option>';
            }
            ?>
        </select>
    </div>
</div>
<div class="row"><hr></div>
<?php 
if($konuts){
foreach ($konuts as $row) {
?>
<blockquote class="bg-danger">
<div class="row" style="margin-bottom: 1%">
    <a href="<?php echo $this->Html->url('/');?>konuts/konut/ilanNo:<?php echo $row['Konut']['ilan_no'];?>">
    <div class="col-xs-6 col-sm-5 text-center">
         <?php 
         if($konutRes[$row['Konut']['id']]){
            echo '<img class="imgHome" src="'.$this->Html->url('/').$konutRes[$row['Konut']['id']][0]['KonutResim']['path'].'"/>';
        }else{
            echo '<img class="imgHome" src="'.$this->Html->url('/').'img/nofoto.png"/>';
        }
            ?>
    </div>
    </a>
    
    <div class="col-xs-6 col-sm-7">
        <div class="row">
            <div class="col-xs-12" style="text-align: right"><strong><?php echo $row['Konut']['ilan_no'];?></strong></div>
            <div class="col-xs-12"><?php echo CakeText::truncate($row['Konut']['baslik'],45);?></div>
            <div class="col-xs-12"><?php echo $row['Konut']['m_kare'];?>m<sup>2</sup></div>
            <div class="col-xs-12"><?php $str = strrev($row['Konut']['fiyat']);
            echo substr(strrev(chunk_split($str,3,".")),1).''.$row['Konut']['parabirimi'];?></div>
            <div class="col-xs-12"><?php echo $row['Konut']['sat_kir']==1?'Satılık':'Kiralık';?></div>
            <?php 
            // Giriş yapan User
            if($this->Session->check('UserLogin')){
            ?>
            <div class="col-xs-12 fRight" style="text-align: right">                
                <a href="<?php echo $this->Html->url('/');?>konuts/konut/ilanNo:<?php echo $row['Konut']['ilan_no'];?>" class="btn btn3d btn-primary btn-sm"><span class="glyphicon glyphicon-chevron-right"></span></a>
                <a href="<?php echo $this->Html->url('/');?>konuts/duzenle/ilanNo:<?php echo $row['Konut']['ilan_no'];?>" class="btn btn3d btn-default btn-sm"><span class="glyphicon glyphicon-pencil"></span></a>
                <button type="button" class="btn btn3d btn-warning btn-sm" onclick="KonutSil(<?php echo $row['Konut']['id'];?>)"><span class="glyphicon glyphicon-remove"></span></button>
            </div>
            <?php
            }else{
            ?>
            <div class="col-xs-12" style="text-align: right;">
                <a href="<?php echo $this->Html->url('/');?>konuts/konut/ilanNo:<?php echo $row['Konut']['ilan_no'];?>" class="btn btn3d btn-primary btn-sm"><span class="glyphicon glyphicon-chevron-right"></span></a>
            </div>
            <?php
            }
            ?>
        </div>
    </div>
</div>
</blockquote>
<?php
}
}
if($isyeris){
foreach($isyeris as $row){
?>
<blockquote class="bg-primary"> 
<div class="row" style="margin-bottom: 1%">
    <a href="<?php echo $this->Html->url('/');?>isyeris/isyeri/ilanNo:<?php echo $row['Isyeri']['ilan_no'];?>">
    <div class=" col-xs-6 col-sm-5 text-center">
        <?php 
        if($isyeriRes[$row['Isyeri']['id']]){
            echo '<img class="imgHome" src="'.$this->Html->url('/').$isyeriRes[$row['Isyeri']['id']][0]['IsyeriResim']['path'].'"/>';
        }else{
            echo '<img class="imgHome" src="'.$this->Html->url('/').'img/nofoto.png"/>';
        }
        ?>
    </div>
    </a>
    
    <div class="col-xs-6 col-sm-7">
        <div class="row">
            <div class="col-xs-12" style="text-align: right"><strong><?php echo $row['Isyeri']['ilan_no'];?></strong></div>
            <div class="col-xs-12"><?php echo CakeText::truncate($row['Isyeri']['baslik'],45);?></div>
            <div class="col-xs-12"><?php echo $row['Isyeri']['m_kare'];?> m<sup>2</sup></div>
            <div class="col-xs-12"><?php $str = strrev($row['Isyeri']['fiyat']);
            echo substr(strrev(chunk_split($str,3,".")),1).''.$row['Isyeri']['parabirimi'];?></div>
            <div class="col-xs-12"><?php echo $row['Isyeri']['sat_kir']==1?'Satılık':'Kiralık';?></div>
<!--            <div class="col-xs-12" style="text-align: center;"><?php echo $row['Sehir']['sehir_adi'];
            if($row['Ilce']['id']){
                echo ' / '.$row['Ilce']['ilce_adi'];
                if($row['Semt']['id']){
                    echo ' / '.$row['Semt']['semt_adi'];
                    if($row['Mahalle']['id']){
                        echo ' / '.$row['Mahalle']['mahalle_adi'];
                    }
                }
            }?></div>-->
            <?php 
            // Giriş yapan User
            if($this->Session->check('UserLogin')){
            ?>
            <div class="col-xs-12 fRight" style="text-align: right">                
                <a href="<?php echo $this->Html->url('/');?>isyeris/isyeri/ilanNo:<?php echo $row['Isyeri']['ilan_no'];?>" class="btn btn3d btn-warning btn-group btn-sm"><span class="glyphicon glyphicon-chevron-right"></span></a>
                <a href="<?php echo $this->Html->url('/');?>isyeris/duzenle/ilanNo:<?php echo $row['Isyeri']['ilan_no'];?>" class="btn btn3d btn-default btn-group btn-sm"><span class="glyphicon glyphicon-pencil"></span></a>
                <button type="button" class="btn btn3d btn-danger btn-sm" onclick="IsyeriSil(<?php echo $row['Isyeri']['id'];?>)"><span class="glyphicon glyphicon-remove"></span></button>
            </div>
            <?php
            }else{
            ?>
            <div class="col-xs-12" style="text-align: right;">
                <a href="<?php echo $this->Html->url('/');?>isyeris/isyeri/ilanNo:<?php echo $row['Isyeri']['ilan_no'];?>" class="btn btn3d btn-warning btn-group btn-sm"><span class="glyphicon glyphicon-chevron-right"></span></a>
            </div>
            <?php
            }
            ?>
        </div>
    </div>
</div>
</blockquote>
<?php
}
}
if($arsas){
foreach($arsas as $row){
?>
<blockquote class="bg-success">
<div class="row" style="margin-bottom: 1%">
    <a href="<?php echo $this->Html->url('/');?>arsas/arsa/ilanNo:<?php echo $row['Arsa']['ilan_no'];?>">
    <div class="col-xs-6 col-sm-5 text-center" >
    <?php 
    if($arsaRes[$row['Arsa']['id']]){
        echo '<img class="imgHome" src="'.$this->Html->url('/').$arsaRes[$row['Arsa']['id']][0]['ArsaResim']['path'].'"/>';
    }else{
        echo '<img class="imgHome" src="'.$this->Html->url('/').'img/nofoto.png"/>';
    }
    ?>
    </div>
    </a>
    
    <div class="col-xs-6 col-sm-7">
        <div class="row">
            <div class="col-xs-12" style="text-align: right"><strong><?php echo $row['Arsa']['ilan_no'];?></strong></div>
            <div class="col-xs-12"><?php echo CakeText::truncate($row['Arsa']['baslik'],45);?></div>
            <div class="col-xs-12"><?php echo $row['Arsa']['m_kare'];?>m<sup>2</sup></div>
            <div class="col-xs-12"><?php $str = strrev($row['Arsa']['fiyat']);
            echo substr(strrev(chunk_split($str,3,".")),1).''.$row['Arsa']['parabirimi'];?></div>
            <div class="col-xs-12"><?php echo $row['Arsa']['sat_kir']==1?'Satılık':'Kiralık';?></div>
            <?php 
            // Giriş yapan User
            if($this->Session->check('UserLogin')){
            ?>
            <div class="col-xs-12 fRight" style="text-align: right">
                <a href="<?php echo $this->Html->url('/');?>arsas/arsa/ilanNo:<?php echo $row['Arsa']['ilan_no'];?>" class="btn btn3d btn-warning btn-group btn-sm"><span class="glyphicon glyphicon-chevron-right"></span></a>
                <a href="<?php echo $this->Html->url('/');?>arsas/duzenle/ilanNo:<?php echo $row['Arsa']['ilan_no'];?>" class="btn btn3d btn-default btn-group btn-sm"><span class="glyphicon glyphicon-pencil"></span></a>
                <button type="button" class="btn btn3d btn-danger btn-sm" onclick="ArsaSil(<?php echo $row['Arsa']['id'];?>)"><span class="glyphicon glyphicon-remove"></span></button>
            </div>
            <?php
            }else{
            ?>
            <div class="col-xs-12 fRight" style="text-align: right;">
                <a onclick="<?php echo $this->Html->url('/');?>arsas/arsa/ilanNo:<?php echo $row['Arsa']['ilan_no'];?>" class="btn btn3d btn-warning btn-group btn-sm"><span class="glyphicon glyphicon-chevron-right"></span></a>
            </div>
            <?php
            }
            ?>
        </div>
    </div>
</div>
</blockquote>
<?php
}
}
 echo '<ul class="pagination" style="display:table; margin:0 auto;">';
 
        echo $this->Paginator->prev( '<<', array( 'class' => '', 'tag' => 'li','escape' => false), null, array( 'class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a','escape' => false) );
        echo $this->Paginator->numbers( array( 'tag' => 'li', 'separator' => '', 'currentClass' => 'active', 'currentTag'=>'a' ) );
        echo $this->Paginator->next( '>>', array( 'class' => '', 'tag' => 'li','escape' => false ), null, array( 'class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a','escape' => false) );
        
    echo '</ul>'; 
?>
<script type="text/javascript">
$(function() {
      
$('#siralama').on('change',function(){
   var vall = $(this).val();
   var urll = '<?php echo $this->Html->url('/');?>users/search<?php echo getPagg($pagg);?>';
   urll = urll.replace('/tarih:desc','');
   urll = urll.replace('/tarih:asc','');
   urll = urll.replace('/fiyat:desc','');
   urll = urll.replace('/fiyat:asc','');
   window.location.href = urll+'/'+vall;
});

});

function ArsaSil(arsaId){
    if(confirm('Arsayı silmek istediğinizden emin misiniz?')){
        $('#Loader').modal({
            keyboard:false,
            backdrop:'static'
        });
        $.ajax({
            async: false,
            type: 'POST',
            url: "<?php echo $this->Html->url('/');?>arsas/ajaxDeleteArsa",
            data:"arsaId="+arsaId,
            success: function (data) {
                        alert('Arsa silme işlemi başarıyla gerçekleşti.');
                        window.location.href="<?php echo $this->Html->url('/')?>arsas/home";
                    }
        });
    }else{
        return false;
    }
}

function IsyeriSil(isyeriId){
    if(confirm('İşyerini silmek istediğinizden emin misiniz?')){
        $('#Loader').modal({
            keyboard:false,
            backdrop:'static'
        });
        $.ajax({
            async: false,
            type: 'POST',
            url: "<?php echo $this->Html->url('/');?>isyeris/ajaxDeleteIsyeri",
            data:"isyeriId="+isyeriId,
            success: function (data) {
                        alert('İşyeri silme işlemi başarıyla gerçekleşti.');
                        window.location.href="<?php echo $this->Html->url('/')?>isyeris/home";
                    }
        });
    }else{
        return false;
    }
}

function KonutSil(konutId){
    if(confirm('Konutu silmek istediğinizden emin misiniz?')){
        $('#Loader').modal({
            keyboard:false,
            backdrop:'static'
        });
        $.ajax({
            async: false,
            type: 'POST',
            url: "<?php echo $this->Html->url('/');?>konuts/ajaxDeleteKonut",
            data:"konutId="+konutId,
            success: function (data) {
                        alert('Konut silme işlemi başarıyla gerçekleşti.');
                        window.location.href="<?php echo $this->Html->url('/')?>konuts/home";
                    }
        });
    }else{
        return false;
    }
}
</script>
<?php 
function getPagg($data){
    $ekle = '';
    foreach($data as $key=>$val){
        $ekle .= '/'.$key.':'.$val;
    }
    return $ekle;
}
?>