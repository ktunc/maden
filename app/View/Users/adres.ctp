<div class="col-xs-12"><strong class="col-xs-12 text-center h3">Malıköy Mah. Ihlamurkent Arıkan Sk. No:16/33 Sincan Ankara</strong></div>
<div id="adresLocate"  class="col-xs-12" style="min-height: 300px;margin-top: 2%;" ></div>
<div class="col-xs-12" style="margin-top: 4%;"><button class="btn btn3d btn-danger fontBold" onclick="window.location.href='<?php echo $this->Html->url('/');?>'">Geri</button></div>
<script type="text/javascript">
$(document).ready(function(){
    document.title = "İLETİŞİM - Maden Gayrimenkul Yatırım";
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&' +
        'callback=initialize';
    document.body.appendChild(script); 
});

function initialize() {
   var mapOptions = {
    zoom: 17,
    center: new google.maps.LatLng(39.758322,32.425278)
    };

    var map = new google.maps.Map(document.getElementById('adresLocate'),  mapOptions);

      var marker = new google.maps.Marker({
              position: {lat: 39.758322, lng: 32.425278},
              map: map
      });
      var infowindow = new google.maps.InfoWindow({
              content: '<p style="width:100%">BATIKAPI Gayrimenkül</p>'
      });

      google.maps.event.addListener(marker, 'click', function() {
              infowindow.open(map, marker);
      });
}
</script>