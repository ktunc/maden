<div class="">
<div class="row">
    <div class="col-xs-12 col-sm-12 text-center" style="margin-bottom:15px">
        <span class="text-white h2 fontBold">İletişim</span>
    </div>
</div>
<div class="row thumb iletFont">
    <div class="col-xs-12 col-sm-12">
        <ul style="padding-left: 0px;">
        <?php 
        foreach($ilet as $row){
            echo '<li>';
            echo '<div class="row">';
            echo '<div class="col-xs-5 col-sm-3">';
            if($row['Iletisim']['type'] == 1){
                echo '<i class="fa fa-phone">';
            }else if($row['Iletisim']['type'] == 2){
                echo '<i class="fa fa-phone-square">';
            }else if($row['Iletisim']['type'] == 3){
                 echo '<i class="fa fa-envelope">';
            }else if($row['Iletisim']['type'] == 4){
                echo '<i class="fa fa-globe">';
            }else if($row['Iletisim']['type'] == 5){
                echo '<i class="fa fa-lg fa-map-marker">';
            }
            echo '</i> ';
            echo '<strong>';
            if($row['Iletisim']['type'] == 1){
                echo 'Telefon';
            }else if($row['Iletisim']['type'] == 2){
                echo 'Faks';
            }else if($row['Iletisim']['type'] == 3){
                 echo 'Email';
            }else if($row['Iletisim']['type'] == 4){
                echo 'Web Sitesi';
            }else if($row['Iletisim']['type'] == 5){
                echo 'Adres';
            }
            echo '</strong>';
            echo '</div>';
            echo '<div class="col-xs-7 col-sm-9">';
            echo '<strong>:</strong> '.$row['Iletisim']['iletisim'];
            echo '</div>';
            echo '</div>';
            echo '</li>';
        }
        ?>
    </ul>
    </div>
</div>
<?php if($loc){ ?>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div id="us1" class="gmap"></div>
    </div>    
</div>
<?php } ?>

<?php //echo $this->element('iletisim');?>

<?php if($this->Session->check('UserLogin')){ ?>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <a href="<?php echo $this->Html->url('/');?>users/iletisimedit" class="btn3d btn btn-primary pull-right font16"><i class="fa fa-pencil"></i> Düzenle</a>
    </div>
</div>
<?php } ?>
</div>
<?php 
// Google Maps Kontrolü
echo $this->Html->script('locationpicker.jquery.notdrag');
//echo $this->Html->script('http://maps.google.com/maps/api/js?sensor=false&libraries=places');
// Google Maps Kontrolü Son
?>

<script type="text/javascript">
$(document).ready(function(){
    document.title = "İletişim - MADEN Gayrimenkul A.Ş.";
    
    // Gmap Uygulaması u1
    $('#us1').locationpicker({
	location: {latitude: <?php echo !empty($loc)?$loc['Location']['latitude']:0;?>, longitude: <?php echo !empty($loc)?$loc['Location']['longitude']:0;?>},
	radius: 10,
        zoom:17,
        draggable:false
    });
    // Gmap Uygulaması u1 SON 
});
</script>