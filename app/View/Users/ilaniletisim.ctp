<?php 
$iletArray = array(1=>'Telefon',2=>'Faks',3=>'Email',4=>'Web',5=>'Adres');
$iletSelect = '';
foreach($iletArray as $key=>$val){
    $iletSelect .= '<option value="'.$key.'">'.$val.'</option>';
}
?>
<div class="container">
<form method="POST" enctype="multipart/form-data" id="IletisimForm" role="form" class="form-signin" action="<?php echo $this->Html->url('/');?>users/ilaniletkaydet">
<div class="row">
    <div class="col-xs-12 col-sm-12 text-center" style="margin-bottom:15px">
        <span class="text-danger h2 fontBold">İlan İletişim</span>
    </div>
</div>
<div class="row thumb iletFont" id="Ilets">
    <?php 
    foreach ($ilet as $row){
        $value = "";
        foreach($iletArray as $key=>$val){
            if($row['IlanIletisim']['type'] == $key){
                $value = $val;
            }
        }
    ?>
    <div class="col-xs-12 col-sm-12 borderBottom">
        <div class="row">
            <div class="col-xs-4">
                <?php echo $value; ?>:
            </div>
            <div class="col-xs-6">
                <?php echo $row['IlanIletisim']['iletisim']; ?>
            </div>
            <div class="col-xs-2 text-right">
                <button type="button" class="btn3d btn btn-sm btn-danger fontBold" onclick="FuncIlanIletSil(<?php echo $row['IlanIletisim']['id'];?>)">Sil</button>
            </div>
        </div>
    </div>
    <?php
    }
    ?>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <button type="button" class="btn3d btn btn-sm btn-primary fontBold" id="addIlet"><i class="fa fa-plus-square"></i> İleteşim Ekle</button>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <button type="button" class="btn3d btn btn-success fontBold" id="iletKaydet1">Kaydet</button>
    </div>
</div>
</form>
</div>
<script type="text/javascript">

var ilet = '<div class="col-xs-12">'+
        '<div class="row">'+
            '<div class="col-xs-4">'+
            '<select name="iletType[]" class="form-control"><?php echo $iletSelect; ?></select>'+
            '</div>'+
            '<div class="col-xs-6">'+
            '<input type="text" name="iletName[]" class="form-control"/>'+
            '</div>'+
            '<div class="col-xs-2">'+
            '<button type="button" class="btn3d btn-sm btn btn-danger fontBold" id="iletSil">Sil</button>'+
            '</div>'+
        '</div>'+
'</div>';

function FuncIlanIletSil(IletId){
    if(confirm('İlan İletişimini Silmek İstediğinizden Emin Misiniz?')){
        $.ajax({
            async: false,
            type: 'POST',
            url: "<?php echo $this->Html->url('/');?>users/ilaniletsil",
            data: 'IletId='+IletId
        }).done(function(data){
            window.location.reload();
        });
    }
    return false;
}
$(document).ready(function(){
    
    $('#addIlet').on('click',function(e){
        e.preventDefault();
        $('#Ilets').append(ilet);
    });
    
    $('#Ilets').on('click','#iletSil',function(e){
        e.preventDefault();
        $(this).closest('div.col-xs-12').remove();
    });

    $('#iletKaydet1').on('click',function(e){
        e.preventDefault();
        var iletHata = 0;
        $('#IletisimForm input[name="iletName[]"]').each(function(key,val){
            if(val.value == '' || val.length == 0){
                iletHata++;
            }
        });

        if(iletHata>0){
            alert('İletişim bilgilerini boş bırakmayınız. Boş olan iletişim bilgilerini silip tekrar deneyin.');
        }else{
            $('#IletisimForm').submit();
        }
    });
});

</script>