<?php 
// create new empty worksheet and set default font
$this->PhpExcel->createWorksheet()
    ->setDefaultFont('Calibri', 12);

// define table cells
$table = array(
    array('label' => 'İi', 'filter' => true),
    array('label' => 'Şş', 'filter' => true),
    array('label' => 'Üü'),
    array('label' => 'Ğğ', 'width' => 50, 'wrap' => true),
    array('label' => 'ÇçÖöı')
);

// add heading with different font and bold text
$this->PhpExcel->addTableHeader($table, array('name' => 'Cambria', 'bold' => true));

// add data
for ($i = 0; $i<5; $i++) {
    $this->PhpExcel->addTableRow(array(
        'İi',
        'ÜüÖö',
        'Çç',
        'Ğğ',
        'Şş'
    ));
}

// close table and output
$this->PhpExcel->addTableFooter()
    ->output();
?>