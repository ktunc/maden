<?php 
$iletArray = array(1=>'Telefon',2=>'Faks',3=>'Email',4=>'Web',5=>'Adres');
$iletSelect = '';
foreach($iletArray as $key=>$val){
    $iletSelect .= '<option value="'.$key.'">'.$val.'</option>';
}
?>

<div class="bg-white printwidth">
<form method="POST" enctype="multipart/form-data" id="IletisimForm" role="form" class="form-signin" action="<?php echo $this->Html->url('/');?>users/iletisimkaydet">
<div class="row">
    <div class="col-xs-12 text-center">
        <span class="text-danger h2 fontBold">İletişim</span>
    </div>
</div>
<div class="row" id="Ilets">
    <?php 
    foreach ($ilet as $row){
        $rowSelect = '';
        foreach($iletArray as $key=>$val){
            $selected = '';
            if($row['Iletisim']['type'] == $key){
                $selected = 'selected="selected"';
            }
            $rowSelect .= '<option value="'.$key.'" '.$selected.'>'.$val.'</option>';
        }
    ?>
    <div class="col-xs-12 col-sm-12">
        <div class="row">
            <div class="col-xs-4">
                <select name="iletType[]" class="form-control"><?php echo $rowSelect; ?></select>
            </div>
            <div class="col-xs-6">
                <input type="text" name="iletName[]" class="form-control" value="<?php echo $row['Iletisim']['iletisim']; ?>"/>
            </div>
            <div class="col-xs-2">
                <button type="button" class="btn3d btn btn-sm btn-danger fontBold" id="iletSil">Sil</button>
            </div>
        </div>
    </div>
    <?php
    }
    ?>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <button type="button" class="btn3d btn btn-sm btn-primary fontBold" id="addIlet"><i class="fa fa-plus-square"></i> İleteşim Ekle</button>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <label for="location"><h3 class="text-danger">Adres Location:</h3></label>
        <input type="text" id="us1-address" name="location"  class="form-control"/>
        <div id="us1" class="gmap"></div>
        <input type="hidden" id="us1-lat" name="latitude"/>
        <input type="hidden" id="us1-lon" name="longitude"/>
    </div>    
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <button type="button" class="btn3d btn btn-success fontBold" id="iletKaydet1">Kaydet</button>
    </div>
</div>
</form>
</div>
<?php 
// Google Maps Kontrolü
echo $this->Html->script('locationpicker.jquery');
echo $this->Html->script('http://maps.google.com/maps/api/js?sensor=false&libraries=places');
// Google Maps Kontrolü Son
?>
<script type="text/javascript">

var ilet = '<div class="col-xs-12">'+
        '<div class="row">'+
            '<div class="col-xs-4">'+
            '<select name="iletType[]" class="form-control"><?php echo $iletSelect; ?></select>'+
            '</div>'+
            '<div class="col-xs-6">'+
            '<input type="text" name="iletName[]" class="form-control"/>'+
            '</div>'+
            '<div class="col-xs-2">'+
            '<button type="button" class="btn3d btn-sm btn btn-danger fontBold" id="iletSil">Sil</button>'+
            '</div>'+
        '</div>'+
'</div>';

$(document).ready(function(){
    
    $('#addIlet').on('click',function(e){
        e.preventDefault();
        $('#Ilets').append(ilet);
    });
    
    $('#Ilets').on('click','#iletSil',function(e){
        e.preventDefault();
        $(this).closest('div.col-xs-12').remove();
    });

    $('#iletKaydet1').on('click',function(e){
        e.preventDefault();
        var iletHata = 0;
        $('#IletisimForm input[name="iletName[]"]').each(function(key,val){
            if(val.value == '' || val.length == 0){
                iletHata++;
            }
        });

        if(iletHata>0){
            alert('İletişim bilgilerini boş bırakmayınız. Boş olan iletişim bilgilerini silip tekrar deneyin.');
        }else{
            $('#IletisimForm').submit();
        }
    });
    
    // Gmap Uygulaması u1
    $('#us1').locationpicker({
	location: {latitude: <?php echo empty($loc)?0:$loc['Location']['latitude'];?>, longitude: <?php echo empty($loc)?0:$loc['Location']['longitude'];?>},
	radius: 10,
        zoom: 17,
	inputBinding: {
        latitudeInput: $('#us1-lat'),
        longitudeInput: $('#us1-lon'),
        radiusInput: $('#us1-radius'),
        locationNameInput: $('#us1-address')
        },
        enableAutocomplete: true
    });
    
    // Gmap Uygulaması u1 SON 
});
</script>