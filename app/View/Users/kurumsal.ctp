<div class="printwidth text-white kurback text-justify" style="padding: 10px;">
	<h3 class="text-center">MADEN GAYRİMENKUL</h3>
	<h4 class="text-center">KURUMSAL</h4>
    <p style="font-size: 16px;"><em><b>"Girişimcilik bizim işimiz."</b></em> Konusunda uzman profesyoneller ile faaliyetlerini sürdüren gayrimenkul danışmanlık şirketidir. Bizi diğerlerinden farklı kılan unsur yaptığımız iş değil, işimizi nasıl yaptığımızdır. Girişimcilik kültürümüz, çalışanlarını farklı düşünmeye, müthiş fikirleri paylaşmaya ve müşterilerinin başarısına ivme katacak etkili çözümler oluşturmaya teşvik etmektedir. Maden Gayrimenkul  yenilikçi ve başarılı yatırımcıların bir çoğunun tercih ettiği danışmandır.</p>
    <p  style="font-size: 16px;">Maden Gayrimenkul, gayrimenkul sahiplerine, arazi sahiplerine ve yatırımcılara tam kapsamlı ticari gayrimenkul hizmetleri sunar.</p>
    <p  style="font-size: 16px;">Maden Gayrimenkul , tüm sektörlerde gayrimenkul kiracılarına, mal sahiplerine ve yatırımcılara, her zaman nihai ve tarafsız bir seçenek olarak kalacak şekilde, tam bir hizmet yelpazesi sunmaktadır.</p>
	<p  style="font-size: 16px;">Hizmet, uzmanlık, takım ruhu ve eğlence, Maden Gayrimenkul’un  kimliğinin cevherini oluşturmaktadır. Bu değerler yaşam ve çalışma tarzımıza yön vererek müşterilerimiz için hatırlanmaya değer deneyimler oluşturmaya ve birlikte çalıştığımız herkesin başarısını arttırabilmemize olanak vermektedir.</p>
	<p  style="font-size: 16px;">Hizmetlerimizin paylaşılan ortak değerler ışığında şekillenmesiyle, ekiplerimiz, meslektaşlarımız, müşterilerimiz ve hizmet sunduğumuz topluluklar arasında uzun ömürlü kişisel bağlantılar yaratarak daha yüksek performans göstermektedirler.</p>
    <h4 class="text-center">HİZMET</h4>
    <p  style="font-size: 16px;">Hem elde ettiğimiz ticari neticelerde hem de kişisel ilişkilerimizde beklentileri aşarak olumlu ve hatırlanmaya değer deneyimler oluşturmaktayız. Kurduğumuz ilişkiler dürüstlük, ahlak ve anlayış çerçevesinde şekillenerek, müşterilerimize ve meslektaşlarımıza hizmet yaklaşımımızı tanımlamaktadır.</p>
	<h4 class="text-center">UZMANLIK</h4>
    <p  style="font-size: 16px;">Müşterilerimiz ve çalışanlarımız için yakaladığımız başarı ile sahip olduğumuz derin uzmanlığı paylaşmaktan gurur duymaktayız. Bilgi tabanımızı durmaksızın geliştirmekten yana sergilediğimiz kararlılığımız, yaptığımız her şeye taze ve yaratıcı bir yaklaşım getirmemize olanak vermektedir.</p>
	<h4 class="text-center">EKİP RUHU</h4>
    <p  style="font-size: 16px;">Faaliyet gösterdiğimiz toplumlarda, sürdürülebilir iş teamülleri, iş ortamları ve çalışma alanları yaratmayı ilke edinmiş bulunmaktayız. Farklılıklara ve bireyselliğe değer veriyor ve bu değerlerin büyümesi için yatırım yapıyoruz.</p>
	<h4 class="text-center">EĞLENCE</h4>
    <p  style="font-size: 16px;">İşimiz konusunda ciddiyiz, ama hayattaki keyifleri de kaçırmıyoruz. Sosyal olduğu kadar mesleki etkileşimi de vurgulayan iyimser, enerjik ve dinamik bir çalışma ortamından yanayız. Başarılıyız, çünkü yaptığımız şeyden keyif alıyoruz.</p>
	
	 <p style="font-size: 16px;"><b>Türkiye ve Ankara’daki girişimcilik ekosisteminden insanları bir araya getirmek, Ankara’yı web/mobil GAYRİMENKUL girişimciliğinin merkezi konumuna getirmek için beraber çalışmaya hazırız. Yeni ve yeniden yeni bir anlayış getirmek için biz buradayız.<b></p>
</div>
<script type="text/javascript">
$(document).ready(function(){
document.title = "Kurumsal - MADEN Gayrimenkul";
    
});
</script>