<?= Security::hash('123456', 'sha1', true);?>
<div class="container">
<form id="loginForm" action="<?php echo $this->Html->url('/');?>users/loginCheck" method="POST" role="form" class="form-horizontal text-white" enctype="multipart/form-data">
    <div class="form-group">
            <label for="username" class="col-xs-4 col-sm-2 control-label">Kullanıcı Adı</label>
            <div class="col-xs-8 col-sm-10">
                <input type="text" name="username" paceholder="Kullanıcı Adı" class="form-control"/>
            </div>
        </div>
        <div class="form-group">
            <label for="pass" class="col-xs-4 col-sm-2 control-label">Şifre</label>
            <div class="col-xs-8 col-sm-10">
                <input type="password" name="pass" paceholder="Şifre" class="form-control" />
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-8 col-sm-10 col-xs-offset-4 col-sm-offset-2">
                <button type="button" id="loginButton" class="btn btn-block btn-success">Giriş</button>
            </div>
        </div>
</form>
</div>
<script type="text/javascript">
$(document).ready(function (){
    $('#loginButton').on('click',function (){
        $.blockUI({ css: { backgroundColor: 'transparent', border: 'none'},message: $('#LoaderBlock') });
        $('#loginForm').submit();
    });
});
</script>