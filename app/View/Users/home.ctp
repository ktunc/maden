<div class="row">
    <div class="col-xs-1"></div>
    <div class="col-xs-10" style="">
    <!-- SearcForm Bas -->
    <?php echo $this->element('searchForm'); ?>
    <!-- SearcForm Son -->
    </div>
    <div class="col-xs-1"></div>
</div>
<div class="row"><hr></div>
<div class="row">
    <div class="col-xs-6 text-center">
        <a href="<?php echo $this->Html->url('/');?>konuts/home/SatKir:0">
            <img class="iconHome" src="<?php echo $this->Html->url('/');?>img/konut2.png"/><br>
            <span class="fontItalic fontBold font18 text-danger">Konut</span>
        </a>
    </div>
    <div class="col-xs-6 linkHome">
        <div class="row">
            <div class="col-xs-12 col-sm-8"><a href="<?php echo $this->Html->url('/');?>konuts/home/SatKir:1" class="btn btn3d btn-danger btn-block btn-sm fontBold" type="button">SATILIK (<?php echo $kSatCount;?>)</a></div>
            <div class="hidden-xs col-sm-4"></div>
       </div>
        <div class="row">
            <div class="col-xs-12 col-sm-8"><a href="<?php echo $this->Html->url('/');?>konuts/home/SatKir:2" class="btn btn3d btn-danger btn-block btn-sm fontBold" type="button">KİRALIK (<?php echo $kKirCount;?>)</a></div>
            <div class="hidden-xs col-sm-4"></div>
        </div>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-xs-6 text-center">
        <a href="<?php echo $this->Html->url('/');?>isyeris/home/SatKir:0">
            <img class="iconHome" src="<?php echo $this->Html->url('/');?>img/isyeri.png"/><br>
            <span class="fontItalic fontBold font18 text-primary">İşyeri</span>
        </a>
    </div>
    <div class="col-xs-6 linkHome">
        <div class="row">
            <div class="col-xs-12 col-sm-8"><a href="<?php echo $this->Html->url('/');?>isyeris/home/SatKir:1" class="btn btn3d btn-primary btn-block btn-sm fontBold" type="button">SATILIK (<?php echo $iSatCount;?>)</a></div>
            <div class="hidden-xs col-sm-4"></div>
       </div>
        <div class="row">
            <div class="col-xs-12 col-sm-8"><a href="<?php echo $this->Html->url('/');?>isyeris/home/SatKir:2" class="btn btn3d btn-primary btn-block btn-sm fontBold" type="button">KİRALIK (<?php echo $iKirCount;?>)</a></div>
            <div class="hidden-xs col-sm-4"></div>
        </div>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-xs-6 text-center">
        <a href="<?php echo $this->Html->url('/');?>arsas/home/SatKir:0">
            <img class="iconHome" src="<?php echo $this->Html->url('/');?>img/tree.png" />
            <br>
            <span class="fontItalic fontBold font18 text-success">Arsa</span>
        </a>
    </div>
    <div class="col-xs-6 linkHome">
        <div class="row">
            <div class="col-xs-12 col-sm-8"><a href="<?php echo $this->Html->url('/');?>arsas/home/SatKir:1" class="btn btn3d btn-success btn-block btn-sm fontBold" type="button">SATILIK (<?php echo $aSatCount;?>)</a></div>
            <div class="hidden-xs col-sm-4"></div>
       </div>
        <div class="row">
            <div class="col-xs-12 col-sm-8"><a href="<?php echo $this->Html->url('/');?>arsas/home/SatKir:2" class="btn btn3d btn-success btn-block btn-sm fontBold" type="button">KİRALIK (<?php echo $aKirCount;?>)</a></div>
            <div class="hidden-xs col-sm-4"></div>
        </div>
    </div>
</div>
<hr>
<div class="row">
    <div class="hidden-xs col-sm-1 col-md-1 col-lg-1"></div>
    <div class="col-xs-6 col-sm-5 col-md-5 col-lg-5"><button class="btn btn3d btn-block btn-sm fontBold btn-gray"  onclick="window.location.href='<?php echo $this->Html->url('/');?>projes/home'">PROJELER (<?php echo $projeCount; ?>)</button></a></div>
    <div class="col-xs-6 col-sm-5 col-md-5 col-lg-5"><button class="btn btn3d btn-block btn-sm fontBold btn-gray"  onclick="window.location.href='<?php echo $this->Html->url('/');?>habers/home'">HABERLER (<?php echo $haberCount; ?>)</button></a></div>
    <div class="hidden-xs col-sm-1 col-md-1 col-lg-1"></div>
</div>

<!-- MODALLAR **********************************************************************************************-->
<!-- Loader ***********************************************-->
<div id="Loader" class="modal fade bs-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="progress progress-striped active">
            <div class="progress-bar progress-bar-primary"  role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
            </div>
        </div>
    </div>
</div>
<!-- Loader ***********************************************-->
<script type="text/javascript">
$(function(){
document.title = "ANASAYFA- MADEN Gayrimenkul A.Ş.";
//   var winHeight = $(window).height();
//   jQuery('.fLeft').height(winHeight/4);
//   jQuery('.fLeft button').css('font-size',(jQuery('.fLeft').height()/3)+'px');
//   jQuery('.fRight').height(winHeight/4);
//   jQuery('.fRight button').css('font-size',(jQuery('.fRight').height()/4)+'px');
});
</script>