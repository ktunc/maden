<?php
echo $this->Html->css('jquery-ui');
echo $this->Html->script('jquery-ui.min');
?>
<style>
    
</style>
<div class="container text-white">
    <h3 class="text-center">Anasayfa Slayt Ayarları</h3>
</div>
<div class="container bg-white">
    <ul class="sortslide">
        <?php
        foreach($slayts as $row){
            echo '<li class="ui-state-default" data-slide-id="'.$row['Slider']['id'].'">
                    <div style="display: block;position: relative;">
                        <img src="'.$this->webroot.$row['Slider']['path'].'" style="max-width:100%;height:200px;"/>
                        <div class="DeleteIcon">
                            <button class="btn btn-danger btn-sm DeleteBut" data-slide-id="'.$row['Slider']['id'].'"><i class="fa fa-2x fa-trash"></i></button>
                        </div>
                    </div>
                 </li>';
        }
        ?>
    </ul>
</div>
<div class="container">
    <form method="POST" role="form" class="form-horizontal" action="<?php echo $this->Html->url('/');?>users/addslayt" id="SlaytForm" enctype="multipart/form-data">
        <div class="form-group">
            <table id="resler">

            </table>
        </div>
        <div class="form-group">
            <button class="btn btn-sm btn-warning" type="button" id="addResim"><i class="fa fa-plus"></i> Resim Ekle</button>
            <button type="button" class="btn btn-sm btn-success" id="SlaytKaydet">Kaydet</button>
        </div>
    </form>
</div>
<script type="text/javascript">
    var resimTR = '<tr><td style="padding: 5px 0;"><div class="input-group"><input type="file" name="Res[]" class="form-control"/> ' +
        '<span class="input-group-btn"> ' +
        '<button type="button" class="btn btn-danger" id="resIptal"><i class="fa fa-minus"></i> İptal</button> ' +
        '</span>' +
        '</div></td></tr>';
    $(document).ready(function(){
        $( ".sortslide" ).sortable({
            placeholder: "ui-state-highlight",
            update: function( event, ui ) {
                $.blockUI({ css: { backgroundColor: 'transparent', border: 'none'},message: $('#LoaderBlock') });
                setTimeout(function(){FuncSiraDegistir();$.unblockUI();},500);
            }
        });
        $( ".sortslide" ).disableSelection();

        $('#addResim').on('click',function(){
            if($('input[name="konutRes[]"]').length >= 20){
                alert('20 taneden fazla resim eklenemez.');
            }else{
                $('#resler').append(resimTR);
            }
        });

        $('#resler').on('click','#resIptal',function(){
            $(this).closest('tr').remove();
        });

        $('#SlaytKaydet').on('click',function(){
            if($('#resler tr').length > 0){
                $.blockUI({ css: { backgroundColor: 'transparent', border: 'none'},message: $('#LoaderBlock') });
                $('#SlaytForm').submit();
            }
        });

        $('.DeleteBut').on('click',function(){
            if(confirm('Resmi silmek istediğinizden emin misiniz?')){
                $.blockUI({ css: { backgroundColor: 'transparent', border: 'none'},message: $('#LoaderBlock') });
                var sId = $(this).data('slide-id');
                setTimeout(function(){FuncSlideSil(sId);FuncSiraDegistir();$.unblockUI();},500);
            }
        });
    });

function FuncSiraDegistir(){
    var sira = 0;
    $('.sortslide li').each(function(){
        var sonuc = true;
        var slideId = $(this).data('slide-id');
        sira += 1;
        $.ajax({
            async:false,
            type:'POST',
            url:'<?php echo $this->Html->url('/');?>users/slidersiradegis',
            data:'sira='+sira+'&sId='+slideId
        }).done(function(data){
            var dat = $.parseJSON(data);
            sonuc = dat['sonuc'];
            if(!dat['sonuc']){
                $.unblockUI();
                alert('Bir hata meydana geldi. Sayfayı yenileyerek tekrar deneyin.');
            }
        }).fail(function(){
            $.unblockUI();
            sonuc = false;
            alert('İnternet bağlantınızı kontrol ediniz.');
        });
        return sonuc;
    });
}

function FuncSlideSil(sId){
    $.ajax({
        async:false,
        type:'POST',
        url:'<?php echo $this->Html->url('/');?>users/slidesil',
        data:'sId='+sId
    }).done(function(data){
        var dat = $.parseJSON(data);
        sonuc = dat['sonuc'];
        if(dat['sonuc']){
            $('li[data-slide-id="'+sId+'"]').remove();
        }else{
            alert('Bir hata meydana geldi. Sayfayı yenileyerek tekrar deneyin.');
        }
    }).fail(function(){
        sonuc = false;
        alert('İnternet bağlantınızı kontrol ediniz.');
    });
}
</script>

