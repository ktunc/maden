<style>
    .fLeft{
        width:50%;
        float:left;
        margin-bottom: 5%;
    }
    
    .fLeft>button{
        width:100%;
        height: 100%;
        border-radius: 10px;
        border-color: #FFFFFF;
        border-width: 5px;
        border-style: solid;
        padding: 0px;
    }
    
    .fRight{
        width:50%;
        float:right;
        margin-bottom: 5%;
    }
    
    .fRight>button{
        width:100%;
        height: 50%;
        border-color: #FFFFFF;
        border-width: 5px;
        border-style: solid;
        padding: 0px;
    }
    
    .fRight>button:first-child{
        border-top-left-radius: 10px;
        border-bottom-left-radius: 5px;
        border-top-right-radius: 10px;
        border-bottom-right-radius: 10px;
    }
    
    .fRight>button:last-child{
        border-top-left-radius: 5px;
        border-bottom-left-radius: 10px;
        border-top-right-radius: 10px;
        border-bottom-right-radius: 10px;
    }
</style>
<form method="POST" role="form" class="form-signin" action="<?php echo $this->Html->url('/');?>users/search" enctype="multipart/form-data" style="margin-bottom: 2%">
    <div class="input-group">
    <input type="text" class="form-control" name="searchName" />
     <span class="input-group-btn">
         <button class="btn btn-default" type="submit"><span class="glyphicon glyphicon-search"></span></button>
      </span>
    </div>
</form>
<div class="fLeft">
    <button  class="btn btn-primary" type="button">ARSA</button>
</div>
<div class="fRight">
    <button  class="btn btn-primary" type="button">SATILIK</button>
    <button  class="btn btn-primary" type="button">KİRALIK</button>
</div>

<div class="fLeft">
    <button  class="btn btn-danger" type="button">KONUT</button>
</div>
<div class="fRight">
    <button  class="btn btn-danger" type="button">SATILIK</button>
    <button  class="btn btn-danger" type="button">KİRALIK</button>
</div>

<div class="fLeft">
    <button  class="btn btn-warning" type="button">İŞYERİ</button>
</div>
<div class="fRight">
    <button  class="btn btn-warning" type="button">SATILIK</button>
    <button  class="btn btn-warning" type="button">KİRALIK</button>
</div>


<script type="text/javascript">
$(function(){
   var winHeight = $(window).height();
   jQuery('.fLeft').height(winHeight/4);
   jQuery('.fLeft button').css('font-size',(jQuery('.fLeft').height()/3)+'px');
   jQuery('.fRight').height(winHeight/4);
   jQuery('.fRight button').css('font-size',(jQuery('.fRight').height()/4)+'px');
});
</script>