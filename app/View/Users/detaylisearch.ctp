<?php
$sorgular = $this->passedArgs;
$aramaTip = array(0=>'Hepsi',1=>'Konut',2=>'İşyeri',3=>'Arsa');
$TipSel = '';
foreach($aramaTip as $key=>$val){
    $selected = '';
    if(array_key_exists('tip',$sorgular) && $sorgular['tip'] == $key){
        $selected = ' selected="selected"';
    }
    $TipSel .= '<option value="'.$key.'" '.$selected.'>'.$val.'</option>';
}
$il = $this->requestAction('users/GetIl');
$sehir = '<option value="0">Seçiniz</option>';
foreach ($il as $row) {
    $selected = '';
    if(array_key_exists('il',$sorgular) && $sorgular['il'] == $row['Sehir']['id']){
        $selected = ' selected="selected"';
    }
    $sehir .= '<option value="'.$row['Sehir']['id'].'" '.$selected.'>'.$row['Sehir']['sehir_adi'].'</option>';
}
?>
<div class="container bg-white">
<div class="col-xs-12">
<h3 class="text-center">Detaylı Arama</h3>
<form action="<?php echo $this->Html->url('/');?>users/arama" method="POST" role="form" class="form-horizontal" enctype="multipart/form-data" id="searchFormDetayli">
    <div class="form-group">
        <label for="tip" class="col-xs-3 control-label">Arama Tipi:</label>
        <div class="col-xs-9">
            <select name="tip" class="form-control">
                <?php echo $TipSel; ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="satkir" class="col-xs-3 control-label">Satılık-Kiralık:</label>
        <div class="col-xs-9">
            <select name="satkir" class="form-control">
                <option value="0">Hepsi</option>
                <option value="1">Satılık</option>
                <option value="2">Kiralık</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="il" class="col-xs-3 control-label">Şehir:</label>
        <div class="col-xs-9">
            <select id="il" name="il" class="form-control"><?php echo $sehir; ?></select>
        </div>
    </div>
    <div class="form-group">
        <label for="ilce" class="col-xs-3 control-label">İlçe:</label>
        <div class="col-xs-9">
            <select id="ilce" name="ilce" class="form-control"></select>
        </div>
    </div>
    <div class="form-group">
        <label for="semt" class="col-xs-3 control-label">Semt:</label>
        <div class="col-xs-9">
            <select id="semt" name="semt" class="form-control"></select>
        </div>
    </div>
    <div class="form-group">
        <label for="mahalle" class="col-xs-3 control-label">Mahalle:</label>
        <div class="col-xs-9">
            <select id="mahalle" name="mahalle" class="form-control"></select>
        </div>
    </div>
    <div class="form-group">
        <label for="fiyat" class="col-xs-3 control-label">Fiyat:</label>
        <div class="col-xs-4">
            <input type="number" class="form-control" name="fiy1" onkeypress="return isNumberKey(event)"/>
        </div>
        <div class="col-xs-4">
            <input type="number" class="form-control" name="fiy2" onkeypress="return isNumberKey(event)"/>
        </div>
    </div>
    <div class="form-group">
        <label for="mahalle" class="col-xs-3 control-label">Metre Kare:</label>
        <div class="col-xs-4">
            <input type="number" class="form-control" name="m1" onkeypress="return isNumberKey(event)"/>
        </div>
        <div class="col-xs-4">
            <input type="number" class="form-control" name="m2" onkeypress="return isNumberKey(event)"/>
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-3"></div>
        <div class="col-xs-9">
            <button type="button" class="btn btn3d btn-primary" id="searchButtonDetayli">Ara</button>
            <button type="reset" class="btn btn3d btn-danger">Sıfırla</button>
        </div>
    </div>
</form>
</div>
</div>
<!-- MODALLAR **********************************************************************************************-->
<!-- Loader ***********************************************-->
<div id="Loader" class="modal fade bs-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="progress progress-striped active">
            <div class="progress-bar progress-bar-warning"  role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
            </div>
        </div>
    </div>
</div>
<!-- Loader ***********************************************-->
<script type="text/javascript">
$(document).ready(function(){
document.title = "Detaylı Arama - MADEN Gayrimenkul A.Ş.";
    

    $('#searchButtonDetayli').on('click',function(){
        $('#Loader').modal({
            keyboard:false,
            backdrop:'static'
        });
        $('#searchFormDetayli').submit();
    });
    
   $('#il').on('change',function (){
       $('#Loader').modal({
            keyboard:false,
            backdrop:'static'
        });
       $('#ilce').html('');
       $('#semt').html('');
       var il = $(this).val();
       if(il != 0){
           $.ajax({
               async: false,
               type: 'POST',
               url: "<?php echo $this->Html->url('/');?>konuts/getIlce",
               data: 'il='+il,
               success: function (data) {
                        var dat = $.parseJSON(data);
                        if(dat){
                            var ekle = '<option value="0">Seçiniz</option>';
                            $.each(dat,function(key,vall){
                               ekle+='<option value="'+vall['Ilce']['id']+'">'+vall['Ilce']['ilce_adi']+'</option>';
                            });
                            $('#ilce').html(ekle);
                            $('#Loader').modal('hide');
                        }
                    }
           });
        }else{
            $('#Loader').modal('hide');
        }
    });
    
    $('#ilce').on('change',function (){
        $('#Loader').modal({
            keyboard:false,
            backdrop:'static'
        });
       $('#semt').html('');
       $('#mahalle').html('');
       var ilce = $(this).val();
       if(ilce != 0){
           $.ajax({
               async: false,
               type: 'POST',
               url: "<?php echo $this->Html->url('/');?>konuts/getSemt",
               data: 'ilce='+ilce,
               success: function (data) {
                        var dat = $.parseJSON(data);
                        if(dat){
                            var ekle = '<option value="0">Seçiniz</option>';
                            $.each(dat,function(key,vall){
                               ekle+='<option value="'+vall['Semt']['id']+'">'+vall['Semt']['semt_adi']+'</option>';
                            });
                            $('#semt').html(ekle);
                            $('#Loader').modal('hide');
                        }
                    }
           });
        }else{
            $('#Loader').modal('hide');
        }
    });
    
    $('#semt').on('change',function (){
        $('#Loader').modal({
            keyboard:false,
            backdrop:'static'
        });
       $('#mahalle').html('');
       var semt = $(this).val();
       if(semt != 0){
           $.ajax({
               async: false,
               type: 'POST',
               url: "<?php echo $this->Html->url('/');?>konuts/getMahalle",
               data: 'semt='+semt,
               success: function (data) {
                        var dat = $.parseJSON(data);
                        if(dat){
                            var ekle = '<option value="0">Seçiniz</option>';
                            $.each(dat,function(key,vall){
                               ekle+='<option value="'+vall['Mahalle']['id']+'">'+vall['Mahalle']['mahalle_adi']+'</option>';
                            });
                            $('#mahalle').html(ekle);
                            $('#Loader').modal('hide');
                        }
                    }
           });
        }else{
            $('#Loader').modal('hide');
        }
    }); 
});
</script>