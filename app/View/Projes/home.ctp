<?php
foreach($projes as $row){
?>
<blockquote class="bg-warning">
<div class="row" style="margin-bottom: 1%">
    <a href="<?php echo $this->Html->url('/')?>projes/proje/ilanNo:<?php echo $row['Proje']['id'];?>">
    <div class="col-xs-6 col-sm-6 text-center">
         <?php 
         if($projeRes[$row['Proje']['id']]){
             echo '<img class="imgHome" src="'.$this->Html->url('/').$projeRes[$row['Proje']['id']][0]['ProjeResim']['path'].'"/>';
        }else{
            echo '<img class="imgHome" src="'.$this->Html->url('/').'img/nofoto.png"/>';
        }
        ?>
    </div>
    </a>
    <div class="col-xs-6 col-sm-6 bg-warning">
        <div class="row">
            <div class="col-xs-12 h6" style="color:#000000"><?php echo CakeText::truncate($row['Proje']['baslik'],50);?></div>
            <div class="col-xs-12"><?php echo CakeText::truncate(strip_tags($row['Proje']['aciklama']),100);?></div>
            <?php 
            // Giriş yapan User
            if($this->Session->check('UserLogin')){
            ?>
            <div class="col-xs-12 fRight" style="text-align: right">                
                    
                <a href="<?php echo $this->Html->url('/');?>projes/proje/ilanNo:<?php echo $row['Proje']['id'];?>" class="btn  btn-primary btn-sm"><span class="glyphicon glyphicon-chevron-right"></span></a>
                <a href="<?php echo $this->Html->url('/');?>admins/projeedit/id=<?php echo $row['Proje']['id'];?>" class="btn  btn-default btn-sm"><span class="glyphicon glyphicon-pencil"></span></a>
                <!--<button type="button" class="btn  btn-danger btn-sm" onclick="ProjeSil(<?php echo $row['Proje']['id'];?>)"><span class="glyphicon glyphicon-remove"></span></button>-->
            </div>
            <?php
            }else{
            ?>
            <div class="col-xs-12 fRight" style="text-align: right;">
                <a href="<?php echo $this->Html->url('/');?>projes/proje/ilanNo:<?php echo $row['Proje']['id'];?>" class="btn  btn-primary btn-sm"><span class="glyphicon glyphicon-chevron-right"></span></a>
            </div>
            <?php
            }
            ?>
        </div>
    </div>
</div>
</blockquote>
<?php
}
// pagination section
    echo '<ul class="pagination" style="display:table; margin:0 auto;">';
 
        echo $this->Paginator->prev( '<<', array( 'class' => '', 'tag' => 'li','escape' => false), null, array( 'class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a','escape' => false) );
        echo $this->Paginator->numbers( array( 'tag' => 'li', 'separator' => '', 'currentClass' => 'active', 'currentTag'=>'a' ) );
        echo $this->Paginator->next( '>>', array( 'class' => '', 'tag' => 'li','escape' => false ), null, array( 'class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a','escape' => false) );
        
    echo '</ul>'; 
?>
<script type="text/javascript">
$(document).ready(function() {
    document.title = "PROJE - Maden Gayrimenkul";
});
</script>