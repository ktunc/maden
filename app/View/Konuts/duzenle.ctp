<?php
echo $this->Html->script('locationpicker.jquery');
//echo $this->Html->script('http://maps.google.com/maps/api/js?key=AIzaSyBb6wy1FSr2ms69Cy7BSuZQLOB9-EPIkIA&sensor=false&libraries=places');

$sehir = '<option value="0">Seçiniz</option>';
foreach ($il as $row) {
    if($row['Sehir']['id'] == $konuts['Sehir']['id']){
        $sehir .= '<option selected="selected" value="'.$row['Sehir']['id'].'">'.$row['Sehir']['sehir_adi'].'</option>';
    }else{
        $sehir .= '<option value="'.$row['Sehir']['id'].'">'.$row['Sehir']['sehir_adi'].'</option>';
    }
}
$ilceler = '';

$ilceler = '<option value="0">Seçiniz</option>';
foreach ($ilce as $row) {
    if($row['Ilce']['id'] == $konuts['Ilce']['id']){
        $ilceler .= '<option selected="selected" value="'.$row['Ilce']['id'].'">'.$row['Ilce']['ilce_adi'].'</option>';
    }else{
        $ilceler .= '<option value="'.$row['Ilce']['id'].'">'.$row['Ilce']['ilce_adi'].'</option>';
    }
}

$semtler='';
if($konuts['Ilce']['id']){
    $semtler = '<option value="0">Seçiniz</option>';
    foreach ($semt as $row) {
        if($row['Semt']['id'] == $konuts['Semt']['id']){
            $semtler .= '<option selected="selected" value="'.$row['Semt']['id'].'">'.$row['Semt']['semt_adi'].'</option>';
        }else{
            $semtler .= '<option value="'.$row['Semt']['id'].'">'.$row['Semt']['semt_adi'].'</option>';
        }
    }
}
$mahalleler='';
if($konuts['Semt']['id']){
    $mahalleler = '<option value="0">Seçiniz</option>';
    foreach ($mahalle as $row) {
        if($row['Mahalle']['id'] == $konuts['Mahalle']['id']){
            $mahalleler .= '<option selected="selected" value="'.$row['Mahalle']['id'].'">'.$row['Mahalle']['mahalle_adi'].'</option>';
        }else{
            $mahalleler .= '<option value="'.$row['Mahalle']['id'].'">'.$row['Mahalle']['mahalle_adi'].'</option>';
        }
    }
}

$satkir = '';
if(1 == $konuts['Konut']['sat_kir']){
    $satkir .= '<option value="1" selected="selected">Satılık</option>';
    $satkir .= '<option value="2">Kiralık</option>';
}else if(2 == $konuts['Konut']['sat_kir']){
    $satkir .= '<option value="1">Satılık</option>';
    $satkir .= '<option value="2" selected="selected">Kiralık</option>';
}

$paraArray = array('TL','$','€');
$paraBirim = '';
foreach ($paraArray as $row){
    if($row == $konuts['Konut']['parabirimi']){
        $paraBirim .= '<option value="'.$row.'" selected="selected">'.$row.'</option>';
    }else{
        $paraBirim .= '<option value="'.$row.'">'.$row.'</option>';
    }
}

$malParaBirim = '';
foreach ($paraArray as $row){
    if($row == $konuts['Konut']['mal_parabirimi']){
        $malParaBirim .= '<option value="'.$row.'" selected="selected">'.$row.'</option>';
    }else{
        $malParaBirim .= '<option value="'.$row.'">'.$row.'</option>';
    }
}

$iletSel = '<div class="anaDiv"><div class="div85">';
$iletSel .= '<select name="ilanilet[]" class="ilanilet form-control"><option value="0">Seçiniz</option>';
foreach($iletisim as $cow){
    $iletSel .= '<option value="'.$cow['IlanIletisim']['id'].'">';
    if($cow['IlanIletisim']['type'] == 1){
        $iletSel .= 'Telefon: ';
    }else if($cow['IlanIletisim']['type'] == 2){
        $iletSel .= 'Faks: ';
    }else if($cow['IlanIletisim']['type'] == 3){
         $iletSel .= 'Email: ';
    }else if($cow['IlanIletisim']['type'] == 4){
        $iletSel .= 'Web Sitesi: ';
    }else if($cow['IlanIletisim']['type'] == 5){
        $iletSel .= 'Adres: ';
    }
    $iletSel .= $cow['IlanIletisim']['iletisim'].'</option>';
}
$iletSel .= '</select></div><div class="div15 text-left"><button type="button" class="btn btn-danger btn-sm iletSil"><i class="fa fa-minus"></i> Sil</button></div></div>';
?>
<div class="container text-white kback">
<h3><?php echo $konuts['Konut']['ilan_no'];?> İlan No'lu Konut Düzenle</h3>
<form method="POST" role="form" class="form-horizontal" action="<?php echo $this->Html->url('/');?>konuts/upgradekonut/ilanNo:<?php echo $konuts['Konut']['ilan_no'];?>" id="KonutForm" enctype="multipart/form-data">
    <div class="form-group">
        <label for="baslik" class="col-xs-3 col-md-2 control-label">Başlık</label>
        <div class="col-xs-9 col-md-10">
            <input type="text" name="baslik" id="baslik" placeholder="Başlik" class="form-control" value="<?php echo $konuts['Konut']['baslik'] ?>" />
        </div>
    </div>
    <div class="form-group">
        <label for="aciklama" class="col-xs-3 col-md-2 control-label">Açıklama</label>
        <div class="col-xs-9 col-md-10">
            <textarea name="aciklama" rows="5" id="aciklama" placeholder="Açıklama" class="form-control"><?php echo $konuts['Konut']['aciklama']; ?></textarea>
        </div>
    </div>
    <div class="form-group">
        <label for="il" class="col-xs-3 col-md-2 control-label">Şehir</label>
        <div class="col-xs-9 col-md-10">
            <select id="il" name="il" class="form-control"><?php echo $sehir; ?></select>
        </div>
    </div>
    <div class="form-group">
        <label for="ilce" class="col-xs-3 col-md-2 control-label">İlçe</label>
        <div class="col-xs-9 col-md-10">
            <select id="ilce" name="ilce" class="form-control"><?php echo $ilceler; ?></select>
        </div>
    </div>
    <div class="form-group">
        <label for="semt" class="col-xs-3 col-md-2 control-label">Semt</label>
        <div class="col-xs-9 col-md-10">
            <select id="semt" name="semt" class="form-control"><?php echo $semtler; ?></select>
        </div>
    </div>
    <div class="form-group">
        <label for="mahalle" class="col-xs-3 col-md-2 control-label">Mahalle</label>
        <div class="col-xs-9 col-md-10">
            <select id="mahalle" name="mahalle" class="form-control"><?php echo $mahalleler; ?></select>
        </div>
    </div>
    <div class="form-group">
        <label for="oda" class="col-xs-3 col-md-2 control-label">Oda Sayısı</label>
        <div class="col-xs-9 col-md-10">
            <input type="text" name="oda" id="oda" placeholder="Oda Sayısı" class="form-control"  value="<?php echo $konuts['Konut']['oda_sayisi']; ?>"/>
        </div>
    </div>
    <div class="form-group">
        <label for="m2" class="col-xs-3 col-md-2 control-label">Metre Kar</label>
        <div class="col-xs-9 col-md-10">
            <input type="number" name="m2" id="m2" placeholder="Metre Kare" class="form-control"  value="<?php echo $konuts['Konut']['m_kare']; ?>"/>
        </div>
    </div>
    <div class="form-group">
        <label for="kat" class="col-xs-3 col-md-2 control-label">Kaçıncı Katta</label>
        <div class="col-xs-9 col-md-10">
            <input type="text" name="kat" id="kat" placeholder="Kaçıncı Katta" class="form-control"  value="<?php echo $konuts['Konut']['kat']; ?>"/>
        </div>
    </div>
    <div class="form-group">
        <label for="binakat" class="col-xs-3 col-md-2 control-label">Bina Katı</label>
        <div class="col-xs-9 col-md-10">
            <input type="number" name="binakat" id="binakat" placeholder="Bina Katı" class="form-control"  value="<?php echo $konuts['Konut']['bina_kat']; ?>"/>
        </div>
    </div>
    <div class="form-group">
        <label for="kredi" class="col-xs-3 col-md-2 control-label">Kredi Uygunluğu</label>
        <div class="col-xs-9 col-md-10">
            <input type="text" name="kredi" id="kredi" placeholder="Kredi Uygunluğu" class="form-control"  value="<?php echo $konuts['Konut']['kredi'];?>"/>
        </div>
    </div>
    <div class="form-group">
        <label for="fiyat" class="col-xs-3 col-md-2 control-label">Fiyat</label>
        <div class="col-xs-9 col-md-10">
            <div class="input-group">
                <span class="input-group-addon" style="padding-top:0;padding-bottom:0;">
                    <select name="paraBirimi"><?php echo $paraBirim; ?></select>
                </span>
                <input type="number" name="fiyat" id="fiyat" placeholder="Fiyat" class="form-control"  value="<?php echo $konuts['Konut']['fiyat']; ?>"/>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label for="sat_kir" class="col-xs-3 col-md-2 control-label">Satılık - Kiralık</label>
        <div class="col-xs-9 col-md-10">
            <select id="sat_kir" name="sat_kir" class="form-control"><?php echo $satkir;?></select>
        </div>
    </div>

    <!-- Mal Sahibi Bilgileri -->
    <div class="form-group">
        <label for="malName" class="col-xs-3 col-md-2 control-label">Mal Sahibi Adı Soyadı</label>
        <div class="col-xs-9 col-md-10">
            <input type="text" name="malName" id="malName" placeholder="Mal Sahibi Adı Soyadı" class="form-control" value="<?php echo $konuts['Konut']['mal_name'];?>"/>
        </div>
    </div>
    <div class="form-group">
        <label for="malTelNo" class="col-xs-3 col-md-2 control-label">Mal Sahibi Telefon No:</label>
        <div class="col-xs-9 col-md-10">
            <input type="text" name="malTelNo" id="malTelNo" placeholder="Mal Sahibi Telefon No" class="form-control" value="<?php echo $konuts['Konut']['mal_tel'];?>"/>
        </div>
    </div>
    <div class="form-group">
        <label for="malFiyat" class="col-xs-3 col-md-2 control-label">Mal Sahibi Fiyat</label>
        <div class="col-xs-9 col-md-10">
            <div class="input-group">
                <span class="input-group-addon" style="padding-top:0;padding-bottom:0;">
                    <select name="malParaBirimi"><?php echo $malParaBirim; ?></select>
                </span>
                <input type="number" name="malFiyat" id="malFiyat" placeholder="Mal Sahibi Fiyat" class="form-control" value="<?php echo $konuts['Konut']['mal_fiyat'];?>"/>
            </div>
        </div>
    </div>
    <div class="form-group">
        <hr>
    </div>
    <!-- Mal Sahibi Bilgileri SON -->

    <!-- İlan İletişim Bilgileri -->
    <!--
    <div class="anaDiv h3">
        İlan İletişimi
    </div>
    <div class="anaDiv" id="ilanDiv">
    <?php
    /*
    foreach($KonutIlet as $tow){
    echo '<div class="anaDiv"><div class="div85">';
    echo  '<select name="ilanilet[]" class="ilanilet form-control"><option value="0">Seçiniz</option>';
    foreach($iletisim as $cow){
        $selected = '';
        if($tow['KonutIletisim']['ilet_id'] == $cow['IlanIletisim']['id']){
            $selected = 'selected="selected"';
        }
        
        echo '<option value="'.$cow['IlanIletisim']['id'].'" '.$selected.'>';
        if($cow['IlanIletisim']['type'] == 1){
            echo 'Telefon: ';
        }else if($cow['IlanIletisim']['type'] == 2){
            echo 'Faks: ';
        }else if($cow['IlanIletisim']['type'] == 3){
             echo 'Email: ';
        }else if($cow['IlanIletisim']['type'] == 4){
            echo 'Web Sitesi: ';
        }else if($cow['IlanIletisim']['type'] == 5){
            echo 'Adres: ';
        }
        echo $cow['IlanIletisim']['iletisim'].'</option>';
    }
    echo '</select></div><div class="div15 text-left"><button type="button" class="btn btn-danger btn-sm iletSil"><i class="fa fa-minus"></i> Sil</button></div></div>';
    
    }
    */
    ?>
    </div>
    <div class="form-group">
        <button type="button" class="btn btn-primary btn-sm" id="IletEkle"><i class="fa fa-plus"></i> İletişim Ekle</button>
    </div>
    <div class="form-group">
        <hr>
    </div>
    -->
    <!-- İlan İletişim Bilgileri SON -->
    <div class="form-group">
        <label for="video" class="control-label col-xs-3 col-md-2 ">Video</label>
        <div class="col-xs-9 col-md-10">
            <div class="input-group">
                <input type="url" name="video" class="form-control ytvideo" value="<?php echo $konuts['Konut']['video']; ?>">
                <span class="input-group-btn">
                  <button type="button" class="btn btn-primary" onclick="YTVideoAc(0)">İzle</button>
                </span>
            </div>
        </div>
    </div>
    <div class="form-group">
        <hr>
    </div>
    <div class="form-group">
        <label for="resim" class="control-label col-xs-3 col-md-2 ">Resimler</label>
    </div>
    <div class="form-group">
        <?php
        foreach($konutRes as $cow){
            ?>
            <div class="col-sm-6 col-md-3 resims" id="Res_<?php echo $cow['KonutResim']['id'];?>">
                <div class="thumbnail" style="background-color: #FFFFFF" >
                    <img src="<?php echo $this->Html->url('/').$cow['KonutResim']['path'];?>" alt="" style="width:300px;height:200px;" alt="<?php echo $konuts['Konut']['baslik'].' '.$cow['KonutResim']['id']; ?>"/>
                    <div class="text-right">
                        <button type="button" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Resmi Sil" onclick="DeleteResim('<?php echo $cow['KonutResim']['id'];?>')"><i class="fa fa-trash fa-2x"></i></button>
                    </div>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
    <div class="form-group">
        <table id="resler">

        </table>
    </div>
    <div class="form-group">
        <button class="btn btn-sm btn-warning" type="button" id="addResim"><i class="fa fa-plus"></i> Resim Ekle</button>
    </div>
    <div class="form-group">
        <hr>
    </div>
    <div class="form-group">
        <label for="us2-address" class="col-xs-3 col-md-2 control-label">Location</label>
        <div class="col-xs-9 col-md-10">
            <input type="text" id="us2-address" name="location"  class="form-control"/>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12" id="us2" style="min-height: 300px;margin-top: 2%;"></div>
        <input type="hidden" id="us2-lat" name="latitude" />
        <input type="hidden" id="us2-lon" name="longitude" />
    </div>
    <div class="form-group">
        <div class="col-xs-12" style="margin-top:5%">
            <button type="button" class="btn btn-sm btn-primary" id="KonutKaydet">Kaydet</button>
            <button type="button" class="btn btn-sm btn-default" id="KonutIptal">İptal</button>
            <button type="button" class="btn btn-sm btn-danger" style="float:right" onclick="KonutSil(<?php echo $konuts['Konut']['id'];?>)">Sil</button>
        </div>
    </div>
</form>
</div>
<?php echo $this->Html->script('tinymce/tinymce.min'); ?>
<script type="text/javascript">
var resimTR = '<tr><td style="padding: 5px 0;"><div class="input-group"><input multiple type="file" name="konutRes[]" class="form-control resimekleclass"/> ' +
        '<span class="input-group-btn"> ' +
        '<button type="button" class="btn btn-danger" id="resIptal"><i class="fa fa-minus"></i> İptal</button> ' +
        '</span>' +
        '</div></td></tr>';
$(document).ready(function(){
    tinymce.init({
        selector: '#aciklama',
        language: 'tr_TR',
        height: 300,
        theme: 'modern',
        plugins: [
            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools'
        ],
        toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        toolbar2: 'print preview media | forecolor backcolor emoticons',
        image_advtab: true
    });

    $('#il,#ilce,#semt,#mahalle,.ilanilet').select2({
        width:'100%'
    });
    $('[data-toggle="tooltip"]').tooltip();

    $('#IletEkle').on('click',function(e){
        e.preventDefault();
        $('#ilanDiv').append('<?php echo $iletSel; ?>');
        $('.ilanilet').select2({
            width:'100%'
        });
    });
    $('#ilanDiv').on('click','.iletSil',function(e){
        e.preventDefault();
        $(this).closest('div.anaDiv').remove();
    });
    
    $('#il').on('change',function (){
        $.blockUI({ css: { backgroundColor: 'transparent', border: 'none'},message: $('#LoaderBlock') });
       $('#ilce').html('');
       $('#semt').html('');
       $('#mahalle').html('');
       var il = $(this).val();
       if(il != 0){
           $.ajax({
               async: false,
               type: 'POST',
               url: "<?php echo $this->Html->url('/');?>konuts/getIlce",
               data: 'il='+il,
               success: function (data) {
                        var dat = $.parseJSON(data);
                        if(dat){
                            var ekle = '<option value="0">Seçiniz</option>';
                            $.each(dat,function(key,vall){
                               ekle+='<option value="'+vall['Ilce']['id']+'">'+vall['Ilce']['ilce_adi']+'</option>';
                            });
                            $('#ilce').html(ekle);
                            $.unblockUI();
                        }
                    }
           });
        }else{
            $.unblockUI();
        }
    });
    
    $('#ilce').on('change',function (){
        $.blockUI({ css: { backgroundColor: 'transparent', border: 'none'},message: $('#LoaderBlock') });
       $('#semt').html('');
       $('#mahalle').html('');
       var ilce = $(this).val();
       if(ilce != 0){
           $.ajax({
               async: false,
               type: 'POST',
               url: "<?php echo $this->Html->url('/');?>konuts/getSemt",
               data: 'ilce='+ilce,
               success: function (data) {
                        var dat = $.parseJSON(data);
                        if(dat){
                            var ekle = '<option value="0">Seçiniz</option>';
                            $.each(dat,function(key,vall){
                               ekle+='<option value="'+vall['Semt']['id']+'">'+vall['Semt']['semt_adi']+'</option>';
                            });
                            $('#semt').html(ekle);
                            $.unblockUI();
                        }
                    }
           });
        }else{
            $.unblockUI();
        }
    });
    
    $('#semt').on('change',function (){
        $.blockUI({ css: { backgroundColor: 'transparent', border: 'none'},message: $('#LoaderBlock') });
       $('#mahalle').html('');
       var semt = $(this).val();
       if(semt != 0){
           $.ajax({
               async: false,
               type: 'POST',
               url: "<?php echo $this->Html->url('/');?>konuts/getMahalle",
               data: 'semt='+semt,
               success: function (data) {
                        var dat = $.parseJSON(data);
                        if(dat){
                            var ekle = '<option value="0">Seçiniz</option>';
                            $.each(dat,function(key,vall){
                               ekle+='<option value="'+vall['Mahalle']['id']+'">'+vall['Mahalle']['mahalle_adi']+'</option>';
                            });
                            $('#mahalle').html(ekle);
                            $.unblockUI();
                        }
                    }
           });
        }else{
            $.unblockUI();
        }
    });
    
    $('#addResim').on('click',function(){
		var resTop = 0;
		for(var i = 0; i < $('input.resimekleclass').length; i++){
			resTop += $('input.resimekleclass').get(i).files.length;
		}
		
        if((resTop+$('.resims').length) >= 20){
            alert('20 taneden fazla resim eklenemez.');
        }else{
            $('#resler').append(resimTR);
        }
    });
    
    $('#resler').on('click','#resIptal',function(){
       $(this).closest('tr').remove();
    });
    
    $('#addVideo').on('click',function(){
        $('#videolar').append('<tr><td><input type="file" name="konutVideo" class="form-control"/></td>\n\
        <td><button type="button" class="btn btn-sm btn-danger btn-small" id="videoIptal">İptal</button></td></tr>');
        $('#addVideo').hide();
    });
    
    $('#videolar').on('click','#videoIptal',function(){
        $(this).closest('tr').remove();
        $('#addVideo').show();
    });
    
    $('#KonutKaydet').on('click',function(){
        var iletHata = 0;
		var resTop = 0;
		for(var i = 0; i < $('input.resimekleclass').length; i++){
			resTop += $('input.resimekleclass').get(i).files.length;
		}
		resTop += $('.resims').length;
		
        $('#ilanDiv .ilanilet').each(function(){
            if($(this).val() == 0){
                iletHata++;
            }
        });
        if($('#baslik').val() == ''){
            $('#UyariModal #UyariContent').html('Başlık boş bırakılamaz.');
            $('#UyariModal').modal({
                keyboard:false,
                backdrop:'static'
            });
            return false;
        } else if($('#il').val() == 0){
            $('#UyariModal #UyariContent').html('Lütfen şehir seçiniz.');
            $('#UyariModal').modal({
                keyboard:false,
                backdrop:'static'
            });
            return false;
        }else if(iletHata > 0){
            $('#ilanDiv').find('input, textarea, select')
            .not('input[type=hidden],input[type=button],input[type=submit],input[type=reset],input[type=image],button')
            .filter(':enabled:visible:first')
            .focus();
            $('#UyariModal #UyariContent').html('Lütfen ilan iletişimlerini boş bırakmayınız.');
            $('#UyariModal').modal({
                keyboard:false,
                backdrop:'static'
            });
            return false;
        }else if(!validateYouTubeUrl($('input[name="video"]').val()) && $('input[name="video"]').val() != ''){
            $('input[name="video"]').not('input[type=hidden],input[type=button],input[type=submit],input[type=reset],input[type=image],button')
                .filter(':enabled:visible:first')
                .focus();
            $('#UyariModal #UyariContent').html("Eklemek istediğiniz video youtube videosu değildir. Lütfen önce videonuzu <a href='https://www.youtube.com/' target='_blank'>youtube</a>'a ekleyip linkini giriniz.");
            $('#UyariModal').modal({
                keyboard:false,
                backdrop:'static'
            });
            return false;
        }else if(resTop > 20){
			$('#UyariModal #UyariContent').html('En fazla 20 tane resim ekliyebilirsiniz. Lütfen eklemek istediğiniz 20 resmi seçiniz.');
            $('#UyariModal').modal({
                keyboard:false,
                backdrop:'static'
            });
            return false;
		}
        else{
            $.blockUI({ css: { backgroundColor: 'transparent', border: 'none'},message: $('#LoaderBlock') });
            $('#KonutForm').submit();
        }
    });
    
   $('#us2').locationpicker({
	location: {latitude: <?php echo empty($konuts['KonutLocation']['latitude'])?0:$konuts['KonutLocation']['latitude'];?>, longitude: <?php echo empty($konuts['KonutLocation']['longitude'])?0:$konuts['KonutLocation']['longitude'];?>},	
	radius: 10,
	inputBinding: {
        latitudeInput: $('#us2-lat'),
        longitudeInput: $('#us2-lon'),
        radiusInput: $('#us2-radius'),
        locationNameInput: $('#us2-address')
        },
        enableAutocomplete: true
    });
    
    $('#KonutIptal').on('click',function(){
       window.location.href = '<?php echo $this->Html->url('/');?>konuts/home/SatKir:0';
    });
});

function DeleteResim(resId){
    if(confirm('Resmi silmek istediğinizden emin misiniz?')){
        $.ajax({
           async: false,
           type: 'POST',
           url: "<?php echo $this->Html->url('/');?>konuts/deleteResim",
           data: 'resId='+resId,
           success: function (data) {
               var dat = $.parseJSON(data);
               if(dat){
                   $('#Res_'+resId).remove();
               }else{
                   alert('Bir hata meydana geldi. Lütfen tekrar deneyin.');
               }
            }
        });
    }
}

function KonutSil(konutId){
    if(confirm('Konutu silmek istediğinizden emin misiniz?')){
        $.blockUI({ css: { backgroundColor: 'transparent', border: 'none'},message: $('#LoaderBlock') });
        $.ajax({
            async: false,
            type: 'POST',
            url: "<?php echo $this->Html->url('/');?>konuts/ajaxDeleteKonut",
            data:"konutId="+konutId,
            success: function (data) {
                $.unblockUI();
                        alert('Konut silme işlemi başarıyla gerçekleşti.');
                        window.location.href="<?php echo $this->Html->url('/')?>konuts/home";
                    }
        });
    }else{
        return false;
    }
}
</script>