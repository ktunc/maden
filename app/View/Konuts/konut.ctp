<?php
echo $this->Html->css('photoswipe');
echo $this->Html->script('simple-inheritance.min');
echo $this->Html->script('code-photoswipe-1.0.11');
echo $this->Html->css('../js/Flickerplate-master/css/flickerplate');
echo $this->Html->script('Flickerplate-master/js/min/modernizr-custom-v2.7.1.min');
echo $this->Html->script('Flickerplate-master/js/min/jquery-finger-v0.1.0.min');
echo $this->Html->script('Flickerplate-master/js/min/flickerplate.min');
?>
<div class="bg-white printwidth">
<div class="ilanUst row">
    <div class="col-xs-12 text-danger h3 fontBold"><?php echo $konuts['Konut']['baslik'];?></div>
    <div class="col-xs-12 h6"><?php echo $konuts['Sehir']['sehir_adi'];
    if($konuts['Ilce']['id']){
                echo ' / '.$konuts['Ilce']['ilce_adi'];
                if($konuts['Semt']['id']){
                    echo ' / '.$konuts['Semt']['semt_adi'];
                    if($konuts['Mahalle']['id']){
                        echo ' / '.$konuts['Mahalle']['mahalle_adi'];
                    }
                }
            }?></div>
</div>

<div class="row">
    <div class="col-xs-12 col-sm-6">
        <div class="FlickPadLR">
        <div class="flicker-example flickCSS hidden-print" data-block-text="false">
            <ul id="Gallery">
                <?php
                if(count($konutRes)>0){
                    foreach($konutRes as $row){
                         echo '<li style="background-size:100% 100%">'
                                 .'<div class="flick-title">'
                                 . '<a href="'.$this->Html->url('/').$row['KonutResim']['path'].'">'
                                 . '<img class="imgIcerik" src="'.$this->Html->url('/').$row['KonutResim']['path'].'" alt="'.$konuts['Konut']['baslik'].' '.$row['KonutResim']['id'].'"/>'
                                 . '</a>'
                                 . '</div>'
                                 . '</li>';
                    }
                }else{
                    echo '<li style="background-size:100% 100%">'
                        .'<div class="flick-title">'
                        . '<a href="'.$this->Html->url('/').'img/nofoto.png">'
                        . '<img class="imgIcerik" src="'.$this->Html->url('/').'img/nofoto.png" alt="'.$konuts['Konut']['baslik'].'"/>'
                        . '</a>'
                        . '</div>'
                        . '</li>';
                } ?>
            </ul>
            <!--
            <div class="IlanShareIcon">
                <button class="btn btn-warning btn-sm" id="ShareBut"><i class="fa fa-2x fa-share-alt"></i></button>
            </div>
            -->
        </div>
            <div class="visible-print">
                    <?php
                    if(count($konutRes)>0){
                        echo '<img class="imgIcerik" src="'.$this->Html->url('/').$konutRes[0]['KonutResim']['path'].'" alt="'.$konuts['Konut']['baslik'].' '.$konutRes[0]['KonutResim']['id'].'"/>';
                    }else{
                        echo '<img class="imgIcerik" src="'.$this->Html->url('/').'img/logo.png" alt="'.$konuts['Konut']['baslik'].'"/>';
                    } ?>
            </div>
        </div>
        <?php
        $str = strrev($konuts['Konut']['fiyat']);
        echo '<div class="anaDiv text-danger h3 fontBold">'.substr(strrev(chunk_split($str,3,".")),1).' '.$konuts['Konut']['parabirimi'].'</div>';
        ?>
    </div>
    <div class="col-xs-12 col-sm-6 font16">
        <table class="table table-bordered table-striped table-hover">
            <tr>
                <td>İlan Numarası</td>
                <td><?php echo $konuts['Konut']['ilan_no'];?></td>
            </tr>
            <tr>
                <td>Emlak Tipi</td>
                <td>Konut</td>
            </tr>
            <?php
            $konArr = array('tarih'=>'İlan Tarihi','oda_sayisi'=>'Oda Sayisi','m_kare'=>'m<sup>2</sup>',
                'bina_kat'=>'Toplam Kat','kat'=>'Bulunduğu Kat','kredi'=>'Krediye Uygunluk');
            foreach($konArr as $key=>$val){
                if(!empty($konuts['Konut'][$key])){
                    echo '<tr>';
                    echo '<td>'.$val.'</td>';
                    echo '<td>'.$konuts['Konut'][$key].'</td>';
                    echo '</tr>';
                }
            }
            ?>
            <?php if($this->Session->check('UserLogin')){?>
                <tr>
                    <td>Mal Sahibi Adı Soyadı:</td>
                    <td><?php echo $konuts['Konut']['mal_name'];?></td>
                </tr>
                <tr>
                    <td>Mal Sahibi Telefon No:</td>
                    <td><?php echo $konuts['Konut']['mal_tel'];?></td>
                </tr>
                <tr>
                    <td>Mal Sahibi Fiyat:</td>
                    <?php
                    $str = strrev($konuts['Konut']['mal_fiyat']);
                    echo '<td>'.substr(strrev(chunk_split($str,3,".")),1).' '.$konuts['Konut']['mal_parabirimi'].'</td>';
                    ?>
                </tr>
            <?php } ?>
        </table>
        <div class="FlickPadLR hidden-print" style="width: 100%;">
            <?php if(!empty($konuts['KonutLocation']) && !empty($konuts['KonutLocation']['id'])){ ?>
            <div class="divYan">
                <!--<a target="_blank" href="https://maps.google.com?saddr=Current+Location&daddr=<?php echo $konuts['KonutLocation']['latitude']; ?>,<?php echo $konuts['KonutLocation']['longitude']; ?>"><img class="ShareIcon" alt="map_direction_icon" src="<?php echo $this->webroot;?>img/map.png" /></a>-->
                <img class="ShareIcon" onclick="FuncMapGoruntule(<?php echo $konuts['KonutLocation']['latitude']; ?>,<?php echo $konuts['KonutLocation']['longitude']; ?>)" alt="map_direction_icon" src="<?php echo $this->webroot;?>img/map.png" />
            </div>
            <?php } ?>
            <?php if(!empty($konuts['Konut']['video'])){ ?>
                <div class="divYan fRight">
                    <img class="ShareIcon ytvideo" onclick="YTVideoAc(1)" data-yturl="<?php echo $konuts['Konut']['video']; ?>" src="<?php echo $this->webroot;?>img/shareicon/cam.png"/>
                </div>
            <?php } ?>

            <?php if($IlanAdres && 1==0){ ?>
                <div class="divYan fRight">
                    <img class="ShareIcon" id="OpenIlanAdresModal" src="<?php echo $this->webroot;?>img/shareicon/adres.png"/>
                </div>
            <?php } ?>
            <?php if($IlanMail && 1==0){ ?>
                <div class="divYan fRight">
                    <img class="ShareIcon" id="OpenIlanMailModal" src="<?php echo $this->webroot;?>img/shareicon/mail.png"/>
                </div>
            <?php } ?>
            <?php if($IlanTel && 1==0){ ?>
                <div class="divYan fRight">
                    <img class="ShareIcon" id="OpenIlanTelModal" src="<?php echo $this->webroot;?>img/shareicon/tel.png"/>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<?= $this->element('IlanDanisman', array('data' => $konuts['KonutDanisman'])) ?>
</div>

<input type="hidden" id="konutId" value="<?php echo $konuts['Konut']['id']; ?>" />
<?php if(!empty($konuts['Konut']['aciklama'])){ ?>
    <div class="kback text-white printwidth">
        <div class="row">
            <div class="text-center col-xs-12 hidden-print">
                <h4 type="button" onclick="FuncAciklamaGetir(<?php echo $konuts['Konut']['id']; ?>)" >Açıklama</h4>
            </div>
            <div class="text-center col-xs-12 visible-print ilanaciklama">
                <?php echo nl2br($konuts['Konut']['aciklama']); ?>
            </div>
        </div>
    </div>
<?php }?>

<?php 
function generateTarih($tarih){
    $date = explode(' ', $tarih);
    $tarih = explode('-', $date[0]);
    return $tarih[2].'/'.$tarih[1].'/'.$tarih[0];
}
?>
<script type="text/javascript">
$(document).ready(function() {
    document.title = "<?php echo $konuts['Konut']['baslik'];?>";

Code.photoSwipe('a', '#Gallery');

$('.flicker-example').flicker({
    auto_flick: true,
    auto_flick_delay: 5,
    flick_animation: "transform-slide"
});

    $('#ShareBut').on('click',function(){
        $('#ShareIlanModal').modal({
            keyboard:false,
            backdrop:'static'
        });
    });
    $('#OpenIlanTelModal').on('click',function(){
        $('#IlanTelModal').modal({
            keyboard:false,
            backdrop:'static'
        });
    });
    $('#OpenIlanAdresModal').on('click',function(){
        $('#IlanAdresModal').modal({
            keyboard:false,
            backdrop:'static'
        });
    });
    $('#OpenIlanMailModal').on('click',function(){
        $('#IlanMailModal').modal({
            keyboard:false,
            backdrop:'static'
        });
    });
});

function FuncAciklamaGetir(kId){
    $.blockUI({ css: { backgroundColor: 'transparent', border: 'none'},message: $('#LoaderBlock') });
    setTimeout(function(){
        $.ajax({
            async:false,
            type:'POST',
            url:'<?php echo $this->Html->url('/');?>konuts/aciklamagetir',
            data:'kId='+kId
        }).done(function(data){
            var dat = $.parseJSON(data);
            if(dat){
                $('#AciklamaModal .bodyaciklama').html(dat);
                $('#AciklamaModal #header, #AciklamaModal #footer').removeClass();
                $('#AciklamaModal #header').addClass('modal-header kback text-white');
                $('#AciklamaModal #footer').addClass('modal-footer kback text-white');
                $('#AciklamaModal').modal({
                    keyboard:false,
                    backdrop:'static'
                });
                $.unblockUI();
            }else{
                $('#UyariModal #UyariContent').html('Bir hata meydana geldi. Lütfen sayfayı yenileyerek tekrar deneyin');
                $('#UyariModal').modal({
                    keyboard:false,
                    backdrop:'static'
                });
                $.unblockUI();
            }
        }).fail(function(){
            $('#UyariModal #UyariContent').html('Lütfen internet bağlantınızı kontrol ediniz.');
            $('#UyariModal').modal({
                keyboard:false,
                backdrop:'static'
            });
            $.unblockUI();
        });
    },500);

}
//function OpenMaps(lati,long){
//    window.location.href = 'http://www.google.com/maps/@'+lati+','+long+',18z';
//}

</script>
