<div class="row">
    <div class="col-xs-6">
<!-- SearcForm Bas -->
<?php echo $this->element('searchForm'); ?>
<!-- SearcForm Son -->
    </div>
    <div class="col-xs-6">
        <select id="siralama" class="form-control">
            <?php $selected = 'selected="selected"'; ?>
            <?php
            if(array_key_exists('Konut.tarih', $sirala)){
                if($sirala['Konut.tarih']=='DESC'){
                    echo '<option value="tarih:desc" selected="selected">Tarih Azalan</option>';
                    echo '<option value="tarih:asc">Tarih Artan</option>'
                    . '<option value="fiyat:desc">Fiyat Azalan</option>
                        <option value="fiyat:asc">Fiyat Artan</option>';
                }else if($sirala['Konut.tarih']=='ASC'){
                    echo '<option value="tarih:desc">Tarih Azalan</option>';
                    echo '<option value="tarih:asc" selected="selected">Tarih Artan</option>'
                    . '<option value="fiyat:desc">Fiyat Azalan</option>
                       <option value="fiyat:asc">Fiyat Artan</option>';
                }
            }else if(array_key_exists('Konut.fiyat', $sirala)){
                if($sirala['Konut.fiyat']=='DESC'){
                    echo '<option value="tarih:desc">Tarih Azalan</option>';
                    echo '<option value="tarih:asc">Tarih Artan</option>'
                    . '<option value="fiyat:desc" selected="selected">Fiyat Azalan</option>
                        <option value="fiyat:asc">Fiyat Artan</option>';
                }else if($sirala['Konut.fiyat']=='ASC'){
                    echo '<option value="tarih:desc">Tarih Azalan</option>';
                    echo '<option value="tarih:asc">Tarih Artan</option>'
                    . '<option value="fiyat:desc">Fiyat Azalan</option>
                        <option value="fiyat:asc" selected="selected">Fiyat Artan</option>';
                }
            }else{
                echo '<option value="tarih:desc" selected="selected">Tarih Azalan</option>';
                    echo '<option value="tarih:asc">Tarih Artan</option>'
                    . '<option value="fiyat:desc">Fiyat Azalan</option>
                        <option value="fiyat:asc">Fiyat Artan</option>';
            }
            ?>
        </select>
    </div>
</div>
<div class="row"><hr></div>
<?php 
echo $this->Html->css('font-awesome.min');

foreach($konuts as $row){
?>
<blockquote class="bg-danger">
<div class="row" style="margin-bottom: 1%">
    <a href="<?php echo $this->Html->url('/')?>konuts/konut/ilanNo:<?php echo $row['Konut']['ilan_no'];?>">
    <div class="col-xs-6 col-sm-5 text-center">
        <?php 
         if($konutRes[$row['Konut']['id']]){
                echo '<img class="imgHome" src="'.$this->Html->url('/').$konutRes[$row['Konut']['id']][0]['KonutResim']['path'].'" alt="'.$row['Konut']['baslik'].' '.$konutRes[$row['Konut']['id']][0]['KonutResim']['id'].'"/>';
        }else{
            echo '<img class="imgHome" src="'.$this->Html->url('/').'img/nofoto.png" style="width:100%" alt="'.$row['Konut']['baslik'].'"/>';
        }
        ?>
    </div>
    </a>
    <div class="col-xs-6 col-sm-7">
        <!--<div class="row">-->
            <div class="fontBold" style="text-align: right"><strong><?php echo $row['Konut']['ilan_no'];?></strong></div>
            <div class="fontBold"><?php echo CakeText::truncate($row['Konut']['baslik'],45);?></div>
            <div class=""><?php echo $row['Konut']['m_kare'];?>m<sup>2</sup></div>
            <div class=""><?php
            $str = strrev($row['Konut']['fiyat']);
            echo substr(strrev(chunk_split($str,3,".")),1).' '.$row['Konut']['parabirimi'];
            ?>
            </div>
            <div class=""><?php echo $row['Konut']['sat_kir']==1?'Satılık':'Kiralık';?></div>
            <?php 
            // Giriş yapan User
            if($this->Session->check('UserLogin')){
            ?>
            <div class="fRight" style="text-align: right">                
                <a href="<?php echo $this->Html->url('/');?>konuts/konut/ilanNo:<?php echo $row['Konut']['ilan_no'];?>" class="btn  btn-primary btn-sm"><span class="glyphicon glyphicon-chevron-right"></span></a>
                <a href="<?php echo $this->Html->url('/');?>konuts/duzenle/ilanNo:<?php echo $row['Konut']['ilan_no'];?>" class="btn  btn-default btn-sm"><span class="glyphicon glyphicon-pencil"></span></a>
                <button type="button" class="btn  btn-warning btn-sm" onclick="KonutSil(<?php echo $row['Konut']['id'];?>)"><span class="glyphicon glyphicon-remove"></span></button>
            </div>
            <?php
            }else{
            ?>
            <div class="fRight" style="text-align: right">
                <a href="<?php echo $this->Html->url('/');?>konuts/konut/ilanNo:<?php echo $row['Konut']['ilan_no'];?>" class="btn  btn-primary btn-sm"><span class="glyphicon glyphicon-chevron-right"></span></a>
            </div>
            <?php
            }
            ?>
        <!--</div>-->
    </div>
</div>
</blockquote>
<?php
}
// pagination section
    echo '<ul class="pagination" style="display:table; margin:0 auto;">';
 
        echo $this->Paginator->prev( '<<', array( 'class' => '', 'tag' => 'li','escape' => false), null, array( 'class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a','escape' => false) );
        echo $this->Paginator->numbers( array( 'tag' => 'li', 'separator' => '', 'currentClass' => 'active', 'currentTag'=>'a' ) );
        echo $this->Paginator->next( '>>', array( 'class' => '', 'tag' => 'li','escape' => false ), null, array( 'class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a','escape' => false) );
        
    echo '</ul>'; 
?>
<script type="text/javascript">
$(function() {
document.title = "KONUT - Maden Gayrimenkul";
    $('#siralama').on('change',function(){
       var vall = $(this).val();
       var urll = '<?php echo $this->Html->url();?>';
       urll = urll.replace('/tarih:desc','');
       urll = urll.replace('/tarih:asc','');
       urll = urll.replace('/fiyat:desc','');
       urll = urll.replace('/fiyat:asc','');
       window.location.href = urll+'/'+vall;
    });
});

function KonutSil(konutId){
    if(confirm('Konutu silmek istediğinizden emin misiniz?')){
        $('#Loader').modal({
            keyboard:false,
            backdrop:'static'
        });
        $.ajax({
            async: false,
            type: 'POST',
            url: "<?php echo $this->Html->url('/');?>konuts/ajaxDeleteKonut",
            data:"konutId="+konutId,
            success: function (data) {
                $('#Loader').modal('hide');
                var dat = $.parseJSON(data);
                if(dat['hata']){
                    alert(dat['message']);
                }else{
                    alert(dat['message']);
                    window.location.reload();
                }
            }
        });
    }else{
        return false;
    }
}
</script>