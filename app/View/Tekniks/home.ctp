<?php
foreach($tekniks as $row){
?>
<blockquote class="bg-warning">
<div class="row" style="margin-bottom: 1%">
    <a href="<?php echo $this->Html->url('/')?>teknikanaliz/ilanNo:<?php echo $row['TeknikAnaliz']['id'];?>">
    <div class="col-xs-6 col-sm-5 text-center">
         <?php 
         if($teknikRes[$row['TeknikAnaliz']['id']]){
             echo '<img class="imgHome" src="'.$this->Html->url('/').$teknikRes[$row['TeknikAnaliz']['id']][0]['TeknikAnalizResim']['path'].'"  alt="'.$row['TeknikAnaliz']['baslik'].' '.$teknikRes[$row['TeknikAnaliz']['id']][0]['TeknikAnalizResim']['id'].'"/>';
        }else{
            echo '<img class="imgHome" src="'.$this->Html->url('/').'img/nofoto.png" alt="'.$row['TeknikAnaliz']['baslik'].'"/>';
        }
        ?>
    </div>
    </a>
    <div class="col-xs-6 col-sm-7">
        <!--<div class="row">-->
            <div class="fontBold" style="color:#000000"><?php echo CakeText::truncate($row['TeknikAnaliz']['baslik'],50);?></div>
            <div class=""><?php echo CakeText::truncate(strip_tags($row['TeknikAnaliz']['aciklama']),175);?></div>
            <?php 
            // Giriş yapan User
            if($this->Session->check('UserLogin')){
            ?>
            <div class="fRight" style="text-align: right">
                <a href="<?php echo $this->Html->url('/');?>teknikanaliz/ilanNo:<?php echo $row['TeknikAnaliz']['id'];?>" class="btn  btn-primary btn-sm"><span class="glyphicon glyphicon-chevron-right"></span></a>
                <a href="<?php echo $this->Html->url('/');?>tekniks/duzenle/ilanNo:<?php echo $row['TeknikAnaliz']['id'];?>" class="btn  btn-default btn-sm"><span class="glyphicon glyphicon-pencil"></span></a>
                <button type="button" class="btn  btn-danger btn-sm" onclick="TeknikAnalizSil(<?php echo $row['TeknikAnaliz']['id'];?>)"><span class="glyphicon glyphicon-remove"></span></button>
            </div>
            <?php
            }else{
            ?>
            <div class="fRight" style="text-align: right">
                <a href="<?php echo $this->Html->url('/');?>teknikanaliz/ilanNo:<?php echo $row['TeknikAnaliz']['id'];?>" class="btn  btn-primary btn-sm"><span class="glyphicon glyphicon-chevron-right"></span></a>
            </div>
            <?php
            }
            ?>
        <!--</div>-->
    </div>
</div>
</blockquote>
<?php
}
// pagination section
    echo '<ul class="pagination" style="display:table; margin:0 auto;">';
 
        echo $this->Paginator->prev( '<<', array( 'class' => '', 'tag' => 'li','escape' => false), null, array( 'class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a','escape' => false) );
        echo $this->Paginator->numbers( array( 'tag' => 'li', 'separator' => '', 'currentClass' => 'active', 'currentTag'=>'a' ) );
        echo $this->Paginator->next( '>>', array( 'class' => '', 'tag' => 'li','escape' => false ), null, array( 'class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a','escape' => false) );
        
    echo '</ul>'; 
?>
<script type="text/javascript">
$(document).ready(function() {
document.title = "HABER - Maden Gayrimenkul";
});

function TeknikAnalizSil(teknikId){
    if(confirm('TeknikAnalizyi silmek istediğinizden emin misiniz?')){
        $('#Loader').modal({
            keyboard:false,
            backdrop:'static'
        });
        $.ajax({
            async: false,
            type: 'POST',
            url: "<?php echo $this->Html->url('/');?>tekniks/ajaxDeleteTeknikAnaliz",
            data:"teknikId="+teknikId,
            success: function (data) {
                        alert('TeknikAnaliz silme işlemi başarıyla gerçekleşti.');
                        window.location.href="<?php echo $this->Html->url('/')?>tekniks/home";
                    }
        });
    }else{
        return false;
    }
}
</script>