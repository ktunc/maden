<?php 
echo $this->Html->css('photoswipe');
echo $this->Html->script('simple-inheritance.min');
echo $this->Html->script('code-photoswipe-1.0.11');
echo $this->Html->css('../js/Flickerplate-master/css/flickerplate');
echo $this->Html->script('Flickerplate-master/js/min/modernizr-custom-v2.7.1.min');
echo $this->Html->script('Flickerplate-master/js/min/jquery-finger-v0.1.0.min');
echo $this->Html->script('Flickerplate-master/js/min/flickerplate.min');
if(!$this->Session->check('Mobile') && !empty($tekniks['TeknikAnaliz']['video'])){
    echo $this->Html->css('video-js');
    echo $this->Html->script('video');
}
?>
<style>
    .vjs-default-skin .vjs-mute-control,
    .vjs-default-skin .vjs-volume-control{
        display: none;
    }
</style>

<input type="hidden" id="teknikId" value="<?php echo $tekniks['TeknikAnaliz']['id']; ?>" />
<div class="container">
    <div class="ilanUst row">
        <div class="col-xs-12 text-danger h3 fontBold"><?php echo $tekniks['TeknikAnaliz']['baslik'];?></div>
        <div class="h5 col-xs-12"><?php echo generateTarih($tekniks['TeknikAnaliz']['tarih']);?></div>
    </div>

    <?php if(!empty($teknikRes)){ ?>
    <div class="row">
        <div class="col-xs-12">
            <div class="FlickPadLR">
                <div class="flicker-example flickCSS" data-block-text="false">
                <ul id="Gallery">
                    <?php
                    if(count($teknikRes)>0){
                        foreach($teknikRes as $row){
                             echo '<li style="background-size:100% 100%">'
                                     .'<div class="flick-title">'
                                     . '<a href="'.$this->Html->url('/').$row['TeknikAnalizResim']['path'].'">'
                                     . '<img class="imgIcerik" src="'.$this->Html->url('/').$row['TeknikAnalizResim']['path'].'" alt="'.$tekniks['TeknikAnaliz']['baslik'].' '.$row['TeknikAnalizResim']['id'].'"/>'
                                     . '</a>'
                                     . '</div>'
                                     . '</li>';
                        }
                    }else{
                         echo '<li style="background-size:100% 100%">'
                            .'<div class="flick-title">'
                            . '<a href="'.$this->Html->url('/').'img/logo.png">'
                            . '<img class="imgIcerik" src="'.$this->Html->url('/').'img/logo.png" alt="'.$tekniks['TeknikAnaliz']['baslik'].'"/>'
                            . '</a>'
                            . '</div>'
                            . '</li>';
                    } ?>
                </ul>
                <div class="IlanShareIcon">
                    <button class="btn btn-warning btn-sm" id="ShareBut"><i class="fa fa-2x fa-share-alt"></i></button>
                </div>
                <?php
                if(!empty($tekniks['TeknikAnaliz']['video'])){
                    ?>
                    <div class="IlanVideoIcon">
                        <button id="videoGetir" class="btn btn-primary btn-sm"><i class="fa fa-video-camera fa-2x"></i></button>
                    </div>
                <?php } ?>
            </div>
            </div>
        </div>
    </div>
    <?php }else{ ?>
        <div class="row text-right">
            <button class="btn btn-warning btn-sm" id="ShareBut"><i class="fa fa-2x fa-share-alt"></i></button>
        </div>
    <?php } ?>
    <div class="row">
        <div class="ilanIcerik col-xs-12">
            <?php echo nl2br($tekniks['TeknikAnaliz']['aciklama']);?>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12"><hr></div>
    </div>
    <div class="row"><h4 class="text-center">Yorumlar</h4></div>
    <div class="row" id="YorumDiv">
    <?php if(!empty($teknikYorum)){
        $say = 0;
        foreach($teknikYorum as $row){
            $panelClass = "panel-default";
            if($say%2 == 0){
                $panelClass = "panel-primary";
            }
            echo '<div class="panel '.$panelClass.'">';
            echo '<div class="panel-heading"><div class="row panel-title">';
            if($say == 0){
                echo '<div class="col-xs-9 yorumClass"><i class="fa fa-arrow-down"></i> '.$row['TeknikAnalizYorum']['isim'].' # '.$row['TeknikAnalizYorum']['tarih'].'</div>';
            }else{
                echo '<div class="col-xs-9 yorumClass"><i class="fa fa-arrow-right"></i> '.$row['TeknikAnalizYorum']['isim'].' # '.$row['TeknikAnalizYorum']['tarih'].'</div>';
            }

            if($this->Session->check('UserLogin')){
                echo '<div class="col-xs-3 text-right"><button onclick="FuncYorumEdit('.$row['TeknikAnalizYorum']['teknik_analiz_id'].','.$row['TeknikAnalizYorum']['id'].')" type="button" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i></button><button onclick="FuncYorumSil('.$row['TeknikAnalizYorum']['id'].')" type="button" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button></div>';
            }
            echo '</div></div>';
            if($say == 0){
                echo '<div class="panel-body">'.nl2br($row['TeknikAnalizYorum']['yorum']).'</div>';
            }else{
                echo '<div class="panel-body" style="display: none;">'.nl2br($row['TeknikAnalizYorum']['yorum']).'</div>';
            }

            echo '</div>';
            $say++;
        }
    } ?>
    </div>
    <div class="row">
        <div class="col-xs-12"><button type="button" class="btn btn-sm btn-success" onclick="FuncYorumModal(<?php echo $tekniks['TeknikAnaliz']['id']; ?>)">Yorum Yap</button></div>
    </div>
    <div class="row">
        <div class="col-xs-12"><hr></div>
    </div>
</div>

<div class="container">
    <?php echo $this->element('iletisim');?>
</div>
<!--<div class="col-xs-12" id="us2" style="min-width: 300px; min-height: 300px;margin-top: 2%;" ></div>-->
<?php 
function generateTarih($tarih){
    $date = explode(' ', $tarih);
    $tarih = explode('-', $date[0]);
    return $tarih[2].'/'.$tarih[1].'/'.$tarih[0].' - '.$date[1];
}
?>
<script type="text/javascript">
$(document).ready(function() {
    document.title = "<?php echo $tekniks['TeknikAnaliz']['baslik'];?>";
    
    <?php if(!empty($tekniks['TeknikAnaliz']['video'])){ ?>
    $('#VideoModal .bodyvideo #myvideo source').attr('src','<?php echo $this->Html->url('/').$tekniks['TeknikAnaliz']['video']?>');
    $('#VideoModal .bodyvideo video').load();
    <?php } ?>

    <?php if(!empty($teknikRes)){ ?>
    Code.photoSwipe('a', '#Gallery');
    $('.flicker-example').flicker({
        auto_flick: true,
        auto_flick_delay: 5,
        flick_animation: "transform-slide"
    });
    <?php } ?>

    $('.yorumClass').on('click',function(e){
        e.preventDefault();
        if($(this).find('i').hasClass('fa-arrow-right')){
            $(this).find('i').removeClass('fa-arrow-right');
            $(this).find('i').addClass('fa-arrow-down');
            $(this).closest('.panel').find('.panel-body').toggle('slow');
        }else{
            $(this).find('i').removeClass('fa-arrow-down');
            $(this).find('i').addClass('fa-arrow-right');
            $(this).closest('.panel').find('.panel-body').toggle('slow');
        }
    });


//$('#us2').locationpicker({
//	location: {latitude: <?php echo empty($tekniks['TeknikAnalizLocation']['latitude'])?0:$tekniks['TeknikAnalizLocation']['latitude'];?>, longitude: <?php echo empty($tekniks['TeknikAnalizLocation']['longitude'])?0:$tekniks['TeknikAnalizLocation']['longitude'];?>},
//	radius: 10,
//        zoom:17,
//        draggable:false
//    });

    $('#videoGetir').on('click',function(e){
        e.preventDefault();
        $('#VideoModal').modal({
            keyboard:false,
            backdrop:'static'
        });
        
    });
    
    $('#videoHide').on('click',function(){
        $('#VideoModal').modal('hide');
        <?php if($this->Session->check('Mobile')){ ?>
            $('#myvideo').get(0).pause();
            $('#myaudio').get(0).pause();
        <?php }else{ ?>
        videojs("myvideo").ready(function(){
            this.pause();
        });
        <?php }?>
    });

    $('#ShareBut').on('click',function(){
        $('#ShareIlanModal').modal({
            keyboard:false,
            backdrop:'static'
        });
    });
});

<?php if(!empty($tekniks['TeknikAnaliz']['video'])){ ?>
<?php if($this->Session->check('Mobile')){ ?>
$(document).ready(function () {
    $('#myvideo').on('click', function () {
        if ($(this).get(0).paused) {
            $('#myaudio').get(0).play();
        } else if ($(this).get(0).played) {
            $('#myaudio').get(0).pause();
        }
    });
});
<?php }else{ ?>
videojs("myvideo").ready(function () {
    var myPlayer = this;
    myPlayer.muted(true);

    myPlayer.on("pause", pause_event);
    myPlayer.on("play", play_event);
    myPlayer.on("ended", ended_event);
});

function pause_event() {
    $('#myaudio').get(0).pause();
}

function play_event() {
    $('#myaudio').get(0).play();
}

function ended_event() {
    $('#myaudio').get(0).pause();
}
<?php }
}
?>

function FuncYorumModal(tId){
    $('#YorumForm input[name="isim"]').val('');
    $('#YorumForm input[name="email"]').val('');
    $('#YorumForm textarea[name="yorum"]').val('');
    $('#YorumForm input[name="teknikId"]').val(tId);
    $('#YorumForm input[name="yorumId"]').val(0);
    $('#YorumModal').modal({
        keyboard:false,
        backdrop:'static'
    });
}

function FuncYorumEdit(tId,yId){
    $.blockUI({ css: { backgroundColor: 'transparent', border: 'none'},message: $('#LoaderBlock') });
    $('#YorumForm input[name="isim"]').val('');
    $('#YorumForm input[name="email"]').val('');
    $('#YorumForm textarea[name="yorum"]').val('');
    $('#YorumForm input[name="teknikId"]').val(tId);
    $('#YorumForm input[name="yorumId"]').val(0);
    $.ajax({
        type:'POST',
        url:'<?php echo $this->Html->url('/');?>tekniks/ajaxGetTeknikYorum',
        data:'tId='+tId+'&yId='+yId
    }).done(function(data){
        var dat = $.parseJSON(data);
        if(dat['hata']){
            $('div#UyariModal div#UyariContent').html('Bir hata meydana geldi. Lütfen tekrar deneyin.');
            $('#UyariModal').modal({
                keyboard:false,
                backdrop:'static'
            });
            $.unblockUI();
        }else{
            var pata = dat['data'];
            $('#YorumForm input[name="isim"]').val(pata['TeknikAnalizYorum']['isim']);
            $('#YorumForm input[name="email"]').val(pata['TeknikAnalizYorum']['mail']);
            $('#YorumForm textarea[name="yorum"]').val(pata['TeknikAnalizYorum']['yorum']);
            $('#YorumForm input[name="teknikId"]').val(tId);
            $('#YorumForm input[name="yorumId"]').val(yId);
            $('#YorumModal').modal({
                keyboard:false,
                backdrop:'static'
            });
            $.unblockUI();
        }
    }).fail(function(){
        $('div#UyariModal div#UyariContent').html('Bir hata meydana geldi. Lütfen tekrar deneyin.');
        $('#UyariModal').modal({
            keyboard:false,
            backdrop:'static'
        });
        $.unblockUI();
    });
}

function FuncYorumSil(yId){
    if(confirm('Yorumu silmek istediğinizden emin misiniz?')){
        $.blockUI({ css: { backgroundColor: 'transparent', border: 'none'},message: $('#LoaderBlock') });
        $.ajax({
            type:'POST',
            url:'<?php echo $this->Html->url('/');?>tekniks/ajaxGetTeknikYorumSil',
            data:'yId='+yId
        }).done(function(data){
            var dat = $.parseJSON(data);
            if(dat['hata']){
                $('div#UyariModal div#UyariContent').html('Bir hata meydana geldi. Lütfen tekrar deneyin.');
                $('#UyariModal').modal({
                    keyboard:false,
                    backdrop:'static'
                });
                $.unblockUI();
            }else{
                $.unblockUI();
                alert('Yorum başarıyla silindi.');
                window.location.reload();
            }
        }).fail(function(){
            $('div#UyariModal div#UyariContent').html('Bir hata meydana geldi. Lütfen tekrar deneyin.');
            $('#UyariModal').modal({
                keyboard:false,
                backdrop:'static'
            });
            $.unblockUI();
        });
    }
}
</script>