<?php
//echo $this->Html->script('locationpicker.jquery');
//echo $this->Html->script('http://maps.google.com/maps/api/js?sensor=false&libraries=places');
?>
<div class="container">
<h3>Yeni Teknik Analiz</h3>
<form method="POST" role="form" class="form-signin" action="<?php echo $this->Html->url('/');?>tekniks/addteknikanaliz" id="TeknikAnalizForm" enctype="multipart/form-data">
    <label for="baslik">Başlık</label>
    <input type="text" name="baslik" id="baslik" placeholder="Başlik" class="form-control">
    <label for="aciklama">Açıklama</label>
    <textarea name="aciklama" id="aciklama" placeholder="Açıklama" class="form-control" rows="10"></textarea>
    <br>
    <!-- Video -->
    <label for="video">Video</label>
    <table id="videolar"></table>
    <button class="btn  btn-warning" type="button" id="addVideo">Video Ekle</button>
    <br>
    <!-- Resim -->
    <label for="resim">Resim</label>
    <table id="resler">
        
    </table>
    <button class="btn  btn-warning" type="button" id="addResim">Resim Ekle</button>
    <br>
    
    <div class="btn  btn-group">
        <button type="button" class="btn  btn-primary" id="TeknikAnalizKaydet">Kaydet</button>
        <button type="reset" class="btn  btn-danger">Sıfırla</button>
    </div>
</form>
</div>
<?php echo $this->Html->script('tinymce/tinymce.min'); ?>
<script type="text/javascript">
var resimTR = '<tr><td><input type="file" name="teknikRes[]" class="form-control"/></td>\n\
<td><button type="button" class="btn  btn-danger" id="resIptal">İptal</button></td></tr>';
$(document).ready(function(){
    tinymce.init({
        selector: '#aciklama',
        language: 'tr_TR',
        height: 300,
        theme: 'modern',
        plugins: [
            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools'
        ],
        toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        toolbar2: 'print preview media | forecolor backcolor emoticons',
        image_advtab: true
    });

    $('#addResim').on('click',function(){
        if($('input[name="teknikRes[]"]').length >= 20){
            alert('20 taneden fazla resim eklenemez.');
        }else{
            $('#resler').append(resimTR);
        }
    });
    
    $('#addVideo').on('click',function(){
        $('#videolar').append('<tr><td><input type="file" name="teknikVideo" class="form-control"/></td>\n\
<td><button type="button" class="btn  btn-danger btn-small" id="videoIptal">İptal</button></td></tr>');
        $('#addVideo').hide();
    });
    
    $('#videolar').on('click','#videoIptal',function(){
        $(this).closest('tr').remove();
        $('#addVideo').show();
    });
    
    $('#resler').on('click','#resIptal',function(){
       $(this).closest('tr').remove();
    });
    
    $('#TeknikAnalizKaydet').on('click',function(){
        if($('#baslik').val() == ''){
            alert('Lütfen başlık giriniz.');
        }
        else{
            $.blockUI({ css: { backgroundColor: 'transparent', border: 'none'},message: $('#LoaderBlock') });
            $('#TeknikAnalizForm').submit();
        }
    });
});   
</script>