<?php 
echo $this->Html->css('photoswipe');
echo $this->Html->script('simple-inheritance.min');
echo $this->Html->script('code-photoswipe-1.0.11');
echo $this->Html->css('../js/Flickerplate-master/css/flickerplate');
echo $this->Html->script('Flickerplate-master/js/min/modernizr-custom-v2.7.1.min');
echo $this->Html->script('Flickerplate-master/js/min/jquery-finger-v0.1.0.min');
echo $this->Html->script('Flickerplate-master/js/min/flickerplate.min');

echo $this->Html->script('locationpicker.jquery.notdrag');
echo $this->Html->script('http://maps.google.com/maps/api/js?sensor=false&libraries=places');
?>
<div class="bg-white printwidth">
<div class="ilanUst row">
    <div class="h3 col-xs-12 text-danger fontBold"><?php echo $isyeris['Isyeri']['baslik'];?></div>
    <div class="col-xs-12 h6"><?php echo $isyeris['Sehir']['sehir_adi'];
    if($isyeris['Ilce']['id']){
                echo ' / '.$isyeris['Ilce']['ilce_adi'];
                if($isyeris['Semt']['id']){
                    echo ' / '.$isyeris['Semt']['semt_adi'];
                    if($isyeris['Mahalle']['id']){
                        echo ' / '.$isyeris['Mahalle']['mahalle_adi'];
                    }
                }
            }?>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-sm-6">
        <div class="FlickPadLR">
        <div class="flicker-example flickCSS hidden-print" data-block-text="false">
            <ul id="Gallery">
                <?php
                if(count($isyeriRes)>0){
                    foreach($isyeriRes as $row){
                         echo '<li style="background-size:100% 100%">'
                                 .'<div class="flick-title">'
                                 . '<a href="'.$this->Html->url('/').$row['IsyeriResim']['path'].'">'
                                 . '<img class="imgIcerik" src="'.$this->Html->url('/').$row['IsyeriResim']['path'].'" alt="'.$isyeris['Isyeri']['baslik'].' '.$row['IsyeriResim']['id'].'"/>'
                                 . '</a>'
                                 . '</div>'
                                 . '</li>';
                    }
                }else{
                    echo '<li style="background-size:100% 100%">'
                        .'<div class="flick-title">'
                        . '<a href="'.$this->Html->url('/').'img/logo.png">'
                        . '<img class="imgIcerik" src="'.$this->Html->url('/').'img/logo.png" alt="'.$isyeris['Isyeri']['baslik'].'"/>'
                        . '</a>'
                        . '</div>'
                        . '</li>';
                } ?>
            </ul>
            <!--
            <div class="IlanShareIcon">
                <button class="btn btn-warning btn-sm" id="ShareBut"><i class="fa fa-2x fa-share-alt"></i></button>
            </div>
            -->
        </div>
            <div class="visible-print">
                <?php
                if(count($isyeriRes)>0){
                    echo '<img class="imgIcerik" src="'.$this->Html->url('/').$isyeriRes[0]['IsyeriResim']['path'].'" alt="'.$isyeris['Isyeri']['baslik'].' '.$isyeriRes[0]['IsyeriResim']['id'].'"/>';
                }else{
                    echo '<img class="imgIcerik" src="'.$this->Html->url('/').'img/logo.png" alt="'.$isyeris['Isyeri']['baslik'].'"/>';
                } ?>
            </div>
        </div>
        <?php
        $str = strrev($isyeris['Isyeri']['fiyat']);
        echo '<div class="h4 anaDiv text-danger fontBold">'.substr(strrev(chunk_split($str,3,".")),1).' '.$isyeris['Isyeri']['parabirimi'].'</div>';
        ?>
    </div>
    <div class="col-xs-12 col-sm-6 font16">
        <table class="table table-bordered table-striped table-hover">
            <tr>
                <td>İlan Numarası</td>
                <td><?php echo $isyeris['Isyeri']['ilan_no'];?></td>
            </tr>
            <tr>
                <td>Emlak Tipi</td>
                <td>Isyeri</td>
            </tr>
            <tr>
                <td>İlan Tarihi</td>
                <td><?php echo generateTarih($isyeris['Isyeri']['tarih']);?></td>
            </tr>
            <?php
            $isArray = array('tarih'=>'İlan Tarihi','oda_sayisi'=>'Oda Sayısı','m_kare'=>'m<sup>2</sup>',
                'bina_kat'=>'Toplam Kat','kat'=>'Bulunduğu Kat','kredi'=>'Krediye Uygunluk');
            foreach($isArray as $key=>$val){
                if(!empty($isyeris['Isyeri'][$key])){
                    echo '<tr>';
                    echo '<td>'.$val.'</td>';
                    echo '<td>'.$isyeris['Isyeri'][$key].'</td>';
                    echo '</tr>';
                }
            }
            if($this->Session->check('UserLogin')){?>
                <tr>
                    <td>Mal Sahibi Adı Soyadı:</td>
                    <td><?php echo $isyeris['Isyeri']['mal_name'];?></td>
                </tr>
                <tr>
                    <td>Mal Sahibi Telefon No:</td>
                    <td><?php echo $isyeris['Isyeri']['mal_tel'];?></td>
                </tr>
                <tr>
                    <td>Mal Sahibi Fiyat:</td>
                    <?php
                    $str = strrev($isyeris['Isyeri']['mal_fiyat']);
                    echo '<td>'.substr(strrev(chunk_split($str,3,".")),1).' '.$isyeris['Isyeri']['mal_parabirimi'].'</td>';
                    ?>
                </tr>
            <?php } ?>
        </table>
        <div class="FlickPadLR hidden-print" style="width: 100%;">
            <?php if(!empty($isyeris['IsyeriLocation']) && !empty($isyeris['IsyeriLocation']['id'])){ ?>
                <div class="divYan">
                    <img class="ShareIcon" onclick="FuncMapGoruntule(<?php echo $isyeris['IsyeriLocation']['latitude']; ?>,<?php echo $isyeris['IsyeriLocation']['longitude']; ?>)" alt="map_direction_icon" src="<?php echo $this->webroot;?>img/map.png" />
                </div>
            <?php } ?>
            <?php if(!empty($isyeris['Isyeri']['video'])){ ?>
                <div class="divYan fRight">
                    <img class="ShareIcon ytvideo" onclick="YTVideoAc(1)" data-yturl="<?php echo $isyeris['Isyeri']['video']; ?>" src="<?php echo $this->webroot;?>img/shareicon/cam.png"/>
                </div>
            <?php } ?>

            <?php if($IlanAdres && 1==0){ ?>
                <div class="divYan fRight">
                    <img class="ShareIcon" id="OpenIlanAdresModal" src="<?php echo $this->webroot;?>img/shareicon/adres.png"/>
                </div>
            <?php } ?>
            <?php if($IlanMail && 1==0){ ?>
                <div class="divYan fRight">
                    <img class="ShareIcon" id="OpenIlanMailModal" src="<?php echo $this->webroot;?>img/shareicon/mail.png"/>
                </div>
            <?php } ?>
            <?php if($IlanTel && 1==0){ ?>
                <div class="divYan fRight">
                    <img class="ShareIcon" id="OpenIlanTelModal" src="<?php echo $this->webroot;?>img/shareicon/tel.png"/>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
</div>
<input type="hidden" id="isyeriId" value="<?php echo $isyeris['Isyeri']['id']; ?>" />
<?php if(!empty($isyeris['Isyeri']['aciklama'])){ ?>
    <div class="iback text-white printwidth">
        <div class="row">
            <div class="text-center col-xs-12 hidden-print">
                <h4 type="button" onclick="FuncAciklamaGetir(<?php echo $isyeris['Isyeri']['id']; ?>)" >Açıklama</h4>
            </div>
            <div class="text-center col-xs-12 visible-print ilanaciklama">
                <?php echo nl2br($isyeris['Isyeri']['aciklama']); ?>
            </div>
        </div>
    </div>
<?php }?>

<?php 
function generateTarih($tarih){
    $date = explode(' ', $tarih);
    $tarih = explode('-', $date[0]);
    return $tarih[2].'/'.$tarih[1].'/'.$tarih[0];
}
?>
<script type="text/javascript">
$(document).ready(function() {
    document.title = "<?php echo $isyeris['Isyeri']['baslik'];?>";

Code.photoSwipe('a', '#Gallery');
$('.flicker-example').flicker({
    auto_flick: true,
    auto_flick_delay: 5,
    flick_animation: "transform-slide"
});

    $('#ShareBut').on('click',function(){
        $('#ShareIlanModal').modal({
            keyboard:false,
            backdrop:'static'
        });
    });
    $('#OpenIlanTelModal').on('click',function(){
        $('#IlanTelModal').modal({
            keyboard:false,
            backdrop:'static'
        });
    });
    $('#OpenIlanAdresModal').on('click',function(){
        $('#IlanAdresModal').modal({
            keyboard:false,
            backdrop:'static'
        });
    });
    $('#OpenIlanMailModal').on('click',function(){
        $('#IlanMailModal').modal({
            keyboard:false,
            backdrop:'static'
        });
    });
});

function FuncAciklamaGetir(iId){
    $.blockUI({ css: { backgroundColor: 'transparent', border: 'none'},message: $('#LoaderBlock') });
    setTimeout(function(){
        $.ajax({
            async:false,
            type:'POST',
            url:'<?php echo $this->Html->url('/');?>isyeris/aciklamagetir',
            data:'iId='+iId
        }).done(function(data){
            var dat = $.parseJSON(data);
            if(dat){
                $('#AciklamaModal .bodyaciklama').html(dat);
                $('#AciklamaModal #header, #AciklamaModal #footer').removeClass();
                $('#AciklamaModal #header').addClass('modal-header iback text-white');
                $('#AciklamaModal #footer').addClass('modal-footer iback text-white');
                $('#AciklamaModal').modal({
                    keyboard:false,
                    backdrop:'static'
                });
                $.unblockUI();
            }else{
                $('#UyariModal #UyariContent').html('Bir hata meydana geldi. Lütfen sayfayı yenileyerek tekrar deneyin');
                $('#UyariModal').modal({
                    keyboard:false,
                    backdrop:'static'
                });
                $.unblockUI();
            }
        }).fail(function(){
            $('#UyariModal #UyariContent').html('Lütfen internet bağlantınızı kontrol ediniz.');
            $('#UyariModal').modal({
                keyboard:false,
                backdrop:'static'
            });
            $.unblockUI();
        });
    },500);

}
//function OpenMaps(lati,long){
//    window.location.href = 'http://www.google.com/maps/@'+lati+','+long+',18z';
//}
</script>