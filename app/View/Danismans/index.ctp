<?php
foreach($danismans as $row){
    ?>
    <blockquote class="bg-warning">
        <div class="row" style="margin-bottom: 1%">
            <?php
            $link = $this->Html->url('/').'danismans/ilanlar/id:'.$row['Danisman']['id'];
            ?>
            <a href="<?php echo $link;?>">
                <div class="col-xs-6 col-sm-5 text-center">
                    <?php
                    if(!empty($row['Danisman']['resim']) && file_exists($row['Danisman']['resim'])){
                        echo '<img class="imgHome" src="'.$this->webroot.$row['Danisman']['resim'].'"  alt="'.$row['Danisman']['adi'].'_'.$row['Danisman']['soyadi'].'"/>';
                    }else{
                        echo '<img class="imgHome" src="'.$this->Html->url('/').'img/nofoto.png" alt="'.$row['Danisman']['adi'].'_'.$row['Danisman']['soyadi'].'"/>';
                    }
                    ?>
                </div>
            </a>
            <div class="col-xs-6 col-sm-7">
                <!--<div class="row">-->
                <div class="fontBold" style="color:#000000"><?php echo CakeText::truncate(($row['Danisman']['adi'].' '.$row['Danisman']['soyadi']),50);?></div>
                <div class=""><?php echo CakeText::truncate(strip_tags($row['Danisman']['hakkinda']),150);?></div>
                <?php
                // Giriş yapan User
                if($this->Session->check('UserLogin')){
                    ?>
                    <div class="fRight" style="text-align: right">
                        <a href="<?php echo $link;?>" class="btn btn-primary btn-sm tooltipss" title="Danışmanın İlanlarını Görüntüle"><span class="glyphicon glyphicon-chevron-right"></span></a>
                        <a href="<?php echo $this->Html->url('/');?>admins/danismanedit?id=<?php echo $row['Danisman']['id'];?>" class="btn  btn-default btn-sm tooltipss" title="Danışmanı Düzenle"><span class="glyphicon glyphicon-pencil"></span></a>
                        <!--<button type="button" class="btn  btn-danger btn-sm" onclick="DanismanSil(<?php echo $row['Danisman']['id'];?>)"><span class="glyphicon glyphicon-remove"></span></button>-->
                    </div>
                    <?php
                }else{
                    ?>
                    <div class="fRight" style="text-align: right">
                        <a href="<?php echo $link;?>" class="btn  btn-primary btn-sm"><span class="glyphicon glyphicon-chevron-right"></span></a>
                    </div>
                    <?php
                }
                ?>
                <!--</div>-->
            </div>
        </div>
    </blockquote>
    <?php
}
// pagination section
echo '<ul class="pagination" style="display:table; margin:0 auto;">';

echo $this->Paginator->prev( '<<', array( 'class' => '', 'tag' => 'li','escape' => false), null, array( 'class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a','escape' => false) );
echo $this->Paginator->numbers( array( 'tag' => 'li', 'separator' => '', 'currentClass' => 'active', 'currentTag'=>'a' ) );
echo $this->Paginator->next( '>>', array( 'class' => '', 'tag' => 'li','escape' => false ), null, array( 'class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a','escape' => false) );

echo '</ul>';
?>
<script type="text/javascript">
    $(document).ready(function() {
        document.title = "Danışmanlar - Maden Gayrimenkul";
    });
</script>