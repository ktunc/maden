<!-- Telefon MODAL -->
<div id="TelefonModal" class="modal fade bs-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3 class="modal-title" id="myModalLabel">Ara</h3>
          </div>
          <div class="modal-body text-center">
              <button onclick="window.location.href='tel:03126455050'" class="btn btn3d btn-lg btn-primary"><span class="glyphicon glyphicon-phone-alt"></span> 0312 645 50 50</button>
              <br>
              <br>
              <button onclick="window.location.href='tel:05325260909'" class="btn btn3d btn-lg btn-success"><span class="glyphicon glyphicon-phone"></span> 0532 526 09 09</button>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn3d btn-danger btn-small" data-dismiss="modal">Kapat</button>
          </div>
        </div>
    </div>
</div>
<!-- Telefon MODAL SON-->