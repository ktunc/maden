<!-- MapText Modal -->
<div class="modal fade" id="IlanAdresModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title text-warning text-center" id="myModalLabel">İletişim Adresi</h3>
            </div>
            <div class="modal-body">
                <?php if(isset($IlanAdres) && !empty($IlanAdres)){
                    echo '<ul>';
                    foreach($IlanAdres as $row){
                        echo '<li>';
                        echo $row['IlanIlet']['iletisim'];
                        echo '</li>';
                    }
                    echo '</ul>';
                } ?>
            </div>
            <div class="modal-footer">
                <div class="form-group">
                    <button type="button" class="btn btn-danger fontBold" data-dismiss="modal">Kapat</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- MapText Modal SON-->