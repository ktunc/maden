<?php
if(!empty($data)){
    ?>
    <div class="row">
        <?php
        foreach ($data as $row) {
            ?>
            <div class="col-sm-6 col-md-3 resims">
                <div class="thumbnail" style="background-color: #FFFFFF" >
                    <img src="<?php echo $this->webroot.$row['Danisman']['resim'];?>" class="ilandanismanresim" alt="<?= $row['Danisman']['adi'].' '.$row['Danisman']['soyadi']; ?>"/>
                    <div class="row">
                        <p class="text-center"><?=$row['Danisman']['adi'].' '.$row['Danisman']['soyadi']?></p>
                        <p class="danismaniletisim">
                            <?php
                            if(!empty($row['Danisman']['hakkinda'])){
                                echo '<button type="button" class="btn btn-xs btn-primary tooltipss" title="Danışman Hakkında" onclick="FuncAlert(\''.$row['Danisman']['adi'].' '.$row['Danisman']['soyadi'].'\', \''.preg_replace("/\r|\n/", " ", nl2br(htmlentities($row['Danisman']['hakkinda']))).'\', \'xl\', \'blue\')"><i class="fa fa-chain"></i></button>';
                            }
                            if(!empty($row['Danisman']['DanismanIletisim'])){
                                $tel = $mail = '';
                                foreach ($row['Danisman']['DanismanIletisim'] as $cow){
                                    if($cow['type'] == 1){
                                        $tel .= '<div class="row text-center" style="margin: 2px;"><a href="tel:'.$cow['iletisim'].'" class="btn btn-success"><i class="fa fa-phone"></i> '.$cow['iletisim'].'</a></div>';
                                    }else if($cow['type'] == 3){
                                        $mail .= '<div class="row text-center" style="margin: 2px;"><a href="mailto:'.$cow['iletisim'].'" class="btn btn-warning"><i class="fa fa-envelope"></i> '.$cow['iletisim'].'</a></div>';
                                    }
                                }

                                if(!empty($tel)){
                                    echo '<button type="button" class="btn btn-xs btn-success tooltipss" title="Danışman Telefon" onclick="FuncAlert(\'Telefon\', \''.htmlentities($tel).'\', \'m\', \'green\');"><i class="fa fa-phone"></i></button>';
                                }

                                if(!empty($mail)){
                                    echo '<button type="button" class="btn btn-xs btn-warning tooltipss" title="Danışman Mail" onclick="FuncAlert(\'Mail\', \''.htmlentities($mail).'\', \'m\', \'orange\');"><i class="fa fa-envelope"></i></button>';
                                }
                            }
                            ?>
                        </p>
                    </div>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
    <?php
}
?>