<!-- Yorum MODAL -->
<div id="YorumModal" class="modal fade bs-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 class="modal-title text-center text-warning" id="myModalLabel">Yorum Yap</h3>
            </div>
            <div class="modal-body">
                <form method="POST" enctype="multipart/form-data" id="YorumForm" role="form" class="form-signin" >
                    <input type="hidden" name="teknikId" value="0"/>
                    <input type="hidden" name="yorumId" value="0"/>
                        <div class="row control-group">
                            <div class="form-group col-xs-12 controls">
                                <label>İsim</label>
                                <input type="text" class="form-control" placeholder="İsim" name="isim">
                            </div>
                        </div>
                        <div class="row control-group">
                            <div class="form-group col-xs-12 controls">
                                <label>Email</label>
                                <input type="email" class="form-control" placeholder="Email" name="email">
                            </div>
                        </div>
                        <div class="row control-group">
                            <div class="form-group col-xs-12 controls">
                                <label>Yorum</label>
                                <textarea name="yorum" rows="5" class="form-control"></textarea>
                            </div>
                        </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success btn-small" onclick="FuncYorumKaydet()">Kaydet</button>
                <button type="button" class="btn btn-danger btn-small" data-dismiss="modal">Kapat</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
function FuncYorumKaydet(){
    var isim = $('#YorumForm input[name="isim"]').val();
    var email = $('#YorumForm input[name="email"]').val();
    var yorum = $('#YorumForm textarea[name="yorum"]').val();
    var teknikId = $('#YorumForm input[name="teknikId"]').val();

    if(isim == ''){
        alert('Lütfen isim alanını boş bırakmayınız.');
    }else if(!checkemail(email)){
        alert('Lütfen geçerli bir email adresi giriniz.');
    }else if(yorum == ''){
        alert('Lütfen yorum alanını boş bırakmayınız.');
    }else if(teknikId == 0 || teknikId == ''){
        alert('Bir hata meydana geldi. Lütfen sayfayı yenileyerek tekrar deneyin.');
    }else{
        $('#YorumModal').modal('hide');
        $.blockUI({ css: { backgroundColor: 'transparent', border: 'none'},message: $('#LoaderBlock') });
        $.ajax({
            type:'POST',
            url:"<?php echo $this->Html->url('/');?>tekniks/yorumkaydet",
            data:$('#YorumForm').serialize()
        }).done(function(data){
            var dat = $.parseJSON(data);
            if(dat['hata']){
                $.unblockUI();
                alert('Bir hata meydana geldi. Lütfen tekrar deneyin.');
                $('#YorumModal').modal({
                    keyboard:false,
                    backdrop:'static'
                });
            }else{
                $.unblockUI();
                alert('Yorum başarıyla kaydedildi.');
                window.location.reload();
            }
        }).fail(function(){
            $.unblockUI();
            alert('İnternet bağlantınızı kontrol ederek, lütfen tekrar deneyin.');
            $('#YorumModal').modal({
                keyboard:false,
                backdrop:'static'
            });
        });
    }
}
</script>

<!-- Yorum MODAL SON-->