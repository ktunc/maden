<?php
/**
 * @var View   $this
 * @var View   $html
 */
//echo $this->Html->css('menu/normalize');
echo $this->Html->css('menu/component.min');
//echo $this->Html->script('menu/modernizr.custom');
$ilanSay = $this->requestAction(array('controller' => 'ilans', 'action' => 'ilanSay'));
?>

<nav class="st-menu st-effect-11" id="menu-11">
    <h2 class="icon icon-stack text-center"><img class="stmenuIcon" alt="batikapigayrimenkul_logo" src="<?php echo $this->Html->url('/');?>img/logo.png"/></h2>
    <ul class="fontBold bLi">
        <li><a href="<?php echo $this->Html->url('/');?>"><span class="valign">Anasayfa</span></a></li>
        <li class="dropdown"><a id="themes" href="#" data-toggle="dropdown" class="dropdown-toggle"><span class="valign">Konut</span> <span class="badge"><?php echo $ilanSay['kCount'];?></span> <span class="caret"></span></a>
            <ul aria-labelledby="themes" class="dropdown-menu bsub">
                <li><a href="<?php echo $this->Html->url('/');?>ilans/index/tip:1/satkir:1">Satılık <span class="badge"><?php echo $ilanSay['kSatCount'];?></span></a></li>
                <li><a href="<?php echo $this->Html->url('/');?>ilans/index/tip:1/satkir:2">Kİralık <span class="badge"><?php echo $ilanSay['kKirCount'];?></span></a></li>
            </ul>
        </li>
        <li class="dropdown"><a id="themes" href="#" data-toggle="dropdown" class="dropdown-toggle"><span class="valign">İşyerİ</span> <span class="badge"><?php echo $ilanSay['iCount'];?></span> <span class="caret"></span></a>
            <ul aria-labelledby="themes" class="dropdown-menu bsub">
                <li><a href="<?php echo $this->Html->url('/');?>ilans/index/tip:2/satkir:1">Satılık <span class="badge"><?php echo $ilanSay['iSatCount'];?></span></a></li>
                <li><a href="<?php echo $this->Html->url('/');?>ilans/index/tip:2/satkir:2">Kİralık <span class="badge"><?php echo $ilanSay['iKirCount'];?></span></a></li>
            </ul>
        </li>
        <li class="dropdown"><a id="themes" href="#" data-toggle="dropdown" class="dropdown-toggle"><span class="valign">Arsa</span> <span class="badge"><?php echo $ilanSay['aCount'];?></span> <span class="caret"></span></a>
            <ul aria-labelledby="themes" class="dropdown-menu bsub">
                <li><a href="<?php echo $this->Html->url('/');?>ilans/index/tip:3/satkir:1">Satılık <span class="badge"><?php echo $ilanSay['aSatCount'];?></span></a></li>
                <li><a href="<?php echo $this->Html->url('/');?>ilans/index/tip:3/satkir:2">Kİralık <span class="badge"><?php echo $ilanSay['aKirCount'];?></span></a></li>
            </ul>
        </li>
        <li><a href="<?php echo $this->Html->url('/');?>detaylisearch"><span class="valign">Detaylı ilans/index</span></a></li>
        <li><a href="<?php echo $this->Html->url('/');?>projes/home">Projeler <span class="badge"><?php echo $ilanSay['projeCount'];?></span></a></li>
        <li><a href="<?php echo $this->Html->url('/');?>habers/home">Haberler <span class="badge"><?php echo $ilanSay['haberCount'];?></span></a></li>
        <li><a href="<?php echo $this->Html->url('/');?>kurumsal"><span class="valign">Kurumsal</span></a></li>
        <li><a href="<?php echo $this->Html->url('/');?>iletisim"><span class="valign">İletİşİm</span></a></li>
        <?php
        if($this->Session->check('UserLogin')){
        ?>
            <li><a href="<?php echo $this->Html->url('/');?>users/iletisimedit">İletİşİm Düzenle</a></li>
            <!-- <li><a href="<?php echo $this->Html->url('/');?>users/ilaniletisim">İlan İletİşİmlerİ</a></li> -->
        <?php
        }
        ?>
        <?php
        if($this->Session->check('UserLogin')){
        ?>
            <li><a rel="nofollow" href="<?php echo $this->Html->url('/');?>admins">Admin Paneli</a></li>
            <li><a rel="nofollow" href="<?php echo $this->Html->url('/');?>logout">Çıkış</a></li>
        <?php
        }else{
        ?>
            <li><a rel="nofollow" href="<?php echo $this->Html->url('/');?>giris"><span class="valign">Gİrİş</span></a></li>
        <?php    
        }
        ?>
    </ul>
</nav>
