<?php
/**
 * @var View   $this
 * @var View   $html
 */
//echo $this->Html->css('menu/normalize');
echo $this->Html->css('menu/component.min');
//echo $this->Html->script('menu/modernizr.custom');
$ilanSay = $this->requestAction(array('controller' => 'users', 'action' => 'ilanSay'));
?>

<nav class="st-menu st-effect-11" id="menu-11">
    <h2 class="icon icon-stack text-center"><img class="stmenuIcon" alt="batikapigayrimenkul_logo" src="<?php echo $this->Html->url('/');?>img/logo.png"/></h2>
    <ul class="fontBold bLi">
        <li><a href="<?php echo $this->Html->url('/');?>"><span class="imgspanwidth"><img class="MenuIcon" alt="batikapigayrimenkul_home_icon" src="<?php echo $this->webroot;?>img/menuicon/mana.png"></span><span class="valign">Anasayfa</span></a></li>
        <li class="dropdown"><a id="themes" href="#" data-toggle="dropdown" class="dropdown-toggle"><span class="imgspanwidth"><img class="MenuIcon" alt="batikapigayrimenkul_konut_icon" src="<?php echo $this->webroot;?>img/menuicon/mkon.png"></span><span class="valign">Konut</span> <span class="badge"><?php echo $ilanSay['kCount'];?></span> <span class="caret"></span></a>
            <ul aria-labelledby="themes" class="dropdown-menu bsub">
                <li><a href="<?php echo $this->Html->url('/');?>arama/tip:1/satkir:1">Satılık <span class="badge"><?php echo $ilanSay['kSatCount'];?></span></a></li>
                <li><a href="<?php echo $this->Html->url('/');?>arama/tip:1/satkir:2">Kİralık <span class="badge"><?php echo $ilanSay['kKirCount'];?></span></a></li>
            </ul>
        </li>
        <li class="dropdown"><a id="themes" href="#" data-toggle="dropdown" class="dropdown-toggle"><span class="imgspanwidth"><img class="MenuIcon" alt="batikapigayrimenkul_isyeri_icon" src="<?php echo $this->webroot;?>img/menuicon/mis.png"></span><span class="valign">İşyerİ</span> <span class="badge"><?php echo $ilanSay['iCount'];?></span> <span class="caret"></span></a>
            <ul aria-labelledby="themes" class="dropdown-menu bsub">
                <li><a href="<?php echo $this->Html->url('/');?>arama/tip:2/satkir:1">Satılık <span class="badge"><?php echo $ilanSay['iSatCount'];?></span></a></li>
                <li><a href="<?php echo $this->Html->url('/');?>arama/tip:2/satkir:2">Kİralık <span class="badge"><?php echo $ilanSay['iKirCount'];?></span></a></li>
            </ul>
        </li>
        <li class="dropdown"><a id="themes" href="#" data-toggle="dropdown" class="dropdown-toggle"><span class="imgspanwidth"><img class="MenuIcon" alt="batikapigayrimenkul_arsa_icon" src="<?php echo $this->webroot;?>img/menuicon/mar.png"></span><span class="valign">Arsa</span> <span class="badge"><?php echo $ilanSay['aCount'];?></span> <span class="caret"></span></a>
            <ul aria-labelledby="themes" class="dropdown-menu bsub">
                <li><a href="<?php echo $this->Html->url('/');?>arama/tip:3/satkir:1">Satılık <span class="badge"><?php echo $ilanSay['aSatCount'];?></span></a></li>
                <li><a href="<?php echo $this->Html->url('/');?>arama/tip:3/satkir:2">Kİralık <span class="badge"><?php echo $ilanSay['aKirCount'];?></span></a></li>
            </ul>
        </li>
        <?php
        if($this->Session->check('UserLogin')){
            echo '<li><a href="'.$this->Html->url('/').'yenikonut">Yenİ Konut Ekle</a></li>';
            echo '<li><a href="'.$this->Html->url('/').'yeniisyeri">Yenİ İşyerİ Ekle</a></li>';
            echo '<li><a href="'.$this->Html->url('/').'yeniarsa">Yenİ Arsa Ekle</a></li>';
        }
        ?>
        <li><a href="<?php echo $this->Html->url('/');?>users/detaylisearch"><span class="imgspanwidth"><img class="MenuIcon" alt="batikapigayrimenkul_filter_icon" src="<?php echo $this->webroot;?>img/menuicon/mfilter.png"></span><span class="valign">Detaylı Arama</span></a></li>
        <!--<li><a href="<?php echo $this->Html->url('/');?>projes/home"><span class="imgspanwidth"><img class="MenuIcon" alt="batikapigayrimenkul_proje_icon" src="<?php echo $this->webroot;?>img/menuicon/mpro.png"></span><span class="valign">Projeler</span> <span class="badge"><?php echo $ilanSay['projeCount'];?></span></a></li>-->
        <?php
        if($this->Session->check('UserLogin')){
        ?>
        <!--<li><a href="<?php echo $this->Html->url('/');?>projes/yeniproje">Yenİ Proje Ekle</a></li>-->
        <?php }?>
        <!--<li><a href="<?php echo $this->Html->url('/');?>habers/home"><span class="imgspanwidth"><img class="MenuIcon" alt="batikapigayrimenkul_haber_icon" src="<?php echo $this->webroot;?>img/menuicon/mhab.png"></span><span class="valign">Haberler</span> <span class="badge"><?php echo $ilanSay['haberCount'];?></span></a></li>-->
        <?php
        if($this->Session->check('UserLogin')){
        ?>
        <!--<li><a href="<?php echo $this->Html->url('/');?>habers/yenihaber">Yenİ Haber Ekle</a></li>-->
        <?php }?>
        <!--<li><a href="<?php echo $this->Html->url('/');?>tekniks/home"><span class="imgspanwidth"><img class="MenuIcon" alt="batikapigayrimenkul_haber_icon" src="<?php echo $this->webroot;?>img/menuicon/mteknik.png"></span><span class="valign">Teknik Analiz & Yorumlar</span> <span class="badge"><?php echo $ilanSay['teknikCount'];?></span></a></li>-->
        <?php
        if($this->Session->check('UserLogin')){
            ?>
            <!--<li><a href="<?php echo $this->Html->url('/');?>tekniks/yeniteknik">Yenİ Teknik Analiz Ekle</a></li>-->
        <?php }?>
        <li><a href="<?php echo $this->Html->url('/');?>kurumsal"><span class="imgspanwidth"><img class="MenuIcon" alt="batikapigayrimenkul_kurumsal_icon" src="<?php echo $this->webroot;?>img/menuicon/mkur.png"></span><span class="valign">Kurumsal</span></a></li>
        <li><a href="<?php echo $this->Html->url('/');?>iletisim"><span class="imgspanwidth"><img class="MenuIcon" alt="batikapigayrimenkul_iletisim_icon" src="<?php echo $this->webroot;?>img/menuicon/milet.png"></span><span class="valign">İletİşİm</span></a></li>
        <?php
        if($this->Session->check('UserLogin')){
        ?>
            <li><a href="<?php echo $this->Html->url('/');?>users/iletisimedit">İletİşİm Düzenle</a></li>
            <li><a href="<?php echo $this->Html->url('/');?>users/ilaniletisim">İlan İletİşİmlerİ</a></li>
        <?php
        }
        ?>
        <?php
        if($this->Session->check('UserLogin')){
        ?>
            <li><a rel="nofollow" href="<?php echo $this->Html->url('/');?>logout">Çıkış</a></li>
        <?php
        }else{
        ?>
            <li><a rel="nofollow" href="<?php echo $this->Html->url('/');?>login"><span class="imgspanwidth"><img class="MenuIcon" alt="batikapigayrimenkul_giris_icon" src="<?php echo $this->webroot;?>img/menuicon/mgir.png"></span><span class="valign">Gİrİş</span></a></li>
        <?php    
        }
        ?>
    </ul>
</nav>
