<!-- Video MODAL -->
<div id="VideoModal" class="modal fade bs-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <!--            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>-->
                <h3 class="modal-title" id="myModalLabel">Video</h3>
            </div>
            <div class="modal-body bodyvideo">
                <video width="100%" height="200" class="video-js vjs-default-skin" id="myvideo" controls muted>
                    <source src="" type="video/mp4" id="sourceMp4">
                </video>
                <audio controls loop id="myaudio" class="hidden">
                    <source src="<?php echo $this->Html->url('/');?>music/west.mp3" type="audio/mpeg">
                </audio>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn3d btn-danger" id="videoHide">Kapat</button>
            </div>
        </div>
    </div>
</div>
<!-- Video MODAL SON-->