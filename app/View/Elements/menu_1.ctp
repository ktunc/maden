<!-- Üst menü -->
    <div class="navbar navbar-default navbar-fixed-top" style="color: #FFFFFF">
           <div class="container">
               <div class="navbar-header">
                   <a style="color: #FFFFFF" class="navbar-brand" href="<?php echo $this->Html->url('/');?>"><img src="<?php echo $this->Html->url('/');?>img/logo 3.png" class="iphone4logo"/></a>
                    <button data-target="#navbar-main" data-toggle="collapse" type="button" class="navbar-toggle iphone4">
                    <!--<span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>-->
                    <span>MENÜ</span>
                    </button>
                   <button onclick="window.location.href='<?php echo $this->Html->url('/');?>'" type="button" class="navbar-toggle iphone4" style="margin-right: 5px;">
<!--                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>-->
                       <span>ANASAYFA</span>
                    </button>
               </div>
               <div id="navbar-main" class="navbar-collapse collapse">
                   <ul class="nav navbar-nav">
                       <li><a style="color: #FFFFFF" href="<?php echo $this->Html->url('/');?>">Anasayfa</a></li>
                       <li class="dropdown"><a style="color: #FFFFFF" id="themes" href="#" data-toggle="dropdown" class="dropdown-toggle">Konut <span class="caret"></span></a>
                           <ul aria-labelledby="themes" class="dropdown-menu">
                               <?php
                               if($this->Session->check('UserLogin')){
                               ?>
                               <li><a style="color: #FFFFFF" href="<?php echo $this->Html->url('/');?>konuts/yenikonut">Yeni Konut Ekle</a></li> 
                               <?php
                               }
                               ?>
                               <li><a style="color: #FFFFFF" href="<?php echo $this->Html->url('/');?>konuts/home/SatKir:1">Satılık</a></li> 
                               <li><a style="color: #FFFFFF" href="<?php echo $this->Html->url('/');?>konuts/home/SatKir:2">Kiralık</a></li> 
                               <li class="divider"></li>
                           </ul>
                       </li>
                       <li class="dropdown"><a style="color: #FFFFFF" id="themes" href="#" data-toggle="dropdown" class="dropdown-toggle">İşyeri <span class="caret"></span></a>
                           <ul aria-labelledby="themes" class="dropdown-menu">
                               <?php
                               if($this->Session->check('UserLogin')){
                               ?>
                               <li><a style="color: #FFFFFF" href="<?php echo $this->Html->url('/');?>isyeris/yeniisyeri">Yeni İşyeri Ekle</a></li> 
                               <?php
                               }
                               ?>
                               <li><a style="color: #FFFFFF" href="<?php echo $this->Html->url('/');?>isyeris/home/SatKir:1">Satılık</a></li> 
                               <li><a style="color: #FFFFFF" href="<?php echo $this->Html->url('/');?>isyeris/home/SatKir:2">Kiralık</a></li>
                               <li class="divider"></li>
                           </ul>
                       </li>
                       <li class="dropdown"><a style="color: #FFFFFF" id="themes" href="#" data-toggle="dropdown" class="dropdown-toggle">Arsa <span class="caret"></span></a>
                           <ul aria-labelledby="themes" class="dropdown-menu">
                               <?php
                               if($this->Session->check('UserLogin')){
                               ?>
                               <li><a style="color: #FFFFFF" href="<?php echo $this->Html->url('/');?>arsas/yeniarsa">Yeni Arsa Ekle</a></li> 
                               <?php
                               }
                               ?>
                               <li><a style="color: #FFFFFF" href="<?php echo $this->Html->url('/');?>arsas/home/SatKir:1">Satılık</a></li> 
                               <li><a style="color: #FFFFFF" href="<?php echo $this->Html->url('/');?>arsas/home/SatKir:2">Kiralık</a></li>
                               <li class="divider"></li>
                           </ul>
                       </li>
                       <li><a style="color: #FFFFFF" href="<?php echo $this->Html->url('/');?>users/detaylisearch">Detaylı Arama</a></li>
                       <li><a style="color: #FFFFFF" href="<?php echo $this->Html->url('/');?>projes/home">Projeler</a></li>
                       <?php
                       if($this->Session->check('UserLogin')){
                       ?>
                       <li><a style="color: #FFFFFF" href="<?php echo $this->Html->url('/');?>projes/yeniproje">Yeni Proje Ekle</a></li>
                       <?php }?>
                       <li><a style="color: #FFFFFF" href="<?php echo $this->Html->url('/');?>habers/home">Haberler</a></li>
                       <?php
                       if($this->Session->check('UserLogin')){
                       ?>
                       <li><a style="color: #FFFFFF" href="<?php echo $this->Html->url('/');?>habers/yenihaber">Yeni Haber Ekle</a></li>
                       <?php }?>
                       <li><a style="color: #FFFFFF" href="<?php echo $this->Html->url('/');?>users/kurumsal">Kurumsal</a></li>
                   </ul>
                   <ul class="nav navbar-nav navbar-right">
                   <?php
                   if($this->Session->check('UserLogin')){
                   ?>
                       <li><a style="color: #FFFFFF" href="<?php echo $this->Html->url('/');?>users/logOut">Çıkış</a></li>
                   <?php
                   }else{
                   ?>
                       <li><a style="color: #FFFFFF" href="<?php echo $this->Html->url('/');?>users/login">Giriş</a></li>
                   <?php    
                   }
                   ?>
                   </ul>
               </div>
           </div>
       </div>
    <!-- Üst menü Son -->