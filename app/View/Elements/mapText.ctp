<!-- MapText Modal -->
<div class="modal fade" id="MapTextModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="mapText" action="<?php echo $this->Html->url(NULL,true);?>" method="POST">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title text-warning text-center" id="myModalLabel">Ara</h3>
            </div>
            <div class="modal-body">
                    <div class="form-group">
                        <input class="form-control inputM" type="search" name="mapIlan" placeholder="Kelime veya İlan No Arayın" value="<?php echo isset($mapIlan)?$mapIlan:''; ?>">
                    </div>
            </div>
            <div class="modal-footer">
                <div class="form-group">
                    <button type="button" class="btn btn-danger fontBold" data-dismiss="modal">Kapat</button>
                    <button type="submit" class="btn btn-primary fontBold">Ara</button>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- MapText Modal SON-->