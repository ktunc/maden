<!-- MapText Modal -->
<div class="modal fade" id="ShareIlanModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title text-warning text-center" id="myModalLabel">Paylaş</h3>
            </div>
            <div class="modal-body">
                <?php $urlPath = urlencode('http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);?>
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <div class="divYan">
                            <a target="_blank" class="btn btn-primary btn-sm" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $urlPath;?>"><i class="fa fa-facebook fa-2x"></i></a>
                        </div>
                        <div class="divYan">
                            <a target="_blank" class="btn btn-primary btn-sm" href="https://twitter.com/home?status=<?php echo $urlPath;?>"><i class="fa fa-twitter fa-2x"></i></a>
                        </div>
                        <div class="divYan">
                            <a target="_blank" class="btn btn-danger btn-sm" href="https://plus.google.com/share?url=<?php echo $urlPath;?>"><i class="fa fa-google-plus fa-2x"></i></a>
                        </div>
						<!--<div class="fb-share-button" data-layout="box_count" style="display: inline-flex"></div>
						<div class="g-plusone" data-size="tall"></div>
						<a href="https://twitter.com/share" class="twitter-share-button">Tweet</a>-->
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="form-group">
                    <button type="button" class="btn btn-danger fontBold" data-dismiss="modal">Kapat</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- MapText Modal SON-->