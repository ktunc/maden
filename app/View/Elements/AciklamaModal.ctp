
<div id="AciklamaModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header" id="header">
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="text-white fa fa-times"></i></span></button>
                <h3 class="modal-title text-center" id="myModalLabel">Açıklama</h3>
            </div>
            <div class="modal-body bodyaciklama ilanIcerik">
            </div>
            <div class="modal-footer" id="footer">
                <button type="button" id="acikModalKapat" class="close text-white"><span aria-hidden="true"><i class="text-white fa fa-times"></i></span></button>
                <!--<button type="button" class="btn btn-sm btn-danger" id="acikModalKapat">Kapat</button>-->
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
    $('#AciklamaModal #acikModalKapat').on('click',function(){
        $('#AciklamaModal').modal('hide');
        $('#AciklamaModal .bodyaciklama').html('');
    });
});
</script>