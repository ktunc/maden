<?php $pagg = $this->passedArgs; ?>
    <form id="searchForm" method="POST" role="form" class="form-signin" action="<?php echo $this->Html->url('/');?>users/arama<?php echo $this->requestAction(array('controller'=>'users','action'=>'GetPagUrl'),$pagg);?>" enctype="multipart/form-data">
        <div class="input-group">
            <input type="text" class="form-control inputM" name="sval" />
         <span class="input-group-btn">
             <button class="btn btn-sm btn-primary" type="button" id="searchButton"><span class="fa fa-search fa-2x"></span></button>
          </span>
        </div>
    </form>


<!-- MODALLAR **********************************************************************************************-->
<!-- Loader ***********************************************-->
<div id="Loader" class="modal fade bs-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="progress progress-striped active">
            <div class="progress-bar progress-bar-warning"  role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
            </div>
        </div>
    </div>
</div>
<!-- Loader ***********************************************-->

<script type="text/javascript">
$(document).ready(function(){
   $('#searchButton').on('click',function(){
       $.blockUI({ css: { backgroundColor: 'transparent', border: 'none'},message: $('#LoaderBlock') });
        $('#searchForm').submit();
    }); 
});
</script>