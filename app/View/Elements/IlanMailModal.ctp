<!-- MapText Modal -->
<div class="modal fade" id="IlanMailModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title text-warning text-center" id="myModalLabel">Mail</h3>
            </div>
            <div class="modal-body">
                <?php if(isset($IlanMail) && !empty($IlanMail)){
                    foreach($IlanMail as $row){
                        echo '<div class="anaDiv text-center">';
                        echo '<a class="btn btn-primary" target="_blank" href="mailto:'.$row['IlanIlet']['iletisim'].'?Subject='.htmlentities('İlan No:'.$IlanNo).'"><i class="fa fa-envelope"></i> '.$row['IlanIlet']['iletisim'].'</a>';
                        echo '</div>';
                    }
                } ?>
            </div>
            <div class="modal-footer">
                <div class="form-group">
                    <button type="button" class="btn btn-danger fontBold" data-dismiss="modal">Kapat</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- MapText Modal SON-->