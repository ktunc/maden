<?php
$sorgular = $this->passedArgs;
$aramaTip = array(0=>'Hepsi',1=>'Konut',2=>'İşyeri',3=>'Arsa');
$TipSel = '';
foreach($aramaTip as $key=>$val){
    $selected = '';
    if(array_key_exists('tip',$sorgular) && $sorgular['tip'] == $key){
        $selected = ' selected="selected"';
    }
    $TipSel .= '<option value="'.$key.'" '.$selected.'>'.$val.'</option>';
}
$il = $this->requestAction('users/GetIl');
$sehir = '<option value="0">Seçiniz</option>';
foreach ($il as $row) {
    $selected = '';
    if(array_key_exists('il',$sorgular) && $sorgular['il'] == $row['Sehir']['id']){
        $selected = ' selected="selected"';
    }
    $sehir .= '<option value="'.$row['Sehir']['id'].'" '.$selected.'>'.$row['Sehir']['sehir_adi'].'</option>';
}
$fiy1 = '';$fiy2 = '';$m1 = '';$m2 = '';
if(array_key_exists('fiy1',$sorgular) && is_numeric($sorgular['fiy1'])){
    $fiy1 = $sorgular['fiy1'];
}
if(array_key_exists('fiy2',$sorgular) && is_numeric($sorgular['fiy2'])){
    $fiy2 = $sorgular['fiy2'];
}
if(array_key_exists('m1',$sorgular) && is_numeric($sorgular['m1'])){
    $m1 = $sorgular['m1'];
}
if(array_key_exists('m2',$sorgular) && is_numeric($sorgular['m2'])){
    $m2 = $sorgular['m2'];
}
?>
<!-- detSearch Modal -->
<div class="modal fade" id="DetSearchModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="<?php echo $this->Html->url('/');?>users/gmaphome" method="POST" role="form" class="form-horizontal" enctype="multipart/form-data" id="searchFormDetayli">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title text-warning text-center" id="myModalLabel">Detaylı Arama</h3>
            </div>
            <div class="modal-body">
                    <div class="form-group">
                        <label for="tip" class="col-xs-3 control-label">Arama Tipi:</label>
                        <div class="col-xs-9">
                            <select name="tip" class="form-control">
                                <?php echo $TipSel; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="satkir" class="col-xs-3 control-label">Satılık-Kiralık:</label>
                        <div class="col-xs-9">
                            <select name="satkir" class="form-control">
                                <option value="0">Hepsi</option>
                                <option value="1">Satılık</option>
                                <option value="2">Kiralık</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="il" class="col-xs-3 control-label">Şehir:</label>
                        <div class="col-xs-9">
                            <select id="il" name="il" class="form-control"><?php echo $sehir; ?></select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="ilce" class="col-xs-3 control-label">İlçe:</label>
                        <div class="col-xs-9">
                            <select id="ilce" name="ilce" class="form-control"></select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="semt" class="col-xs-3 control-label">Semt:</label>
                        <div class="col-xs-9">
                            <select id="semt" name="semt" class="form-control"></select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="mahalle" class="col-xs-3 control-label">Mahalle:</label>
                        <div class="col-xs-9">
                            <select id="mahalle" name="mahalle" class="form-control"></select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="fiyat" class="col-xs-3 control-label">Fiyat:</label>
                        <div class="col-xs-4">
                            <input type="number" class="form-control" name="fiy1" value="<?php echo $fiy1; ?>" onkeypress="return isNumberKey(event)"/>
                        </div>
                        <div class="col-xs-4">
                            <input type="number" class="form-control" name="fiy2" value="<?php echo $fiy2; ?>" onkeypress="return isNumberKey(event)"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="mahalle" class="col-xs-3 control-label">Metre Kare:</label>
                        <div class="col-xs-4">
                            <input type="number" class="form-control" name="m1" value="<?php echo $m1; ?>" onkeypress="return isNumberKey(event)"/>
                        </div>
                        <div class="col-xs-4">
                            <input type="number" class="form-control" name="m2" value="<?php echo $m2; ?>" onkeypress="return isNumberKey(event)"/>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="form-group">
                    <button type="button" class="btn btn-danger fontBold" data-dismiss="modal">Kapat</button>
                    <!-- <button type="reset" class="btn btn3d btn-danger">Sıfırla</button> -->
                    <button type="button" class="btn btn-primary fontBold" id="searchButtonDetayli">Ara</button>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- detSearch Modal SON-->
<script type="text/javascript">
    $(document).ready(function(){
        $('#searchButtonDetayli').on('click',function(){
            $('#DetSearchModal').modal('hide');
            $('#searchFormDetayli').submit();
        });

        $('#il').on('change',function (){
            $('#ilce').html('');
            $('#semt').html('');
            var il = $(this).val();
            if(il != 0){
                $.ajax({
                    async: false,
                    type: 'POST',
                    url: "<?php echo $this->Html->url('/');?>konuts/getIlce",
                    data: 'il='+il,
                    success: function (data) {
                        var dat = $.parseJSON(data);
                        if(dat){
                            var ekle = '<option value="0">Seçiniz</option>';
                            $.each(dat,function(key,vall){
                                ekle+='<option value="'+vall['Ilce']['id']+'">'+vall['Ilce']['ilce_adi']+'</option>';
                            });
                            $('#ilce').html(ekle);
                        }
                    }
                });
            }
        });

        $('#ilce').on('change',function (){
            $('#semt').html('');
            $('#mahalle').html('');
            var ilce = $(this).val();
            if(ilce != 0){
                $.ajax({
                    async: false,
                    type: 'POST',
                    url: "<?php echo $this->Html->url('/');?>konuts/getSemt",
                    data: 'ilce='+ilce,
                    success: function (data) {
                        var dat = $.parseJSON(data);
                        if(dat){
                            var ekle = '<option value="0">Seçiniz</option>';
                            $.each(dat,function(key,vall){
                                ekle+='<option value="'+vall['Semt']['id']+'">'+vall['Semt']['semt_adi']+'</option>';
                            });
                            $('#semt').html(ekle);
                        }
                    }
                });
            }
        });

        $('#semt').on('change',function (){
            $('#mahalle').html('');
            var semt = $(this).val();
            if(semt != 0){
                $.ajax({
                    async: false,
                    type: 'POST',
                    url: "<?php echo $this->Html->url('/');?>konuts/getMahalle",
                    data: 'semt='+semt,
                    success: function (data) {
                        var dat = $.parseJSON(data);
                        if(dat){
                            var ekle = '<option value="0">Seçiniz</option>';
                            $.each(dat,function(key,vall){
                                ekle+='<option value="'+vall['Mahalle']['id']+'">'+vall['Mahalle']['mahalle_adi']+'</option>';
                            });
                            $('#mahalle').html(ekle);
                        }
                    }
                });
            }
        });
    });
</script>