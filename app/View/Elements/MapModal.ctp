<?php
//echo $this->Html->script('locationpicker.jquery.notdrag');
echo $this->Html->script('http://maps.google.com/maps/api/js?key=YOUR_API_KEY&callback=initMap');
?>
<!--<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBb6wy1FSr2ms69Cy7BSuZQLOB9-EPIkIA&libraries=places" type="text/javascript"></script>-->
<div id="MapModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header" id="header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
                <h3 class="modal-title text-center" id="myModalLabel">Harita</h3>
            </div>
            <div class="modal-body" style="padding: 0;">
                <div class="gmap" id="modalmap"></div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var map;
    var marker;
    var infowindow;
    $(document).ready(function(){
        $('#MapModal #MapModalKapat').on('click',function(){
            $('#MapModal').modal('hide');
        });
        /*$('#MapModal #us2').locationpicker({
            location: {latitude: 0, longitude: 0},
            radius: 10,
            zoom:17,
            draggable:false
        });*/

        function init(){
            map = new google.maps.Map(document.getElementById('modalmap'), {
                zoom: 15,
                center: new google.maps.LatLng(0,0),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                mapTypeControl: false,
                streetViewControl: false,
                scaleControl: false,
                rotateControl: false
            });
            marker = new google.maps.Marker({
                position: {lat:0, lng:0},
                map: map,
                zIndex:99999999
            });
        }
        google.maps.event.addDomListener(window, 'load', init);

        $('#MapModal').on('shown.bs.modal', function () {
            var currentCenter = map.getCenter();  // Get current center before resizing
            google.maps.event.trigger(map, "resize");
            map.setCenter(currentCenter); // Re-set
            map.setZoom(15);
        });
    });

    function FuncMapGoruntule(lat,long){
        $('#MapModal').modal({
            keyboard:false,
            backdrop:'static'
        });
        map.setCenter({lat:lat, lng:long});
        marker.setPosition({lat:lat, lng:long});
        //google.maps.event.addDomListener(window, 'load', init(lat,long));
        //google.maps.event.trigger(map, 'resize');
    }
</script>