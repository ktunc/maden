<div id="DanismanModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header" id="header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
                <h3 class="modal-title text-center" id="myModalLabel">Danışmanlar</h3>
            </div>
            <div class="modal-body" style="padding: 0;">
                <?= $this->element('IlanDanisman', array('data' => $danismanlar)) ?>
            </div>
        </div>
    </div>
</div>

<script>
    function FuncDanismanGoruntule() {
        if($('body>#DanismanModal').length == 0){
            $('#DanismanModal').appendTo("body");
        }
        $('#DanismanModal').modal({
            keyboard:false,
            backdrop:'static'
        });
    }
</script>