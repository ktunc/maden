<!-- Uyari Modal -->
<div class="modal fade" id="UyariModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title text-warning text-center" id="myModalLabel">Uyarı</h3>
      </div>
      <div class="modal-body h4" id="UyariContent">
          
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Kapat</button>
      </div>
    </div>
  </div>
</div>
<!-- Uyari Modal SON-->