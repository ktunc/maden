<!-- Video MODAL -->
<div id="YoutubeModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title text-center" id="myModalLabel">Video</h3>
            </div>
            <div class="modal-body bodyvideo">
                <iframe id="ytplayer" type="text/html" width="100%" height="300px" src="https://www.youtube.com/embed/<?php echo $id ?>?rel=0&showinfo=0&color=white&iv_load_policy=3" frameborder="0" allowfullscreen></iframe>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-danger" id="ytModalKapat">Kapat</button>
            </div>
        </div>
    </div>
</div>
<!-- Video MODAL SON-->

<script type="text/javascript">
$(document).ready(function(){
    $('#YoutubeModal #ytModalKapat').on('click',function(){
        $('#YoutubeModal').modal('hide');
        $('#YoutubeModal #ytplayer').attr('src','');
    });
});
function YTVideoAc(durum){
    if(durum == 1){
        //var yturl = $('.ytvideo').data('yturl');
        var yturl = $('[data-yturl]').data('yturl');
    }else{
        var yturl = $('.ytvideo').val();
    }

    var ytiframe = validateYouTubeUrl(yturl);
    if(ytiframe){
        $('#YoutubeModal #ytplayer').attr('src',ytiframe);
        $('#YoutubeModal').modal({
            keyboard:false,
            backdrop:'static'
        });
    }else{
        $('#UyariModal #UyariContent').html("Eklemek istediğiniz video youtube videosu değildir. Lütfen önce videonuzu <a href='https://www.youtube.com/' target='_blank'>youtube</a>'a ekleyip linkini giriniz.");
        $('#UyariModal').modal({
            keyboard:false,
            backdrop:'static'
        });
    }
}

function validateYouTubeUrl(yturl) {
    var url = yturl;
    if (url != undefined || url != '') {
        //var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=|\?v=)([^#\&\?]*).*/;
        var regExp = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
        var match = url.match(regExp);
        if (match && match[1].length == 11) {
            // Do anything for being valid
            // if need to change the url to embed url then use below line
            //$('#videoObject').attr('src', 'https://www.youtube.com/embed/' + match[2] + '?autoplay=1&enablejsapi=1');
            return 'https://www.youtube.com/embed/' + match[1] + '?autoplay=1&rel=0&iv_load_policy=3';
        } else {
            return false;
        }
    }else{
        return false;
    }
}
</script>